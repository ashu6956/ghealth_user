package com.patient.ghealth.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.fragment.CartDetailsFragment;
import com.patient.ghealth.fragment.ProductDetailFragment;
import com.patient.ghealth.fragment.StoreProductListFragment;
import com.patient.ghealth.model.ProductCategory;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

public class HealthStoreActivity extends AppCompatActivity {

    public String currentFragment = "", referenceFragment = "";
    public TextView cartCount;
    private Toolbar storeToolbar;
    private ImageView backButton, cartIcon;
    private TextView toolbarHeader;
    private Bundle bundle;
    private ProductCategory productCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_store_actiivty);
        storeToolbar = (Toolbar) findViewById(R.id.store_toolbar);
        setSupportActionBar(storeToolbar);
        getSupportActionBar().setTitle("");
        backButton = (ImageView) findViewById(R.id.iv_storeBackButton);
        cartIcon = (ImageView) findViewById(R.id.iv_store_cart);
        toolbarHeader = (TextView) findViewById(R.id.tv_store_header);
        toolbarHeader.setText(getString(R.string.health_store));
        cartCount = (TextView) findViewById(R.id.tv_count_chat_message);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        String catId = (String) PreferenceHelper.getParam(this, Const.Params.CATEGORY_ID, "");
        AndyUtils.appLog("HealthStoreActivity", "CategoryId" + catId);
        try {
            bundle = getIntent().getExtras();
            if (bundle != null && bundle.getString(Const.INCOMING_BUNDLE) != null) {
                CartDetailsFragment cartDetailsFragment = new CartDetailsFragment();
                Bundle bundle = new Bundle();
                bundle.putString(Const.INCOMING_BUNDLE, Const.StoreCategoryFragment);
                cartDetailsFragment.setArguments(bundle);
                addFragment(cartDetailsFragment, false, "Cart", Const.CartDetailsFragment, false);
            } else if (!catId.equals("")) {
                addFragment(new StoreProductListFragment(), false, getString(R.string.health_store), getString(R.string.health_store), false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        cartIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFragment(new CartDetailsFragment(), false, "Cart", "Cart", false);
            }
        });

    }


    @Override
    public void onBackPressed() {
        if (currentFragment.equals(Const.ProductDetailFragment)) {
            addFragment(new StoreProductListFragment(), false, "", "", false);
        } else if (currentFragment.equals(Const.CartDetailsFragment) && referenceFragment.equals(Const.StoreCategoryFragment)) {
            Intent intent = new Intent(this, MainActivity.class);
            Bundle bundle = new Bundle();
            referenceFragment = "";
            bundle.putString(Const.INCOMING_BUNDLE, Const.StoreProductListFragment);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        } else if (currentFragment.equals(Const.CartDetailsFragment)) {

            addFragment(new StoreProductListFragment(), false, getString(R.string.health_store), Const.StoreProductListFragment, false);
        } else if (currentFragment.equals(Const.StoreProductListFragment)) {
            Intent intent = new Intent(this, MainActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString(Const.INCOMING_BUNDLE, Const.StoreProductListFragment);
            intent.putExtras(bundle);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        } else if (currentFragment.equals(Const.ProductAllReviewsFragment)) {
            addFragment(new ProductDetailFragment(), false, getString(R.string.health_store), Const.ProductDetailFragment, false);
        } else if (currentFragment.equals(Const.WebViewFragment)) {
            addFragment(new ProductDetailFragment(), false, getString(R.string.health_store), Const.ProductDetailFragment, false);
        }

    }


    public void addFragment(Fragment fragment, boolean addToBackStack, String fragmentTitle,
                            String tag, boolean isAnimate) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (fragmentTitle == "") {
//            toolbarHeader.setText(Html.fromHtml(sHeader));
        } else {
            toolbarHeader.setText(fragmentTitle);
        }
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(R.id.contentFrame, fragment, tag);
        ft.commit();
    }


}

