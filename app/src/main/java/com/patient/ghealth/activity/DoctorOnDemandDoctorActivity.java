package com.patient.ghealth.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.patient.ghealth.R;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.model.InvoiceDetails;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class DoctorOnDemandDoctorActivity extends Activity implements AsyncTaskCompleteListener, View.OnClickListener {


    DecimalFormat form = new DecimalFormat("0.0");
    private MapView mapView;
    private boolean mapsSupported = true;
    private GoogleMap mGoogleMap;
    private MainActivity activity;
    private ImageView homeImageView, statusImageView;
    private TextView yourDoctorText, doctorDepartText, doctorName, doctorRating, etaDistance, etatTime;
    private Bundle doctorBundle;
    private int clickedPosition;
    private Location myLocation;
    private Marker markerpatient, markerDcotor;
    private DoctorOnLineDetails onLineDetails;
    private Bundle bundle;
    private EditText sourceEdiText, destinationEditText;
    private ImageView doctorIcon;
    private Bundle mBundle;
    private int status_position;
    private InvoiceDetails invoiceDetails;
    private Dialog statusDialog;
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            AndyUtils.removeProgressDialog();
            if (statusDialog != null && statusDialog.isShowing()) {
                statusDialog.cancel();
            }
            mGoogleMap.clear();
            Log.d("FragmentRequest", "onReceive");
            String notification = intent.getStringExtra("ashutosh");
            Log.d("My Push notification", notification);
            String message = intent.getStringExtra(Const.DOCTOR_INTENT_MESSAGE);
            try {
                JSONObject messageObj = new JSONObject(message);
                AndyUtils.appLog("JsonResponse", messageObj.toString());
                if (Integer.parseInt(messageObj.getString("status")) == 5) {
                    invoiceDetails = new InvoiceDetails();
                    JSONArray invoiceJsonArray = messageObj.getJSONArray("invoice");
                    JSONObject invoiceJsonObject = invoiceJsonArray.getJSONObject(0);
                    invoiceDetails.setInvoiceDoctorId(Integer.parseInt(invoiceJsonObject.getString("doctor_id")));
                    invoiceDetails.setBasePrice(invoiceJsonObject.optString("base_price"));
                    invoiceDetails.setPaymentMode(invoiceJsonObject.optString("payment_mode"));
                    invoiceDetails.setTotalPrice(invoiceJsonObject.optString("total"));
                    invoiceDetails.setMedicineFee(invoiceJsonObject.optString("medicine_fee"));
                    invoiceDetails.setReferralBonus(invoiceJsonObject.optString("referral_bonus"));
                    invoiceDetails.setPromoBonus(invoiceJsonObject.optString("promo_bonus"));
                    invoiceDetails.setTotalTime(invoiceJsonObject.optString("total_time"));
                    invoiceDetails.setInvoiceRequestId(messageObj.optString("request_id"));
                    invoiceDetails.setTreatmentFee(invoiceJsonObject.optString("time_price"));
                    invoiceDetails.setInvoiceDoctorStatus(messageObj.optString("status"));
                    invoiceDetails.setInvoiceDoctorPictureUrl(invoiceJsonObject.optString("doctor_picture"));
                    invoiceDetails.setInvoiceDoctorName(invoiceJsonObject.getString("doctor_name"));
                    invoiceDetails.setDistance(invoiceJsonObject.optString("distance"));
                    invoiceDetails.setDistanceCost(invoiceJsonObject.optString("distance_cost"));
                    invoiceDetails.setPerKm(invoiceJsonObject.optString("per_km"));
                    invoiceDetails.setInvoiceType(Const.ONLINE_CONSULT);
                    Bundle invoiceBundle = new Bundle();
                    invoiceBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, invoiceDetails);
                    invoiceBundle.putString(Const.DOCTOR_CONSULT_CONNECTIVITY, "normal");
                    Intent feedbackIntent = new Intent(DoctorOnDemandDoctorActivity.this, FeedBackActivity.class);
                    feedbackIntent.putExtras(invoiceBundle);
                    startActivity(feedbackIntent);
                    finish();


                } else {
                    if (messageObj.getString("success").equals("true")) {

                        JSONObject jsonObject = messageObj.getJSONObject("data");
                        onLineDetails = new DoctorOnLineDetails();
                        onLineDetails.setDoctorOnLineId(jsonObject.getString("doctor_id"));
                        onLineDetails.setDoctorPictureUrl(jsonObject.getString("doctor_picture"));
                        onLineDetails.setDoctorRating(jsonObject.getString("doctor_rating"));
                        onLineDetails.setDoctorName(jsonObject.getString("doctor_name"));
                        onLineDetails.setDoctorStatus(messageObj.getString("status"));
                        onLineDetails.setRequestId(jsonObject.getString("request_id"));
                        onLineDetails.setLatitude(jsonObject.getString("s_latitude"));
                        onLineDetails.setLongitude(jsonObject.getString("s_longitude"));
                        onLineDetails.setdLatitude(jsonObject.getString("d_latitude"));
                        onLineDetails.setdLongitude(jsonObject.getString("d_longitude"));
                        onLineDetails.setBetweenDistance(jsonObject.optString("eta"));
                        onLineDetails.setEtaTime(jsonObject.optString("time"));
                        onLineDetails.setClinicAddress(jsonObject.optString("c_address"));
                        onLineDetails.setSourceAddress(jsonObject.optString(Const.SOURCE_ADDRESS));

                        setMarkerOnMap(onLineDetails);

                    } else {
                        AndyUtils.showLongToast(messageObj.getString("error_message"), DoctorOnDemandDoctorActivity.this);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
    };
    private Dialog invoiceDialog;
    private Dialog feedbackDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_on_demand_doctor);
        mapView = (MapView) findViewById(R.id.mapview_doctor_on_demand_doctor);
        homeImageView = (ImageView) findViewById(R.id.iv_doctor_on_demand_home);
        statusImageView = (ImageView) findViewById(R.id.iv_doctor_on_demand_doctor_status);
        doctorName = (TextView) findViewById(R.id.tv_doctor_on_demand_doctor_name);
        doctorDepartText = (TextView) findViewById(R.id.tv_doctor_on_demand_depart_status);
        yourDoctorText = (TextView) findViewById(R.id.tv_doctor_on_demand_your);
        doctorRating = (TextView) findViewById(R.id.tv_doctor_on_demand_doctor_rating);
        etaDistance = (TextView) findViewById(R.id.tv_doctor_on_demand_doctor_distance);
        AndyUtils.appLog("ClinicDeatils", "OnCreate");
        etatTime = (TextView) findViewById(R.id.tv_doctor_on_demand_doctor_eta);
        doctorIcon = (ImageView) findViewById(R.id.iv_doctor_on_demand_doctor_icon);
        sourceEdiText = (EditText) findViewById(R.id.et_doctor_on_demand_doctor_source_address);
        sourceEdiText.setEnabled(false);
        destinationEditText = (EditText) findViewById(R.id.et_doctor_on_demand_doctor_destination_address);
        destinationEditText.setEnabled(false);
        homeImageView.setOnClickListener(this);
        statusImageView.setOnClickListener(this);
        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            mapsSupported = false;
        }
        if (mapView != null) {
            mapView.onCreate(mBundle);
        }
        setUpMap();

    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
//        activity.currentFragment = Const.DoctorOnDemandDoctorFragment;
        IntentFilter filter = new IntentFilter(Const.DOCTOR_NEW_REQUEST_STATUS);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
                filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    private void setUpMap() {
        // map.
        if (mGoogleMap == null) {
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mGoogleMap = googleMap;
                    bundle = getIntent().getExtras();
                    if (bundle != null && bundle.getString(Const.DOCTOR_REQUEST_STATUS).equals(Const.REQUEST)) {
                        onLineDetails = (DoctorOnLineDetails) bundle.getSerializable(Const.DoctorOnDemandFragment);
                        if (onLineDetails != null) {
                            setMarkerOnMap(onLineDetails);
                        }
                    } else if (bundle != null && bundle.getString(Const.DOCTOR_REQUEST_STATUS).equals(Const.LAST_REQUEST)) {
                        invoiceDetails = (InvoiceDetails) bundle.getSerializable(Const.DoctorOnDemandFragment);
                        if (invoiceDetails != null) {
//                            showInvoiceDialog(invoiceDetails);
                        }
                    }

                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_doctor_on_demand_doctor_status:
                showDoctorStatusDialog();
                break;
            case R.id.iv_doctor_on_demand_home:
                break;
            case R.id.tv_doctor_on_demand_depart_status:
//               showInvoiceDialog();
                break;
        }
    }

    private void showDoctorStatusDialog() {
        statusDialog = new Dialog(this, R.style.DialogDocotrTheme);
        statusDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        statusDialog.setContentView(R.layout.dialog_doctor_status_layout);
        TextView doctorDepart = (TextView) statusDialog.findViewById(R.id.tv_doctor_depart);
        TextView doctorOnTheWay = (TextView) statusDialog.findViewById(R.id.tv_doctor_on_the_way);
        TextView doctorArrived = (TextView) statusDialog.findViewById(R.id.tv_doctor_arrived);
        TextView startTreatment = (TextView) statusDialog.findViewById(R.id.tv_start_treatment);
        TextView endTreatment = (TextView) statusDialog.findViewById(R.id.tv_end_treatment);
        status_position = Integer.parseInt(onLineDetails.getDoctorStatus());
        AndyUtils.appLog("DialogStatusPosition", status_position + "");
        if (bundle != null) {
            doctorDepart.setText(getString(R.string.doctor_depart));
            doctorOnTheWay.setText(getString(R.string.doctor_on_the_way));
            doctorArrived.setText(getString(R.string.doctor_arrived));
            startTreatment.setText(getString(R.string.start_treatment));
            endTreatment.setText(getString(R.string.end_treatment));
        } else {

            doctorDepart.setText(getString(R.string.doctor_accept_appointment));
            doctorOnTheWay.setText(getString(R.string.trip_started));
            doctorArrived.setText(getString(R.string.reached_destination));
            startTreatment.setText(getString(R.string.treatment_started));
            endTreatment.setText(getString(R.string.end_treatment));
        }

        switch (status_position) {
            case 1:
                doctorDepart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                break;
            case 2:
                doctorDepart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                doctorOnTheWay.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                break;
            case 3:
                doctorDepart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                doctorOnTheWay.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                doctorArrived.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                break;
            case 4:
                doctorDepart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                doctorOnTheWay.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                doctorArrived.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                startTreatment.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                break;
            case 5:
                doctorDepart.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                doctorOnTheWay.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                doctorArrived.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                startTreatment.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                endTreatment.setCompoundDrawablesWithIntrinsicBounds(R.drawable.selected, 0, 0, 0);
                break;
        }
        statusDialog.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GOOGLE_MATRIX:
                AndyUtils.removeProgressDialog();
                AndyUtils.appLog("Ashutosh", "GoogleMatrixResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("status").equals("OK")) {
                        JSONArray sourceArray = jsonObject.getJSONArray("origin_addresses");
                        String sourceObject = (String) sourceArray.get(0);

                        JSONArray destinationArray = jsonObject.getJSONArray("destination_addresses");
                        String destinationObject = (String) destinationArray.get(0);

                        JSONArray jsonArray = jsonObject.getJSONArray("rows");
                        JSONObject elementsObject = jsonArray.getJSONObject(0);
                        JSONArray elementsArray = elementsObject.getJSONArray("elements");
                        JSONObject distanceObject = elementsArray.getJSONObject(0);
                        JSONObject dObject = distanceObject.getJSONObject("distance");
                        String distance = dObject.getString("text");
                        JSONObject durationObject = distanceObject.getJSONObject("duration");
                        String duration = durationObject.getString("text");
                        AndyUtils.appLog("Distance And Duration", distance + " " + duration);


                    }
                } catch (JSONException e) {
                    AndyUtils.removeProgressDialog();
                    e.printStackTrace();
                }
                break;

        }

    }

    private void setDoctorStatusTitle(int position) {
        AndyUtils.appLog("DoctorStatusPosition", position + "");
        switch (position) {
            case 1:
                doctorDepartText.setText(getString(R.string.doctor_depart));
                break;
            case 2:
                doctorDepartText.setText(getString(R.string.doctor_on_the_way));
                break;
            case 3:
                doctorDepartText.setText(getString(R.string.doctor_arrived));
                break;
            case 4:
                doctorDepartText.setText(getString(R.string.start_treatment));
                break;
            case 5:
                doctorDepartText.setText(getString(R.string.end_treatment));
                break;

        }
    }

    private void setMarkerOnMap(DoctorOnLineDetails onLineDetails) {
        AndyUtils.appLog("DesLatLng", Double.valueOf(onLineDetails.getdLatitude()) + " " + Double.valueOf(onLineDetails.getLatitude()));
        LatLng srcLang = new LatLng(Double.valueOf(onLineDetails.getLatitude()), Double.valueOf(onLineDetails.getLongitude()));
        LatLng desLang = new LatLng(Double.valueOf(onLineDetails.getdLatitude()), Double.valueOf(onLineDetails.getdLongitude()));


        CameraPosition sourecCameraPosition = CameraPosition.builder()
                .target(srcLang)
                .zoom(15)
                .build();
        if (markerpatient != null) {
            markerpatient.remove();
        }
        if (markerDcotor != null) {
            markerDcotor.remove();
        }

        MarkerOptions opt = new MarkerOptions();
        opt.position(srcLang);
        opt.title(getString(R.string.my_location));
        opt.icon(BitmapDescriptorFactory
                .fromResource(R.drawable.source));
        markerpatient = mGoogleMap.addMarker(opt);
        mGoogleMap.addMarker(opt);
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(sourecCameraPosition));
        MarkerOptions doctorOpt = new MarkerOptions();
        doctorOpt.position(desLang);
        doctorOpt.title(onLineDetails.getDoctorName());
        doctorOpt.icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_marker));
        markerDcotor = mGoogleMap.addMarker(doctorOpt);
        mGoogleMap.addMarker(doctorOpt);
        doctorName.setText(onLineDetails.getDoctorName());
        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
        doctorRating.setText(form.format(Double.valueOf(onLineDetails.getDoctorRating())));

        if (!onLineDetails.getBetweenDistance().equals("")) {
            String distance[] = onLineDetails.getBetweenDistance().split(" ");
            StringBuilder sb = new StringBuilder();
            if (distance[0] != null) {
                sb.append(distance[0]);
            }
            if (distance[1] != null) {
                sb.append(" ").append(distance[1]);
            }

            etaDistance.setText(getString(R.string.distance_dot) + " " + sb.toString());
        }
        etatTime.setText(getString(R.string.eta_dot) + " " + onLineDetails.getEtaTime());
        sourceEdiText.setText(onLineDetails.getSourceAddress());
        destinationEditText.setText(onLineDetails.getClinicAddress());
        setDoctorStatusTitle(Integer.parseInt(onLineDetails.getDoctorStatus()));

        yourDoctorText.setText(getString(R.string.your_doctor));
        if (!onLineDetails.getDoctorPictureUrl().equals("")) {
            Glide.with(this).load(onLineDetails.getDoctorPictureUrl()).into(doctorIcon);
        } else {
            doctorIcon.setImageResource(R.drawable.profile);
        }

    }

    @Override
    public void onBackPressed() {

    }
}
