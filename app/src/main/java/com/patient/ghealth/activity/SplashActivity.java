package com.patient.ghealth.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.patient.ghealth.R;
import com.patient.ghealth.gcmHandlers.GCMRegisterHandler;
import com.patient.ghealth.gcmHandlers.GcmBroadcastReceiver;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

public class SplashActivity extends Activity {

    PreferenceHelper pHelper;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        pHelper = new PreferenceHelper(this);
        if (TextUtils.isEmpty(pHelper.getDeviceToken())) {
            registerGcmReceiver(new GcmBroadcastReceiver());
            Log.d("SplashActivity", "FirstTime");

        }
//        startHandler();
    }

    private void startHandler() {
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if ((boolean) PreferenceHelper.getParam(SplashActivity.this, Const.LOGIN_FIRST_TIME, false) == false) {
                    Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(mainIntent);
                    finish();
                } else if ((boolean) PreferenceHelper.getParam(SplashActivity.this, Const.LOGIN_FIRST_TIME, false) == true) {
                    Intent mainIntent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(mainIntent);
                    finish();
                }

            }
        }, 2000);
    }


    @Override
    protected void onResume() {
        super.onResume();
        startHandler();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        handler.removeMessages(0);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (handler != null) {
                finishAffinity();
                handler.removeMessages(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (handler != null) {
                finishAffinity();
                handler.removeMessages(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registerGcmReceiver(BroadcastReceiver mHandleMessageReceiver) {
        if (mHandleMessageReceiver != null) {
            new GCMRegisterHandler(this, mHandleMessageReceiver);
        }
    }

    public void unregisterGcmReceiver(BroadcastReceiver mHandleMessageReceiver) {


        try {
            if (mHandleMessageReceiver != null) {
                unregisterReceiver(mHandleMessageReceiver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

