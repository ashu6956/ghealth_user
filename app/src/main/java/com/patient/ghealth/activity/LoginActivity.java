package com.patient.ghealth.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.patient.ghealth.R;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.SocialMediaProfile;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.realmDB.PatientProfile;
import com.patient.ghealth.realmDB.RealmDBController;
import com.patient.ghealth.receiver.InterNetBroadCastConnectionReceiver;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;

/**
 * Created by getit on 8/5/2016.
 */
public class LoginActivity extends Activity implements AsyncTaskCompleteListener, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {


    private static final String TAG = "LoginActivity";
    private static final int GOOGLE_SIGN_IN = 205;
    private static final int REQUEST_CODE_TOKEN_AUTH = 207;
    Button loginButton;
    TextView forgotPassword;
    Button registerButton;
    EditText loginUserId;
    EditText loginPassword;
    ImageView facebookRegistration;
    ImageView googlePlusRegistration;
    ImageView twitterRegistration;
    TwitterAuthClient mTwitterAuthClient;
    private String sLoginUserId, sLoginPassword;
    private CallbackManager callbackManager;
    private String sFirstName, sLastName, sEmailId, sPassword, sUserName, sSocial_unique_id, pictureUrl;
    private boolean mSignInClicked, mIntentInProgress;
    private String loginType = Const.MANUAL;
    //google api client
    private GoogleApiClient mGoogleApiClient;
    private ConnectionResult mConnectionResult;
    private boolean isNetDialogShowing = false;
    private Dialog internetDialog;
    private InterNetBroadCastConnectionReceiver internetBroadCastConnectionReceiver;
    private ImageButton vis_pass, passwordIcon;
    private boolean isclicked = false;
    private String sPictureUrl;
    private AQuery aQuery;
    private String filePath = "";
    private SocialMediaProfile mediaProfile;
    private String accessToken = "";
    private Dialog forgotDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        callbackManager = CallbackManager.Factory.create();
        if (!TextUtils.isEmpty((CharSequence) PreferenceHelper.getParam(this, Const.Params.LANGUAGE, ""))) {
            String languageToLoad = (String) PreferenceHelper.getParam(this, Const.Params.LANGUAGE, ""); // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config2 = new Configuration();
            config2.locale = locale;
            getResources().updateConfiguration(config2,
                    getResources().getDisplayMetrics());

        }

        setContentView(R.layout.activity_login);
        getPermission();
        facebookRegisterCallBack();
        loginButton = (Button) findViewById(R.id.bn_login);
        registerButton = (Button) findViewById(R.id.bn_sign_up);
        facebookRegistration = (ImageView) findViewById(R.id.iv_login_fb);
        googlePlusRegistration = (ImageView) findViewById(R.id.iv_login_google_plus);
        twitterRegistration = (ImageView) findViewById(R.id.iv_login_twitter);
        twitterRegistration.setVisibility(View.GONE);
        loginUserId = (EditText) findViewById(R.id.et_login_userid);
        loginPassword = (EditText) findViewById(R.id.et_login_password);
        aQuery = new AQuery(this);
        forgotPassword = (TextView) findViewById(R.id.btn_forgot_pass);
        String forgotPasswordTxt = getString(R.string.forgot_password) + "?";
        forgotPassword.setText(Html.fromHtml("<u>" + forgotPasswordTxt + "</u>"));

        forgotPassword.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        registerButton.setOnClickListener(this);
        facebookRegistration.setOnClickListener(this);
        googlePlusRegistration.setOnClickListener(this);
        twitterRegistration.setOnClickListener(this);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        Scope scope = new Scope("https://www.googleapis.com/auth/plus.login");
        // ATTENTION: This "addApi(AppIndex.API)"was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .requestServerAuthCode(Const.AUTH_TOKEN)
//                .requestIdToken(Const.AUTH_TOKEN)
//                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(scope)
                .addApi(AppIndex.API).build();
        AndyUtils.generateKeyHAsh(this);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(Const.TWITTER_KEY, Const.TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        vis_pass = (ImageButton) findViewById(R.id.vis_pass);
        passwordIcon = (ImageButton) findViewById(R.id.iv_pass);

        loginPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordIcon.setVisibility(View.GONE);
                vis_pass.setVisibility(View.VISIBLE);
                AndyUtils.appLog("LoginActivity", "onTextChanged");
                vis_pass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isclicked == false) {
                            AndyUtils.appLog("LoginActivitytrue", "onTextChanged");
                            loginPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            isclicked = true;
                            loginPassword.setSelection(loginPassword.getText().length());
                            passwordIcon.setVisibility(View.GONE);
                            vis_pass.setVisibility(View.VISIBLE);

                        } else {
                            AndyUtils.appLog("LoginActivityfalse", "onTextChanged");
                            isclicked = false;
                            loginPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                            vis_pass.setVisibility(View.GONE);
                            loginPassword.setSelection(loginPassword.getText().length());
//                            passwordIcon.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    private void facebookRegisterCallBack() {
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            //    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                AndyUtils.appLog(TAG, "onSuccess");
                accessToken = loginResult.getAccessToken().getToken();
                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
                                if (jsonObject != null && graphResponse != null) {
                                    AndyUtils.appLog("Json Object", jsonObject.toString());
                                    AndyUtils.appLog("Graph response", graphResponse.toString());
                                    try {
                                        sUserName = jsonObject.getString("name");
                                        sEmailId = jsonObject.getString("email");
                                        sSocial_unique_id = jsonObject.getString("id");
                                        sPictureUrl = "https://graph.facebook.com/" + sSocial_unique_id + "/picture?type=large";
                                        mediaProfile = new SocialMediaProfile();

                                        if (sUserName != null) {
                                            String[] name = sUserName.split(" ");
                                            if (name[0] != null) {
                                                mediaProfile.setFirstName(name[0]);
                                            }
                                            if (name[1] != null) {
                                                mediaProfile.setLastName(name[1]);
                                            }
                                        }
                                        mediaProfile.setEmailId(sEmailId);
                                        mediaProfile.setSocialUniqueId(sSocial_unique_id);
                                        mediaProfile.setPictureUrl(sPictureUrl);
                                        mediaProfile.setLoginType(Const.FACEBOOK);
                                        mediaProfile.setAccessToken(accessToken);

                                        AndyUtils.appLog("all details", sUserName + "" + sEmailId + " " + " " + sPictureUrl);
                                        if (sSocial_unique_id != null) {
                                            loginType = Const.FACEBOOK;
                                            patientLogin(accessToken);
                                        } else {
                                            AndyUtils.showShortToast("Invalidate Data", LoginActivity.this);
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }
                        }

                );
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,locale,hometown,email,gender,birthday,location");
                request.setParameters(parameters);
                request.executeAsync();
            }


            @Override
            public void onCancel() {
                AndyUtils.showLongToast(getString(R.string.login_cancelled), LoginActivity.this);
            }

            @Override
            public void onError(FacebookException error) {
                AndyUtils.showLongToast(getString(R.string.login_failed), LoginActivity.this);
                AndyUtils.appLog("login failed Error", error.toString());
            }
        });
    }


    private void showForgotPasswordDialog() {
        forgotDialog = new Dialog(this, R.style.DialogThemeforview);
        forgotDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        forgotDialog.setContentView(R.layout.dialog_forgot_password_layout);
        final EditText emailId = (EditText) forgotDialog.findViewById(R.id.et_forgot_emailID);
        Button requestButton = (Button) forgotDialog.findViewById(R.id.bn_request_password);
        ImageView backButton = (ImageView) forgotDialog.findViewById(R.id.iv_back);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotDialog.cancel();
            }
        });
        requestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (emailId.getText().toString().trim().length() == 0) {
                    AndyUtils.showShortToast(getString(R.string.please_enter_emailid), LoginActivity.this);
                } else {
                    requestForgotPassword(emailId.getText().toString().trim());
                }
            }
        });
        forgotDialog.show();
    }


    private void getLoginDetails() {
        sEmailId = loginUserId.getText().toString().trim();
        sPassword = loginPassword.getText().toString().trim();
    }

    @Override
    protected void onResume() {
        super.onResume();
        internetBroadCastConnectionReceiver = new InterNetBroadCastConnectionReceiver(LoginActivity.this);
        registerReceiver(internetBroadCastConnectionReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bn_login:
                getLoginDetails();
                if (sEmailId.length() == 0) {
                    loginUserId.setError(getString(R.string.please_enter_emailid));
                    return;
                } else if (sPassword.length() == 0) {
                    loginPassword.setError(getString(R.string.please_enter_password));
                    return;
                } else {
                    loginType = Const.MANUAL;
                    patientLogin("");
                }
                break;
            case R.id.bn_sign_up:
                Intent registerIntent = new Intent(this, RegisterActivity.class);
                startActivity(registerIntent);
                finish();
                break;
            case R.id.iv_login_fb:
                AndyUtils.appLog(TAG, "On Click of Facebook::");
                LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email", "user_birthday", "user_photos", "user_location"));
                loginType = Const.FACEBOOK;
                break;
            case R.id.iv_login_google_plus:
                AndyUtils.appLog(TAG, "On Click of GooglePlus::");
                mSignInClicked = true;
                if (!mGoogleApiClient.isConnecting()) {
//                    GoogleSignIn();
                    AndyUtils.showSimpleProgressDialog(this, getString(R.string.connecting_gmail), false);
                    mGoogleApiClient.connect();
                }
                break;
            case R.id.iv_login_twitter:
                loginToTwitter();
                break;
            case R.id.btn_forgot_pass:
                showForgotPasswordDialog();
                break;
        }
    }


    public void requestForgotPassword(String emailId) {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showShortToast(this.getString(R.string.no_internet), this);
            return;
        }
        AndyUtils.showSimpleProgressDialog(this, getString(R.string.please_wait), false);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.POST_FORGOT_PASSWORD_URL);
        map.put(Const.Params.EMAIL, emailId);

        Log.d("Ashutosh", "ForgotMap" + map);
        new HttpRequester(this, Const.POST, map, Const.ServiceCode.POST_FORGOT_PASSWORD, this);
    }


    private void loginToTwitter() {
        mTwitterAuthClient = new TwitterAuthClient();
        mTwitterAuthClient.authorize(this, new Callback<TwitterSession>() {

            @Override
            public void success(Result<TwitterSession> twitterSessionResult) {
                // Success
                login(twitterSessionResult);
                String name = twitterSessionResult.data.getUserName();
            }

            @Override
            public void failure(TwitterException e) {
                e.printStackTrace();
            }
        });
    }

    private void login(Result<TwitterSession> twitterSessionResult) {

        //Creating a twitter session with result's data
        TwitterSession session = twitterSessionResult.data;

        //Getting the username from session
        final String username = session.getUserName();
        sSocial_unique_id = String.valueOf(session.getUserId());


        //This code will fetch the profile image URL
        //Getting the account service of the user logged in
        Twitter.getApiClient(session).getAccountService()
                .verifyCredentials(true, false, new Callback<User>() {
                    @Override
                    public void failure(TwitterException e) {
                        //If any error occurs handle it here
                    }

                    @Override
                    public void success(Result<User> userResult) {
                        //If it succeeds creating a User object from userResult.data
                        User user = userResult.data;

                        //Getting the profile image url
                        String name = user.name;
                        sEmailId = user.email;
                        sPictureUrl = user.profileImageUrlHttps.replace("_normal", "");
                        mediaProfile = new SocialMediaProfile();
                        mediaProfile.setFirstName(name);
                        mediaProfile.setPictureUrl(sPictureUrl);
                        mediaProfile.setLoginType(Const.TWITTER);
                        mediaProfile.setLastName("");
                        mediaProfile.setEmailId("");
                        mediaProfile.setSocialUniqueId(sSocial_unique_id);

                        if (sSocial_unique_id != null) {
                            loginType = Const.TWITTER;
                            patientLogin("");
                        } else {
                            AndyUtils.showShortToast(getString(R.string.invalid_data), LoginActivity.this);
                        }
                    }
                });
    }

    private void patientLogin(String token) {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showShortToast(getString(R.string.no_internet), this);
            return;
        }
        AndyUtils.showSimpleProgressDialog(this, getString(R.string.please_wait), false);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.LOGIN_URL);
        if (loginType.equals(Const.FACEBOOK) || loginType.equals(Const.GOOGLE_PLUS) || loginType.equals(Const.TWITTER)) {
            map.put(Const.Params.SOCIAL_UNIQUE_ID, sSocial_unique_id);
            map.put(Const.Params.LOGIN_BY, loginType);
            map.put(Const.Params.ACCESS_TOKEN, token);
        } else {
            map.put(Const.Params.PASSWORD, sPassword);
            map.put(Const.Params.LOGIN_BY, Const.MANUAL);
            map.put(Const.Params.EMAIL, sEmailId);
        }

        map.put(Const.Params.DEVICE_TYPE, Const.ANDROID);
        map.put(Const.Params.DEVICE_TOKEN, new PreferenceHelper(this).getDeviceToken());

        AndyUtils.appLog("Ashutosh", "LoginMap" + map);
        new HttpRequester(this, Const.POST, map, Const.ServiceCode.LOGIN, this);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.LOGIN:
                AndyUtils.removeProgressDialog();
                AndyUtils.appLog("Ashutosh", "LoginResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        String session_token = jsonObject.optString("session_token");
                        PreferenceHelper.setParam(this, Const.Params.ECOM_TOKEN, jsonObject.optString(Const.Params.ECOM_TOKEN));
                        PreferenceHelper.setParam(this, Const.Params.SESSION_TOKEN, session_token);
                        PreferenceHelper.setParam(this, Const.Params.ID, jsonObject.optString("id"));
                        PreferenceHelper.setParam(this, Const.Params.PICTURE, jsonObject.optString("picture"));
                        PreferenceHelper.setParam(this, Const.Params.CURRENCY, jsonObject.optString(Const.Params.CURRENCY));
                        String language1 = jsonObject.optString(Const.Params.LANGUAGE);
                        String language = language1.substring(language1.indexOf("(") + 1, language1.indexOf(")"));
                        PreferenceHelper.setParam(this, Const.Params.LANGUAGE, language);

                        PreferenceHelper.setParam(this, Const.LOGIN_FIRST_TIME, true);
                        Realm realm = RealmDBController.with(this).getRealm();
                        final PatientProfile patientProfile = new PatientProfile();
                        patientProfile.setPatientId(Integer.parseInt(jsonObject.optString("id")));
                        patientProfile.setFirstName(jsonObject.optString("first_name"));
                        PreferenceHelper.setParam(this, Const.Params.FIRST_NAME, jsonObject.optString("first_name"));
                        patientProfile.setLastName(jsonObject.optString("last_name"));
                        patientProfile.setEmailId(jsonObject.optString("email"));
                        patientProfile.setPictureUrl(jsonObject.optString("picture"));
                        patientProfile.setMobileNumber(jsonObject.optString("phone"));
                        patientProfile.setBillingAddress(jsonObject.optString("billing_address"));
                        patientProfile.setShippingAddress(jsonObject.optString("shipping_address"));
                        patientProfile.setBillingLandMark(jsonObject.optString("blandmark"));
                        patientProfile.setBillingBlockNo(jsonObject.optString("bblock_no"));
                        patientProfile.setShippingLandMark(jsonObject.optString("slandmark"));
                        patientProfile.setShippingBlockNo(jsonObject.optString("sblock_no"));
                        patientProfile.setDob(jsonObject.optString("dob"));
                        patientProfile.setGender(jsonObject.optString("gender"));
                        patientProfile.setGenderTitle(jsonObject.optString("title"));
                        patientProfile.setNationality(jsonObject.optString(Const.Params.NATIONALITY));
                        patientProfile.setCountryCode(jsonObject.optString(Const.Params.PHONE_COUNTRY_CODE));
                        patientProfile.setCurrency(jsonObject.optString(Const.Params.CURRENCY));
                        patientProfile.setLanguage(jsonObject.optString(Const.Params.LANGUAGE));
                        patientProfile.setTimeZone(jsonObject.optString(Const.Params.TIMEZONE));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(patientProfile);
                            }
                        });


                        Intent loginIntent = new Intent(this, MainActivity.class);
                        startActivity(loginIntent);
                        finish();
                    } else {
                        if (jsonObject.optString("error_message").equals(Const.NEW_USER)) {
                            if (mediaProfile != null) {
                                Intent newUserIntent = new Intent(this, RegisterActivity.class);
                                Bundle bundle = new Bundle();
                                bundle.putSerializable(Const.LOGIN_FIRST_TIME, mediaProfile);
                                newUserIntent.putExtras(bundle);
                                startActivity(newUserIntent);
                                finish();
                            }

                        } else {
                            AndyUtils.showShortToast(jsonObject.getString("error_message"), this);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;

            case Const.ServiceCode.POST_GOOGLE_ACCESS_TOKEN:
                AndyUtils.appLog("Ashutosh", "PostGoogleAccesssToken Response" + response);
                try {
                    JSONObject jsonObject1 = new JSONObject(response);
                    if (jsonObject1.optInt(Const.Params.STATUS_CODE) == 200) {
                        JSONObject responseObject = jsonObject1.getJSONObject(Const.RESPONSE);
                        accessToken = responseObject.optString("access_token");
                    } else {
                        Toast.makeText(this, jsonObject1.optString(Const.Params.STATUS_MESSAGE), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                break;
            case Const.ServiceCode.POST_FORGOT_PASSWORD:
                AndyUtils.removeProgressDialog();
                AndyUtils.appLog(TAG, "ForgotPasswordResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        if (forgotDialog != null && forgotDialog.isShowing()) {
                            forgotDialog.cancel();
                        }
                        AndyUtils.showShortToast(jsonObject.optString("message"), this);
                    } else {
                        AndyUtils.showShortToast(jsonObject.optString("error_message"), this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }


    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (!mIntentInProgress) {
            // Store the ConnectionResult so that we can use it later when the
            // user clicks
            // 'sign-in'.

            mConnectionResult = connectionResult;
            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all

                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }

    }

    @SuppressLint("NewApi")
    private void getPermission() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion > android.os.Build.VERSION_CODES.LOLLIPOP) {

            String[] permissions_dummy = new String[7];
            int i = 0;

            String permission = "android.permission.ACCESS_COARSE_LOCATION";
            int res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;
            }
            permission = "android.permission.READ_CONTACTS";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;


            }
            permission = "android.permission.WRITE_EXTERNAL_STORAGE";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;


            }
            permission = "android.permission.ACCESS_FINE_LOCATION";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;


            }
            permission = "android.permission.CAMERA";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;
            }
            permission = "android.permission.GET_ACCOUNTS";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;

            }
            permission = "android.permission.RECORD_AUDIO";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;

            }
//
//            permission = "android.permission.READ_SMS";
//            res = checkCallingOrSelfPermission(permission);
//            if (res != PackageManager.PERMISSION_GRANTED) {
//
//
//                permissions_dummy[i] = permission;
//                i = i + 1;
//
//
//            }


            String[] permissions = new String[i];

            for (int j = 0; j < i; j++) {

                permissions[j] = permissions_dummy[j];

            }


            int yourRequestId = 1;
            if (i != 0) {


                // Do something for lollipop and above versions
                requestPermissions(permissions, yourRequestId);
            }

        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        AndyUtils.removeProgressDialog();
        mSignInClicked = false;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        final String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

        Person currentPerson = Plus.PeopleApi
                .getCurrentPerson(mGoogleApiClient);

        String personName = currentPerson.getDisplayName();

        String personPhoto = currentPerson.getImage().getUrl().toString();

        personPhoto = personPhoto.substring(0, personPhoto.length() - 2)
                + "150";

        sPictureUrl = personPhoto;
        mediaProfile = new SocialMediaProfile();

        if (personName.contains(" ")) {
            String[] split = personName.split(" ");
            mediaProfile.setFirstName(split[0]);
            mediaProfile.setLastName(split[1]);
        } else {
            mediaProfile.setFirstName(personName);
        }
        if (!TextUtils.isEmpty(personPhoto)
                || !personPhoto.equalsIgnoreCase("null")) {

        }
        mediaProfile.setEmailId(email);
        sSocial_unique_id = currentPerson.getId();
        mediaProfile.setSocialUniqueId(sSocial_unique_id);
        mediaProfile.setPictureUrl(sPictureUrl);
        mediaProfile.setLoginType(Const.GOOGLE_PLUS);

        new GetAccessToken(email).execute(email);

    }


//    private void GoogleSignIn() {
//        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//        startActivityForResult(signInIntent, RC_SIGN_IN);
//    }


    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    startIntentSenderForResult(mConnectionResult
                                    .getResolution().getIntentSender(), GOOGLE_SIGN_IN, null,
                            0, 0, 0, null);
                }
            } catch (IntentSender.SendIntentException e) {
                // The intent was canceled before it was sent. Return to the
                // default
                // state and attempt to connect to get an updated
                // ConnectionResult.
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(internetBroadCastConnectionReceiver);

        LoginManager.getInstance().logOut();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
        }
        TwitterSession twitterSession = TwitterCore.getInstance().getSessionManager().getActiveSession();
        if (twitterSession != null) {
            CookieSyncManager.createInstance(this);
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeSessionCookie();
            Twitter.getSessionManager().clearActiveSession();
            Twitter.logOut();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Activity Res", "" + requestCode);
        switch (requestCode) {
            case GOOGLE_SIGN_IN:
                if (resultCode != Activity.RESULT_OK) {
                    mSignInClicked = false;
                    AndyUtils.removeProgressDialog();
                }
                mIntentInProgress = false;

                if (!mGoogleApiClient.isConnecting()) {
                    mGoogleApiClient.connect();
                }
                break;
            case REQUEST_CODE_TOKEN_AUTH:
                if (requestCode == REQUEST_CODE_TOKEN_AUTH && resultCode == RESULT_OK) {
                    Bundle extra = data.getExtras();
                    accessToken = extra.getString("authtoken");
                    AndyUtils.appLog("OneTimeToken", accessToken);
                    if (sSocial_unique_id != null && accessToken != null) {
                        loginType = Const.GOOGLE_PLUS;
                        mediaProfile.setAccessToken(accessToken);
                        patientLogin(accessToken);
                    } else {
                        AndyUtils.showShortToast(getString(R.string.invalid_data), LoginActivity.this);
                    }
                }
                break;

            case 140:
                AndyUtils.appLog("LoginActivity", "OnActivityTwitter");
                mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
                break;
            default:
                callbackManager.onActivityResult(requestCode, resultCode, data);

        }
    }


//    private void handleSignInResult(GoogleSignInResult result) {
//
//        AndyUtils.appLog("GoogleSignInResult", result.isSuccess() + "");
//        if (result.isSuccess()) {
//            // Signed in successfully, show authenticated UI.
//            GoogleSignInAccount acct = result.getSignInAccount();
//            getAccessTokenFromServer(acct.getServerAuthCode());
//            // callSocialLoginApi(acct.getServerAuthCode(), "google");
//            //  mStatusTextView.setText(getString(R.string.signed_in_fmt, acct.getDisplayName()));
//            //  updateUI(true);
//        } else {
//            // Signed out, show unauthenticated UI.
//            // updateUI(false);
//        }
//    }

    private void getAccessTokenFromServer(String token) {

        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showShortToast(this.getString(R.string.no_internet), this);
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.POST_GOOGLE_ACCESS_TOKEN_URL);
        map.put(Const.Params.CODE, token);

        AndyUtils.appLog("LoginActivity", "GetAccessTokenMap" + map);
        new HttpRequester(this, Const.POST, map, Const.ServiceCode.POST_GOOGLE_ACCESS_TOKEN, this);

    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
        }


    }


    class GetAccessToken extends AsyncTask<String, Void, String> {

        String emailId;

        public GetAccessToken(String emailId) {
            this.emailId = emailId;
        }

        @Override
        protected String doInBackground(String... strings) {
            String email = strings[0];
            String scopes = "oauth2:profile email";
            String token = null;
            AndyUtils.appLog("Email", email);
            try {

                token = GoogleAuthUtil.getToken(LoginActivity.this, email, scopes);
//                accessToken = GoogleAuthUtil.getToken(LoginActivity.this, email,
//                        "oauth2:" + Scopes.PLUS_LOGIN + " https://www.googleapis.com/auth/plus.profile.emails.read");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (UserRecoverableAuthException e) {
                // Recover (with e.getIntent())
                Log.e(TAG, e.toString());
                Intent recover = e.getIntent();
                startActivityForResult(recover, REQUEST_CODE_TOKEN_AUTH);
            } catch (GoogleAuthException e) {
                e.printStackTrace();
            }


            return token;
        }

        @Override
        protected void onPostExecute(String token) {
            Log.i(TAG, "Access token retrieved:" + token);

            if (sSocial_unique_id != null && token != null) {
                loginType = Const.GOOGLE_PLUS;
                mediaProfile.setAccessToken(token);
                patientLogin(token);
            } else {
//                AndyUtils.showShortToast(getString(R.string.invalid_data), LoginActivity.this);
            }
        }
    }


}

