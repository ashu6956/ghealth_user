package com.patient.ghealth.activity;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.opentok.android.Connection;
import com.opentok.android.Session;
import com.patient.ghealth.R;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.fragment.VideoChatFragment;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.model.InvoiceDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.ui.PreviewCameraFragment;
import com.patient.ghealth.ui.PreviewControlFragment;
import com.patient.ghealth.ui.RemoteControlFragment;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.OpenTokConfig;
import com.patient.ghealth.utils.PreferenceHelper;
import com.tokbox.android.accpack.OneToOneCommunication;
import com.tokbox.android.logging.OTKAnalytics;
import com.tokbox.android.logging.OTKAnalyticsData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class VideoCallActivity extends AppCompatActivity implements OneToOneCommunication.Listener, PreviewControlFragment.PreviewControlCallbacks,
        RemoteControlFragment.RemoteControlCallbacks, PreviewCameraFragment.PreviewCameraCallbacks, AsyncTaskCompleteListener, Session.SignalListener {

    private final String LOG_TAG = VideoCallActivity.class.getSimpleName();
    public VideoChatFragment mTextChatFragment;
    ProgressDialog mProgressDialog;
    long starttime = 0L;
    long timeInMilliseconds = 0L;
    Handler handler = new Handler();
    public Runnable updateTimer = new Runnable() {

        public void run() {
            timeInMilliseconds = System.currentTimeMillis() - starttime;
            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(timeInMilliseconds),
                    TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.MILLISECONDS.toSeconds(timeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));
            videoCallTimer.setText(hms);
            handler.postDelayed(this, 0);
        }

    };
    NotificationCompat.Builder nBuilder;
    PreferenceHelper pHelper;
    private String SESSION = "", TOKEN = "";
    private int mFragmentId = 0;
    private String mFragmentTag = null;
    //OpenTok calls
    private OneToOneCommunication mComm;
    private RelativeLayout mPreviewViewContainer;
    private RelativeLayout mRemoteViewContainer;
    private RelativeLayout mAudioOnlyView;
    private RelativeLayout mLocalAudioOnlyView;
    private RelativeLayout.LayoutParams layoutParamsPreview;
    private FrameLayout mTextChatContainer;
    private RelativeLayout mCameraFragmentContainer;
    private RelativeLayout mActionBarContainer;
    private TextView mAlert;
    private ImageView mAudioOnlyImage;
    //UI control bars fragments
    private PreviewControlFragment mPreviewFragment;
    private RemoteControlFragment mRemoteFragment;
    private PreviewCameraFragment mCameraFragment;
    private FragmentTransaction mFragmentTransaction;
    private Dialog invoiceDialog, feedbackDialog;
    private OTKAnalyticsData mAnalyticsData;
    private OTKAnalytics mAnalytics;
    private TextView doctorName;
    private TextView endVideoCall, videoCallTimer;
    private Bundle bundle;
    private DoctorOnLineDetails onLineDetails;
    private InvoiceDetails invoiceDetails;
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            AndyUtils.removeProgressDialog();
            if (mComm != null) {
                mComm.destroy();
            }
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }

//            handler.removeCallbacks(updateTimer);
            Log.d("FragmentRequest", "onReceive");
            String notification = intent.getStringExtra("ashutosh");
            Log.d("My Push notification", notification);
            String message = intent.getStringExtra(Const.DOCTOR_INTENT_MESSAGE);
            try {

                if (mComm != null) {
                    mComm.destroy();
                }
                JSONObject messageObj = new JSONObject(message);
                AndyUtils.appLog("JsonResponse", messageObj.toString());
                if (Integer.parseInt(messageObj.getString("status")) == 5) {
                    invoiceDetails = new InvoiceDetails();
                    JSONArray invoiceJsonArray = messageObj.getJSONArray("invoice");
                    JSONObject invoiceJsonObject = invoiceJsonArray.getJSONObject(0);
                    invoiceDetails.setInvoiceDoctorId(Integer.parseInt(invoiceJsonObject.getString("doctor_id")));
                    invoiceDetails.setBasePrice(invoiceJsonObject.getString("base_price"));
                    invoiceDetails.setPaymentMode(invoiceJsonObject.getString("payment_mode"));
                    invoiceDetails.setTotalPrice(invoiceJsonObject.getString("total"));
                    invoiceDetails.setMedicineFee(invoiceJsonObject.getString("medicine_fee"));
                    invoiceDetails.setReferralBonus(invoiceJsonObject.getString("referral_bonus"));
                    invoiceDetails.setPromoBonus(invoiceJsonObject.getString("promo_bonus"));
                    invoiceDetails.setTotalTime(invoiceJsonObject.getString("total_time"));
                    invoiceDetails.setTotalDistance(invoiceJsonObject.getString("distance"));
                    invoiceDetails.setInvoiceRequestId(messageObj.getString("request_id"));
                    invoiceDetails.setTreatmentFee(invoiceJsonObject.getString("time_price"));
                    invoiceDetails.setInvoiceDoctorStatus(messageObj.getString("status"));
                    invoiceDetails.setInvoiceDoctorPictureUrl(invoiceJsonObject.getString("doctor_picture"));
                    invoiceDetails.setInvoiceDoctorName(invoiceJsonObject.getString("doctor_name"));
                    Bundle invoiceBundle = new Bundle();
                    invoiceBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, invoiceDetails);
                    invoiceBundle.putString(Const.DOCTOR_CONSULT_CONNECTIVITY, "normal");
                    Intent invoiceIntent = new Intent(VideoCallActivity.this, FeedBackOnlineActivity.class);
                    invoiceIntent.putExtras(invoiceBundle);
                    startActivity(invoiceIntent);
                    finish();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    private Session mSession;
    private BroadcastReceiver recieve_chat;
    private boolean isdisplayed = false;
    private Dialog errorDialog;
    private RelativeLayout.LayoutParams lay_preview;
    private int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOG_TAG, "onCreate");
        super.onCreate(savedInstanceState);
        //Init the analytics logging for internal use
        String source = this.getPackageName();
        SharedPreferences prefs = this.getSharedPreferences("opentok", Context.MODE_PRIVATE);
        String guidVSol = prefs.getString("guidVSol", null);
        if (null == guidVSol) {
            guidVSol = UUID.randomUUID().toString();
            prefs.edit().putString("guidVSol", guidVSol).commit();
        }

        mAnalyticsData = new OTKAnalyticsData.Builder(OpenTokConfig.LOG_CLIENT_VERSION, source, OpenTokConfig.LOG_COMPONENTID, guidVSol).build();
        mAnalytics = new OTKAnalytics(mAnalyticsData);

        //add INITIALIZE attempt log event
        addLogEvent(OpenTokConfig.LOG_ACTION_INITIALIZE, OpenTokConfig.LOG_VARIATION_ATTEMPT);

        setContentView(R.layout.activity_video_connectivity);
        pHelper = new PreferenceHelper(this);

        doctorName = (TextView) findViewById(R.id.tv_video_call_doctor_name);
        endVideoCall = (TextView) findViewById(R.id.tv_video_call_end_video);
        videoCallTimer = (TextView) findViewById(R.id.tv_video_call_timer);
        endVideoCall.setVisibility(View.GONE);
        bundle = getIntent().getExtras();
        if (bundle != null && bundle.getString(Const.DOCTOR_CONSULT_CONNECTIVITY).equals("normal")) {
            onLineDetails = (DoctorOnLineDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
            doctorName.setText(onLineDetails.getDoctorName());
            PreferenceHelper.setParam(this, Const.DOCTOR_NAME, onLineDetails.getDoctorName());
            AndyUtils.appLog("Consult", "DoctorID" + onLineDetails.getDoctorOnLineId());
//            PreferenceHelper.setParam(this,Const.Params.DOCTOR_ID,onLineDetails.getDoctorOnLineId());
        } else if (!PreferenceHelper.getParam(this, Const.DOCTOR_NAME, "").equals("")) {
            doctorName.setText((CharSequence) PreferenceHelper.getParam(this, Const.DOCTOR_NAME, ""));
        }
        gteopentok();
        mPreviewViewContainer = (RelativeLayout) findViewById(R.id.publisherview);
        mRemoteViewContainer = (RelativeLayout) findViewById(R.id.subscriberview);
        mAlert = (TextView) findViewById(R.id.quality_warning);
        mAudioOnlyView = (RelativeLayout) findViewById(R.id.audioOnlyView);
        mLocalAudioOnlyView = (RelativeLayout) findViewById(R.id.localAudioOnlyView);
        mTextChatContainer = (FrameLayout) findViewById(R.id.textchat_fragment_container);
        mCameraFragmentContainer = (RelativeLayout) findViewById(R.id.camera_preview_fragment_container);
        mActionBarContainer = (RelativeLayout) findViewById(R.id.actionbar_preview_fragment_container);


        //show connecting dialog
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setTitle(getString(R.string.please_wait));
        mProgressDialog.setMessage(getString(R.string.connecting));
        mProgressDialog.show();

        addLogEvent(OpenTokConfig.LOG_ACTION_INITIALIZE, OpenTokConfig.LOG_VARIATION_SUCCESS);

        recieve_chat = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                String message = intent.getStringExtra("message");
                mPreviewFragment.countMessages.setVisibility(View.VISIBLE);
                count++;
                mPreviewFragment.countMessages.setText(String.valueOf(count));
                if (message.contains("http")) {
                    notifyMsg("Image");
                } else {
                    notifyMsg(message);
                }
            }
        };

        LocalBroadcastManager.getInstance(this).registerReceiver(recieve_chat,
                new IntentFilter("message_recieved"));

    }


    private void Notifymsg(String msg) {
        NotificationManager mNotificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(this, VideoCallActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(),
                notificationIntent, 0);

        nBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(msg))
                .setVibrate(new long[]{300, 1000})
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true)
                .setContentText(msg);
        nBuilder.build().flags |= Notification.FLAG_AUTO_CANCEL;
        nBuilder.setContentIntent(intent);
        mNotificationManager.notify(1, nBuilder.build());

    }


    private void gteopentok() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_OPENTOK + Const.Params.ID + "="
                + new PreferenceHelper(this).getParam(this, Const.Params.ID, "") + "&" + Const.Params.SESSION_TOKEN + "="
                + (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.REQUEST_ID + "="
                + new PreferenceHelper(this).getRequestId());

        Log.d("mahi", "map" + map);

        new HttpRequester(this, Const.GET, map, Const.ServiceCode.GET_OPENTOK, this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (mComm != null) {
            mComm.reloadViews(); //reload the local preview and the remote views
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (null != mSession) {
            mSession.disconnect();
        }
//        mComm.destroy();

//        if(handler!=null)
//        {
//            handler.removeCallbacks(updateTimer);
//        }

        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }


    public void hideFragment() {
        count = 0;
        mPreviewFragment.countMessages.setVisibility(View.GONE);
        mTextChatContainer.setVisibility(View.GONE);
        showAVCall(true);

    }


    private void showErrorDialog() {
        isdisplayed = true;
        errorDialog = new Dialog(this, R.style.DialogTheme);
        errorDialog.setCancelable(false);
        errorDialog.setContentView(R.layout.reconnect_dialog);
        Button btn_reconnect = (Button) errorDialog.findViewById(R.id.btn_reconect);
        final TextView tv_name = (TextView) errorDialog.findViewById(R.id.tv_name);
        btn_reconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                gteopentok();
//                tv_name.setText(getString(R.string.connecting));
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
        errorDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions,
                                           int[] grantResults) {
        switch (permsRequestCode) {
            case 200:
                boolean video = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                boolean audio = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                break;
        }
    }

    //Get OneToOneCommunicator object
    public OneToOneCommunication getComm() {
        return mComm;
    }

    public void showRemoteControlBar(View v) {
        if (mRemoteFragment != null && mComm.isRemote()) {
            mRemoteFragment.show();
        }
    }

    //Video local button event
    @Override
    public void onDisableLocalVideo(boolean video) {
        if (mComm != null) {
            mComm.enableLocalMedia(OneToOneCommunication.MediaType.VIDEO, video);
            lay_preview = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            if (mComm.isRemote()) {
                if (!video) {
                    lay_preview.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
                            RelativeLayout.TRUE);
                    lay_preview.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,
                            RelativeLayout.TRUE);

                    mAudioOnlyImage = new ImageView(this);
                    lay_preview.width = (int) getResources().getDimension(R.dimen.preview_width);
                    lay_preview.height = (int) getResources().getDimension(R.dimen.preview_height);
                    lay_preview.rightMargin = (int) getResources().getDimension(R.dimen.preview_rightMargin);
                    lay_preview.bottomMargin = (int) getResources().getDimension(R.dimen.preview_bottomMargin);

                    mAudioOnlyImage.setImageResource(R.mipmap.avatar);
                    mAudioOnlyImage.setBackgroundResource(R.drawable.bckg_audio_only);
                    mPreviewViewContainer.addView(mAudioOnlyImage, lay_preview);
                    //mAudioOnlyImage.getLayoutParams().height = getResources().getInteger(R.integer.dimen_preview);
                    //mAudioOnlyImage.getLayoutParams().width = getResources().getInteger(R.integer.dimen_preview);

                } else {
                    mPreviewViewContainer.removeView(mAudioOnlyImage);

                }
            } else {
                if (!video) {
                    mLocalAudioOnlyView.setVisibility(View.VISIBLE);
                    mPreviewViewContainer.addView(mLocalAudioOnlyView);
                } else {
                    mLocalAudioOnlyView.setVisibility(View.GONE);
                    mPreviewViewContainer.removeView(mLocalAudioOnlyView);
                }
            }
        }
    }

    //Call button event
    @Override
    public void onCall() {
//        if (mComm != null && mComm.isStarted()) {
//            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//            mComm.end();
//            cleanViewsAndControls();
//            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
//        } else {
//            addLogEvent(OpenTokConfig.LOG_ACTION_START_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//            mComm.start();
//            if (mPreviewFragment != null) {
//                mPreviewFragment.setEnabled(true);
//            }
//        }
    }

    //TextChat button event
    @Override
    public void onTextChat() {
        count = 0;
        mPreviewFragment.countMessages.setVisibility(View.GONE);
        if (mTextChatContainer.getVisibility() == View.VISIBLE) {
            addLogEvent(OpenTokConfig.LOG_ACTION_CLOSE_TEXTCHAT, OpenTokConfig.LOG_VARIATION_ATTEMPT);
            mTextChatContainer.setVisibility(View.GONE);
            showAVCall(true);
            addLogEvent(OpenTokConfig.LOG_ACTION_CLOSE_TEXTCHAT, OpenTokConfig.LOG_VARIATION_SUCCESS);
        } else {
            addLogEvent(OpenTokConfig.LOG_ACTION_OPEN_TEXTCHAT, OpenTokConfig.LOG_VARIATION_ATTEMPT);
            showAVCall(false);
            mTextChatContainer.setVisibility(View.VISIBLE);
            addLogEvent(OpenTokConfig.LOG_ACTION_OPEN_TEXTCHAT, OpenTokConfig.LOG_VARIATION_SUCCESS);
        }
    }

    //Audio remote button event
    @Override
    public void onDisableRemoteAudio(boolean audio) {
        if (mComm != null) {
            mComm.enableRemoteMedia(OneToOneCommunication.MediaType.AUDIO, audio);
        }
    }

    //Video remote button event
    @Override
    public void onDisableRemoteVideo(boolean video) {
        if (mComm != null) {
            mComm.enableRemoteMedia(OneToOneCommunication.MediaType.VIDEO, video);
        }
    }

    //Camera control button event
    @Override
    public void onCameraSwap() {
        if (mComm != null) {
            mComm.swapCamera();
        }
    }


    //OneToOneCommunicator listener events
    @Override
    public void onInitialized() {
        mProgressDialog.dismiss();

        if (mComm != null && mComm.isStarted()) {
            AndyUtils.appLog(LOG_TAG, "StartNotVideo");
            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
            mComm.end();
            cleanViewsAndControls();
            addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
        } else {
            AndyUtils.appLog(LOG_TAG, "StartVideo");
            addLogEvent(OpenTokConfig.LOG_ACTION_START_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
            mComm.start();
            if (mPreviewFragment != null) {
                mPreviewFragment.setEnabled(true);
            }
        }
        addLogEvent(OpenTokConfig.LOG_ACTION_START_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
    }

    //OneToOneCommunication callbacks
    @Override
    public void onError(String error) {

        // mComm.init();
        Log.d(LOG_TAG, "error connection lost" + error);
        mComm.end();
        mProgressDialog.dismiss();
        // cleanViewsAndControls(); //restart views
        if (isdisplayed == false) {
            showErrorDialog();
            // mComm.destroy();
            isdisplayed = true;
        }
    }


    @Override
    public void onQualityWarning(boolean warning) {

//        mAlert.bringToFront();
        mAlert.setVisibility(View.VISIBLE);
        mAlert.setText(getString(R.string.weak_internet));
        mAlert.postDelayed(new Runnable() {
            public void run() {
                mAlert.setVisibility(View.GONE);
            }
        }, 7000);
    }

    @Override
    public void onAudioOnly(boolean enabled) {
        if (enabled) {
            mAudioOnlyView.setVisibility(View.VISIBLE);
        } else {
            mAudioOnlyView.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        AndyUtils.appLog("Ashutosh", "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(Const.DOCTOR_NEW_REQUEST_STATUS);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
                filter);

    }

    @Override
    public void onPreviewReady(View preview) {
        mPreviewViewContainer.removeAllViews();
        if (preview != null) {
            layoutParamsPreview = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

            if (mComm.isRemote()) {
                layoutParamsPreview.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,
                        RelativeLayout.TRUE);
                layoutParamsPreview.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,
                        RelativeLayout.TRUE);
                layoutParamsPreview.width = (int) getResources().getDimension(R.dimen.preview_width);
                layoutParamsPreview.height = (int) getResources().getDimension(R.dimen.preview_height);
                layoutParamsPreview.rightMargin = (int) getResources().getDimension(R.dimen.preview_rightMargin);
                layoutParamsPreview.bottomMargin = (int) getResources().getDimension(R.dimen.preview_bottomMargin);
            }
            mPreviewViewContainer.addView(preview, layoutParamsPreview);
            if (!mComm.getLocalVideo()) {
                onDisableLocalVideo(false);
            }

        }

    }

    public void addFragment(Fragment fragment,
                            String tag) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        ft.addToBackStack(tag);

        ft.replace(R.id.textchat_fragment_container, fragment, tag);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onRemoteViewReady(View remoteView) {
        //update preview when a new participant joined to the communication
        if (mPreviewViewContainer.getChildCount() > 0) {
            onPreviewReady(mPreviewViewContainer.getChildAt(0)); //main preview view
        }
        if (!mComm.isRemote()) {
            AndyUtils.appLog(LOG_TAG, "EndRemoteView");
            //clear views
            onAudioOnly(false);
            mRemoteViewContainer.removeView(remoteView);
            mRemoteViewContainer.setClickable(false);
            mAlert.setVisibility(View.VISIBLE);
            if (!PreferenceHelper.getParam(this, Const.DOCTOR_NAME, "").equals(""))
                mAlert.setText((CharSequence) PreferenceHelper.getParam(this, Const.DOCTOR_NAME, "") + " " + getString(R.string.has_disconnected));
            mAlert.postDelayed(new Runnable() {
                public void run() {
                    mAlert.setVisibility(View.GONE);
                }
            }, 7000);
        } else {
//            if (pHelper.getAccept_time() == 0L) {
//                pHelper.putAccept_time(System.currentTimeMillis());
//            }
//
//            starttime = new PreferenceHelper(this).getAccept_time();
//            handler.postDelayed(updateTimer, 0);
//            AndyUtils.appLog(LOG_TAG, "StartRemoteView");
//            //show remote view
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                    this.getResources().getDisplayMetrics().widthPixels, this.getResources()
                    .getDisplayMetrics().heightPixels);
            mRemoteViewContainer.removeView(remoteView);
            mRemoteViewContainer.addView(remoteView, layoutParams);
            mRemoteViewContainer.setClickable(true);
        }
    }


    @Override
    public void onReconnecting() {

    }

    @Override
    public void onReconnected() {

    }

    @Override
    public void onCameraChanged(int i) {

    }

    //TextChat Fragment listener events
    private void notifyMsg(String msg) {
        NotificationManager mNotificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);

        Intent notificationIntentvideo = new Intent(this, VideoCallActivity.class);
        notificationIntentvideo.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Random generator = new Random();
        PendingIntent intent_video = PendingIntent.getActivity(this, generator.nextInt(),
                notificationIntentvideo, PendingIntent.FLAG_CANCEL_CURRENT);
        NotificationCompat.Builder nBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setVibrate(new long[]{300, 1000})
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setAutoCancel(true)
                        .setContentText(msg);

        nBuilder.build().flags |= Notification.FLAG_AUTO_CANCEL;
        nBuilder.setContentIntent(intent_video);
        mNotificationManager.notify(5, nBuilder.build());

    }
//
//    @Override
//    public void onTextChatError(String error) {
//        Log.i(LOG_TAG, "Error on text chat " + error);
//    }
//
//    @Override
//    public void onClosed() {
//        Log.i(LOG_TAG, "OnClosed text-chat");
//        mTextChatContainer.setVisibility(View.GONE);
//        count = 0;
//        mPreviewFragment.countMessages.setVisibility(View.GONE);
//        showAVCall(true);
//        restartTextChatLayout(true);
//    }
//
//    @Override
//    public void onRestarted() {
//        Log.i(LOG_TAG, "OnRestarted text-chat");
//    }

    //Private methods
    private void initPreviewFragment() {
        mPreviewFragment = new PreviewControlFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.actionbar_preview_fragment_container, mPreviewFragment).commit();
    }

    private void initRemoteFragment() {
        mRemoteFragment = new RemoteControlFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.actionbar_remote_fragment_container, mRemoteFragment).commit();
    }

    private void initCameraFragment() {
        mCameraFragment = new PreviewCameraFragment();
        getSupportFragmentManager().beginTransaction()
                .add(R.id.camera_preview_fragment_container, mCameraFragment).commit();
    }

    private void initTextChatFragment() {
        mTextChatFragment = new VideoChatFragment();
        addFragment(mTextChatFragment, "VideoChat");

    }

    //Audio local button event
    @Override
    public void onDisableLocalAudio(boolean audio) {
        if (mComm != null) {
            mComm.enableLocalMedia(OneToOneCommunication.MediaType.AUDIO, audio);
        }
    }

    //cleans views and controls
    private void cleanViewsAndControls() {
        if (mPreviewFragment != null)
            mPreviewFragment.restart();
        if (mRemoteFragment != null)
            mRemoteFragment.restart();
//        if (mTextChatFragment != null) {
//            restartTextChatLayout(true);
////            mTextChatFragment.restart();
//            mTextChatContainer.setVisibility(View.GONE);
//        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = null;

        if (mFragmentId > 0) {
            fragment = getSupportFragmentManager().findFragmentById(
                    mFragmentId);
        } else if (mFragmentTag != null
                && !mFragmentTag.equalsIgnoreCase("")) {
            fragment = getSupportFragmentManager().findFragmentByTag(
                    mFragmentTag);
        }
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

    }


    public void startActivityForResult(Intent intent, int requestCode,
                                       String fragmentTag) {


        mFragmentTag = fragmentTag;
        mFragmentId = 0;
        super.startActivityForResult(intent, requestCode);


    }


    private void showAVCall(boolean show) {
        if (show) {
            mActionBarContainer.setVisibility(View.VISIBLE);
            mPreviewViewContainer.setVisibility(View.VISIBLE);
            mRemoteViewContainer.setVisibility(View.VISIBLE);
            mCameraFragmentContainer.setVisibility(View.VISIBLE);
        } else {
            mActionBarContainer.setVisibility(View.GONE);
            mPreviewViewContainer.setVisibility(View.GONE);
            mRemoteViewContainer.setVisibility(View.GONE);
            mCameraFragmentContainer.setVisibility(View.GONE);
        }
    }

    private void restartTextChatLayout(boolean restart) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mTextChatContainer.getLayoutParams();

        if (restart) {
            //restart to the original size
            params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.height = RelativeLayout.LayoutParams.MATCH_PARENT;
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
        } else {
            //go to the minimized size
            params.height = dpToPx(40);
            params.addRule(RelativeLayout.ABOVE, R.id.actionbar_preview_fragment_container);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        }
        mTextChatContainer.setLayoutParams(params);
    }

    /**
     * Converts dp to real pixels, according to the screen density.
     *
     * @param dp A number of density-independent pixels.
     * @return The equivalent number of real pixels.
     */
    private int dpToPx(int dp) {
        double screenDensity = this.getResources().getDisplayMetrics().density;
        return (int) (screenDensity * (double) dp);
    }

    private void addLogEvent(String action, String variation) {
        if (mAnalytics != null) {
            mAnalytics.logEvent(action, variation);
        }
    }


    @Override
    public void onBackPressed() {

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode) {
            case Const.ServiceCode.GET_OPENTOK:
                Log.e("mahi", "map" + response);

                try {
                    JSONObject job1 = new JSONObject(response);
                    if (job1.getString("success").equals("true")) {
                        SESSION = job1.getString("session_id");
                        TOKEN = job1.getString("token");

                        if (!SESSION.equals("") && !TOKEN.equals("")) {
                            mComm = new OneToOneCommunication(VideoCallActivity.this, SESSION, TOKEN, OpenTokConfig.API_KEY);
                            //set listener to receive the communication events, and add UI to these events
                            mComm.setListener(this);
                            mComm.init();


                            mSession = new Session.Builder(VideoCallActivity.this, OpenTokConfig.API_KEY, SESSION).build();
                            mSession.setSignalListener(VideoCallActivity.this);
                            mSession.connect(TOKEN);
                        }


                        //init controls fragments


                        if (errorDialog != null && errorDialog.isShowing()) {
                            if (!isFinishing()) {
                                errorDialog.dismiss();
                            }
                        }


                        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
                        initCameraFragment(); //to swap camera
                        initPreviewFragment(); //to enable/disable local media
                        initRemoteFragment(); //to enable/disable remote media
//                        initTextChatFragment(); //to send/receive text-messages
                        mFragmentTransaction.commitAllowingStateLoss();


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;
        }

    }

    @Override
    public void onSignalReceived(Session session, String s, final String s1, Connection connection) {
        AndyUtils.appLog("Timer", "fromServer" + s1 + s);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                videoCallTimer.setText(s1);
            }
        });


    }
}
