package com.patient.ghealth.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.patient.ghealth.R;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;


public class ActivityCardDetails extends AppCompatActivity implements AsyncTaskCompleteListener {

    private EditText cardNumberEdit, addressEdit, cityEdit, postalCodeEdit, stateEdit, phoneEdit, cvvEdit;
    private String sCardNumber, sMonth, sCvv, sYear, sAddress, sCity, sState, sCountryCode, sPostalCode, sPhone, sCardType;
    private Button addCardButton;
    private String accessToken = "", payPalLiveUrl = "";
    private TextView noCardAdded;
    private TextView toolbarHeaderText;
    private ImageView backButton;
    private Toolbar addCardToolbar;
    private int current_year = 0, currentMonth;
    private Spinner yearSpinner, monthSpinner, countryCodeSpinner, cardTypeSpinner;
    private String[] countryCode = {"MY", "US", "C2", "SG", "ID", "VN", "JP", "TH"};
    private String[] cardType = {"visa", "mastercard", "amex", "discover", "maestro"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_details);
        try {
            accessToken = getIntent().getStringExtra(Const.Params.DATE);
            payPalLiveUrl = getIntent().getStringExtra("url");
        } catch (Exception e) {
            e.printStackTrace();
        }
        current_year = Calendar.getInstance().get(Calendar.YEAR);
        currentMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
        initView();
    }

    private void initView() {
        addCardToolbar = (Toolbar) findViewById(R.id.tb_add_card);
        setSupportActionBar(addCardToolbar);
        getSupportActionBar().setTitle(null);
        toolbarHeaderText = (TextView) findViewById(R.id.tv_add_card);
        toolbarHeaderText.setText("Credit Card Details");
        backButton = (ImageView) findViewById(R.id.iv_addCard);
        noCardAdded = (TextView) findViewById(R.id.tv_no_card_added);
        cardNumberEdit = (EditText) findViewById(R.id.et_user_card_number);
        cvvEdit = (EditText) findViewById(R.id.et_user_card_cvv);
        monthSpinner = (Spinner) findViewById(R.id.sp_expiry_month);
        yearSpinner = (Spinner) findViewById(R.id.sp_expiry_year);
        addressEdit = (EditText) findViewById(R.id.et_user_address);
        cityEdit = (EditText) findViewById(R.id.et_user_city);
        stateEdit = (EditText) findViewById(R.id.et_user_state);
        countryCodeSpinner = (Spinner) findViewById(R.id.sp_countryCode);
        cardTypeSpinner = (Spinner) findViewById(R.id.sp_cardType);
        postalCodeEdit = (EditText) findViewById(R.id.et_user_postalCode);
        phoneEdit = (EditText) findViewById(R.id.et_user_phone);
        addCardButton = (Button) findViewById(R.id.bn_addCardDetails);
        addCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValidateData()) {
                    sendDataToPayPalServer();
                }


            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(ActivityCardDetails.this, R.layout.view_spinner_layout_item, getYear());
        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearSpinner.setAdapter(yearAdapter);

        ArrayAdapter<String> countryCodeAdapter = new ArrayAdapter<String>(ActivityCardDetails.this, R.layout.view_spinner_layout_item, countryCode);
        countryCodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryCodeSpinner.setAdapter(countryCodeAdapter);

        ArrayAdapter<String> cardTypeAdapter = new ArrayAdapter<String>(ActivityCardDetails.this, R.layout.view_spinner_layout_item, cardType);
        cardTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cardTypeSpinner.setAdapter(cardTypeAdapter);

        try {
            AndyUtils.appLog("Size of month", "" + getMonth().length);
            ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(ActivityCardDetails.this, R.layout.view_spinner_layout_item, getMonth());
            monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            monthSpinner.setAdapter(monthAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String[] getYear() {
        String[] next10Year = new String[10];
        for (int i = 0; i < 10; i++) {
            next10Year[i] = String.valueOf(current_year++);
        }
        return next10Year;
    }

    private String[] getCurrentYearMonth() {
        String[] month = new String[15];
        for (int i = currentMonth, k = 0; i <= 12; i++, k++) {
            month[k] = String.valueOf(currentMonth++);
        }
        return month;
    }

    private String[] getMonth() {
        String[] month = new String[12];
        for (int i = 0; i < 12; i++) {
            month[i] = String.valueOf(i + 1);
        }
        return month;
    }


    private void getDataFromView() {
        sCardNumber = cardNumberEdit.getText().toString().trim();
        sMonth = monthSpinner.getSelectedItem().toString();
        sYear = yearSpinner.getSelectedItem().toString();
        sAddress = addressEdit.getText().toString().trim();
        sCity = cityEdit.getText().toString().trim();
        sState = stateEdit.getText().toString().trim();
        sCountryCode = countryCodeSpinner.getSelectedItem().toString();
        sCardType = cardTypeSpinner.getSelectedItem().toString();
        sPostalCode = postalCodeEdit.getText().toString().trim();
        sPhone = phoneEdit.getText().toString().trim();
        sCvv = cvvEdit.getText().toString().trim();
    }

    private boolean isValidateData() {
        getDataFromView();
        if (sCardNumber.length() == 0) {
            cardNumberEdit.requestFocus();
            Toast.makeText(this, "Please enter card number", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (sMonth.length() == 0) {
            Toast.makeText(this, "Please enter month", Toast.LENGTH_SHORT).show();
            monthSpinner.requestFocus();
            return false;
        }

        if (sYear.length() == 0) {
            Toast.makeText(this, "Please enter year", Toast.LENGTH_SHORT).show();
            yearSpinner.requestFocus();
            return false;
        }

        if (sCvv.length() == 0) {
            Toast.makeText(this, "Please enter cvv", Toast.LENGTH_SHORT).show();
            cvvEdit.requestFocus();
            return false;
        }
        if (sAddress.length() == 0) {
            Toast.makeText(this, "Please enter address", Toast.LENGTH_SHORT).show();
            addressEdit.requestFocus();
            return false;
        }
        // if all is correct then,

        if (sCity.length() == 0) {
            cityEdit.requestFocus();
            AndyUtils.showShortToast("Please enter city", this);
            return false;
        } else if (sState.length() == 0) {
            stateEdit.requestFocus();
            AndyUtils.showShortToast("Please enter state", this);
            return false;
        } else if (sCountryCode.length() == 0) {
            countryCodeSpinner.requestFocus();
            AndyUtils.showShortToast("Please enter country code", this);
            return false;
        } else if (sPostalCode.length() == 0) {
            postalCodeEdit.requestFocus();
            AndyUtils.showShortToast("Please enter postal code", this);
            return false;
        } else if (sPhone.length() == 0) {
            phoneEdit.requestFocus();
            AndyUtils.showShortToast("Please enter phone", this);
            return false;
        } else {
            return true;
        }
    }

    private void sendDataToPayPalServer() {
        AndyUtils.showSimpleProgressDialog(this, getString(R.string.please_wait), false);
        String token = "Bearer " + accessToken;
        AndyUtils.appLog("Token", token);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("number", sCardNumber);
        jsonObject.addProperty("type", sCardType);
        jsonObject.addProperty("expire_month", sMonth);
        jsonObject.addProperty("expire_year", sYear);
        jsonObject.addProperty("cvv2", sCvv);
        JsonObject addressObject = new JsonObject();
        addressObject.addProperty("line1", sAddress);
        addressObject.addProperty("city", sCity);
        addressObject.addProperty("state", sState);
        addressObject.addProperty("postal_code", sPostalCode);
        addressObject.addProperty("country_code", sCountryCode);
        addressObject.addProperty("phone", sPhone);
        jsonObject.add("billing_address", addressObject);

        AndyUtils.appLog("JsonData", jsonObject.toString());

        String payPalUrl = payPalLiveUrl + "/v1/vault/credit-cards/";
        AndyUtils.appLog("PayPAlUrl", payPalUrl);
        Ion.with(this).
                load(payPalUrl)
                .setHeader("Authorization", token)
                .setHeader("Content-Type", "application/json")
                .setJsonObjectBody(jsonObject).asJsonObject().setCallback(new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                AndyUtils.removeProgressDialog();
                if (result == null) {
                    AndyUtils.appLog("Response", "Error" + e.toString());
                    return;
                }
                try {
                    JSONObject resultObject = new JSONObject(result.toString());
                    if (resultObject.optString("state").equals("ok")) {
                        sendVaultIdToOurServer(resultObject.optString("id"), resultObject.optString("number"));
                    } else {
                        AndyUtils.showLongToast("Please enter correct card details", ActivityCardDetails.this);
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }


            }
        });
    }

    private void sendVaultIdToOurServer(String vaultId, String cardNumber) {
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.POST_SAVE_VAULT_ID_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        map.put("vault_id", vaultId);
        map.put("card_no", cardNumber);
        map.put("card_type", sCardType);
        AndyUtils.appLog("Map", map.toString());
        new HttpRequester(this, Const.POST, map, Const.ServiceCode.POST_SAVE_VAULT_ID, this);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode) {
            case Const.ServiceCode.POST_SAVE_VAULT_ID:
                AndyUtils.appLog("ResponseVaultSendServer", "Result" + response);
                try {
                    JSONObject resultObject = new JSONObject(response);
                    if (resultObject.optString("success").equals("true")) {
                        Intent intent = new Intent(ActivityCardDetails.this, AddCardActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    } else {
                        AndyUtils.showShortToast(resultObject.optString("error_message"), ActivityCardDetails.this);
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
        }
    }
}
