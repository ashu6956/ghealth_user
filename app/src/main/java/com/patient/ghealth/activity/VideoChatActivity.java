//package com.patient.ghealth.activity;
//
//import android.app.Activity;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.database.Cursor;
//import android.graphics.Bitmap;
//import android.net.Uri;
//import android.os.Bundle;
//import android.provider.MediaStore;
//import android.support.design.widget.FloatingActionButton;
//import android.support.v4.content.LocalBroadcastManager;
//import android.support.v7.app.AlertDialog;
//import android.view.View;
//import android.view.inputmethod.InputMethodManager;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.patient.ghealth.R;
//import com.patient.ghealth.adapter.ChatAdapter;
//import com.patient.ghealth.model.ChatObject;
//import com.patient.ghealth.realmDB.DatabaseHandler;
//import com.patient.ghealth.service.ImageUploadService;
//import com.patient.ghealth.service.XmppService;
//import com.patient.ghealth.utils.AndyUtils;
//import com.patient.ghealth.utils.Const;
//import com.patient.ghealth.utils.Helper;
//import com.patient.ghealth.utils.PreferenceHelper;
//
//import org.jivesoftware.smack.packet.Message;
//
//import java.util.ArrayList;
//import java.util.List;
//
///**
// * Created by user on 11/22/2016.
// */
//public class VideoChatActivity extends Activity  implements View.OnClickListener
//{
//    public TextView doctorName;
//    private ListView message_listview;
//    private ImageView videoChatClose,attachment;
//    private FloatingActionButton send;
//    private EditText messageEdit;
//    private DatabaseHandler db;
//    private String request_id="", sendermsg;
//    private MediaUploadReceiver mediaReceiver;
//    public static List<ChatObject> messages;
//    private ChatAdapter messageAdapter;
//    private BroadcastReceiver recieve_chat;
//    private static final int CAPTURE_IMAGE = 1, GET_FROM_GALLERY = 2;
//
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.chat_layout);
//        db = new DatabaseHandler(this);
//        AndyUtils.appLog("OnCreate","DoctorId"+ PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, ""));
//
//        XmppService.setupAndConnect(this,
//                Const.SERVER, "", Const.ServiceType.XMPPDRIVER
//                        + String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")), Const.XMPP_PASSWORD);
//        attachment = (ImageView)findViewById(R.id.attachment);
//        doctorName= (TextView)findViewById(R.id.chat_video_name);
//        if (!PreferenceHelper.getParam(this, Const.DOCTOR_NAME, "").equals("")) {
//            doctorName.setText((CharSequence) PreferenceHelper.getParam(this, Const.DOCTOR_NAME, ""));
//        }
//        videoChatClose= (ImageView)findViewById(R.id.iv_video_chat_close);
//        attachment.setOnClickListener(this);
//        request_id = new PreferenceHelper(this).getRequestId();
//        IntentFilter filter1 = new IntentFilter("media_upload_receiver");
//        filter1.addCategory(Intent.CATEGORY_DEFAULT);
//        mediaReceiver = new MediaUploadReceiver();
//        message_listview = (ListView)findViewById(R.id.listView_chat_messages);
//        registerReceiver(mediaReceiver, filter1);
//        AndyUtils.appLog("VideoChatFragment", "RequestId" + request_id);
//        messages = db.get_Chat("" + request_id);
//        if (messages != null && messages.size() > 0) {
//            AndyUtils.appLog("Ashutosh", messages.size() + "");
//            messageAdapter = new ChatAdapter(this, R.layout.chat_adapter, messages);
//            message_listview.setAdapter(messageAdapter);
//        }
//
//        send = (FloatingActionButton)findViewById(R.id.send);
//        send.setOnClickListener(this);
//        messageEdit = (EditText)findViewById(R.id.et_message);
//
//        recieve_chat = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//
//                String message = intent.getStringExtra("message");
//                String data_type = intent.getStringExtra("data_type");
//                showChat("recieve", data_type, message);
//
//            }
//        };
//
//        LocalBroadcastManager.getInstance(this).registerReceiver(recieve_chat,
//                new IntentFilter("message_recieved"));
//
//        videoChatClose.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                AndyUtils.appLog("Close", "Attempt");
////
////                try {
////                    InputMethodManager e = (InputMethodManager)this.getContext().getSystemService("input_method");
////                    e.hideSoftInputFromWindow(VideoChatFragment.this.messageEdit.getWindowToken(), 0);
////                } catch (Exception var3) {
////                    throw var3;
////                }
////
////                VideoChatFragment.this.close();
//            }
//        });
//
//
//    }
//
//
//    public void close()
//    {
//        this.close();
//    }
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.send:
//                sendermsg = messageEdit.getText().toString();
//
//                if (messageEdit.getText().toString().length() == 0) {
//
//                    Toast.makeText(this,
//                            "Enter text before sending", Toast.LENGTH_LONG).show();
//
//                } else {
//
//                    AndyUtils.appLog("DoctorId",String.valueOf(PreferenceHelper.getParam(this,Const.Params.DOCTOR_ID, "")));
//                    XmppService.sendMessage(this,
//                            Const.ServiceType.XMPPUSER + String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, ""))
//                                    + Const.SUFFIX_CHAT,
//                            Message.Type.chat, AndyUtils.message_json("0", Const.Params.TEXT, String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, "")),
//                                    String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")), "" + new PreferenceHelper(this).getRequestId(), sendermsg));
//
//                    messageEdit.setText("");
//
//                    showChat("sent", Const.Params.TEXT, sendermsg);
//                }
//                break;
//            case R.id.attachment:
//                showPictureDialog();
//                break;
//
//        }
//    }
//
//
//    private void showChat(String type, String data_type, String message) {
//
//        if (messages == null || messages.size() == 0) {
//
//            messages = new ArrayList<ChatObject>();
//        }
//
//        AndyUtils.appLog("MessageActivity", "InsertChat" + String.valueOf(PreferenceHelper.getParam(this,Const.Params.DOCTOR_ID,"")));
//
//        messages.add(new ChatObject(message, type, data_type, ""
//                + request_id, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")), String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, ""))));
//
//        ChatAdapter chatAdabter = new ChatAdapter(this, R.layout.chat_adapter, messages);
//
//        message_listview.setAdapter(chatAdabter);
//        // chatAdabter.notifyDataSetChanged();
//        if (type.equals("sent")) {
//            AndyUtils.appLog("MessageActivity", "InsertChat" + request_id);
//            ChatObject chat = new ChatObject(message, type, data_type, ""
//                    + request_id, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")), String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, "")));
//
//            db.insertChat(chat);
//        }
//        // chatAdabter.notifyDataSetChanged();
//
//    }
//
//
//    private void showPictureDialog() {
//        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
//        dialog.setTitle("Select Option");
//        String[] items = {"Gallery", "Camera"};
//
//        dialog.setItems(items, new DialogInterface.OnClickListener() {
//
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                // TODO Auto-generated method stub
//                switch (which) {
//                    case 0:
//                        openGallery();
//                        break;
//                    case 1:
//                        openCamera();
//                        break;
//
//
//                }
//            }
//        });
//        dialog.show();
//    }
//
//    private void openGallery() {
//
//        Intent intent = new Intent(
//                Intent.ACTION_PICK,
//                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//        startActivityForResult(intent, GET_FROM_GALLERY);
//    }
//
//    public void openCamera() {
//
//        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        startActivityForResult(cameraIntent, CAPTURE_IMAGE);
//    }
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        super.onActivityResult(requestCode, resultCode, data);
//
//        Intent intent;
//        if (resultCode == RESULT_OK) {
//            switch (requestCode) {
//
//                case CAPTURE_IMAGE:
//
//                    Bitmap photo = (Bitmap) data.getExtras().get("data");
//                    Uri selectedImageUri = Helper.getImageUri(this, photo);
//                    String selectedImagePath = Helper.getRealPathFromURI(selectedImageUri, this);
//                    sendermsg = selectedImagePath;
//                    Intent captureUploadService = new Intent(this, ImageUploadService.class);
//                    captureUploadService.putExtra("image_path", selectedImagePath);
//                    captureUploadService.putExtra("request_id", request_id);
//                    startService(captureUploadService);
//                    break;
//
//
//                case GET_FROM_GALLERY:
//                    Uri selectedImage = data.getData();
//                    String[] filePath = {MediaStore.MediaColumns.DATA};
//                    Cursor c = getContentResolver().query(selectedImage, filePath,
//                            null, null, null);
//                    c.moveToFirst();
//                    int columnIndex = c.getColumnIndex(filePath[0]);
//                    String picturePath = c.getString(columnIndex);
//                    c.close();
//                    picturePath = Helper.compressImage(this, picturePath);
//                    sendermsg = picturePath;
//                    Intent galleryUploadService = new Intent(this, ImageUploadService.class);
//                    galleryUploadService.putExtra("image_path", picturePath);
//                    galleryUploadService.putExtra("request_id", request_id);
//                    startService(galleryUploadService);
//                    break;
//                default:
//                    break;
//            }
//        }
//
//
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
////        IntentFilter filter = new IntentFilter(Const.DOCTOR_NEW_REQUEST_STATUS);
////        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver,
////                filter);
//    }
//
//
//    @Override
//    public void onPause() {
//        super.onPause();
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        unregisterReceiver(mediaReceiver);
//
//    }
//
//
//
//    private void parseMediaAndSendMessage(String response) {
//
//        sendermsg = response;
//
//        AndyUtils.appLog("DoctorId",String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, "")));
//        XmppService.sendMessage(this,
//                Const.ServiceType.XMPPUSER + String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, ""))
//                        + Const.SUFFIX_CHAT,
//                Message.Type.chat, AndyUtils.message_json("0", Const.Params.IMAGE, String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, "")),
//                        String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")), "" + new PreferenceHelper(this).getRequestId(), sendermsg));
//
//
//        showChat("sent", Const.Params.IMAGE, sendermsg);
//
//    }
//
//    class MediaUploadReceiver extends BroadcastReceiver {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            String response = intent.getStringExtra("response");
//
//            parseMediaAndSendMessage(response);
//        }
//
//    }
//
//}
