package com.patient.ghealth.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.braintreepayments.api.BraintreePaymentActivity;
import com.braintreepayments.api.PaymentRequest;
import com.braintreepayments.api.models.PaymentMethodNonce;
import com.patient.ghealth.R;
import com.patient.ghealth.adapter.GetCardsAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.CardDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;
import com.patient.ghealth.utils.RecyclerLongPressClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AddCardActivity extends AppCompatActivity implements AsyncTaskCompleteListener {


    private static final int REQUEST_CODE = 133;
    private FloatingActionButton addCardButton;
    private RecyclerView cardRecyclerView;
    private TextView noCardAdded;
    private TextView toolbarHeaderText;
    private ImageView backButton;
    private Toolbar addCardToolbar;
    private List<CardDetails> cardDetailsList;
    private GetCardsAdapter cardAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card_layout);
        addCardButton = (FloatingActionButton) findViewById(R.id.bn_add_card);
        cardRecyclerView = (RecyclerView) findViewById(R.id.rv_add_card);
        addCardToolbar = (Toolbar) findViewById(R.id.tb_add_card);
        setSupportActionBar(addCardToolbar);
        toolbarHeaderText = (TextView) findViewById(R.id.tv_add_card);
        toolbarHeaderText.setText(getString(R.string.add_card));
        backButton = (ImageView) findViewById(R.id.iv_addCard);
        noCardAdded = (TextView) findViewById(R.id.tv_no_card_added);
        getAddedCard();
        addCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBrainTreeClientToken();
            }
        });
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        cardRecyclerView.addOnItemTouchListener(
                new RecyclerLongPressClickListener(this, cardRecyclerView, new RecyclerLongPressClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {


                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        deleteCardLogoutDialog(cardDetailsList.get(position).getCardId());
                    }


                })
        );
    }

    private void deleteCardLogoutDialog(final String cardId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String message = getString(R.string.are_you_sure_delete_this_card);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteCard(cardId);

            }
        });

        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    private void deleteCard(String cId) {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        AndyUtils.showSimpleProgressDialog(this, getString(R.string.please_wait), false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.POST_DELETE_CARD_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        map.put("card_id", String.valueOf(cId));
        AndyUtils.appLog("Ashutosh", "deleteCardMap" + map);

        new HttpRequester(this, Const.POST, map, Const.ServiceCode.POST_DELETE_CARD, this);


    }

    private void getAddedCard() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        AndyUtils.showSimpleProgressDialog(this, getString(R.string.please_wait), false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.GET_ADDED_CARDS_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "GetAddedCardMap" + map);

        new HttpRequester(this, Const.GET, map, Const.ServiceCode.GET_ADDED_CARDS, this);


    }


    private void getBrainTreeClientToken() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        AndyUtils.showSimpleProgressDialog(this, getString(R.string.please_wait), false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.GET_PAYPAL_ACCESS_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "PayPalAccessTokenMap" + map);

        new HttpRequester(this, Const.POST, map, Const.ServiceCode.GET_PAYPAL_ACCESS, this);


    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        AndyUtils.removeProgressDialog();
        switch (serviceCode) {
            case Const.ServiceCode.GET_PAYPAL_ACCESS:
                AndyUtils.appLog("Ashutosh", "BrainTreeClientTokenResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        String clientToken = jsonObject.optString("access_token");
                        String url = jsonObject.optString("url");
//                        PaymentRequest paymentRequest = new PaymentRequest()
//                                .clientToken(clientToken)
//                                .submitButtonText("Add");
//                        startActivityForResult(paymentRequest.getIntent(this), REQUEST_CODE);
                        if (!TextUtils.isEmpty(url)) {
                            Intent intent = new Intent(this, ActivityCardDetails.class);
                            intent.putExtra(Const.Params.DATE, clientToken);
                            intent.putExtra("url", url);
                            startActivity(intent);
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CREATE_ADD_CARD:
                AndyUtils.appLog("Ashutosh", "BrainTreeClientTokenResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {

                        AndyUtils.showShortToast(getString(R.string.card_added), this);
                        getAddedCard();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_ADDED_CARDS:
                AndyUtils.appLog("Ashutosh", "GetAddedCardResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("cards");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            cardDetailsList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject cardObject = jsonArray.getJSONObject(i);
                                CardDetails cardDetails = new CardDetails();
                                cardDetails.setCardId(cardObject.optString("id"));
                                cardDetails.setCardNumber(cardObject.optString("last_four"));
                                cardDetails.setIsDefault(cardObject.optString("is_default"));
                                cardDetails.setType(cardObject.optString("card_type"));
                                cardDetailsList.add(cardDetails);
                            }
                            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
                            cardRecyclerView.setVisibility(View.VISIBLE);
                            cardRecyclerView.setLayoutManager(layoutManager);
                            cardAdapter = new GetCardsAdapter(this, cardDetailsList);
                            cardRecyclerView.setAdapter(cardAdapter);
                            noCardAdded.setVisibility(View.GONE);
                            cardAdapter.notifyDataSetChanged();
                        } else {
                            if (cardAdapter != null) {
                                cardAdapter.notifyDataSetChanged();
                            }
                            noCardAdded.setVisibility(View.VISIBLE);
                            cardRecyclerView.setVisibility(View.GONE);
                        }
                    } else {
                        if (cardDetailsList != null && cardDetailsList.size() > 0 && null != cardAdapter) {
                            cardDetailsList.clear();
                            cardAdapter.notifyDataSetChanged();
                        }
                        noCardAdded.setVisibility(View.VISIBLE);
                        AndyUtils.showShortToast(jsonObject.getString("error_message"), this);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case Const.ServiceCode.POST_DELETE_CARD:
                AndyUtils.removeProgressDialog();
                AndyUtils.appLog("Ashutosh", "deleteCardResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        AndyUtils.showShortToast(getString(R.string.card_deleted), AddCardActivity.this);
                        getAddedCard();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentMethodNonce paymentMethodNonce = data.getParcelableExtra(
                        BraintreePaymentActivity.EXTRA_PAYMENT_METHOD_NONCE);
                String nonce = paymentMethodNonce.getNonce();
                // Send the nonce to your server.
                postNonceToServer(nonce);
            }
        }
    }

    void postNonceToServer(String nonce) {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        AndyUtils.showSimpleProgressDialog(this, getString(R.string.please_wait), false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_ADD_CARD_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.PAYMENT_METHOD_NONCE, nonce);

        AndyUtils.appLog("Ashutosh", "BrainTreeADDCARDMap" + map);

        new HttpRequester(this, Const.POST, map, Const.ServiceCode.CREATE_ADD_CARD, this);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
