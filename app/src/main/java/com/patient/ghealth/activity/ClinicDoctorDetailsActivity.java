package com.patient.ghealth.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.adapter.AboutDoctorPagerAdapter;
import com.patient.ghealth.adapter.ClinicDoctorDetailsAdapter;
import com.patient.ghealth.adapter.RecyclerViewItemClickListener;
import com.patient.ghealth.adapter.VerticalSpaceItemDecoration;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class ClinicDoctorDetailsActivity extends AppCompatActivity implements AsyncTaskCompleteListener
{
    private ViewPager clinicViewPager;
    private TextView clinicName,clinicAddress,noDoctorText,headerText;
    private RecyclerView clinicRecyclerView;
    private ProgressBar clinicProgressBar;
    private CircleIndicator circleIndicator;
    private DoctorOnLineDetails onLineDetails;
    private Bundle bundle;
    private String type="";
    private String clinicId="";
    private  ClinicDoctorDetailsAdapter adapter;
    private Toolbar clinicToolbar;
    private ImageView backButton;
    DecimalFormat form = new DecimalFormat("0.00");

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("FragmentRequest", "onReceive");
            String notification = intent.getStringExtra("ashutosh");
            Log.d("MyOnDemand notification", notification);
            String message = intent.getStringExtra(Const.DOCTOR_INTENT_MESSAGE);
            try {
                JSONObject messageObj = new JSONObject(message);
                AndyUtils.appLog("JsonResponse", messageObj.toString());
                if (messageObj.getString("success").equals("true")) {
                    if (Integer.parseInt(messageObj.getString("status")) == 9) {
                        if(adapter!=null)
                        {
                            adapter.removeProgressDialog();
                        }
                        AndyUtils.showLongToast(messageObj.getString("message"), ClinicDoctorDetailsActivity.this);
                    } else {

                        if(adapter!=null)
                        {
                            adapter.removeProgressDialog();
                        }
                        JSONObject jsonObject = messageObj.getJSONObject("data");
                        DoctorOnLineDetails onLineDetails = new DoctorOnLineDetails();
                        onLineDetails.setDoctorOnLineId(jsonObject.getString("doctor_id"));
                        onLineDetails.setDoctorPictureUrl(jsonObject.getString("doctor_picture"));
                        onLineDetails.setDoctorRating(jsonObject.getString("doctor_rating"));
                        onLineDetails.setDoctorName(jsonObject.getString("doctor_name"));
                        onLineDetails.setDoctorStatus(messageObj.getString("status"));
                        onLineDetails.setRequestId(jsonObject.getString("request_id"));
                        onLineDetails.setLatitude(jsonObject.getString("s_latitude"));
                        onLineDetails.setLongitude(jsonObject.getString("s_longitude"));
                        onLineDetails.setdLatitude(jsonObject.getString("d_latitude"));
                        onLineDetails.setdLongitude(jsonObject.getString("d_longitude"));
                        onLineDetails.setDoctorStatusTitle(jsonObject.optString("title"));
                        onLineDetails.setBetweenDistance(jsonObject.optString("eta"));
                        onLineDetails.setEtaTime(jsonObject.optString("time"));
                        onLineDetails.setClinicAddress(jsonObject.optString("c_address"));
                        onLineDetails.setSourceAddress(jsonObject.optString(Const.SOURCE_ADDRESS));
                        Intent onDemandIntent = new Intent(ClinicDoctorDetailsActivity.this, DoctorOnDemandDoctorActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(Const.DoctorOnDemandFragment, onLineDetails);
                        bundle.putString(Const.DOCTOR_REQUEST_STATUS, Const.REQUEST);
                        onDemandIntent.putExtras(bundle);
                        startActivity(onDemandIntent);
                        finish();
                    }
                } else {
                    AndyUtils.showLongToast(messageObj.getString("error_message"), ClinicDoctorDetailsActivity.this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_doctor_details_layout);
        clinicToolbar= (Toolbar) findViewById(R.id.tb_clinic);
        setSupportActionBar(clinicToolbar);
        getSupportActionBar().setTitle("");
        headerText= (TextView) findViewById(R.id.tb_clinic_header_text);
        headerText.setText(getString(R.string.clinic_details));
        backButton= (ImageView) findViewById(R.id.iv_clinic_back_icon);
        clinicViewPager= (ViewPager) findViewById(R.id.vp_clinic_images);
        clinicRecyclerView= (RecyclerView) findViewById(R.id.rv_doctor_details);
        clinicName= (TextView) findViewById(R.id.tv_clinic_name);
        clinicAddress= (TextView) findViewById(R.id.tv_clinic_address);
        clinicProgressBar= (ProgressBar) findViewById(R.id.clinic_progress);
        noDoctorText= (TextView) findViewById(R.id.tv_no_doc);
        circleIndicator= (CircleIndicator) findViewById(R.id.ci_about_clinic);
        bundle=getIntent().getExtras();
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if(bundle!=null)
        {
            onLineDetails= (DoctorOnLineDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
            type=bundle.getString(Const.DOCTOR_TYPE);
            List<String> imageList = new ArrayList<String>();
            if (!onLineDetails.getDoctor_clinic_image_first().equals("")) {
                imageList.add(onLineDetails.getDoctor_clinic_image_first());
            }

            if (!onLineDetails.getDoctor_clinic_image_second().equals("")) {
                imageList.add(onLineDetails.getDoctor_clinic_image_second());
            }
            if (!onLineDetails.getDoctor_clinic_image_third().equals("")) {
                imageList.add(onLineDetails.getDoctor_clinic_image_third());
            }
            AboutDoctorPagerAdapter aboutDoctorPagerAdapter = new AboutDoctorPagerAdapter(this, imageList);
            clinicViewPager.setAdapter(aboutDoctorPagerAdapter);
            circleIndicator.setViewPager(clinicViewPager);
        }
        clinicName.setText(onLineDetails.getDoctorClinicName());
        clinicAddress.setText(onLineDetails.getClinicAddress());
        clinicId= (String) PreferenceHelper.getParam(this,Const.CLINIC_ID,"");
        String doctorSpecialistsId = (String) PreferenceHelper.getParam(this, Const.SPECIALITIES_DOCTOR_ID, "");
        AndyUtils.appLog("ClinicDoctorDetails","Soecilist Id" +doctorSpecialistsId);
        if(type.equals(Const.ONLINE_CONSULT) && !doctorSpecialistsId.equals("")) {
            getDoctorOnLineDetails(clinicId,doctorSpecialistsId);
        }
        else if(type.equals(Const.BOOKING) && !doctorSpecialistsId.equals(""))
        {
            getDoctorBookingDetails(clinicId,doctorSpecialistsId);
        }
        else if(type.equals(Const.ONDEMAND) && !doctorSpecialistsId.equals(""))
        {
            getDoctorOnDemandDetails(clinicId,doctorSpecialistsId);
        }

        clinicRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(this, new RecyclerViewItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {


            }
        }));


    }


    @Override
    public void onBackPressed() {
//        Intent intent=new Intent(this,MainActivity.class);
//        startActivity(intent);
//        finish();
        super.onBackPressed();
    }

    private void getDoctorOnLineDetails(String cId,String spcId)
    {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        clinicProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CLINIC_DOCTOR_ONLINE_DETAILS_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.CLINIC_ID + "="
                + String.valueOf(cId) + "&" + Const.Params.SPECIALITY_ID + "="
                + String.valueOf(spcId));

        AndyUtils.appLog("Ashutosh", "ClinicNearByOnlineMap" + map);

        new HttpRequester(this, Const.GET, map, Const.ServiceCode.GET_CLINIC_DOCTOR_ONLINE, this);
    }


    private void getDoctorBookingDetails(String cId,String spcId)
    {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        clinicProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CLINIC_DOCTOR_BOOKING_DETAILS_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, "")  + "&" + Const.Params.CLINIC_ID + "="
                + String.valueOf(cId) + "&" + Const.Params.SPECIALITY_ID + "="
                + String.valueOf(spcId));

        AndyUtils.appLog("Ashutosh", "ClinicNearByBookingMap" + map);

        new HttpRequester(this, Const.GET, map, Const.ServiceCode.GET_CLINIC_DOCTOR_BOOKING, this);
    }

    private void getDoctorOnDemandDetails(String cId,String spcId)
    {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        clinicProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CLINIC_DOCTOR_ONDEMAND_DETAILS_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.CLINIC_ID + "="
                + String.valueOf(cId)+ "&" + Const.Params.SPECIALITY_ID + "="
                + String.valueOf(spcId));

        AndyUtils.appLog("Ashutosh", "ClinicOnDemandMap" + map);

        new HttpRequester(this, Const.GET, map, Const.ServiceCode.GET_CLINIC_DOCTOR_ONDEMAND, this);
    }


    @Override
    public void onTaskCompleted(String response, int serviceCode)
    {
        clinicProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject;
        switch (serviceCode)
        {
            case Const.ServiceCode.GET_CLINIC_DOCTOR_ONLINE:
                AndyUtils.appLog("Ashutosh","OnLineDetailsResponse" +response);
                try {

                    List<DoctorOnLineDetails> doctorOnLineDetailsList=new ArrayList<>();
                    jsonObject=new JSONObject(response);
                    if(jsonObject.getString("success").equals("true"))
                    {
                        JSONArray jsonArray=jsonObject.optJSONArray("doctor_list");
                        if(jsonArray!=null && jsonArray.length()>0)
                        {
                           for(int i=0;i<jsonArray.length();i++)
                           {
                               JSONObject onLineJsonObject=jsonArray.optJSONObject(i);
                               DoctorOnLineDetails doctorOnLineDetails=new DoctorOnLineDetails();
                               doctorOnLineDetails.setDoctorOnLineId(onLineJsonObject.optString("id"));
                               doctorOnLineDetails.setDoctorName(onLineJsonObject.optString("d_name"));
                               doctorOnLineDetails.setDoctorPictureUrl(onLineJsonObject.optString("d_photo"));
                               doctorOnLineDetails.setDoctorChatConsultFee(onLineJsonObject.optString("c_fee"));
                               doctorOnLineDetails.setDoctorPhoneConsultFee(onLineJsonObject.optString("p_fee"));
                               doctorOnLineDetails.setDoctorVideoConsultFee(onLineJsonObject.optString("v_fee"));
                               doctorOnLineDetails.setDoctorChatConsult(onLineJsonObject.optString("c_consult"));
                               doctorOnLineDetails.setDoctorVideoConsult(onLineJsonObject.optString("v_consult"));
                               doctorOnLineDetails.setDoctorPhoneConsult(onLineJsonObject.optString("p_consult"));
                               doctorOnLineDetails.setDoctorRating(onLineJsonObject.optString("rating"));
                               doctorOnLineDetails.setDoctorHelpedPeople(onLineJsonObject.optString("helped"));
                               doctorOnLineDetails.setDoctorExperience(onLineJsonObject.optString("experience"));
                               doctorOnLineDetails.setdLatitude(onLineJsonObject.optString("d_latitude"));
                               doctorOnLineDetails.setdLongitude(onLineJsonObject.optString("d_longitude"));
                               doctorOnLineDetails.setDoctor_clinic_image_first(onLineJsonObject.optString("c_pic1"));
                               doctorOnLineDetails.setDoctor_clinic_image_second(onLineJsonObject.optString("c_pic2"));
                               doctorOnLineDetails.setDoctor_clinic_image_third(onLineJsonObject.optString("c_pic3"));
                               doctorOnLineDetails.setDegree1(onLineJsonObject.optString("degree1"));
                               doctorOnLineDetails.setEducation1(onLineJsonObject.optString("univ1"));
                               doctorOnLineDetails.setDegree2(onLineJsonObject.optString("degree2"));
                               doctorOnLineDetails.setEducation2(onLineJsonObject.optString("univ2"));
                               doctorOnLineDetails.setClinicAddress(onLineJsonObject.optString("c_address"));
                               doctorOnLineDetails.setDoctorClinicName(onLineJsonObject.optString("c_name"));
                               doctorOnLineDetails.setFreeCharge(onLineJsonObject.optString("free_minute"));
                               doctorOnLineDetailsList.add(doctorOnLineDetails);
                           }
                            noDoctorText.setVisibility(View.GONE);
                            adapter=new ClinicDoctorDetailsAdapter(this,doctorOnLineDetailsList,type,ClinicDoctorDetailsActivity.this);
                            clinicRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(15));
                            LinearLayoutManager layoutManager=new LinearLayoutManager(this);
                            clinicRecyclerView.setLayoutManager(layoutManager);
                            clinicRecyclerView.setAdapter(adapter);
                        }
                        else
                        {
                            noDoctorText.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_CLINIC_DOCTOR_ONDEMAND:
                AndyUtils.appLog("Ashutosh","OnDemandResponse" +response);
                try {

                    List<DoctorOnLineDetails> doctorOnLineDetailsList=new ArrayList<>();
                    jsonObject=new JSONObject(response);
                    if(jsonObject.getString("success").equals("true"))
                    {
                        JSONArray jsonArray=jsonObject.optJSONArray("doctor_list");
                        if(jsonArray!=null && jsonArray.length()>0)
                        {
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject onLineJsonObject=jsonArray.optJSONObject(i);
                                DoctorOnLineDetails doctorOnLineDetails=new DoctorOnLineDetails();
                                doctorOnLineDetails.setDoctorOnLineId(onLineJsonObject.optString("id"));
                                doctorOnLineDetails.setDoctorName(onLineJsonObject.optString("d_name"));
                                doctorOnLineDetails.setDoctorPictureUrl(onLineJsonObject.optString("d_photo"));
                                doctorOnLineDetails.setDoctorChatConsultFee(onLineJsonObject.optString("ondemand_fee"));
                                doctorOnLineDetails.setDoctorRating(onLineJsonObject.optString("rating"));
                                doctorOnLineDetails.setDoctorHelpedPeople(onLineJsonObject.optString("helped"));
                                doctorOnLineDetails.setDoctorExperience(onLineJsonObject.optString("experience"));
                                doctorOnLineDetails.setdLatitude(onLineJsonObject.optString("d_latitude"));
                                doctorOnLineDetails.setdLongitude(onLineJsonObject.optString("d_longitude"));
                                doctorOnLineDetails.setDoctor_clinic_image_first(onLineJsonObject.optString("c_pic1"));
                                doctorOnLineDetails.setDoctor_clinic_image_second(onLineJsonObject.optString("c_pic2"));
                                doctorOnLineDetails.setDoctor_clinic_image_third(onLineJsonObject.optString("c_pic3"));
                                doctorOnLineDetails.setDegree1(onLineJsonObject.optString("degree1"));
                                doctorOnLineDetails.setEducation1(onLineJsonObject.optString("univ1"));
                                doctorOnLineDetails.setDegree2(onLineJsonObject.optString("degree2"));
                                doctorOnLineDetails.setEducation2(onLineJsonObject.optString("univ2"));
                                doctorOnLineDetails.setClinicAddress(onLineJsonObject.optString("c_address"));
                                doctorOnLineDetails.setDoctorClinicName(onLineJsonObject.optString("c_name"));
                                doctorOnLineDetailsList.add(doctorOnLineDetails);
                            }
                            noDoctorText.setVisibility(View.GONE);
                             adapter=new ClinicDoctorDetailsAdapter(this,doctorOnLineDetailsList,type,this);
                            clinicRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(15));
                            LinearLayoutManager layoutManager=new LinearLayoutManager(this);
                            clinicRecyclerView.setLayoutManager(layoutManager);
                            clinicRecyclerView.setAdapter(adapter);
                        }
                        else
                        {
                            noDoctorText.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_CLINIC_DOCTOR_BOOKING:
                AndyUtils.appLog("Ashutosh","OnDemandResponse" +response);
                try {

                    List<DoctorOnLineDetails> doctorOnLineDetailsList=new ArrayList<>();
                    jsonObject=new JSONObject(response);
                    if(jsonObject.getString("success").equals("true"))
                    {
                        JSONArray jsonArray=jsonObject.optJSONArray("doctor_list");
                        if(jsonArray!=null && jsonArray.length()>0)
                        {
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject onLineJsonObject=jsonArray.optJSONObject(i);
                                DoctorOnLineDetails doctorOnLineDetails=new DoctorOnLineDetails();
                                doctorOnLineDetails.setDoctorOnLineId(onLineJsonObject.optString("id"));
                                doctorOnLineDetails.setDoctorName(onLineJsonObject.optString("d_name"));
                                doctorOnLineDetails.setDoctorPictureUrl(onLineJsonObject.optString("d_photo"));
                                doctorOnLineDetails.setDoctorChatConsultFee(onLineJsonObject.optString("appointment_fee"));
                                doctorOnLineDetails.setDoctorRating(onLineJsonObject.optString("rating"));
                                doctorOnLineDetails.setDoctorHelpedPeople(onLineJsonObject.optString("helped"));
                                doctorOnLineDetails.setDoctorExperience(onLineJsonObject.optString("experience"));
                                doctorOnLineDetails.setdLatitude(onLineJsonObject.optString("d_latitude"));
                                doctorOnLineDetails.setdLongitude(onLineJsonObject.optString("d_longitude"));
                                doctorOnLineDetails.setDoctor_clinic_image_first(onLineJsonObject.optString("c_pic1"));
                                doctorOnLineDetails.setDoctor_clinic_image_second(onLineJsonObject.optString("c_pic2"));
                                doctorOnLineDetails.setDoctor_clinic_image_third(onLineJsonObject.optString("c_pic3"));
                                doctorOnLineDetails.setDegree1(onLineJsonObject.optString("degree1"));
                                doctorOnLineDetails.setEducation1(onLineJsonObject.optString("univ1"));
                                doctorOnLineDetails.setDegree2(onLineJsonObject.optString("degree2"));
                                doctorOnLineDetails.setEducation2(onLineJsonObject.optString("univ2"));
                                doctorOnLineDetails.setClinicAddress(onLineJsonObject.optString("c_address"));
                                doctorOnLineDetails.setDoctorClinicName(onLineJsonObject.optString("c_name"));
                                doctorOnLineDetailsList.add(doctorOnLineDetails);

                            }
                            noDoctorText.setVisibility(View.GONE);
                            adapter=new ClinicDoctorDetailsAdapter(this,doctorOnLineDetailsList,type,this);
                            clinicRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(15));
                            LinearLayoutManager layoutManager=new LinearLayoutManager(this);
                            clinicRecyclerView.setLayoutManager(layoutManager);
                            clinicRecyclerView.setAdapter(adapter);
                        }
                        else
                        {
                            noDoctorText.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


        }

    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(Const.DOCTOR_REQUEST_STATUS);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
                filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

}



