package com.patient.ghealth.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.adapter.HomeAdapter;
import com.patient.ghealth.adapter.SearchAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.DoctorSpecialities;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;
import com.twitter.sdk.android.core.models.Search;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by getit on 8/11/2016.
 */
public class SearchActivity extends AppCompatActivity implements AsyncTaskCompleteListener {
    private Toolbar searchToolbar;
    private ListView searchListView;
    private TextView toolbarHeaderText,noSearchText;
    private ImageView searchBackIcon;
    private EditText searcheditText;
    private ProgressBar searchProgressBar;
    private List<DoctorSpecialities> doctorSpecialitiesList;
    private SearchAdapter searchAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_layout);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        searchToolbar = (Toolbar) findViewById(R.id.tb_search);
        setSupportActionBar(searchToolbar);
        getSupportActionBar().setTitle("");
        searchListView = (ListView) findViewById(R.id.lv_search);
        toolbarHeaderText = (TextView) findViewById(R.id.tb_search_header_text);
        toolbarHeaderText.setText(getString(R.string.search));
        searcheditText = (EditText) findViewById(R.id.et_search_text);
        searchBackIcon = (ImageView) findViewById(R.id.iv_search_back_icon);
        searchProgressBar= (ProgressBar) findViewById(R.id.search_progress);
        noSearchText= (TextView) findViewById(R.id.no_search_speciality_text);
        getSerachDetails();
        searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                String specialist_DoctorId = doctorSpecialitiesList.get(position).getDoctorId();

                AndyUtils.appLog("HomeFragment", "DoctorSpecialistID" + specialist_DoctorId);
                PreferenceHelper.setParam(SearchActivity.this, Const.SPECIALITIES_DOCTOR_ID, specialist_DoctorId);
                Intent intent=new Intent(SearchActivity.this,MainActivity.class);
                Bundle bundle=new Bundle();
                bundle.putString(Const.INCOMING_BUNDLE,Const.SearchFragment);
                bundle.putString(Const.SpecialitiesName,doctorSpecialitiesList.get(position).getDoctorSpeciality());
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
        });

        searchBackIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        searcheditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = searcheditText.getText().toString().toLowerCase(Locale.getDefault());
                if(searchAdapter!=null) {
                    searchAdapter.filter(text);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    private void  getSerachDetails()
    {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        //   AndyUtils.showSimpleProgressDialog(getActivity(), "fetching specialities...", false);
        searchProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.GET_SPECIALITIES_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Session Token", (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "GetSearchMap" + map);
        new HttpRequester(this, Const.GET, map, Const.ServiceCode.GET_SPECIALITIES, this);
    }

    @Override
    public void onBackPressed()
    {

        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_left);
        Intent intent=new Intent(SearchActivity.this,MainActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode)
    {
        searchProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_SPECIALITIES:

                AndyUtils.appLog("Ashutosh", "GetSearchResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    doctorSpecialitiesList = new ArrayList<DoctorSpecialities>();
                    if (jsonObject.getString("success").equals("true")) {
                        JSONArray jsonArray = jsonObject.optJSONArray("speciality");

                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject specialitiesJsonObject = jsonArray.getJSONObject(i);
                                DoctorSpecialities doctorSpecialities = new DoctorSpecialities();
                                doctorSpecialities.setDoctorId(specialitiesJsonObject.getString("id"));
                                doctorSpecialities.setDoctorSpeciality(specialitiesJsonObject.getString("speciality"));
                                doctorSpecialitiesList.add(doctorSpecialities);
                            }
                            searchAdapter = new SearchAdapter(this,doctorSpecialitiesList);
                            searchListView.setAdapter(searchAdapter);
                            noSearchText.setVisibility(View.GONE);

                        } else
                            noSearchText.setVisibility(View.VISIBLE);

                    } else {
                        noSearchText.setVisibility(View.VISIBLE);
                        if (jsonObject.optString("error_message").equalsIgnoreCase(Const.INVALID_TOKEN)) {
                            AndyUtils.showShortToast(getString(R.string.you_have_logged), this);
                            PreferenceHelper.setParam(this, Const.LOGIN_FIRST_TIME, false);
                            Intent homeIntent = new Intent(this, LoginActivity.class);
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(homeIntent);
                            finish();
                        }
                        else
                        {
//                            AndyUtils.showShortToast(jsonObject.optString("error_message"), this);
                        }
                    }
                } catch (JSONException e) {
                    //   AndyUtils.removeProgressDialog();
                    e.printStackTrace();
                }

        }

    }
}
