//package com.patient.ghealth.activity;
//
//import android.app.Dialog;
//import android.app.NotificationManager;
//import android.content.BroadcastReceiver;
//import android.content.Context;
//import android.content.Intent;
//import android.content.IntentFilter;
//import android.content.SharedPreferences;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.SystemClock;
//import android.provider.Settings;
//import android.support.v4.app.FragmentTransaction;
//import android.support.v4.app.NotificationCompat;
//import android.support.v4.content.LocalBroadcastManager;
//import android.support.v7.app.AppCompatActivity;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//import android.widget.FrameLayout;
//import android.widget.RelativeLayout;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.patient.ghealth.R;
//import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
//import com.patient.ghealth.fragment.Messagefragment;
//import com.patient.ghealth.model.ChatObject;
//import com.patient.ghealth.model.DoctorOnLineDetails;
//import com.patient.ghealth.model.InvoiceDetails;
//import com.patient.ghealth.networking.HttpRequester;
//import com.patient.ghealth.realmDB.DatabaseHandler;
//import com.patient.ghealth.utils.AndyUtils;
//import com.patient.ghealth.utils.Const;
//import com.patient.ghealth.utils.OpenTokConfig;
//import com.patient.ghealth.utils.PreferenceHelper;
//import com.tokbox.android.accpack.OneToOneCommunication;
//import com.tokbox.android.accpack.textchat.ChatMessage;
//import com.tokbox.android.accpack.textchat.TextChatFragment;
//import com.tokbox.android.logging.OTKAnalytics;
//import com.tokbox.android.logging.OTKAnalyticsData;
//
//import org.apache.commons.lang3.StringEscapeUtils;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.UnsupportedEncodingException;
//import java.net.URLDecoder;
//import java.util.HashMap;
//import java.util.List;
//import java.util.UUID;
//
///**
// * Created by user on 9/30/2016.
// */
//public class ChatActivity extends AppCompatActivity implements OneToOneCommunication.Listener, TextChatFragment.TextChatListener, AsyncTaskCompleteListener, Messagefragment.RemoteControlCallbacks {
//    List<ChatObject> chat_list;
//    DatabaseHandler database;
//    long starttime = 0L;
//    long timeInMilliseconds = 0L;
//    long timeSwapBuff = 0L;
//    long updatedtime = 0L;
//    int t = 1;
//    int secs = 0;
//    int mins = 0;
//    int milliseconds = 0;
//    int hours = 0;
//    Handler handler = new Handler();
//    public Runnable updateTimer = new Runnable() {
//
//        public void run() {
//
//            timeInMilliseconds = SystemClock.uptimeMillis() - starttime;
//
//            updatedtime = timeSwapBuff + timeInMilliseconds;
//
//            secs = (int) (updatedtime / 1000);
//
//            mins = secs / 60;
//            hours = mins / 60;
//            secs = secs % 60;
//            milliseconds = (int) (updatedtime % 1000);
//            tv_timer.setText(hours + ":" + "" + mins + ":" + String.format("%02d", secs));
//
//            handler.postDelayed(this, 0);
//        }
//
//    };
//    private String SESSION = "", TOKEN = "";
//    //TextChat fragment
//    private Messagefragment mTextChatFragment;
//    private FrameLayout mTextChatContainer;
//    private OTKAnalyticsData mAnalyticsData;
//    private OTKAnalytics mAnalytics;
//    private FragmentTransaction mFragmentTransaction;
//    //OpenTok calls
//    private OneToOneCommunication mComm;
//    private TextView tv_name, tv_watch, btn_end, tv_timer;
//    private String request_id;
//    private String doctoName;
//    private PreferenceHelper pHelper;
//    private InvoiceDetails invoiceDetails;
//    private boolean isdisplayed=false;
//    private Dialog errorDialog;
//    BroadcastReceiver receiver = new BroadcastReceiver() {
//        @Override
//        public void onReceive(Context context, Intent intent) {
////            AndyUtils.removeProgressDialog();
////            handler.removeCallbacks(updateTimer);
//            Log.d("FragmentRequest", "onReceive");
//            String notification = intent.getStringExtra("ashutosh");
//            Log.d("My Push notification", notification);
//            String message = intent.getStringExtra(Const.DOCTOR_INTENT_MESSAGE);
//            try {
//                JSONObject messageObj = new JSONObject(message);
//                AndyUtils.appLog("JsonResponse", messageObj.toString());
//                if (Integer.parseInt(messageObj.getString("status")) == 5) {
//                    invoiceDetails = new InvoiceDetails();
//                    JSONArray invoiceJsonArray = messageObj.getJSONArray("invoice");
//                    JSONObject invoiceJsonObject = invoiceJsonArray.getJSONObject(0);
//                    invoiceDetails.setInvoiceDoctorId(Integer.parseInt(invoiceJsonObject.getString("doctor_id")));
//                    invoiceDetails.setBasePrice(invoiceJsonObject.getString("base_price"));
//                    invoiceDetails.setPaymentMode(invoiceJsonObject.getString("payment_mode"));
//                    invoiceDetails.setTotalPrice(invoiceJsonObject.getString("total"));
//                    invoiceDetails.setMedicineFee(invoiceJsonObject.getString("medicine_fee"));
//                    invoiceDetails.setReferralBonus(invoiceJsonObject.getString("referral_bonus"));
//                    invoiceDetails.setPromoBonus(invoiceJsonObject.getString("promo_bonus"));
//                    invoiceDetails.setTotalTime(invoiceJsonObject.getString("total_time"));
//                    invoiceDetails.setTotalDistance(invoiceJsonObject.getString("distance"));
//                    invoiceDetails.setInvoiceRequestId(messageObj.getString("request_id"));
//                    invoiceDetails.setTreatmentFee(invoiceJsonObject.getString("time_price"));
//                    invoiceDetails.setInvoiceDoctorStatus(messageObj.getString("status"));
//                    invoiceDetails.setInvoiceDoctorPictureUrl(invoiceJsonObject.getString("doctor_picture"));
//                    invoiceDetails.setInvoiceDoctorName(invoiceJsonObject.getString("doctor_name"));
//                    Bundle invoiceBundle = new Bundle();
//                    invoiceBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, invoiceDetails);
//                    invoiceBundle.putString(Const.DOCTOR_CONSULT_CONNECTIVITY, "normal");
//                    Intent feedBackIntent = new Intent(ChatActivity.this, FeedBackOnlineActivity.class);
//                    feedBackIntent.putExtras(invoiceBundle);
//                    startActivity(feedBackIntent);
//                    finish();
//
//                }
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//    };
//    private Bundle bundle;
//    private DoctorOnLineDetails onLineDetails;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        String source = this.getPackageName();
//        database = new DatabaseHandler(this);
//
//        SharedPreferences prefs = this.getSharedPreferences("opentok", Context.MODE_PRIVATE);
//        String guidVSol = prefs.getString("guidVSol", null);
//        if (null == guidVSol) {
//            guidVSol = UUID.randomUUID().toString();
//            prefs.edit().putString("guidVSol", guidVSol).commit();
//        }
//
//        chat_list = database.get_Chat("" + PreferenceHelper.getParam(this, Const.REQUEST_ID, ""));
//        mAnalyticsData = new OTKAnalyticsData.Builder(OpenTokConfig.LOG_CLIENT_VERSION, source, OpenTokConfig.LOG_COMPONENTID, guidVSol).build();
//        mAnalytics = new OTKAnalytics(mAnalyticsData);
//        //add INITIALIZE attempt log event
//        addLogEvent(OpenTokConfig.LOG_ACTION_INITIALIZE, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//        setContentView(R.layout.activity_message);
//        mTextChatContainer = (FrameLayout) findViewById(R.id.textchat_fragment_container);
//        tv_name = (TextView) findViewById(R.id.name);
//        tv_timer = (TextView) findViewById(R.id.tv_msg_call_timer);
//        bundle = getIntent().getExtras();
//        if (bundle != null && bundle.getString(Const.DOCTOR_CONSULT_CONNECTIVITY).equals("normal")) {
//            onLineDetails = (DoctorOnLineDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
//            PreferenceHelper.setParam(this, Const.DOCTOR_NAME, onLineDetails.getDoctorName());
//            tv_name.setText(onLineDetails.getDoctorName());
//
//        }
////        else if(bundle != null && bundle.getString(Const.DOCTOR_CONSULT_CONNECTIVITY).equals("checkStatus"))
////        {
////            invoiceDetails= (InvoiceDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
//////            mComm.destroy();
////            showInvoiceDialog(invoiceDetails);
////        }
//        else if (!PreferenceHelper.getParam(this, Const.DOCTOR_NAME, "").equals("")) {
//            tv_name.setText((CharSequence) PreferenceHelper.getParam(this, Const.DOCTOR_NAME, ""));
//        }
//        addLogEvent(OpenTokConfig.LOG_ACTION_INITIALIZE, OpenTokConfig.LOG_VARIATION_SUCCESS);
//
//        gteopentok();
//
//        starttime = new PreferenceHelper(this).getReq_time();
//        handler.postDelayed(updateTimer, 0);
//    }
//
//    private void gteopentok() {
//
//        HashMap<String, String> map = new HashMap<String, String>();
//        map.put(Const.Params.URL, Const.ServiceType.GET_OPENTOK + Const.Params.ID + "="
//                + new PreferenceHelper(this).getParam(this, Const.Params.ID, "") + "&" + Const.Params.SESSION_TOKEN + "="
//                + (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.REQUEST_ID + "="
//                + new PreferenceHelper(this).getRequestId());
//
//        Log.d("mahi", "map" + map);
//
//        new HttpRequester(this, Const.GET, map, Const.ServiceCode.GET_OPENTOK, this);
//    }
//
//    private void initTextChatFragment() {
//
//
//        mTextChatFragment = Messagefragment.newInstance(mComm.getSession(), OpenTokConfig.API_KEY);
//        getSupportFragmentManager().beginTransaction()
//                .replace(R.id.textchat_fragment_container, mTextChatFragment).commit();
//
//    }
//
//    /**
//     * Converts dp to real pixels, according to the screen density.
//     *
//     * @param dp A number of density-independent pixels.
//     * @return The equivalent number of real pixels.
//     */
//    private int dpToPx(int dp) {
//        double screenDensity = this.getResources().getDisplayMetrics().density;
//        return (int) (screenDensity * (double) dp);
//    }
//
//    private void addLogEvent(String action, String variation) {
//        if (mAnalytics != null) {
//            mAnalytics.logEvent(action, variation);
//        }
//    }
//
//    @Override
//    public void onDestroy() {
//
//        super.onDestroy();
//        mComm.destroy();
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
//        new PreferenceHelper(this).putReq_time(updatedtime);
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//
//    }
//
//    public OneToOneCommunication getComm() {
//        return mComm;
//    }
//
//    //TextChat Fragment listener events
//    @Override
//    public void onNewSentMessage(ChatMessage message) {
//        Log.i("mahi", "New sent message");
//
//
//    }
//
//    @Override
//    public void onNewReceivedMessage(ChatMessage message) {
//        Log.i("Ashutosh", "New received message");
//        try {
//            notifyMsg(URLDecoder.decode(message.getText(),"UTF-8"));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//    @Override
//    public void onTextChatError(String error)
//    {
//        Log.i("Ashutosh", "Error on text chat " + error);
//        if (isdisplayed == false) {
//            showErrorDialog();
//            isdisplayed = true;
//        }
//    }
//
//    private void showErrorDialog() {
//        isdisplayed = true;
//        errorDialog = new Dialog(this, R.style.DialogTheme);
//        errorDialog.setCancelable(false);
//        errorDialog.setContentView(R.layout.reconnect_dialog);
//        Button btn_reconnect = (Button) errorDialog.findViewById(R.id.btn_reconect);
//        final TextView tv_name = (TextView) errorDialog.findViewById(R.id.tv_name);
//        btn_reconnect.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                gteopentok();
//                tv_name.setText("Connecting...");
//            }
//        });
//        errorDialog.show();
//    }
//
//    @Override
//    public void onClosed() {
//
//        mTextChatContainer.setVisibility(View.GONE);
//        restartTextChatLayout(true);
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        if (mTextChatFragment != null) {
//
//
//            mTextChatFragment.setSenderAlias((String) PreferenceHelper.getParam(this, Const.Params.FIRST_NAME, ""));
//            mTextChatFragment.setListener(this);
//
//        }
//
//        IntentFilter filter = new IntentFilter(Const.DOCTOR_NEW_REQUEST_STATUS);
//        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
//                filter);
//
//    }
//
//    public void onTextChat() {
//        if (mTextChatContainer.getVisibility() == View.VISIBLE) {
//            addLogEvent(OpenTokConfig.LOG_ACTION_CLOSE_TEXTCHAT, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//            mTextChatContainer.setVisibility(View.GONE);
//
//            addLogEvent(OpenTokConfig.LOG_ACTION_CLOSE_TEXTCHAT, OpenTokConfig.LOG_VARIATION_SUCCESS);
//        } else {
//            addLogEvent(OpenTokConfig.LOG_ACTION_OPEN_TEXTCHAT, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//
//            mTextChatContainer.setVisibility(View.VISIBLE);
//            addLogEvent(OpenTokConfig.LOG_ACTION_OPEN_TEXTCHAT, OpenTokConfig.LOG_VARIATION_SUCCESS);
//        }
//    }
//
//    @Override
//    public void onRestarted() {
//
//    }
//
//    @Override
//    public void onInitialized() {
//        if (mTextChatFragment != null) {
//            //Init TextChat values
//
//            Log.e("mahi", "coming");
//            mTextChatFragment.setSenderAlias((String) PreferenceHelper.getParam(this, Const.Params.FIRST_NAME, ""));
//            mTextChatFragment.setListener(this);
//
//            if (mComm != null && mComm.isStarted()) {
//                addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//                mComm.end();
//                cleanViewsAndControls();
//                addLogEvent(OpenTokConfig.LOG_ACTION_END_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
//            } else {
//                addLogEvent(OpenTokConfig.LOG_ACTION_START_COMM, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//                mComm.start();
//
//            }
//        }
//        addLogEvent(OpenTokConfig.LOG_ACTION_START_COMM, OpenTokConfig.LOG_VARIATION_SUCCESS);
//    }
//
//    //cleans views and controls
//    private void cleanViewsAndControls() {
//
//        if (mTextChatFragment != null) {
//            restartTextChatLayout(true);
//            mTextChatFragment.restart();
//            mTextChatContainer.setVisibility(View.GONE);
//        }
//    }
//
//
//    private void notifyMsg(String msg) {
//
//        NotificationManager mNotificationManager = (NotificationManager)
//                getSystemService(Context.NOTIFICATION_SERVICE);
//
//
//        NotificationCompat.Builder nBuilder =
//                new NotificationCompat.Builder(this)
//                        .setSmallIcon(R.mipmap.ic_launcher)
//                        .setContentTitle(getResources().getString(R.string.app_name))
//                        .setStyle(new NotificationCompat.BigTextStyle()
//                                .bigText(msg))
//                        .setVibrate(new long[]{300, 1000})
//                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
//                        .setAutoCancel(true)
//                        .setContentText(msg);
//
//        mNotificationManager.notify(1, nBuilder.build());
//
//    }
//
//    private void restartTextChatLayout(boolean restart) {
//        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mTextChatContainer.getLayoutParams();
//
//        if (restart) {
//            //restart to the original size
//            params.width = RelativeLayout.LayoutParams.MATCH_PARENT;
//            params.height = RelativeLayout.LayoutParams.MATCH_PARENT;
//            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
//        } else {
//            //go to the minimized size
//            params.height = dpToPx(40);
//            //params.addRule(RelativeLayout.ABOVE, R.id.actionbar_preview_fragment_container);
//            params.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
//        }
//        mTextChatContainer.setLayoutParams(params);
//    }
//
//    @Override
//    public void onError(String s) {
//        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
//        mComm.end(); //end communication
//
//        cleanViewsAndControls(); //restart views
//    }
//
//    @Override
//    public void onQualityWarning(boolean b) {
//
//    }
//
//    @Override
//    public void onAudioOnly(boolean b) {
//
//    }
//
//
//    @Override
//    public void onPreviewReady(View view) {
//
//    }
//
//    @Override
//    public void onRemoteViewReady(View view) {
//
//    }
//
//    @Override
//    public void onReconnecting() {
//
//    }
//
//    @Override
//    public void onReconnected() {
//
//    }
//
//    @Override
//    public void onCameraChanged(int i) {
//
//    }
//
//    @Override
//    public void onBackPressed() {
//    }
//
//    @Override
//    public void onTaskCompleted(String response, int serviceCode) {
//        switch (serviceCode) {
//            case Const.ServiceCode.GET_OPENTOK:
//                Log.e("mahi", "map" + response);
//
//                try {
//                    JSONObject job1 = new JSONObject(response);
//                    if (job1.getString("success").equals("true")) {
//                        SESSION = job1.getString("session_id");
//                        TOKEN = job1.getString("token");
//
//                        mComm = new OneToOneCommunication(ChatActivity.this, SESSION, TOKEN, OpenTokConfig.API_KEY);
//                        //set listener to receive the communication events, and add UI to these events
//                        // mComm.setListener(this);
//
//                        mComm.init();
//
//                        //init controls fragments
//
//                        if(errorDialog != null && errorDialog.isShowing()){
//                            if(!isFinishing()) {
//                                errorDialog.dismiss();
//                            }
//                        }
//
//                        mFragmentTransaction = getSupportFragmentManager().beginTransaction();
//                        //to enable/disable remote media
//                        initTextChatFragment(); //to send/receive text-messages
//                        mFragmentTransaction.commitAllowingStateLoss();
//
//                        addLogEvent(OpenTokConfig.LOG_ACTION_INITIALIZE, OpenTokConfig.LOG_VARIATION_SUCCESS);
//
//                        if (mTextChatFragment != null) {
//
//                            Log.d("mahi", "chat list");
//                            mTextChatFragment.setSenderAlias((String) PreferenceHelper.getParam(this, Const.Params.FIRST_NAME, ""));
//                            mTextChatFragment.setListener(this);
//                        }
//
//                        if (mTextChatContainer.getVisibility() == View.VISIBLE) {
//                            addLogEvent(OpenTokConfig.LOG_ACTION_CLOSE_TEXTCHAT, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//                            mTextChatContainer.setVisibility(View.GONE);
//
//                            addLogEvent(OpenTokConfig.LOG_ACTION_CLOSE_TEXTCHAT, OpenTokConfig.LOG_VARIATION_SUCCESS);
//                        } else {
//                            addLogEvent(OpenTokConfig.LOG_ACTION_OPEN_TEXTCHAT, OpenTokConfig.LOG_VARIATION_ATTEMPT);
//
//                            mTextChatContainer.setVisibility(View.VISIBLE);
//                            addLogEvent(OpenTokConfig.LOG_ACTION_OPEN_TEXTCHAT, OpenTokConfig.LOG_VARIATION_SUCCESS);
//                        }
//
//                        onTextChat();
//
//
//                    }
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                break;
//        }
//    }
//
//
//    @Override
//    public void onDisableRemoteAudio(boolean audio) {
//        if (mComm != null) {
//            mComm.enableRemoteMedia(OneToOneCommunication.MediaType.AUDIO, audio);
//        }
//
//
//    }
//
//    @Override
//    public void onDisableRemoteVideo(boolean video) {
//
//    }
//}
