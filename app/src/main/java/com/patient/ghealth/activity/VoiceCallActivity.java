package com.patient.ghealth.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.opentok.android.Connection;
import com.opentok.android.OpentokError;
import com.opentok.android.Publisher;
import com.opentok.android.PublisherKit;
import com.opentok.android.Session;
import com.opentok.android.Session.ArchiveListener;
import com.opentok.android.Session.SessionListener;
import com.opentok.android.Stream;
import com.opentok.android.Subscriber;
import com.patient.ghealth.R;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.model.InvoiceDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.service.ClearNotificationService;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.OpenTokConfig;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class VoiceCallActivity extends Activity implements SessionListener,
        ArchiveListener, View.OnClickListener, AsyncTaskCompleteListener, Session.SignalListener {
    private static final String LOGTAG = "demo-voice-only";
    long starttime = 0L;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedtime = 0L;
    int t = 1;
    int secs = 0;
    int mins = 0;
    int milliseconds = 0;
    int hours = 0;
    Handler handler = new Handler();
    private String SESSION = "", TOKEN = "";
    private Session mSession;
    private Publisher mPublisher;
    private ArrayList<Subscriber> mSubscribers = new ArrayList<Subscriber>();
    private HashMap<Stream, Subscriber> mSubscriberStream = new HashMap<Stream, Subscriber>();
    private boolean mIsBound = false;
    private ServiceConnection mConnection;
    private TextView doctorName;
    private ImageView doctorIcon;
    private ImageButton endCallButton, muteCallButton;
    private TextView callDurationText;
    public Runnable updateTimer = new Runnable() {

        public void run() {


            timeInMilliseconds = System.currentTimeMillis() - starttime;


            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(timeInMilliseconds),
                    TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.MILLISECONDS.toSeconds(timeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));

            callDurationText.setText(hms);

            handler.postDelayed(this, 0);
        }

    };
    private DoctorOnLineDetails onLineDetails;
    private Bundle bundle;
    private InvoiceDetails invoiceDetails;
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            AndyUtils.removeProgressDialog();
//            handler.removeCallbacks(updateTimer);
            Log.d("FragmentRequest", "onReceive");
            String notification = intent.getStringExtra("ashutosh");
            Log.d("My Push notification", notification);
            String message = intent.getStringExtra(Const.DOCTOR_INTENT_MESSAGE);
            try {
                JSONObject messageObj = new JSONObject(message);
                AndyUtils.appLog("JsonResponse", messageObj.toString());
                if (Integer.parseInt(messageObj.getString("status")) == 5) {
                    invoiceDetails = new InvoiceDetails();
                    JSONArray invoiceJsonArray = messageObj.getJSONArray("invoice");
                    JSONObject invoiceJsonObject = invoiceJsonArray.getJSONObject(0);
                    invoiceDetails.setInvoiceDoctorId(Integer.parseInt(invoiceJsonObject.getString("doctor_id")));
                    invoiceDetails.setBasePrice(invoiceJsonObject.getString("base_price"));
                    invoiceDetails.setPaymentMode(invoiceJsonObject.getString("payment_mode"));
                    invoiceDetails.setTotalPrice(invoiceJsonObject.getString("total"));
                    invoiceDetails.setMedicineFee(invoiceJsonObject.getString("medicine_fee"));
                    invoiceDetails.setReferralBonus(invoiceJsonObject.getString("referral_bonus"));
                    invoiceDetails.setPromoBonus(invoiceJsonObject.getString("promo_bonus"));
                    invoiceDetails.setTotalTime(invoiceJsonObject.getString("total_time"));
                    invoiceDetails.setTotalDistance(invoiceJsonObject.getString("distance"));
                    invoiceDetails.setInvoiceRequestId(messageObj.getString("request_id"));
                    invoiceDetails.setTreatmentFee(invoiceJsonObject.getString("time_price"));
                    invoiceDetails.setInvoiceDoctorStatus(messageObj.getString("status"));
                    invoiceDetails.setInvoiceDoctorPictureUrl(invoiceJsonObject.getString("doctor_picture"));
                    invoiceDetails.setInvoiceDoctorName(invoiceJsonObject.getString("doctor_name"));
                    Bundle invoiceBundle = new Bundle();
                    invoiceBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, invoiceDetails);
                    invoiceBundle.putString(Const.DOCTOR_CONSULT_CONNECTIVITY, "normal");
                    Intent feedbackIntent = new Intent(VoiceCallActivity.this, FeedBackOnlineActivity.class);
                    feedbackIntent.putExtras(invoiceBundle);
                    startActivity(feedbackIntent);
                    finish();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    private Dialog invoiceDialog;
    private Dialog feedbackDialog;
    private PreferenceHelper pHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(LOGTAG, "ONCREATE");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_call);
        pHelper = new PreferenceHelper(this);
        doctorName = (TextView) findViewById(R.id.tv_voice_call_doctor_name);
        doctorIcon = (ImageView) findViewById(R.id.iv_voice_call_doctor_icon);
        endCallButton = (ImageButton) findViewById(R.id.ib_voice_call_end);
        endCallButton.setOnClickListener(this);
        endCallButton.setVisibility(View.GONE);

        gteopentok();

        bundle = getIntent().getExtras();
        if (bundle != null && bundle.getString(Const.DOCTOR_CONSULT_CONNECTIVITY).equals("normal")) {
            onLineDetails = (DoctorOnLineDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
            PreferenceHelper.setParam(this, Const.DOCTOR_NAME, onLineDetails.getDoctorName());
            PreferenceHelper.setParam(this, Const.PICTURE, onLineDetails.getDoctorPictureUrl());
            setDetailsOnView(onLineDetails);
        } else if (!PreferenceHelper.getParam(this, Const.DOCTOR_NAME, "").equals("")) {
            doctorName.setText((CharSequence) PreferenceHelper.getParam(this, Const.DOCTOR_NAME, ""));
            if (PreferenceHelper.getParam(this, Const.PICTURE, "").equals("")) {
                doctorIcon.setImageResource(R.drawable.profile);
            } else {
                Glide.with(this).load(PreferenceHelper.getParam(this, Const.PICTURE, "")).into(doctorIcon);
            }
        }
        callDurationText = (TextView) findViewById(R.id.tv_voice_call_duration);
        MeterView mv = (MeterView) findViewById(R.id.publishermeter);
        mv.setIcons(BitmapFactory.decodeResource(getResources(),
                R.mipmap.unmute_pub), BitmapFactory.decodeResource(
                getResources(), R.mipmap.mute_pub));


    }


    private void gteopentok() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_OPENTOK + Const.Params.ID + "="
                + new PreferenceHelper(this).getParam(this, Const.Params.ID, "") + "&" + Const.Params.SESSION_TOKEN + "="
                + (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.REQUEST_ID + "="
                + new PreferenceHelper(this).getRequestId());

        Log.d("mahi", "map" + map);

        new HttpRequester(this, Const.GET, map, Const.ServiceCode.GET_OPENTOK, this);
    }

    private void setDetailsOnView(DoctorOnLineDetails doctorOnLineDetails) {
        doctorName.setText(doctorOnLineDetails.getDoctorName());
        if (doctorOnLineDetails.getDoctorPictureUrl().equals("")) {
            doctorIcon.setImageResource(R.drawable.profile);
        } else {
            Glide.with(this).load(doctorOnLineDetails.getDoctorPictureUrl()).into(doctorIcon);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mSession != null) {
            mSession.onPause();
        }


        if (mConnection == null) {
            mConnection = new ServiceConnection() {
                @Override
                public void onServiceConnected(ComponentName className, IBinder binder) {

                }

                @Override
                public void onServiceDisconnected(ComponentName className) {
                    mConnection = null;
                }

            };
        }

        if (!mIsBound) {
            bindService(new Intent(VoiceCallActivity.this,
                            ClearNotificationService.class), mConnection,
                    Context.BIND_AUTO_CREATE);
            mIsBound = true;

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(Const.DOCTOR_NEW_REQUEST_STATUS);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);

        if (mIsBound) {
            unbindService(mConnection);
            mIsBound = false;
        }
        if (mSession != null) {
            mSession.onResume();
        }

    }

    @Override
    public void onStop() {
        super.onStop();

        if (mIsBound) {
            unbindService(mConnection);
            mIsBound = false;
        }
        if (isFinishing()) {

            if (mSession != null) {
                mSession.disconnect();
            }
        }
    }

    @Override
    public void onDestroy() {
        if (handler != null) {
//            handler.removeCallbacks(updateTimer);
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

        if (mIsBound) {
            unbindService(mConnection);
            mIsBound = false;
        }

        if (mSession != null) {
            mSession.disconnect();
        }
//        restartAudioMode();
        super.onDestroy();
        finish();
    }

    @Override
    public void onBackPressed() {
        if (mSession != null) {
            mSession.disconnect();
        }
        restartAudioMode();

//        super.onBackPressed();
    }

    public void restartAudioMode() {
        AudioManager Audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        Audio.setMode(AudioManager.MODE_NORMAL);
        this.setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
    }

    private void sessionConnect() {

    }

    public void onEndCall(View v) {
        finish();
    }

    @Override
    public void onConnected(Session session) {
        Log.d("mahi", "audio");


        mPublisher = new Publisher(this, "Publisher");
        // Publish audio only
        mPublisher.setPublishVideo(false);
        mSession.publish(mPublisher);

        // Initialize publisher meter view
        final MeterView meterView = (MeterView) findViewById(R.id.publishermeter);
        mPublisher.setAudioLevelListener(new PublisherKit.AudioLevelListener() {
            @Override
            public void onAudioLevelUpdated(PublisherKit publisher,
                                            float audioLevel) {
                meterView.setMeterValue(audioLevel);
            }
        });
        meterView.setOnClickListener(new MeterView.OnClickListener() {
            @Override
            public void onClick(MeterView view) {
                mPublisher.setPublishAudio(!view.isMuted());
            }
        });


//        if (pHelper.getAccept_time() == 0L) {
//            pHelper.putAccept_time(System.currentTimeMillis());
//        }
//
//        starttime = new PreferenceHelper(this).getAccept_time();
//        handler.postDelayed(updateTimer, 0);
    }

    @Override
    public void onDisconnected(Session session) {
    }

    @Override
    public void onStreamReceived(Session session, Stream stream) {
        Subscriber subscriber = new Subscriber(this, stream);

        // Subscribe audio only
        subscriber.setSubscribeToVideo(false);

        mSession.subscribe(subscriber);
        mSubscribers.add(subscriber);
        mSubscriberStream.put(stream, subscriber);
//        mSubscriberAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStreamDropped(Session session, Stream stream) {
        Subscriber subscriber = mSubscriberStream.get(stream);
        if (subscriber != null) {
            mSession.unsubscribe(subscriber);
            mSubscribers.remove(subscriber);
            mSubscriberStream.remove(stream);
//            mSubscriberAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onError(Session session, OpentokError error) {
        AndyUtils.showLongToast(getString(R.string.connection_lost), this);
//        Toast.makeText(this, error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onArchiveStarted(Session session, String id, String name) {
//        setArchiving(R.string.archivingOn, R.mipmap.archiving_on);
    }

    @Override
    public void onArchiveStopped(Session session, String id) {
//        setArchiving(R.string.archivingOff, R.mipmap.archiving_off);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ib_voice_call_end:
        }

    }


    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.DOCTOR_RATE:
                break;
            case Const.ServiceCode.GET_OPENTOK:
                Log.e("mahi", "map" + response);

                try {
                    JSONObject job1 = new JSONObject(response);
                    if (job1.getString("success").equals("true")) {
                        SESSION = job1.getString("session_id");
                        TOKEN = job1.getString("token");
                        if (mSession == null) {
                            mSession = new Session(this, OpenTokConfig.API_KEY,
                                    SESSION);
                            mSession.setSessionListener(this);
                            mSession.setSignalListener(this);
                            mSession.setArchiveListener(this);
                            mSession.connect(TOKEN);
                        }


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                break;

        }


    }

    @Override
    public void onSignalReceived(Session session, String s, final String s1, Connection connection) {
        AndyUtils.appLog("VoiceCall Timer", "fromServer" + s1 + s);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                callDurationText.setText(s1);
            }
        });
    }
}

