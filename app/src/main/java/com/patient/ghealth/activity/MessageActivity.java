package com.patient.ghealth.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.patient.ghealth.R;
import com.patient.ghealth.adapter.ChatAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.ChatObject;
import com.patient.ghealth.model.ConsultHistory;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.model.InvoiceDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.realmDB.DatabaseHandler;
import com.patient.ghealth.service.ImageUploadService;
import com.patient.ghealth.service.XmppService;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.Helper;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by user on 9/30/2016.
 */
public class MessageActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskCompleteListener, XmppService.Callbacks {


    public static final int MEDIA_TYPE_AUDIO = 1, MEDIA_TYPE_VIDEO = 2;
    private static final String TAG = MessageActivity.class.getSimpleName();
    private static final int CAPTURE_IMAGE = 1, GET_FROM_GALLERY = 2;
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FILE_EXT_MP3 = ".mp3";
    private static final String AUDIO_RECORDER_FOLDER = "WhatsAppVoiceClips";
    public static List<ChatObject> messagesList;
    public static String name, phone;
    public static Menu menu;
    EditText message;
    Timer timer;
    TimerTask myTimerTask;
    ListView message_listview;
    ChatAdapter messageAdapter;
    DatabaseHandler db;
    ImageView attachment;
    long starttime = 0L;
    long timeInMilliseconds = 0L;
    long timeSwapBuff = 0L;
    long updatedtime = 0L;
    int t = 1;
    int secs = 0;
    int mins = 0;
    int milliseconds = 0;
    int hours = 0;
    Handler mhandler = new Handler();
    public Runnable updateTimer = new Runnable() {

        public void run() {


            timeInMilliseconds = System.currentTimeMillis() - starttime;


            String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(timeInMilliseconds),
                    TimeUnit.MILLISECONDS.toMinutes(timeInMilliseconds) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.MILLISECONDS.toSeconds(timeInMilliseconds) % TimeUnit.MINUTES.toSeconds(1));

            tv_watch.setText(hms);

            mhandler.postDelayed(this, 0);
        }

    };
    NotificationCompat.Builder nBuilder;
    PreferenceHelper pHelper;
    private Uri uri;
    private boolean isReceived = false;
    private IntentFilter filter1;
    private XmppService myService;
    private TextView tv_name, tv_watch;
    private int seconds, minutes;
    private String str_seconds, str_minutes, sendermsg, request_id;
    private MediaUploadReceiver mediaReceiver;
    private FloatingActionButton send;
    private InvoiceDetails invoiceDetails;
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "FragmentRequest" + "onReceive");
            String notification = intent.getStringExtra("ashutosh");
            Log.d(TAG, "My Push notification" + notification);
            String message = intent.getStringExtra(Const.DOCTOR_INTENT_MESSAGE);
            try {
                JSONObject messageObj = new JSONObject(message);
                AndyUtils.appLog("JsonResponse", messageObj.toString());
                if (Integer.parseInt(messageObj.getString("status")) == 5) {
                    invoiceDetails = new InvoiceDetails();
                    JSONArray invoiceJsonArray = messageObj.getJSONArray("invoice");
                    JSONObject invoiceJsonObject = invoiceJsonArray.getJSONObject(0);
                    invoiceDetails.setInvoiceDoctorId(Integer.parseInt(invoiceJsonObject.getString("doctor_id")));
                    invoiceDetails.setBasePrice(invoiceJsonObject.getString("base_price"));
                    invoiceDetails.setPaymentMode(invoiceJsonObject.getString("payment_mode"));
                    invoiceDetails.setTotalPrice(invoiceJsonObject.getString("total"));
                    invoiceDetails.setMedicineFee(invoiceJsonObject.getString("medicine_fee"));
                    invoiceDetails.setReferralBonus(invoiceJsonObject.getString("referral_bonus"));
                    invoiceDetails.setPromoBonus(invoiceJsonObject.getString("promo_bonus"));
                    invoiceDetails.setTotalTime(invoiceJsonObject.getString("total_time"));
                    invoiceDetails.setTotalDistance(invoiceJsonObject.getString("distance"));
                    invoiceDetails.setInvoiceRequestId(messageObj.getString("request_id"));
                    invoiceDetails.setTreatmentFee(invoiceJsonObject.getString("time_price"));
                    invoiceDetails.setInvoiceDoctorStatus(messageObj.getString("status"));
                    invoiceDetails.setInvoiceDoctorPictureUrl(invoiceJsonObject.getString("doctor_picture"));
                    invoiceDetails.setInvoiceDoctorName(invoiceJsonObject.getString("doctor_name"));
                    Bundle invoiceBundle = new Bundle();
                    invoiceBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, invoiceDetails);
                    invoiceBundle.putString(Const.DOCTOR_CONSULT_CONNECTIVITY, "normal");
                    Intent feedBackIntent = new Intent(MessageActivity.this, FeedBackOnlineActivity.class);
                    feedBackIntent.putExtras(invoiceBundle);
                    startActivity(feedBackIntent);
                    finish();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    private List<ConsultHistory> consultHistoryList;
    private Socket mSocket;
    private boolean isConnected = true;
    private Bundle bundle;
    private DoctorOnLineDetails onLineDetails;
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            AndyUtils.appLog(TAG, "OnConnected");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isConnected) {

                        try {

                            AndyUtils.appLog(TAG, "InsideOnConnected");
                            JSONObject object = new JSONObject();
                            object.put("request_id", pHelper.getRequestId());
                            object.put("type", "user");

                            Log.d(TAG, "update_object" + "" + object);
                            mSocket.emit("online_chat_consult", object);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Toast.makeText(getApplicationContext(),
                                R.string.connect, Toast.LENGTH_LONG).show();
                        isConnected = true;
                    }
                    if (isConnected) {


                        try {

                            JSONObject object = new JSONObject();
                            object.put("request_id", pHelper.getRequestId());
                            object.put("type", "user");
                            Log.d(TAG, "update_object" + "" + object);
                            mSocket.emit("online_chat_consult", object);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }
            });
        }
    };
    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            AndyUtils.appLog(TAG, "OnDisConnected");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    AndyUtils.appLog(TAG, "OnInsideDisConnected");
                    isConnected = false;
                    Toast.makeText(getApplicationContext(),
                            R.string.disconnect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            R.string.error_connect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];


                    String data_type;
                    String message;
                    String type;
                    try {

                        data_type = data.getString("data_type");
                        message = data.getString("message");
                        type = data.getString("type");

                        AndyUtils.appLog(TAG, "Type" + type);
                        showChat(type, data_type, message);


                    } catch (JSONException e) {
                        return;
                    }


                }
            });
        }
    };

    private Emitter.Listener onTimer = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            AndyUtils.appLog(TAG, "fromServer" + args[0]);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tv_watch.setText(args[0] + "");
                }
            });

        }
    };
    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d(TAG, "user joined");
        }
    };
    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        return;
                    }

                }
            });
        }
    };
    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d("mahi", "on typing");
        }
    };
    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    try {
                        username = data.getString("username");
                    } catch (JSONException e) {
                        return;
                    }

                }
            });
        }
    };
    private Emitter.Listener onsent = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d(TAG, "calling on sent");
        }
    };
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            AndyUtils.appLog(TAG, "MessageActivity" + "onServiceConnected called");
            // We've binded to LocalService, cast the IBinder and get LocalService instance
            XmppService.LocalBinder binder = (XmppService.LocalBinder) service;
            myService = binder.getServiceInstance(); //Get instance of your service!
            myService.registerMessageClient(MessageActivity.this, Const.MESSAGE); //Activity register in the service as client for callabcks!
//            tvServiceState.setText("Connected to service...");
//            tbStartTask.setEnabled(true);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
//            Toast.makeText(MessageActivity.this, "onServiceDisconnected called", Toast.LENGTH_SHORT).show();
//            tvServiceState.setText("Service disconnected");
//            tbStartTask.setEnabled(false);
        }
    };

    @Override
    public void getMessage(final String data, final String dataType) {
        AndyUtils.appLog(TAG, "MessageActivity" + data);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showChat("recieve", dataType, data);
                if (data.contains("http")) {
                    notifyMsg("Image");
                } else {
                    notifyMsg(data);
                }
            }
        });


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        db = new DatabaseHandler(MessageActivity.this);
        pHelper = new PreferenceHelper(this);
        Intent serviceIntent = new Intent(MessageActivity.this, XmppService.class);
        bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);


        initiateSokect();
        attachment = (ImageView) findViewById(R.id.attachment);
        attachment.setOnClickListener(this);
        request_id = new PreferenceHelper(this).getRequestId();
        getChatHistory(request_id);
        filter1 = new IntentFilter("media_upload_receiver");
        mediaReceiver = new MediaUploadReceiver();
        tv_name = (TextView) findViewById(R.id.name);
        tv_watch = (TextView) findViewById(R.id.tv_watch);
        message_listview = (ListView) findViewById(R.id.message_listview);
        bundle = getIntent().getExtras();
        if (bundle != null && bundle.getString(Const.DOCTOR_CONSULT_CONNECTIVITY).equals("normal")) {
            onLineDetails = (DoctorOnLineDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
            PreferenceHelper.setParam(this, Const.DOCTOR_NAME, onLineDetails.getDoctorName());
            tv_name.setText(onLineDetails.getDoctorName());

        } else if (!PreferenceHelper.getParam(this, Const.DOCTOR_NAME, "").equals("")) {
            tv_name.setText((CharSequence) PreferenceHelper.getParam(this, Const.DOCTOR_NAME, ""));
        }

        AndyUtils.appLog(TAG, "MessageActivity" + "RequestId" + request_id);
        messagesList = db.get_Chat("" + request_id);
        if (messagesList != null && messagesList.size() > 0) {
            AndyUtils.appLog(TAG, messagesList.size() + "");
            messageAdapter = new ChatAdapter(getApplicationContext(), R.layout.chat_adapter, messagesList);
            message_listview.setAdapter(messageAdapter);
        }

        send = (FloatingActionButton) findViewById(R.id.send);
        send.setOnClickListener(this);
        message = (EditText) findViewById(R.id.message);


        //timer

//        if (pHelper.getAccept_time() == 0L) {
//            pHelper.putAccept_time(System.currentTimeMillis());
//        }
//
//        starttime = new PreferenceHelper(this).getAccept_time();
//        mhandler.postDelayed(updateTimer, 0);

    }

    private void initiateSokect() {
        try {
            mSocket = IO.socket(Const.ServiceType.SOCKET_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("online_chat_message", onNewMessage);
        mSocket.on("user_joined_chat", onUserJoined);
        mSocket.on("user left", onUserLeft);
        mSocket.on("online_chat_start_typing", onTyping);
        mSocket.on("stop typing", onStopTyping);
        mSocket.on("online_chat_consult", onsent);
        mSocket.on("timer_tick", onTimer);
        mSocket.connect();
    }

    private void notifyMsg(String msg) {
        NotificationManager mNotificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        Intent notificationIntent = new Intent(this, MessageActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent intent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(),
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        nBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(msg))
                .setVibrate(new long[]{300, 1000})
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true)
                .setContentText(msg);


        nBuilder.build().flags |= Notification.FLAG_AUTO_CANCEL;
        nBuilder.setContentIntent(intent);
        mNotificationManager.notify(1, nBuilder.build());

    }


    @Override
    public void onBackPressed() {

    }


    private void startTimer() {


        if (timer != null) {
            timer.cancel();
        }

        //re-schedule timer here
        //otherwise, IllegalStateException of
        //"TimerTask is scheduled already"
        //will be thrown
        timer = new Timer();
        myTimerTask = new TimerTask() {
            @Override
            public void run() {
                seconds++;
                if (seconds >= 60) {
                    seconds = 0;
                    minutes++;
                }


                if (seconds < 10)
                    str_seconds = "0" + String.valueOf(seconds);
                else {
                    str_seconds = String.valueOf(seconds);
                }
                if (minutes < 10)
                    str_minutes = "0" + String.valueOf(minutes);
                else {
                    str_minutes = String.valueOf(minutes);
                }


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        message.setText(str_minutes + ":" + str_seconds);
                    }
                });

            }

        };


        //singleshot delay 1000 ms
        timer.schedule(myTimerTask, 0, 1000);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send:
                sendermsg = message.getText().toString();

                if (message.getText().toString().length() == 0) {

                    Toast.makeText(getApplicationContext(),
                            getString(R.string.enter_text_before_sending), Toast.LENGTH_LONG).show();

                } else {


                    if (mSocket.connected()) {
                        attemptSend(sendermsg, "TEXT");
                    }
                    message.setText("");

//                    XmppService.sendMessage(MessageActivity.this,
//                            Const.ServiceType.XMPPUSER + String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, ""))
//                                    + Const.SUFFIX_CHAT,
//                            Message.Type.chat, AndyUtils.message_json("0", Const.Params.TEXT, String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, "")),
//                                    String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")), "" + new PreferenceHelper(this).getRequestId(), sendermsg));
//
//                    message.setText("");
//
//                    showChat("sent", Const.Params.TEXT, sendermsg);
                }

                break;
            case R.id.attachment:
                showPictureDialog();
                break;

        }
    }


    private void attemptSend(String sendermsg, String type) {
        if (!mSocket.connected()) return;

        JSONObject messageObj = new JSONObject();
        try {
            messageObj.put("message", sendermsg);
            messageObj.put("message_type", type);

            Log.d(TAG, "message socket in chat" + messageObj.toString());
//showChat("sent", type, sendermsg);
            mSocket.emit("online_chat_message", messageObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void showChat(String type, String data_type, String message) {

        if (messagesList == null || messagesList.size() == 0) {

            messagesList = new ArrayList<ChatObject>();
        }

        messagesList.add(new ChatObject(message, type, data_type, ""
                + request_id, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")), String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, ""))));

        ChatAdapter chatAdabter = new ChatAdapter(MessageActivity.this, R.layout.chat_adapter, messagesList);

        message_listview.setAdapter(chatAdabter);
        // chatAdabter.notifyDataSetChanged();
        if (type.equals("sent")) {
            AndyUtils.appLog(TAG, "InsertChat" + request_id);
            ChatObject chat = new ChatObject(message, type, data_type, ""
                    + request_id, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")), String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, "")));

            db.insertChat(chat);
        }
        // chatAdabter.notifyDataSetChanged();

    }


    private void showPictureDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getString(R.string.choose_your_option));
        String[] items = {getString(R.string.gallery), getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        openGallery();
                        break;
                    case 1:
                        openCamera();
                        break;


                }
            }
        });
        dialog.show();
    }

    private void openGallery() {

        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GET_FROM_GALLERY);
    }

    public void openCamera() {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Calendar cal = Calendar.getInstance();
        File file = new File(Environment.getExternalStorageDirectory(),
                (cal.getTimeInMillis() + ".jpg"));
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {

            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        uri = Uri.fromFile(file);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);

        startActivityForResult(cameraIntent, CAPTURE_IMAGE);

    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        Intent intent;
        if (resultCode == RESULT_OK) {
            switch (requestCode) {

                case CAPTURE_IMAGE:

                    if (uri != null) {
                        String file = Helper.getRealPathFromURI(uri, getApplicationContext());
                        Bitmap compressedImageFile = Compressor.getDefault(this).compressToBitmap(new File(file));
                        Uri uri = Helper.getImageUri(this, compressedImageFile);
                        String selectedImagePath = Helper.getRealPathFromURI(uri, getApplicationContext());
                        sendermsg = selectedImagePath;

                        Intent captureUploadService = new Intent(MessageActivity.this, ImageUploadService.class);
                        captureUploadService.putExtra("image_path", selectedImagePath);
                        // imageUploadService.putExtra("key", key);
                        captureUploadService.putExtra("request_id", request_id);
                        startService(captureUploadService);
                    }

                    // mHandler.sendEmptyMessage(2);

                    break;


                case GET_FROM_GALLERY:

                    if (data != null) {
                        Uri selectedImage = data.getData();
                        String file = Helper.getRealPathFromURI(selectedImage, getApplicationContext());
                        File file1 = new File(file);
                        Bitmap compressedImageFile = Compressor.getDefault(this).compressToBitmap(file1);
                        Uri uri = getImageUri(this, compressedImageFile);
                        String picturePath = Helper.getRealPathFromURI(uri, getApplicationContext());
//                        picturePath = Helper.compressImage(this, picturePath);
                        sendermsg = picturePath;


                        //mHandler.sendEmptyMessage(2);

                        BitmapFactory.Options options = new BitmapFactory.Options();

                        options.inJustDecodeBounds = false;
                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                        options.inDither = true;

                        Bitmap bitmapgallery = BitmapFactory.decodeFile(picturePath, options);

                        Intent imageUploadService = new Intent(MessageActivity.this, ImageUploadService.class);

                        imageUploadService.putExtra("image_path", picturePath);
                        // imageUploadService.putExtra("key", key);
                        imageUploadService.putExtra("request_id", request_id);
                        startService(imageUploadService);
                    } else {
                        Toast.makeText(this, getString(R.string.unable_to_select_image),
                                Toast.LENGTH_LONG).show();
                    }

                    break;
            }
        }


    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {

        switch (serviceCode) {
            case Const.ServiceCode.GET_CHAT_HISTORY_MESSAGE:
                AndyUtils.appLog(TAG, "ChatHistoryResponse" + response);
                if (null != messagesList) {
                    messagesList.clear();
                }
                try {
                    JSONObject job = new JSONObject(response);
                    if (job.getString("success").equals("true")) {
                        JSONArray jarray = job.getJSONArray("chatArray");

                        for (int i = 0; i < jarray.length(); i++) {
                            // ChatHistoryobjt chat = new ChatHistoryobjt();
                            JSONObject chatobj = jarray.getJSONObject(i);

                            messagesList.add(new ChatObject(chatobj.getString("message"), chatobj.getString("type"), chatobj.getString("data_type"), ""
                                    + pHelper.getRequestId(), String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")), (String) PreferenceHelper.getParam(MessageActivity.this, Const.Params.DOCTOR_ID, "")));


                        }


                        if (messagesList != null && messagesList.size() > 0) {
                            messageAdapter = new ChatAdapter(this, R.layout.chat_adapter, messagesList);
                            // messageAdapter.notifyDataSetChanged();

                            message_listview.setAdapter(messageAdapter);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        AndyUtils.appLog("MessageActivity", "onPause");
//        LocalBroadcastManager.getInstance(this).unregisterReceiver(recieve_chat);
        unregisterReceiver(mediaReceiver);

    }


    @Override
    protected void onStop() {
        super.onStop();
        AndyUtils.appLog("MessageActivity", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AndyUtils.appLog("MessageActivity", "onDestroy");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

        mSocket.disconnect();

        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("online_chat_message", onNewMessage);
        mSocket.off("user_joined_chat", onUserJoined);
        mSocket.off("online_chat_start_typing", onTyping);
        mSocket.off("stop typing", onStopTyping);
        mSocket.off("online_chat_consult", onsent);
        mSocket.off("timer_tick", onTimer);

    }

    @Override
    protected void onResume() {
        super.onResume();
        AndyUtils.appLog(TAG, "onResume");
        registerReceiver(mediaReceiver, filter1);
//        LocalBroadcastManager.getInstance(this).registerReceiver(recieve_chat,
//                new IntentFilter("message_recieved"));
        IntentFilter filter = new IntentFilter(Const.DOCTOR_NEW_REQUEST_STATUS);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
                filter);
    }

    private void parseMediaAndSendMessage(String response) {

        sendermsg = response;

        attemptSend(sendermsg, "IMAGE");

//        XmppService.sendMessage(MessageActivity.this,
//                Const.ServiceType.XMPPUSER + String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, ""))
//                        + Const.SUFFIX_CHAT,
//                Message.Type.chat, AndyUtils.message_json("0", Const.Params.IMAGE, String.valueOf(PreferenceHelper.getParam(this, Const.Params.DOCTOR_ID, "")),
//                        String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")), "" + new PreferenceHelper(this).getRequestId(), sendermsg));
//
//
//        showChat("sent", Const.Params.IMAGE, sendermsg);

    }

    private void getChatHistory(String request_id) {


        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showShortToast(getString(R.string.no_internet), this);
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CHAT_HISTORY_MESSAGE_URL + Const.Params.REQUEST_ID + "="
                + request_id);

        AndyUtils.appLog("MessageActivity", "ChatHistory" + map);
        new HttpRequester(this, Const.GET, map, Const.ServiceCode.GET_CHAT_HISTORY_MESSAGE, this);

    }

    class MediaUploadReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            String response = intent.getStringExtra("response");
            parseMediaAndSendMessage(response);
        }

    }
}