package com.patient.ghealth.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.patient.ghealth.R;


public class FinishPaymentActivity extends AppCompatActivity {

    private TextView orderId_text;
    private Button continue_shopping_button;
    private ImageView backButton;
    private Toolbar finishToolbar;

    private AlertDialog alertDialog;
    private BroadcastReceiver mInternetStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            boolean status = intent.getBooleanExtra("status", false);
            if (status) {
                if (alertDialog.isShowing())
                    alertDialog.dismiss();
            } else {
                if (!alertDialog.isShowing())
                    alertDialog.show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish_payment);
        finishToolbar= (Toolbar) findViewById(R.id.finish_toolbar);
        backButton= (ImageView) findViewById(R.id.iv_finishBackButton);

//        alertDialog = HelperMethods.getNointernetDialog(this);
        String orderId = getIntent().getExtras().getString("orderId");

        orderId_text = (TextView) findViewById(R.id.orderId_text);
        orderId_text.setText("#" + orderId);
        continue_shopping_button = (Button) findViewById(R.id.continue_shopping_button);
        continue_shopping_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(FinishPaymentActivity.this, MainActivity.class));
                finish();
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(FinishPaymentActivity.this, MainActivity.class));
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mInternetStatusReceiver,
                new IntentFilter("internetConnectivity"));
//        if (!HelperMethods.isNetworkAvailable(this)) {
//            alertDialog.show();
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mInternetStatusReceiver);
    }


}
