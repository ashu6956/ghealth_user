package com.patient.ghealth.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.fragment.AddAddressFragment;
import com.patient.ghealth.fragment.GetAddressFragment;
import com.patient.ghealth.fragment.PaymentMethodFragment;
import com.patient.ghealth.model.Address;
import com.patient.ghealth.utils.Const;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalService;


public class CheckoutActivity extends AppCompatActivity {

    // Activity level global variable
    public static Address checkoutAddress;
    public static Address billingAddress;
    public static int paymentMethod;
    public static int checkoutMethod;
    public static String productId, productVariant, productLicense;
    public static int productQty;
    // PayPal config;
    public static PayPalConfiguration config = new PayPalConfiguration()
            .environment(Const.CONFIG_ENVIRONMENT)
            .clientId(Const.CONFIG_CLIENT_ID)
            .rememberUser(false)
            .acceptCreditCards(false);
    public String currentFragment = "";
    private Toolbar checkOutToolbar;
    private TextView checkoutHeader;
    private ImageView checkoutBackButton;
    private AlertDialog alertDialog;
    private BroadcastReceiver mInternetStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            boolean status = intent.getBooleanExtra("status", false);
            if (status) {
                if (alertDialog.isShowing())
                    alertDialog.dismiss();
            } else {
                if (!alertDialog.isShowing())
                    alertDialog.show();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        checkOutToolbar = (Toolbar) findViewById(R.id.checkout_toolbar);
        setSupportActionBar(checkOutToolbar);
        getSupportActionBar().setTitle("");
        checkoutBackButton = (ImageView) findViewById(R.id.iv_checkoutBackButton);
        checkoutHeader = (TextView) findViewById(R.id.tv_checkout_header);
        checkoutHeader.setText(getString(R.string.shipping_address));
        checkoutMethod = getIntent().getIntExtra(Const.CHECKOUT_METHOD_KEY, 0);
        try {
            productId = getIntent().getStringExtra(Const.Params.PRODUCT_ID);
            productQty = getIntent().getIntExtra(Const.QUANTITY, 0);
            productVariant = getIntent().getStringExtra("variant");
            productLicense = getIntent().getStringExtra("license");

        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        gotoGetAddressFragment();
        checkoutBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (currentFragment.equals(Const.AddAddressFragment)) {
            gotoGetAddressFragment();
        } else if (currentFragment.equals(Const.SummaryFragment)) {
            addFragment(new PaymentMethodFragment(), false, getString(R.string.checkout), Const.PaymentMethodFragment, true);
        } else if (currentFragment.equals(Const.PaymentMethodFragment)) {
            addFragment(new GetAddressFragment(), false, getString(R.string.shipping_address), Const.GetAddressFragment, true);
        } else if (currentFragment.equals(Const.GetAddressFragment)) {
            super.onBackPressed();
        }

    }

    private void gotoGetAddressFragment() {
        GetAddressFragment fragment = new GetAddressFragment();
        addFragment(fragment, false, getString(R.string.shipping_address), "", false);
    }


    public void addFragment(Fragment fragment, boolean addToBackStack, String fragmentTitle,
                            String tag, boolean isAnimate) {
        android.support.v4.app.FragmentManager manager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction ft = manager.beginTransaction();
        if (fragmentTitle == "") {
//            toolbarHeader.setText(Html.fromHtml(sHeader));
        } else {
            checkoutHeader.setText(fragmentTitle);
        }
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(R.id.checkout_container, fragment, tag);
        ft.commit();
    }

    @Override
    protected void onDestroy() {
//        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mInternetStatusReceiver,
                new IntentFilter("internetConnectivity"));
//        if (!HelperMethods.isNetworkAvailable(this)) {
//            alertDialog.show();
//        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mInternetStatusReceiver);
    }

}
