package com.patient.ghealth.activity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.adapter.DoctorListPagerAdapter;
import com.patient.ghealth.fragment.DoctorBookingFragment;
import com.patient.ghealth.fragment.DoctorNearByBookingFragment;
import com.patient.ghealth.fragment.DoctorNearByOnlineConsultFragment;
import com.patient.ghealth.fragment.DoctorOnlineConsultFragment;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;
import com.patient.ghealth.utils.ScrollableViewPager;

import java.util.Locale;

/**
 * Created by getit on 8/9/2016.
 */
public class DoctorListActivity extends AppCompatActivity implements View.OnClickListener, TabLayout.OnTabSelectedListener {

    public Toolbar doctorListToolbar;
    public TabLayout doctorListTabLayout;
    public TextView toolbarHeaderText;
    public ImageView doctorListBackImage;
    public LinearLayout profileDetailsLayout;
    public String currentFragment;
    private String type;
    private ScrollableViewPager doctorListViewPager;
    private Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!TextUtils.isEmpty((CharSequence) PreferenceHelper.getParam(this, Const.Params.LANGUAGE, ""))) {
            String languageToLoad = (String) PreferenceHelper.getParam(this, Const.Params.LANGUAGE, ""); // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config2 = new Configuration();
            config2.locale = locale;
            this.getResources().updateConfiguration(config2,
                    getResources().getDisplayMetrics());

        }
        setContentView(R.layout.activity_doctor_list);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        doctorListToolbar = (Toolbar) findViewById(R.id.tb_doctor_list);
        doctorListTabLayout = (TabLayout) findViewById(R.id.tl_doctor_list);
        doctorListViewPager = (ScrollableViewPager) findViewById(R.id.vp_doctor_list);
        setSupportActionBar(doctorListToolbar);
        toolbarHeaderText = (TextView) findViewById(R.id.tb_doctorList_header_text);
        getSupportActionBar().setTitle("");
        bundle = getIntent().getExtras();
        if (bundle != null && getIntent().getExtras().getString(Const.DOCTOR_FRAGMENT_TYPE) != null) {
            type = getIntent().getExtras().getString(Const.DOCTOR_FRAGMENT_TYPE);
            AndyUtils.appLog("FragmentType", type);
        }
        doctorListTabLayout.setOnTabSelectedListener(this);
        doctorListBackImage = (ImageView) findViewById(R.id.iv_doctor_list_back_icon);
        doctorListBackImage.setOnClickListener(this);
        if (type.equals(Const.DOCTOR_LIST)) {
            toolbarHeaderText.setText(getString(R.string.doctor_list));
        } else if (type.equals(Const.DOCTOR_NEAR_BY)) {
            toolbarHeaderText.setText(getString(R.string.clinic_nearby));
        }
        setWithViewPager(doctorListViewPager, type);
        doctorListTabLayout.setupWithViewPager(doctorListViewPager);
    }




    @Override
    public void onBackPressed() {

//        overridePendingTransition(R.anim.slide_out_right, R.anim.slide_in_left);
//        Intent intent=new Intent(DoctorListActivity.this,MainActivity.class);
//        startActivity(intent);
//        finish();
        super.onBackPressed();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_doctor_list_back_icon:
                onBackPressed();
                break;
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        doctorListViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void setWithViewPager(ViewPager pager, String type) {
        if (type.equals(Const.DOCTOR_LIST)) {
            doctorListViewPager.canScroll = true;
            DoctorListPagerAdapter pagerAdapter = new DoctorListPagerAdapter(getSupportFragmentManager());
            pagerAdapter.addItem(new DoctorOnlineConsultFragment(), getString(R.string.online_consult));
            pagerAdapter.addItem(new DoctorBookingFragment(), getString(R.string.booking_appointment));
            pager.setAdapter(pagerAdapter);
        } else if (type.equals(Const.DOCTOR_NEAR_BY)) {
            doctorListViewPager.canScroll = false;
            DoctorListPagerAdapter pagerAdapter = new DoctorListPagerAdapter(getSupportFragmentManager());
            pagerAdapter.addItem(new DoctorNearByOnlineConsultFragment(), getString(R.string.online_consult));
            pagerAdapter.addItem(new DoctorNearByBookingFragment(), getString(R.string.booking_appointment));
            pager.setAdapter(pagerAdapter);
            // to disable swipe
        }

    }
}
