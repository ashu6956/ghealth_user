package com.patient.ghealth.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.adapter.DoctorProfilePagerAdapter;
import com.patient.ghealth.adapter.GetCardsAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.fragment.DoctorAvailabilityDialogFragment;
import com.patient.ghealth.fragment.DoctorBookingProfileDetailsFragment;
import com.patient.ghealth.fragment.DoctorOnLineProfileDetailsFragment;
import com.patient.ghealth.model.CardDetails;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 9/22/2016.
 */
public class DoctorProfileActivity extends AppCompatActivity implements  View.OnClickListener, AsyncTaskCompleteListener {
    private TabLayout profileTabLayout;
    private TextView doctorName, doctorDegree1,doctorDegree2, doctorHelped, doctorExperience, betweenDistance;
    private ImageView doctorIcon;

    private ViewPager profileDoctorViewPager;
    private DoctorOnLineDetails doctorOnLineDetails;
    private String connectivityType = "";
    private ImageView backButton, doctorBackGroundImage;
    private TextView doctorClinicName, doctorClinicAddress, doctorConsultantFee, doctorConsult;
    private ImageView doctorLocationStaticMap,degree1Icon,degree2Icon, chat_icon, video_icon, call_icon,clinicIcon,addressIcon,consultedIcon,experienceIcon;
    private Bundle bundle;
    private TextView qualificationHeader,clinicHeader;
    private View qualificationDivider, helpedDivider;
    private LinearLayout helpedLayout;

    private List<DoctorOnLineDetails> doctorOnLineDetailsList;
    private StringBuilder doctorAddressStrongBuilder;
    private int clickedPosition = 0;
    private Dialog progressDialog, paymentDialog, doctorBookingDialog;
    DecimalFormat form = new DecimalFormat("0.00");
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("FragmentRequest", "onReceive");
            String notification = intent.getStringExtra("ashutosh");
            Log.d("My Push notification", notification);
            String message = intent.getStringExtra(Const.DOCTOR_INTENT_MESSAGE);
            try {
                JSONObject messageObj = new JSONObject(message);
                AndyUtils.appLog("JsonResponse", messageObj.toString());
                if (messageObj.getString("success").equals("true")) {
                    if (Integer.parseInt(messageObj.getString("status")) == 9) {
                        removeProgressDialog();
                        AndyUtils.showLongToast(messageObj.getString("message"), DoctorProfileActivity.this);
                    } else {

                        removeProgressDialog();
                        JSONObject jsonObject = messageObj.getJSONObject("data");
                        DoctorOnLineDetails onLineDetails = new DoctorOnLineDetails();
                        onLineDetails.setDoctorOnLineId(jsonObject.optString("doctor_id"));
                        onLineDetails.setDoctorPictureUrl(jsonObject.optString("doctor_picture"));
                        onLineDetails.setDoctorRating(jsonObject.optString("doctor_rating"));
                        onLineDetails.setDoctorName(jsonObject.optString("doctor_name"));
                        onLineDetails.setDoctorStatus(messageObj.optString("status"));
                        onLineDetails.setRequestId(jsonObject.optString("request_id"));
                        onLineDetails.setLatitude(jsonObject.optString("s_latitude"));
                        onLineDetails.setLongitude(jsonObject.optString("s_longitude"));
                        onLineDetails.setdLatitude(jsonObject.optString("d_latitude"));
                        onLineDetails.setdLongitude(jsonObject.optString("d_longitude"));
                        onLineDetails.setDoctorStatusTitle(messageObj.optString("title"));
                        onLineDetails.setBetweenDistance(jsonObject.optString("eta"));
                        onLineDetails.setEtaTime(jsonObject.optString("time"));
                        onLineDetails.setClinicAddress(jsonObject.optString("c_address"));
                        onLineDetails.setSourceAddress(jsonObject.optString(Const.SOURCE_ADDRESS));
                        Intent onDemandIntent = new Intent(DoctorProfileActivity.this, DoctorOnDemandDoctorActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(Const.DoctorOnDemandFragment, onLineDetails);
                        bundle.putString(Const.DOCTOR_REQUEST_STATUS, Const.REQUEST);
                        onDemandIntent.putExtras(bundle);
                        startActivity(onDemandIntent);
                        finish();
                    }
                } else {
                    AndyUtils.showLongToast(messageObj.getString("error_message"), DoctorProfileActivity.this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    private String paymentType = "", demandDoctorId = "";
    private List<CardDetails> cardDetailsList;
    private String requestId;
    private String currency="";

    public static String getGoogleMapThumbnail(double lati, double longi) {
        String staticMapUrl = "http://maps.google.com/maps/api/staticmap?center=" + lati + "," + longi + "&markers=" + lati + "," + longi + "&zoom=14&size=300x150&sensor=false";
        return staticMapUrl;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_profile);
        currency= (String) PreferenceHelper.getParam(this,Const.Params.CURRENCY,"");
        doctorBackGroundImage = (ImageView) findViewById(R.id.iv_doctor_profile_background);
        doctorName = (TextView) findViewById(R.id.tv_profile_doctor_name);
        doctorDegree1 = (TextView) findViewById(R.id.tv_profile_doctor_degree);
        doctorDegree2 = (TextView) findViewById(R.id.tv_profile_doctor_degree2);
        doctorHelped = (TextView) findViewById(R.id.tv_profile_doctor_helped);
        clinicIcon= (ImageView) findViewById(R.id.iv_profile_clinic);
        addressIcon= (ImageView) findViewById(R.id.iv_profile_address);
        consultedIcon= (ImageView) findViewById(R.id.iv_profile_consulted);
        experienceIcon= (ImageView) findViewById(R.id.iv_profile_experience);
        doctorExperience = (TextView) findViewById(R.id.tv_profile_doctor_experience);
        doctorIcon = (ImageView) findViewById(R.id.iv_profile_doctor_icon);
        backButton = (ImageView) findViewById(R.id.iv_profileBackButton);
        doctorClinicName = (TextView) findViewById(R.id.tv_profile_doctor_clinic_name);
        doctorClinicName = (TextView) findViewById(R.id.tv_profile_doctor_clinic_name);
        doctorConsultantFee = (TextView) findViewById(R.id.tv_profile_doctor_ConsultFee);
        doctorLocationStaticMap = (ImageView) findViewById(R.id.iv_profile_doctor_static_location);
        qualificationHeader= (TextView) findViewById(R.id.tv_qualification_header);
        helpedLayout = (LinearLayout) findViewById(R.id.ll_helped_experience);
        qualificationDivider=findViewById(R.id.view_qualification);
        helpedDivider =findViewById(R.id.view_helped);
        clinicHeader= (TextView) findViewById(R.id.tv_clinic_details_header);
        doctorClinicAddress = (TextView) findViewById(R.id.tv_profile_address);
        degree1Icon= (ImageView) findViewById(R.id.iv_profile_education);
        degree2Icon= (ImageView) findViewById(R.id.iv_profile_education2);
        chat_icon = (ImageView) findViewById(R.id.iv_profile_doctor_chat);
        video_icon = (ImageView) findViewById(R.id.iv_profile_doctor_video);
        call_icon = (ImageView) findViewById(R.id.iv_profile_doctor_call);
        doctorConsult = (TextView) findViewById(R.id.tv_profile_doctor_consult);
        betweenDistance = (TextView) findViewById(R.id.tv_profile_distance);
        backButton.setOnClickListener(this);
        bundle = getIntent().getExtras();
        if (bundle != null) {
            doctorOnLineDetails = (DoctorOnLineDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
            connectivityType = bundle.getString(Const.DOCTOR_TYPE);
            setDataOnViews(doctorOnLineDetails, connectivityType);
        }
    }

    private void setDataOnViews(DoctorOnLineDetails onLineDetails, String type) {
        demandDoctorId = doctorOnLineDetails.getDoctorOnLineId();
        AndyUtils.appLog("DemandDoctorId", demandDoctorId);

        if (type.equals(Const.ONLINE_CONSULT)) {
            doctorConsult.setText(getString(R.string.consult));
            try {
//                betweenDistance.setText(doctorOnLineDetails.getBetweenDistance());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1) {
                chat_icon.setVisibility(View.VISIBLE);
            }
            if (Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1) {
                call_icon.setVisibility(View.VISIBLE);
            }
            if (Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                video_icon.setVisibility(View.VISIBLE);
            }
            if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) && Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {
                    doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) +" "+getString(R.string.onwards_consultation_fee));

                } else if (Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {
                   doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) +" "+getString(R.string.onwards_consultation_fee));
                } else {
                    doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + " "+getString(R.string.onwards_consultation_fee));
                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee())) {

                    doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) +" "+getString(R.string.onwards_consultation_fee));

                } else {

                   doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) +" "+getString(R.string.onwards_consultation_fee));

                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {

                   doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) +" "+ getString(R.string.onwards_consultation_fee));

                } else {

                    doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + " "+getString(R.string.onwards_consultation_fee));

                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {

                    doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) + " "+getString(R.string.onwards_consultation_fee));

                } else {

                   doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + " "+getString(R.string.onwards_consultation_fee));

                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1) {
               doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) + " "+getString(R.string.onwards_consultation_fee));
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1) {
                doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + " "+getString(R.string.onwards_consultation_fee));
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + " "+getString(R.string.onwards_consultation_fee));
            }

            doctorConsult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bundle != null && doctorOnLineDetails != null) {
                        Intent consultIntent = new Intent(DoctorProfileActivity.this, DoctorConsultConnectivity.class);
                        consultIntent.putExtras(bundle);
                        startActivity(consultIntent);
                    }

                }
            });
        } else if (type.equals(Const.BOOKING)) {
            doctorConsult.setText(getString(R.string.booking));
            doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) +" "+ getString(R.string.onwards_consultation_fee));
            try {
//                betweenDistance.setText(doctorOnLineDetails.getBetweenDistance());
            } catch (Exception e) {
                e.printStackTrace();
            }
            doctorConsult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (doctorOnLineDetails != null) {
                        String doctorId = doctorOnLineDetails.getDoctorOnLineId();
                        AndyUtils.appLog("DoctorBookingFragment", "BookDoctorId" + doctorId);
                        DoctorAvailabilityDialogFragment availability = new DoctorAvailabilityDialogFragment();
                        PreferenceHelper.setParam(DoctorProfileActivity.this, Const.DOCTOR_ID, doctorId);
                        availability.show(getSupportFragmentManager(), "DoctorAvailabilityDialogFragment");
                    }

                }
            });
        } else if (type.equals(Const.ONDEMAND)) {
            doctorConsult.setText(getString(R.string.on_demand));
            doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + " "+getString(R.string.onwards_consultation_fee));
//            betweenDistance.setText(doctorOnLineDetails.getBetweenDistance());
            doctorConsult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDoctorBookingDialog();
                }
            });
        }
        if (!doctorOnLineDetails.getDoctor_clinic_image_first().equals("")) {
            Glide.with(this).load(doctorOnLineDetails.getDoctor_clinic_image_first()).into(doctorBackGroundImage);
        } else if (!doctorOnLineDetails.getDoctor_clinic_image_second().equals("")) {
            Glide.with(this).load(doctorOnLineDetails.getDoctor_clinic_image_second()).into(doctorBackGroundImage);
        } else if (!doctorOnLineDetails.getDoctor_clinic_image_third().equals("")) {
            Glide.with(this).load(doctorOnLineDetails.getDoctor_clinic_image_third()).into(doctorBackGroundImage);
        }

        doctorName.setText(doctorOnLineDetails.getDoctorName());
        if (doctorOnLineDetails.getDoctorPictureUrl().equals("")) {
            doctorIcon.setImageResource(R.drawable.profile);
        } else {
            Glide.with(this).load(doctorOnLineDetails.getDoctorPictureUrl()).into(doctorIcon);

        }
        StringBuilder builder1=new StringBuilder();
        if(!doctorOnLineDetails.getDegree1().equals("") && !doctorOnLineDetails.getEducation1().equals(""))
        {
            degree1Icon.setVisibility(View.VISIBLE);
            qualificationHeader.setVisibility(View.VISIBLE);
            qualificationDivider.setVisibility(View.VISIBLE);
            builder1.append(doctorOnLineDetails.getDegree1()+" "+getString(R.string.in)+" "+doctorOnLineDetails.getEducation1());
            doctorDegree1.setText(builder1.toString());
        }
        StringBuilder builder2=new StringBuilder();
        if(!doctorOnLineDetails.getDegree2().equals("") && !doctorOnLineDetails.getEducation2().equals(""))
        {
            degree2Icon.setVisibility(View.VISIBLE);
            qualificationDivider.setVisibility(View.VISIBLE);
            qualificationHeader.setVisibility(View.VISIBLE);
            builder2.append(doctorOnLineDetails.getDegree2()+" "+getString(R.string.in)+" "+doctorOnLineDetails.getEducation2());
            doctorDegree2.setText(builder2.toString());
        }

        if(!doctorOnLineDetails.getDoctorHelpedPeople().equals(""))
        {
            helpedLayout.setVisibility(View.VISIBLE);
            helpedDivider.setVisibility(View.VISIBLE);
            consultedIcon.setVisibility(View.VISIBLE);
            doctorHelped.setText(getString(R.string.consulted) + " " + doctorOnLineDetails.getDoctorHelpedPeople() + " " + getString(R.string.people));
        }
        if(!doctorOnLineDetails.getDoctorExperience().equals(""))
        {
            helpedDivider.setVisibility(View.VISIBLE);
            experienceIcon.setVisibility(View.VISIBLE);
            doctorExperience.setText(doctorOnLineDetails.getDoctorExperience() + " " + getString(R.string.year_experience));
        }


       if(onLineDetails.getDoctorClinicName()!=null && !onLineDetails.getDoctorClinicName().equals(""))
       {
           clinicHeader.setVisibility(View.VISIBLE);
           clinicIcon.setVisibility(View.VISIBLE);
           doctorClinicName.setText(onLineDetails.getDoctorClinicName());
       }
        if(onLineDetails.getClinicAddress()!=null && !onLineDetails.getClinicAddress().equals(""))
        {
            clinicHeader.setVisibility(View.VISIBLE);
            addressIcon.setVisibility(View.VISIBLE);
            doctorClinicAddress.setText(onLineDetails.getClinicAddress());
        }

        if (!doctorOnLineDetails.getdLatitude().equals("") && !doctorOnLineDetails.getdLongitude().equals("")) {
            Glide.with(this).load(getGoogleMapThumbnail(Double.valueOf(doctorOnLineDetails.getdLatitude()), Double.valueOf(doctorOnLineDetails.getdLongitude()))).into(doctorLocationStaticMap);
        }
    }



    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(Const.DOCTOR_REQUEST_STATUS);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
                filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_profileBackButton:
                onBackPressed();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void showDoctorBookingDialog() {
        doctorBookingDialog = new Dialog(this);
        doctorBookingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        doctorBookingDialog.setContentView(R.layout.dialog_doctor_booking_layout);
        ImageView doctorNext = (ImageView) doctorBookingDialog.findViewById(R.id.iv_dialog_on_demand_doctor);
        ImageView bookingNext = (ImageView) doctorBookingDialog.findViewById(R.id.iv_dialog_on_demand_booking);
        TextView needDoctorText = (TextView) doctorBookingDialog.findViewById(R.id.tv_on_demand_need);
        TextView doctorText = (TextView) doctorBookingDialog.findViewById(R.id.tv_doctor_on_demand_doctor);
        TextView bookingText = (TextView) doctorBookingDialog.findViewById(R.id.tv_doctor_on_demand_booking);
        ImageView doctorIcon = (ImageView) doctorBookingDialog.findViewById(R.id.iv_doctor_on_demand_doctor);
        ImageView bookingIcon = (ImageView) doctorBookingDialog.findViewById(R.id.iv_doctor_on_demand_calendar);
        RelativeLayout doctorLayout = (RelativeLayout) doctorBookingDialog.findViewById(R.id.rl_doctor_on_demand_doctor);
        RelativeLayout bookingLayout = (RelativeLayout) doctorBookingDialog.findViewById(R.id.rl_doctor_on_demand_booking);

        doctorIcon.setImageResource(R.drawable.stethoscope);
        doctorText.setText(getString(R.string.doctor));
        bookingText.setText(getString(R.string.booking));
        needDoctorText.setText(getString(R.string.need_doctor));
        doctorNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                getAddedCard();
            }
        });
        bookingNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                DoctorAvailabilityDialogFragment availability = new DoctorAvailabilityDialogFragment();
                PreferenceHelper.setParam(DoctorProfileActivity.this, Const.DOCTOR_ID, demandDoctorId);
                AndyUtils.appLog("DoctorOnDemand", "DoctorId" + demandDoctorId);
                availability.show(getSupportFragmentManager(), "DoctorAvailabilityDialogFragment");

            }
        });
        doctorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                getAddedCard();

            }
        });
        bookingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                DoctorAvailabilityDialogFragment availability = new DoctorAvailabilityDialogFragment();
                PreferenceHelper.setParam(DoctorProfileActivity.this, Const.DOCTOR_ID, demandDoctorId);
                AndyUtils.appLog("DoctorOnDemand", "DoctorId" + demandDoctorId);
                availability.show(getSupportFragmentManager(), "DoctorAvailabilityDialogFragment");
            }
        });

        doctorBookingDialog.show();
    }

    private void getAddedCard() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
//        AndyUtils.showSimpleProgressDialog(mContext, "Fetching All Cards...", false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.GET_ADDED_CARDS_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "GetAddedCardMap" + map);

        new HttpRequester(this, Const.GET, map, Const.ServiceCode.GET_ADDED_CARDS, this);


    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {

        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_ADDED_CARDS:
                AndyUtils.appLog("Ashutosh", "GetAddedCardResponse" + response);
                try {

                    cardDetailsList = new ArrayList<>();
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("cards");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject cardObject = jsonArray.getJSONObject(i);
                                CardDetails cardDetails = new CardDetails();
                                cardDetails.setCardId(cardObject.optString("id"));
                                cardDetails.setCardNumber(cardObject.optString("last_four"));
                                cardDetails.setIsDefault(cardObject.optString("is_default"));
                                cardDetails.setType(cardObject.optString("card_type"));
                                cardDetailsList.add(cardDetails);
                            }

                            if (doctorOnLineDetails != null) {
                                showPaymentDialog(cardDetailsList, doctorOnLineDetails);
                            }
                        }
                    } else {
                        if (doctorOnLineDetails != null) {
                            showPaymentDialog(cardDetailsList, doctorOnLineDetails);
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CREATE_REQUEST:
                AndyUtils.appLog("Ashutosh", "CreateRequestResponse" + response);
                try {

                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            JSONObject requestObject = jsonArray.getJSONObject(0);
                            requestId = requestObject.optString(Const.REQUEST_ID);
                            AndyUtils.appLog("RequestId", requestId);
                        }

                    } else {
//                        AndyUtils.removeProgressDialog();
                        if (progressDialog != null) {
                            progressDialog.cancel();
                        }
                        AndyUtils.showShortToast(jsonObject.getString("error_message"), DoctorProfileActivity.this);
                    }

                } catch (JSONException e) {
                    if (progressDialog != null) {
                        progressDialog.cancel();
                    }
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CREATE_CANCEL_REQUEST:
                try {
                    AndyUtils.appLog("Ashutosh", "CancelRequestResponse" + response);
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        AndyUtils.showShortToast(jsonObject.optString("message"), this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    private void showPaymentDialog(final List<CardDetails> cardList, final DoctorOnLineDetails onLineDetails) {
        paymentType = "";

        paymentDialog = new Dialog(this, R.style.DialogDocotrTheme);
        paymentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        paymentDialog.setContentView(R.layout.dialog_payment_mode_layout);
        final RecyclerView paymentRecyclerView = (RecyclerView) paymentDialog.findViewById(R.id.rv_card_details);
        paymentRecyclerView.setVisibility(View.GONE);
        final LinearLayout paymentLinearLayout = (LinearLayout) paymentDialog.findViewById(R.id.ll_no_card_added);
        paymentLinearLayout.setVisibility(View.GONE);
        final Button addCardButton = (Button) paymentDialog.findViewById(R.id.bn_payment_add_card);
        addCardButton.setVisibility(View.GONE);
        final ImageButton cashButton = (ImageButton) paymentDialog.findViewById(R.id.ib_payment_cash);
        final ImageButton cardButton = (ImageButton) paymentDialog.findViewById(R.id.ib_payment_card);
        TextView appointmentCharge = (TextView) paymentDialog.findViewById(R.id.tv_payment_appointment_charge);
        TextView doctorName = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_name);
        TextView clinicName = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_clinic_name);
        TextView clinicAddress = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_clinic_address);
        final Button paymentButton = (Button) paymentDialog.findViewById(R.id.bn_payment_confirm);
        doctorName.setText(onLineDetails.getDoctorName());
        clinicName.setText(onLineDetails.getDoctorClinicName());
        clinicAddress.setText(onLineDetails.getClinicAddress());
        appointmentCharge.setText(getString(R.string.charge) + " "+currency+form.format(Double.valueOf(onLineDetails.getDoctorChatConsultFee())) + " " + getString(R.string.per_appointment));
        paymentDialog.show();

        paymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (paymentType.equals("")) {
                    AndyUtils.showShortToast(getString(R.string.please_choose_payment_mode), DoctorProfileActivity.this);
                } else {
                    paymentDialog.cancel();
                    createDoctorRequest(onLineDetails);
                }
            }
        });
        cardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                paymentType = Const.CARD;
                cardButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.doctor_booked_bg));
                cashButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.doctor_available_bg));
                LinearLayoutManager layoutManager = new LinearLayoutManager(DoctorProfileActivity.this);
                paymentRecyclerView.setLayoutManager(layoutManager);

                if (cardList.size() > 0) {
                    paymentRecyclerView.setVisibility(View.VISIBLE);
                    GetCardsAdapter adapter = new GetCardsAdapter(DoctorProfileActivity.this, cardList);
                    paymentRecyclerView.setAdapter(adapter);
                } else {
                    paymentRecyclerView.setVisibility(View.GONE);
                    paymentLinearLayout.setVisibility(View.VISIBLE);
                    addCardButton.setVisibility(View.VISIBLE);
                    addCardButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            paymentDialog.cancel();
                            Intent intent = new Intent(DoctorProfileActivity.this, AddCardActivity.class);
                            startActivity(intent);
                        }
                    });
                    paymentButton.setEnabled(false);
                    paymentButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.disable_book_consult_bg));

                }

            }
        });
        cashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentType = Const.CASH;
                paymentRecyclerView.setVisibility(View.GONE);
                cardButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.doctor_available_bg));
                cashButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.doctor_booked_bg));
                paymentLinearLayout.setVisibility(View.GONE);
                addCardButton.setVisibility(View.GONE);
                paymentButton.setEnabled(true);
                paymentButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.book_consult_bg));
            }
        });

    }

    private void createDoctorRequest(DoctorOnLineDetails doctorDetails) {

        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), DoctorProfileActivity.this);
            return;
        }
//        if(isAdded()) {
//            AndyUtils.showSimpleProgressDialog(mContext, getString(R.string.requesting_doctor), false);
//        }
        showProgressDialog();
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_REQUEST);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.REQUEST_TYPE, String.valueOf(4));
        map.put(Const.Params.DOCTOR_ID, String.valueOf(demandDoctorId));
        map.put(Const.Params.S_LATITUDE, String.valueOf(doctorDetails.getdLatitude()));
        map.put(Const.Params.S_LONGITUDE, String.valueOf(doctorDetails.getdLongitude()));
        map.put(Const.Params.PAYMENT_MODE, paymentType);
        AndyUtils.appLog("Ashutosh", "GetRequestMap" + map);
        new HttpRequester(this, Const.POST, map, Const.ServiceCode.CREATE_REQUEST, this);
    }

    private void showProgressDialog() {
        progressDialog = new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_custom_progress_layout);
        final ProgressBar progressBar = (ProgressBar) progressDialog.findViewById(R.id.dialog_progressBar);
        Button cancelButton = (Button) progressDialog.findViewById(R.id.bn_cancel_progress_dialog);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.cancel();
                progressBar.setVisibility(View.GONE);
                cancelRequest();
            }
        });

        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void removeProgressDialog() {
        if (progressDialog != null) {
            progressDialog.cancel();
        }
    }

    private void cancelRequest() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_CANCEL_REQUEST_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.REQUEST_ID, String.valueOf(requestId));

        AndyUtils.appLog("Ashutosh", "CancelRequestMap" + map);

        new HttpRequester(this, Const.POST, map, Const.ServiceCode.CREATE_CANCEL_REQUEST, this);

    }


}
