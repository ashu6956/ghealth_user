package com.patient.ghealth.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.fragment.BookMarkFragment;
import com.patient.ghealth.fragment.ConsultHistoryFragment;
import com.patient.ghealth.fragment.DoctorsAvailabilityFragment;
import com.patient.ghealth.fragment.HealthFeedFragment;
import com.patient.ghealth.fragment.HomeFragment;
import com.patient.ghealth.fragment.MyPurchaseItemFragment;
import com.patient.ghealth.fragment.NotificationAnswerFragment;
import com.patient.ghealth.fragment.StoreCategoryFragment;
import com.patient.ghealth.fragment.UserSettingsFragment;
import com.patient.ghealth.fragment.ViewMessageHistoryFragment;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.model.InvoiceDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;
import com.splunk.mint.Mint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskCompleteListener {

    public Toolbar mainToolbar;
    public TextView toolbarHeader, iconTypeName;
    public ImageView searchImage, notificationImage,backbuttonImage, iconType, cart_ambulance_icon;
    public String sHeader;
    public TabLayout mainTabLayout;
    public LinearLayout layoutToolbarOptions, layoutHealthStoreCart;
    public String currentFragment = "", referenceFragment;
    public LinearLayout layoutSearchTopic;
    public EditText editSearchTopic;
    public String currentToolbarHeaderText;
    public ImageView appLogo;
    public int statusPosition = 0;
    public int countNotification = 0;
    public TextView countNotifiactionText, cartCount;
    public LinearLayout cartLayout;
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d("FragmentRequest", "onReceive");
            String notification = intent.getStringExtra("ashutosh");
            Log.d("My Push notification", notification);
            String message = intent.getStringExtra(Const.DOCTOR_INTENT_MESSAGE);
            try {
                JSONObject messageObj = new JSONObject(message);
                AndyUtils.appLog("JsonResponse", messageObj.toString());
                if (Integer.parseInt(messageObj.getString("status")) == 30) {
                    showBookingInvoice(messageObj, Const.BOOKING);
                } else if (Integer.parseInt(messageObj.optString("status")) == 567) {
                    countNotification = (Integer) PreferenceHelper.getParam(MainActivity.this, Const.NOTIFICATIONCOUNT, 0);
                    countNotifiactionText.setVisibility(View.VISIBLE);
                    countNotifiactionText.setText(countNotification + "");


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
    private Dialog mDialog;
    private int verifiedCount = 1;
    private Timer startTimer;
    private Handler timerHandler;
    private AlertDialog gpsAlertDialog, locationAlertDialog;
    private boolean isGpsDialogShowing = false, isRecieverRegistered = false;
    public BroadcastReceiver GpsChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final LocationManager manager = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                // do something
                removeGpsDialog();
            } else {
                // do something else
                if (isGpsDialogShowing) {
                    return;
                }
                ShowGpsDialog();
            }

        }
    };
    private LocationManager manager;
    private ViewPager mainViewPager;
    private Bundle bundle;
    private Dialog invoiceDialog, feedbackDialog;
    private String manufacturer = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!TextUtils.isEmpty((CharSequence) PreferenceHelper.getParam(this, Const.Params.LANGUAGE, ""))) {
            String languageToLoad = (String) PreferenceHelper.getParam(this, Const.Params.LANGUAGE, ""); // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config2 = new Configuration();
            config2.locale = locale;
            this.getResources().updateConfiguration(config2,
                    this.getResources().getDisplayMetrics());

        }
        setContentView(R.layout.activity_main);
        Mint.initAndStartSession(MainActivity.this, "96fe024c");
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        manufacturer = Build.MANUFACTURER;
        AndyUtils.appLog("Device Name", manufacturer);
        mainToolbar = (Toolbar) findViewById(R.id.tb_main);
        setSupportActionBar(mainToolbar);
        getSupportActionBar().setTitle("");
        getPermission();
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        toolbarHeader = (TextView) findViewById(R.id.tb_header_text);
        countNotifiactionText = (TextView) findViewById(R.id.tv_count_notification);
        sHeader = "<font color=#33b5e5>gHealth</font><font color=#000000>.com</font>";
//        sHeader = "<font color=#009688>Health Plus</font>";
        cartLayout = (LinearLayout) findViewById(R.id.rl_store_cart);
        toolbarHeader.setText(Html.fromHtml(sHeader));
        appLogo = (ImageView) findViewById(R.id.iv_app_logo);
        iconType = (ImageView) findViewById(R.id.iv_icon_type);
        iconTypeName = (TextView) findViewById(R.id.tv_icon_type_name);
        layoutSearchTopic = (LinearLayout) findViewById(R.id.ll_follow_more_topics);
        layoutHealthStoreCart = (LinearLayout) findViewById(R.id.ll_health_store_cart);
        editSearchTopic = (EditText) findViewById(R.id.et_search_text);
        searchImage = (ImageView) findViewById(R.id.iv_search);
        cart_ambulance_icon = (ImageView) findViewById(R.id.iv_cart);
        cartCount = (TextView) findViewById(R.id.tv_count_cart);
        layoutToolbarOptions = (LinearLayout) findViewById(R.id.ll_toolbar_options);
        notificationImage = (ImageView) findViewById(R.id.iv_notification);
        mainTabLayout = (TabLayout) findViewById(R.id.tl_main);
        backbuttonImage = (ImageView) findViewById(R.id.iv_main_back_icon);
        countNotification = (Integer) PreferenceHelper.getParam(MainActivity.this, Const.NOTIFICATIONCOUNT, 0);
        if (countNotification != 0) {
            countNotifiactionText.setVisibility(View.VISIBLE);
            countNotifiactionText.setText(countNotification + "");
        }
        setTabIcon(mainTabLayout);
        searchImage.setOnClickListener(this);
        notificationImage.setOnClickListener(this);
        backbuttonImage.setOnClickListener(this);
        cartLayout.setOnClickListener(this);
        if ((Integer) (PreferenceHelper.getParam(this, Const.NOTIFICATIONCOUNT, 0)) == 0) {
            countNotifiactionText.setVisibility(View.GONE); //
        } else {
            countNotifiactionText.setVisibility(View.VISIBLE);
            countNotifiactionText.setText(PreferenceHelper.getParam(this, Const.NOTIFICATIONCOUNT, 0) + "");
        }
        try {
            bundle = getIntent().getExtras();
            if (bundle != null) {
                if (bundle.getString(Const.INCOMING_BUNDLE).equals(Const.Params.LANGUAGE)) {
                    mainTabLayout.getTabAt(3).select();
                    getCartCount();
                    addFragment(new UserSettingsFragment(), false, "", Const.UserSettingsFragment, false);
                } else if (bundle.getString(Const.INCOMING_BUNDLE) != null && bundle.getString(Const.INCOMING_BUNDLE).equals(Const.SearchFragment)) {
                    backbuttonImage.setVisibility(View.VISIBLE);
                    appLogo.setVisibility(View.GONE);
                    layoutToolbarOptions.setVisibility(View.GONE);
                    mainTabLayout.getTabAt(0).select();
                    String specialitiesName = bundle.getString(Const.SpecialitiesName);
                    getCartCount();
                    addFragment(new DoctorsAvailabilityFragment(), false, specialitiesName, Const.DoctorAvailabilityFragment, true);
                } else if (bundle.getString(Const.INCOMING_BUNDLE) != null && bundle.getString(Const.INCOMING_BUNDLE).equals(Const.StoreProductListFragment)) {
                    cartLayout.setVisibility(View.VISIBLE);
                    backbuttonImage.setVisibility(View.GONE);
                    appLogo.setVisibility(View.VISIBLE);
                    toolbarHeader.setText(Html.fromHtml(sHeader));
                    layoutToolbarOptions.setVisibility(View.VISIBLE);
                    mainTabLayout.getTabAt(2).select();
                    getCartCount();
                    addFragment(new StoreCategoryFragment(), false, "", Const.StoreCategoryFragment, true);
                } else if (bundle.getString(Const.INCOMING_BUNDLE) != null && bundle.getString(Const.INCOMING_BUNDLE).equals(Const.NotificationAnswerFragment)) {
                    backbuttonImage.setVisibility(View.VISIBLE);
                    appLogo.setVisibility(View.GONE);
                    layoutToolbarOptions.setVisibility(View.GONE);
                    mainTabLayout.setVisibility(View.GONE);
                    backbuttonImage.setVisibility(View.VISIBLE);
                    getCartCount();
                    addFragment(new NotificationAnswerFragment(), false, "", Const.NotificationAnswerFragment, true);
                }
            } else {
                checkStatusRequestId();
                getCartCount();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void showAutoNotificationDialog() {
        if ((boolean) PreferenceHelper.getParam(MainActivity.this, Const.NOTIFICATION_FIRST_TIME, false) == false) {
            String xiaomi = "Xiaomi";
            final String CALC_PACKAGE_NAME = "com.miui.securitycenter";
            final String CALC_PACKAGE_ACITIVITY = "com.miui.permcenter.autostart.AutoStartManagementActivity";
            if (manufacturer.equalsIgnoreCase(xiaomi)) {
                new AlertDialog.Builder(this)
                        .setTitle("Enable Notification")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                try {
                                    PreferenceHelper.setParam(MainActivity.this, Const.NOTIFICATION_FIRST_TIME, true);
                                    Intent intent = new Intent();
                                    intent.setComponent(new ComponentName(CALC_PACKAGE_NAME, CALC_PACKAGE_ACITIVITY));
                                    startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    AndyUtils.appLog("MainActivity", "Failed to launch AutoStart Screen " + e);
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .show();
            }
        }
    }

    private void checkStatusRequestId() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.CHECK_STATUS_REQUEST_ID_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "CheckStatusRequestIDtMap" + map);

        new HttpRequester(this, Const.POST, map, Const.ServiceCode.CHECK_STATUS_REQUEST_ID, this);
    }

    private void checkStatusRequest(String requestId) {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.CHECK_STATUS_REQUEST_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.REQUEST_ID, String.valueOf(requestId));

        AndyUtils.appLog("Ashutosh", "CheckStatusRequestMap" + map);

        new HttpRequester(this, Const.POST, map, Const.ServiceCode.CHECK_STATUS_REQUEST, this);
    }


    private void getCartCount() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CART_COUNT_URL);

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(this, Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "CartCountMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(this, Const.GET, map, Const.ServiceCode.GET_CART_COUNT, this, headerMap);
    }


    @Override
    protected void onResume() {
        super.onResume();
        startTimer();
        IntentFilter filter = new IntentFilter(Const.DOCTOR_NEW_REQUEST_STATUS);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
                filter);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            ShowGpsDialog();
        } else {
            removeGpsDialog();
        }
        registerReceiver(GpsChangeReceiver, new IntentFilter(
                LocationManager.PROVIDERS_CHANGED_ACTION));
        isRecieverRegistered = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            startTimer.cancel();
            timerHandler.removeMessages(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTabIcon(final TabLayout tabLayout) {
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.home_selected));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.health_feed_unselected));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.shopping_bag_unselected));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.menu_unselected));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                AndyUtils.appLog("TabSelected Position", tab.getPosition() + "");
                switch (tab.getPosition()) {
                    case 0:
                        cartLayout.setVisibility(View.VISIBLE);
                        tab.setIcon(R.drawable.home_selected);
                        appLogo.setVisibility(View.VISIBLE);
                        backbuttonImage.setVisibility(View.GONE);
                        layoutToolbarOptions.setVisibility(View.VISIBLE);
                        toolbarHeader.setText(Html.fromHtml(sHeader));
                        addFragment(new HomeFragment(), false, "", Const.HOME_FRAGMENT, true);
                        break;
                    case 1:
                        cartLayout.setVisibility(View.VISIBLE);
                        tab.setIcon(R.drawable.health_feed_selected);
                        if (bundle != null && bundle.getString(Const.NotificationMessageFragment) != null && bundle.getString(Const.NotificationMessageFragment).equals(Const.NotificationMessageFragment)) {
                            backbuttonImage.setVisibility(View.VISIBLE);
                            appLogo.setVisibility(View.GONE);
                            layoutToolbarOptions.setVisibility(View.GONE);
                        } else {
                            backbuttonImage.setVisibility(View.GONE);
                            appLogo.setVisibility(View.VISIBLE);
                            layoutToolbarOptions.setVisibility(View.VISIBLE);
                        }

                        toolbarHeader.setText(Html.fromHtml(sHeader));
                        layoutToolbarOptions.setVisibility(View.VISIBLE);
                        addFragment(new HealthFeedFragment(), false, "", Const.HealthFeedsFragment, true);
                        break;
                    case 2:
                        cartLayout.setVisibility(View.VISIBLE);
                        tab.setIcon(R.drawable.shopping_bag_selected);
                        backbuttonImage.setVisibility(View.GONE);
                        appLogo.setVisibility(View.VISIBLE);
                        toolbarHeader.setText(Html.fromHtml(sHeader));
                        layoutToolbarOptions.setVisibility(View.VISIBLE);
                        addFragment(new StoreCategoryFragment(), false, "", Const.StoreCategoryFragment, true);
                        break;
                    case 3:
                        cartLayout.setVisibility(View.VISIBLE);
                        tab.setIcon(R.drawable.menu_selected);
                        if (currentFragment.equals(Const.BookMarkFragmentDescription)) {
//                            backbuttonImage.setVisibility(View.GONE);
//                            appLogo.setVisibility(View.VISIBLE);
//                            toolbarHeader.setText(Html.fromHtml(sHeader));
//                            layoutToolbarOptions.setVisibility(View.VISIBLE);
//                            addFragment(new UserSettingsFragment(), false, "", Const.UserSettingsFragment, true);
                        } else {
                            backbuttonImage.setVisibility(View.GONE);
                            appLogo.setVisibility(View.VISIBLE);
                            toolbarHeader.setText(Html.fromHtml(sHeader));
                            layoutToolbarOptions.setVisibility(View.VISIBLE);
                            addFragment(new UserSettingsFragment(), false, "", Const.UserSettingsFragment, true);

                        }
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        tab.setIcon(R.drawable.home_unselected);
                        break;
                    case 1:
                        tab.setIcon(R.drawable.health_feed_unselected);
                        break;
                    case 2:
                        tab.setIcon(R.drawable.shopping_bag_unselected);
                        break;
                    case 3:
                        tab.setIcon(R.drawable.menu_unselected);
                        break;
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        tab.setIcon(R.drawable.home_selected);
                        break;
                    case 1:
                        tab.setIcon(R.drawable.health_feed_selected);
                        break;
                    case 2:
                        tab.setIcon(R.drawable.shopping_bag_selected);
                        break;
//                    case 3:
//                        tab.setIcon(R.drawable.user_selected);
//                        break;
                    case 3:
                        tab.setIcon(R.drawable.menu_selected);
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_search:
                Intent intent = new Intent(this, SearchActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.iv_notification:
                Intent notificationActivity = new Intent(this, NotificationActivity.class);
                startActivity(notificationActivity);
                finish();
                break;
            case R.id.iv_main_back_icon:
                onBackPressed();
                break;
            case R.id.rl_store_cart:
                Intent storeIntent = new Intent(this, HealthStoreActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(Const.INCOMING_BUNDLE, Const.CartDetailsFragment);
                storeIntent.putExtras(bundle);
                startActivity(storeIntent);
                finish();
                break;
        }
    }


    private void startTimer() {
        timerHandler = new Handler();
        timerHandler.post(new Runnable() {
            @Override
            public void run() {
                startTimer = new Timer();
                startTimer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        checkToken();
                    }
                }, 0, 5000);
            }
        });

    }

    private void stopTimer() {
        if (timerHandler != null) {
            timerHandler.removeCallbacks(null);
        }
    }

    private void checkToken() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            //AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.REQUEST_CHECK_TOKEN_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "CheckTokenMap" + map);

        new HttpRequester(this, Const.POST, map, Const.ServiceCode.REQUEST_CHECK_TOKEN, this);

    }

    public void addFragment(Fragment fragment, boolean addToBackStack, String fragmentTitle,
                            String tag, boolean isAnimate) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
//        if (isAnimate) {
//            ft.setCustomAnimations(R.anim.slide_in_right,
//                    R.anim.slide_out_left, R.anim.slide_in_left,
//                    R.anim.slide_out_right);
//        }
        if (fragmentTitle == "") {
            toolbarHeader.setText(Html.fromHtml(sHeader));
        } else {
            toolbarHeader.setText(fragmentTitle);
        }
        if (addToBackStack) {
            ft.addToBackStack(tag);
        }
        ft.replace(R.id.contentFrame, fragment, tag);
        ft.commit();
    }


    @Override
    public void onBackPressed() {
        AndyUtils.appLog("CurrentFragment", currentFragment);

        if (currentFragment.equals(Const.DoctorAvailabilityFragment)) {
            backbuttonImage.setVisibility(View.GONE);
            appLogo.setVisibility(View.VISIBLE);
            layoutToolbarOptions.setVisibility(View.VISIBLE);
            addFragment(new HomeFragment(), false, "", Const.HOME_FRAGMENT, true);
        } else if (currentFragment.equals(Const.DoctorOnDemandFragment)) {
            addFragment(new DoctorsAvailabilityFragment(), false, (String) PreferenceHelper.getParam(this, Const.SPECIALITIES_DOCTOR_TYPE, ""), Const.DoctorAvailabilityFragment, true);
            layoutToolbarOptions.setVisibility(View.GONE);
            backbuttonImage.setVisibility(View.VISIBLE);
            appLogo.setVisibility(View.GONE);
            mainTabLayout.setVisibility(View.VISIBLE);
            layoutSearchTopic.setVisibility(View.GONE);
        } else if (currentFragment.equals(Const.FollowMoreTopicsFragment)) {
            addFragment(new HealthFeedFragment(), false, "", Const.HealthFeedsFragment, true);
            layoutToolbarOptions.setVisibility(View.VISIBLE);
            backbuttonImage.setVisibility(View.GONE);
            appLogo.setVisibility(View.VISIBLE);
            mainTabLayout.setVisibility(View.VISIBLE);
            toolbarHeader.setText(Html.fromHtml(sHeader));
            layoutSearchTopic.setVisibility(View.GONE);
        } else if (currentFragment.equals(Const.HealthFeedsDescriptionFragment)) {
            layoutToolbarOptions.setVisibility(View.VISIBLE);
            backbuttonImage.setVisibility(View.GONE);
            appLogo.setVisibility(View.VISIBLE);
            addFragment(new HealthFeedFragment(), false, "", Const.HealthFeedsFragment, true);
        } else if (currentFragment.equals(Const.AskAQuestionFragment)) {
            addFragment(new HealthFeedFragment(), false, "", Const.HealthFeedsFragment, true);
            layoutToolbarOptions.setVisibility(View.VISIBLE);
            backbuttonImage.setVisibility(View.GONE);
            appLogo.setVisibility(View.VISIBLE);
            mainTabLayout.setVisibility(View.VISIBLE);
            toolbarHeader.setText(Html.fromHtml(sHeader));
            layoutSearchTopic.setVisibility(View.GONE);

        } else if (currentFragment.equals(Const.UserProfileFragment) || currentFragment.equals(Const.AddCardFragment) ||
                currentFragment.equals(Const.ConsultHistoryFragment) || currentFragment.equals(Const.PaymentHistoryFragment) ||
                currentFragment.equals(Const.ReportIssueFragment) || currentFragment.equals(Const.InviteFriendFamilyFragment)
                || currentFragment.equals(Const.MyScheduleFragment) || currentFragment.equals(Const.AddFamilyFragment) ||
                currentFragment.equals(Const.BookMarkFragment) || currentFragment.equals(Const.NotificationAnswerFragment) || currentFragment.equals(Const.MyPurchaseItemFragment) ||
                currentFragment.equals(Const.ChangePassword)) {
            layoutToolbarOptions.setVisibility(View.VISIBLE);
            backbuttonImage.setVisibility(View.GONE);
            appLogo.setVisibility(View.VISIBLE);
            mainTabLayout.setVisibility(View.VISIBLE);
            addFragment(new UserSettingsFragment(), false, "", Const.UserSettingsFragment, true);
        } else if (currentFragment.equals(Const.AmbulanceOnDemandFragment)) {
            mainTabLayout.setVisibility(View.VISIBLE);
            layoutToolbarOptions.setVisibility(View.VISIBLE);
            backbuttonImage.setVisibility(View.GONE);
            appLogo.setVisibility(View.VISIBLE);
            addFragment(new HomeFragment(), false, "", Const.HOME_FRAGMENT, true);
        } else if (currentFragment.equals(Const.BookMarkFragmentDescription)) {
            layoutToolbarOptions.setVisibility(View.GONE);
            backbuttonImage.setVisibility(View.VISIBLE);
            appLogo.setVisibility(View.GONE);
            mainTabLayout.getTabAt(3).select();
            addFragment(new BookMarkFragment(), false, "BookMark", Const.BookMarkFragment, true);

        } else if (currentFragment.equals(Const.VideoConsultHistoryFragment)) {
            layoutToolbarOptions.setVisibility(View.GONE);
            backbuttonImage.setVisibility(View.VISIBLE);
            appLogo.setVisibility(View.GONE);
            mainTabLayout.setVisibility(View.GONE);
            ConsultHistoryFragment historyFragment = new ConsultHistoryFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Const.INCOMING_BUNDLE, Const.VideoConsultHistoryFragment);
            historyFragment.setArguments(bundle);
            addFragment(historyFragment, false, "Consult History", Const.ConsultHistoryFragment, true);
        } else if (currentFragment.equals(Const.ChatConsultHistoryFragment)) {
            layoutToolbarOptions.setVisibility(View.GONE);
            backbuttonImage.setVisibility(View.VISIBLE);
            appLogo.setVisibility(View.GONE);
            mainTabLayout.setVisibility(View.GONE);
            ConsultHistoryFragment historyFragment = new ConsultHistoryFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Const.INCOMING_BUNDLE, Const.ChatConsultHistoryFragment);
            historyFragment.setArguments(bundle);
            addFragment(historyFragment, false, "Consult History", Const.ConsultHistoryFragment, true);
        } else if (currentFragment.equals(Const.FullScreenImageFragment)) {
            backbuttonImage.setVisibility(View.VISIBLE);
            appLogo.setVisibility(View.GONE);
            mainTabLayout.setVisibility(View.GONE);
            ViewMessageHistoryFragment viewMessageHistoryFragment = new ViewMessageHistoryFragment();
            Bundle bundle = new Bundle();
            bundle.putString("name", viewMessageHistoryFragment.name);
            bundle.putString("request_id", viewMessageHistoryFragment.request_id);
            bundle.putString("picture", viewMessageHistoryFragment.picture);
            bundle.putString(Const.INCOMING_BUNDLE, viewMessageHistoryFragment.type);
            viewMessageHistoryFragment.setArguments(bundle);
            addFragment(viewMessageHistoryFragment, false, "Chat History", Const.ViewMessageHistoryFragment, true);
        } else if (currentFragment.equals(Const.ORDER_DETAILS_FRAGMENT)) {
            addFragment(new MyPurchaseItemFragment(), false, getString(R.string.my_purchase_item), Const.MyPurchaseItemFragment, true);
            layoutToolbarOptions.setVisibility(View.GONE);
            backbuttonImage.setVisibility(View.VISIBLE);
            mainTabLayout.setVisibility(View.GONE);
            appLogo.setVisibility(View.GONE);
        } else if (currentFragment.equals(Const.HOME_FRAGMENT) || currentFragment.equals(Const.HealthFeedsFragment) || currentFragment.equals(Const.StoreCategoryFragment)
                || currentFragment.equals(Const.MyMedicalFragmenmt) || currentFragment.equals(Const.UserSettingsFragment)) {
            super.onBackPressed();
        }

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.CHECK_STATUS_REQUEST:
                AndyUtils.appLog("Ashutosh", "CheckStatusResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        if (Integer.parseInt(jsonObject.getString("status")) == 5 || Integer.parseInt(jsonObject.getString("status")) == 6) {
                            showBookingInvoice(jsonObject, Const.ONLINE_CONSULT);
                        } else if (Integer.parseInt(jsonObject.getString("status")) == 30) {
                            showBookingInvoice(jsonObject, Const.BOOKING);
                        } else if (Integer.parseInt(jsonObject.getString("status")) == 15) //for Video Invoice
                        {
                            showOnLineBookingInvoice(jsonObject);
                        } else if (Integer.parseInt(jsonObject.getString("status")) == 12) // for video reconnecting
                        {
                            new PreferenceHelper(this).putRequestId(jsonObject.getString("request_id"));
                            Intent videoIntent = new Intent(this, VideoCallActivity.class);
                            startActivity(videoIntent);
                            finish();
                        } else if (Integer.parseInt(jsonObject.getString("status")) == 11) // for voice reconnecting
                        {
                            new PreferenceHelper(this).putRequestId(jsonObject.getString("request_id"));
                            Intent voiceIntent = new Intent(this, VoiceCallActivity.class);
                            startActivity(voiceIntent);
                            finish();
                        } else if (Integer.parseInt(jsonObject.getString("status")) == 10) // for chat reconnecting
                        {
                            new PreferenceHelper(this).putRequestId(jsonObject.getString("request_id"));
                            Intent chatIntent = new Intent(this, MessageActivity.class);
                            startActivity(chatIntent);
                            finish();
                        } else if (Integer.parseInt(jsonObject.getString("status")) == 14) // for VoiceCall Invoice
                        {
                            showOnLineBookingInvoice(jsonObject);
                        } else if (Integer.parseInt(jsonObject.getString("status")) == 13) //for Chat Invoice
                        {
                            showOnLineBookingInvoice(jsonObject);

                        } else if (Integer.parseInt(jsonObject.getString("status")) == 1 || Integer.parseInt(jsonObject.getString("status")) == 2 || Integer.parseInt(jsonObject.getString("status")) == 3 || Integer.parseInt(jsonObject.getString("status")) == 4) {
                            JSONObject requestObject = jsonObject.getJSONObject("data");
                            DoctorOnLineDetails onLineDetails = new DoctorOnLineDetails();
                            onLineDetails.setDoctorOnLineId(requestObject.getString("doctor_id"));
                            onLineDetails.setDoctorPictureUrl(requestObject.getString("doctor_picture"));
                            onLineDetails.setDoctorRating(requestObject.getString("doctor_rating"));
                            onLineDetails.setDoctorName(requestObject.getString("doctor_name"));
                            onLineDetails.setDoctorStatus(jsonObject.getString("status"));
                            onLineDetails.setRequestId(requestObject.getString("request_id"));
                            onLineDetails.setLatitude(requestObject.getString("s_latitude"));
                            onLineDetails.setLongitude(requestObject.getString("s_longitude"));
                            onLineDetails.setdLatitude(requestObject.getString("d_latitude"));
                            onLineDetails.setdLongitude(requestObject.getString("d_longitude"));
                            onLineDetails.setBetweenDistance(requestObject.optString("eta"));
                            onLineDetails.setEtaTime(requestObject.optString("time"));
                            onLineDetails.setClinicAddress(requestObject.optString("c_address"));
                            onLineDetails.setSourceAddress(requestObject.optString(Const.SOURCE_ADDRESS));
                            Intent onDemandIntent = new Intent(MainActivity.this, DoctorOnDemandDoctorActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(Const.DoctorOnDemandFragment, onLineDetails);
                            bundle.putString(Const.DOCTOR_REQUEST_STATUS, Const.REQUEST);
                            onDemandIntent.putExtras(bundle);
                            startActivity(onDemandIntent);
                            finish();
//                            doctorFragment.setArguments(bundle);
//                            addFragment(doctorFragment, false, getString(R.string.doctor_ondemand), Const.DoctorOnDemandDoctorFragment, false);
                        }
                    } else {
                        AndyUtils.showLongToast(jsonObject.getString("error_message"), this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                }
                break;
            case Const.ServiceCode.CHECK_STATUS_REQUEST_ID:
                AndyUtils.appLog("Ashutosh", "CHECK_STATUS_REQUEST_ID_Response" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        String requestId = jsonObject.optString("request_id");
                        if (requestId.equals(" ")) {
                            addFragment(new HomeFragment(), false, "", Const.HOME_FRAGMENT, true);
                        } else {
                            checkStatusRequest(requestId);
                        }
                    } else if (jsonObject.optString("success").equals("false")) {
                        if (jsonObject.optString("error_message").equalsIgnoreCase(Const.INVALID_TOKEN)) {
                            AndyUtils.showShortToast("You have logged in other device", this);
                            PreferenceHelper.setParam(this, Const.LOGIN_FIRST_TIME, false);
                            Intent homeIntent = new Intent(this, LoginActivity.class);
                            startActivity(homeIntent);
                            finish();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.REQUEST_CHECK_TOKEN:
                AndyUtils.appLog("Ashutosh", "CheckTokenResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        int emailVerified = Integer.parseInt(jsonObject.optString(Const.Params.EMAIL_VERIFICATION));
                        if (emailVerified == 1) {
                            if (mDialog != null && mDialog.isShowing()) {
                                mDialog.cancel();
                            }

                        } else if (emailVerified == 0) {
                            if (verifiedCount == 1) {
                                showDialogEmailNotVerified();
                                verifiedCount++;
                            }

                        }
                    } else if (jsonObject.optString("success").equals("false")) {
                        if (jsonObject.optString("error_message").equalsIgnoreCase(Const.INVALID_TOKEN)) {
                            stopTimer();
                            AndyUtils.showShortToast("You have logged in other device", this);
                            PreferenceHelper.setParam(this, Const.LOGIN_FIRST_TIME, false);
                            Intent homeIntent = new Intent(this, LoginActivity.class);
                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(homeIntent);
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_CART_COUNT:
                AndyUtils.appLog("Ashutosh", "CartCountResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        JSONObject mainObj = jsonObject.optJSONObject(Const.RESPONSE);
                        int count = mainObj.optInt("count");
                        cartCount.setText(count + "");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


        }
    }

    private void ShowGpsDialog() {

        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(
                this);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle("GPS is Off!")
                .setMessage("Please Turn On GPS")
                .setPositiveButton("Enable",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                removeGpsDialog();
                            }
                        })

                .setNegativeButton("Exit",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                removeGpsDialog();
                                finish();
                            }
                        });
        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.show();
    }

    private void removeGpsDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;

        }
    }


    @SuppressLint("NewApi")
    private void getPermission() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion > android.os.Build.VERSION_CODES.LOLLIPOP) {

            String[] permissions_dummy = new String[7];
            int i = 0;

            String permission = "android.permission.ACCESS_COARSE_LOCATION";
            int res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;
            }
            permission = "android.permission.READ_CONTACTS";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;


            }
            permission = "android.permission.WRITE_EXTERNAL_STORAGE";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;


            }
            permission = "android.permission.ACCESS_FINE_LOCATION";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;


            }
            permission = "android.permission.CAMERA";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;
            }
            permission = "android.permission.GET_ACCOUNTS";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;

            }
            permission = "android.permission.RECORD_AUDIO";
            res = checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;

            }
//
//            permission = "android.permission.READ_SMS";
//            res = checkCallingOrSelfPermission(permission);
//            if (res != PackageManager.PERMISSION_GRANTED) {
//
//
//                permissions_dummy[i] = permission;
//                i = i + 1;
//
//
//            }


            String[] permissions = new String[i];

            for (int j = 0; j < i; j++) {

                permissions[j] = permissions_dummy[j];

            }


            int yourRequestId = 1;
            if (i != 0) {


                // Do something for lollipop and above versions
                requestPermissions(permissions, yourRequestId);
            }

        }

    }

    private void showBookingInvoice(JSONObject jsonObject, String invoiceType) {
        InvoiceDetails invoiceDetails = new InvoiceDetails();
        JSONArray invoiceJsonArray = null;
        try {
            invoiceJsonArray = jsonObject.getJSONArray("invoice");
            JSONObject invoiceJsonObject = invoiceJsonArray.getJSONObject(0);
            invoiceDetails.setInvoiceDoctorId(Integer.parseInt(invoiceJsonObject.getString("doctor_id")));
            invoiceDetails.setBasePrice(invoiceJsonObject.optString("base_price"));
            invoiceDetails.setPaymentMode(invoiceJsonObject.optString("payment_mode"));
            invoiceDetails.setTotalPrice(invoiceJsonObject.optString("total"));
            invoiceDetails.setMedicineFee(invoiceJsonObject.optString("medicine_fee"));
            invoiceDetails.setReferralBonus(invoiceJsonObject.optString("referral_bonus"));
            invoiceDetails.setPromoBonus(invoiceJsonObject.optString("promo_bonus"));
            invoiceDetails.setTotalTime(invoiceJsonObject.optString("total_time"));
            invoiceDetails.setTotalDistance(invoiceJsonObject.optString("distance"));
            invoiceDetails.setInvoiceRequestId(jsonObject.optString("request_id"));
            invoiceDetails.setTreatmentFee(invoiceJsonObject.optString("time_price"));
            invoiceDetails.setInvoiceDoctorStatus(jsonObject.optString("status"));
            invoiceDetails.setInvoiceDoctorPictureUrl(invoiceJsonObject.optString("doctor_picture"));
            invoiceDetails.setInvoiceDoctorName(invoiceJsonObject.optString("doctor_name"));
            invoiceDetails.setDistance(invoiceJsonObject.optString("distance"));
            invoiceDetails.setDistanceCost(invoiceJsonObject.optString("distance_cost"));
            invoiceDetails.setPerKm(invoiceJsonObject.optString("per_km"));
            invoiceDetails.setInvoiceType(invoiceType);
            Bundle invoiceBundle = new Bundle();
            invoiceBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, invoiceDetails);
            invoiceBundle.putString(Const.DOCTOR_CONSULT_CONNECTIVITY, "normal");
            Intent feedbackIntent = new Intent(this, FeedBackActivity.class);
            feedbackIntent.putExtras(invoiceBundle);
            startActivity(feedbackIntent);
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void showOnLineBookingInvoice(JSONObject jsonObject) {
        InvoiceDetails invoiceDetails = new InvoiceDetails();
        JSONArray invoiceJsonArray = null;
        try {
            invoiceJsonArray = jsonObject.getJSONArray("invoice");
            JSONObject invoiceJsonObject = invoiceJsonArray.getJSONObject(0);
            invoiceDetails.setInvoiceDoctorId(Integer.parseInt(invoiceJsonObject.optString("doctor_id")));
            invoiceDetails.setBasePrice(invoiceJsonObject.optString("base_price"));
            invoiceDetails.setPaymentMode(invoiceJsonObject.optString("payment_mode"));
            invoiceDetails.setTotalPrice(invoiceJsonObject.optString("total"));
            invoiceDetails.setMedicineFee(invoiceJsonObject.optString("medicine_fee"));
            invoiceDetails.setReferralBonus(invoiceJsonObject.optString("referral_bonus"));
            invoiceDetails.setPromoBonus(invoiceJsonObject.optString("promo_bonus"));
            invoiceDetails.setTotalTime(invoiceJsonObject.optString("total_time"));
            invoiceDetails.setTotalDistance(invoiceJsonObject.optString("distance"));
            invoiceDetails.setInvoiceRequestId(jsonObject.optString("request_id"));
            invoiceDetails.setTreatmentFee(invoiceJsonObject.optString("time_price"));
            invoiceDetails.setInvoiceDoctorStatus(jsonObject.optString("status"));
            invoiceDetails.setInvoiceDoctorPictureUrl(invoiceJsonObject.optString("doctor_picture"));
            invoiceDetails.setInvoiceDoctorName(invoiceJsonObject.optString("doctor_name"));
            Bundle invoiceBundle = new Bundle();
            invoiceBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, invoiceDetails);
            invoiceBundle.putString(Const.DOCTOR_CONSULT_CONNECTIVITY, "normal");
            Intent feedbackIntent = new Intent(this, FeedBackOnlineActivity.class);
            feedbackIntent.putExtras(invoiceBundle);
            startActivity(feedbackIntent);
            finish();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void showDialogEmailNotVerified() {
        mDialog = new Dialog(this);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.patient_approve_dialog);
        mDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setContentView(R.layout.patient_approve_dialog);
        mDialog.setCancelable(false);
        TextView description = (TextView) mDialog.findViewById(R.id.tv_email_verification_text);
        description.setText(getString(R.string.email_not_verified) + "\n" + getString(R.string.please_verify_email));
        ImageView tvApprovedClose = (ImageView) mDialog
                .findViewById(R.id.tvApprovedClose);
        tvApprovedClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mDialog.dismiss();
                finishAffinity();
            }
        });
        mDialog.setCancelable(false);
        mDialog.show();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            startTimer.cancel();
            timerHandler.removeMessages(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        if (isRecieverRegistered) {
            unregisterReceiver(GpsChangeReceiver);
        }
    }
}