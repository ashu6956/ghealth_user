package com.patient.ghealth.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.callback.BitmapAjaxCallback;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.patient.ghealth.R;
import com.patient.ghealth.adapter.SpinnerAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.location.LocationHelper;
import com.patient.ghealth.model.SocialMediaProfile;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.networking.MultiPartRequester;
import com.patient.ghealth.realmDB.PatientProfile;
import com.patient.ghealth.realmDB.RealmDBController;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;
import com.patient.ghealth.utils.ReadFile;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import io.realm.Realm;

public class RegisterActivity extends AppCompatActivity implements AsyncTaskCompleteListener, View.OnClickListener, LocationHelper.OnLocationReceived {


    private static final String TAG = "RegisterActivity";
    private static TextView dobEdit;
    private static String sDob = "";
    private static int yy, mm, dd;
    EditText firstNameEdit, lastNameEdit, emailIdEdit, passwordEdit, otpEdit;
    Button registerButton, requestOtp;
    ImageView backButton;
    private String sFirstName, sLastName, sEmailId, sPassword, sCurrency, sLanguage, sTimeZone, sSocial_unique_id, sPictureUrl, sOtp, social_security_no;
    private ImageView profileImage;
    private String registrationType = Const.MANUAL;
    private String filePath = "";
    private Uri uri;
    private AQuery aQuery;
    private Toolbar registerToolbar;
    private RadioGroup genderRadioGroup;
    private String sGender = "", sPhoneNumber;
    private Spinner nationality, countryCode, currency, language;
    private EditText phoneEdit, et_social_security;
    private String sCountryCode, sNationality;
    private ImageButton vis_pass;
    private boolean isclicked = false;
    private ArrayList<String> countryCodes, countryCodesIso;
    private LinearLayout passworedLayout;
    private Bundle firstTimeBundle;
    private LocationHelper locationHelper;
    private File cameraFile;
    private String accessToken = "", country = "";
    private String[] languages = {" ", "en", "zh", "th", "in", "vi", "my", "fa", "ja"};
    private String selectedLanguage = "";
    private int selectedLanguagePosition = -1;
    private String[] languageArray = {"--Select Language--", "English (en)", "Chinese (zh)", "Thai (th)", "Bahasa Indonesia  (in)", "Vietnamese (vi)", "Burmese (my)", "Farsi (fa)", "Japanese (ja)"};

    private String[] currencyArray = {"--Select Currency--", "MYR", "USD", "CNY", "SGD", "IDR", "VND", "MMK", "IRR", "JPY", "THB"};
    private TextView terms, subTerms, privacy;
    private CheckBox check_terms_condition;


    public static String getCountryName(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && !addresses.isEmpty()) {
                return addresses.get(0).getCountryName();
            }
        } catch (IOException ioe) {
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        locationHelper = new LocationHelper(this);
        locationHelper.setLocationReceivedLister(this);
        setContentView(R.layout.activity_register);
        countryCodes = new ArrayList<>();
        countryCodesIso = new ArrayList<>();
        AndyUtils.generateKeyHAsh(this);
        registerToolbar = (Toolbar) findViewById(R.id.tb_register);
        setSupportActionBar(registerToolbar);
        getSupportActionBar().setTitle("");
        passworedLayout = (LinearLayout) findViewById(R.id.ll_password);
        firstNameEdit = (EditText) findViewById(R.id.et_register_first_name);
        lastNameEdit = (EditText) findViewById(R.id.et_register_last_name);
        dobEdit = (TextView) findViewById(R.id.tv_register_dob);
        emailIdEdit = (EditText) findViewById(R.id.et_register_your_email);
        passwordEdit = (EditText) findViewById(R.id.et_register_your_password);
        otpEdit = (EditText) findViewById(R.id.et_register_otp);
        registerButton = (Button) findViewById(R.id.bn_register);
        backButton = (ImageView) findViewById(R.id.iv_register_backButton);
        genderRadioGroup = (RadioGroup) findViewById(R.id.rg_register);
        countryCode = (Spinner) findViewById(R.id.sp_registration_country_code);
        nationality = (Spinner) findViewById(R.id.sp_registration_nationality);
        currency = (Spinner) findViewById(R.id.sp_currency);
        language = (Spinner) findViewById(R.id.sp_language);
        check_terms_condition = (CheckBox) findViewById(R.id.check_terms_condition);
        phoneEdit = (EditText) findViewById(R.id.et_register_mobile_number);
        et_social_security = (EditText) findViewById(R.id.et_social_security);
        vis_pass = (ImageButton) findViewById(R.id.register_vis_pass);
        requestOtp = (Button) findViewById(R.id.bn_request_otp);
        firstTimeBundle = getIntent().getExtras();
        Log.v("mahi", "country " + Locale.getDefault().getCountry());

        terms = (TextView) findViewById(R.id.terms);
        terms.setText(Html.fromHtml("I have read and agreed to the" + " " + "<a href=\"https://ghealth.net/userterms\">Terms of Service</a>," + " " + "<a href=\"https://ghealth.net/userprivacy\">Privacy Policy</a>" + " and " + "  " + "<a href=\"https://ghealth.net/usersubterms\">Subscriber Terms</a>"));
        terms.setMovementMethod(LinkMovementMethod.getInstance());
        privacy = (TextView) findViewById(R.id.privacy);
        privacy.setText(Html.fromHtml("," + " " + "<a href=\"https://ghealth.net/userprivacy\">Privacy & policy</a>" + "  and"));
        privacy.setMovementMethod(LinkMovementMethod.getInstance());
        subTerms = (TextView) findViewById(R.id.subTerms);
        subTerms.setText(Html.fromHtml(" " + "<a href=\"https://ghealth.net/usersubterms\">Subscription terms.</a> "));
        subTerms.setMovementMethod(LinkMovementMethod.getInstance());
        setDataOnSpinner();
        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_register_male) {
                    sGender = "male";
                } else if (checkedId == R.id.rb_register_female) {
                    sGender = "female";
                }
            }
        });
        registerButton.setOnClickListener(this);
        backButton.setOnClickListener(this);
        dobEdit.setOnClickListener(this);
        requestOtp.setOnClickListener(this);
        profileImage = (ImageView) findViewById(R.id.iv_register_user_icon);
        profileImage.setOnClickListener(this);
        aQuery = new AQuery(this);
        if (firstTimeBundle != null) {
            SocialMediaProfile mediaProfile = (SocialMediaProfile) firstTimeBundle.getSerializable(Const.LOGIN_FIRST_TIME);
            if (mediaProfile != null) {
                registrationType = mediaProfile.getLoginType();
                setSocialDetailsOnView(mediaProfile);
                AndyUtils.appLog("RegistrationType", registrationType);
            }
        }
        passwordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                vis_pass.setVisibility(View.VISIBLE);
                AndyUtils.appLog("RegisterActivity", "onTextChanged");
                vis_pass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isclicked == false) {
                            AndyUtils.appLog("RegisterActivitytrue", "onTextChanged");
                            passwordEdit.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            isclicked = true;
                            passwordEdit.setSelection(passwordEdit.getText().length());
                            vis_pass.setVisibility(View.VISIBLE);

                        } else {
                            AndyUtils.appLog("RegisterActivityfalse", "onTextChanged");
                            isclicked = false;
                            passwordEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                            vis_pass.setVisibility(View.GONE);
                            passwordEdit.setSelection(passwordEdit.getText().length());

                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

    }

    private void generateOtp(String countryCode, String number) {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showShortToast(getResources().getString(R.string.no_internet), this);
            return;
        }
        AndyUtils.showSimpleProgressDialog(this, getString(R.string.request_otp), false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.GENERATE_OTP_URL);
        map.put(Const.Params.PHONE, number);
        map.put(Const.Params.PHONE_COUNTRY_CODE, countryCode);

        AndyUtils.appLog("Ashutosh", "GenerateOtpMap" + map.toString());

        new HttpRequester(this, Const.POST, map, Const.ServiceCode.GENERATE_OTP, this);
    }

    private void setSocialDetailsOnView(SocialMediaProfile mediaProfile) {
        aQuery.id(profileImage).image(mediaProfile.getPictureUrl(), true, true,
                300, 300, new BitmapAjaxCallback() {
                    @Override
                    public void callback(String url, ImageView iv, Bitmap bm,
                                         AjaxStatus status) {
                        AndyUtils.appLog(TAG, "URL FROM AQUERY::" + url);
                        filePath = aQuery.getCachedFile(url).getPath();
                        AndyUtils.appLog(TAG, "URL path FROM AQUERY::" + url + " " + filePath);
                        iv.setImageBitmap(bm);
                    }
                });

        sSocial_unique_id = mediaProfile.getSocialUniqueId();
        firstNameEdit.setText(mediaProfile.getFirstName());
        lastNameEdit.setText(mediaProfile.getLastName());
        emailIdEdit.setText(mediaProfile.getEmailId());
        accessToken = mediaProfile.getAccessToken();
        passwordEdit.setVisibility(View.GONE);
        passworedLayout.setVisibility(View.GONE);
    }


    private void setDataOnSpinner() {
        ArrayAdapter<String> countryCodeAdapter = new ArrayAdapter<String>(this, R.layout.view_spinner_layout_item, parseCountryCodes());
        countryCodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryCode.setAdapter(countryCodeAdapter);
        TelephonyManager tm = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();
//        Collections.sort(countryCodesIso);
//        for (int i = 0; i < countryCodesIso.size(); i++) {
//            if (countryCodesIso.get(i).equalsIgnoreCase(countryCodeValue)) {
//                countryCode.setSelection(i);
//            }
//        }

        ArrayAdapter<String> nationalityAdapter = new ArrayAdapter<String>(this, R.layout.view_spinner_layout_item, new AndyUtils().parseNationality(this));
        nationalityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nationality.setAdapter(nationalityAdapter);


        Integer[] language_imageArray = {
                R.drawable.us, R.drawable.china, R.drawable.ic_thailand, R.drawable.indonesia,
                R.drawable.ic_vietnam, R.drawable.ic_myanmar, R.drawable.ic_iran, R.drawable.ic_japan};

        Integer[] currency_imageArray = {R.drawable.my, R.drawable.us, R.drawable.china, R.drawable.singapore,
                R.drawable.indonesia, R.drawable.ic_vietnam, R.drawable.ic_myanmar, R.drawable.ic_iran,
                R.drawable.ic_japan, R.drawable.ic_thailand};


        SpinnerAdapter currencyAdapter = new SpinnerAdapter(this, R.layout.spinner_value_layout, getResources().getStringArray(R.array.currency), currency_imageArray);
        currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currency.setAdapter(currencyAdapter);

        SpinnerAdapter languageAdapter = new SpinnerAdapter(this, R.layout.spinner_value_layout, getResources().getStringArray(R.array.language), language_imageArray);
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        language.setAdapter(languageAdapter);

    }


    public ArrayList<String> parseCountryCodes() {
        String response = "";
        ArrayList<String> list = new ArrayList<String>();
        try {
            response = ReadFile.readRawFileAsString(this, R.raw.countrycodes);

            list.add("MY (+60)");
            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                list.add(object.getString("alpha-2") + " (" + object.getString("phone-code") + ")");
                countryCodes.add(object.getString("phone-code"));
                countryCodesIso.add(object.getString("alpha-2"));
            }
//            Collections.sort(list);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }


    private void getAllRegisterDetails() {
        sFirstName = firstNameEdit.getText().toString().trim();
        sLastName = lastNameEdit.getText().toString().trim();
        sEmailId = emailIdEdit.getText().toString().trim();
        sPassword = passwordEdit.getText().toString().trim();
        sCountryCode = countryCode.getSelectedItem().toString();
        sNationality = nationality.getSelectedItem().toString();
        sPhoneNumber = phoneEdit.getText().toString().trim();
        sOtp = otpEdit.getText().toString().trim();
        social_security_no = et_social_security.getText().toString().trim();
        sCurrency = currency.getSelectedItem().toString();
        sLanguage = language.getSelectedItem().toString();
        TimeZone tz = TimeZone.getDefault();
        sTimeZone = tz.getID();
        System.out.println("TimeZone   " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent loginIntent = new Intent(this, LoginActivity.class);
        startActivity(loginIntent);
    }

    private boolean validate() {
        getAllRegisterDetails();
        if (sFirstName.length() == 0) {
            firstNameEdit.requestFocus();
            Toast.makeText(this, getString(R.string.please_enter_first_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (sEmailId.length() == 0) {
            Toast.makeText(this, getString(R.string.please_enter_emailid), Toast.LENGTH_SHORT).show();
            emailIdEdit.requestFocus();
            return false;
        } else if (!AndyUtils.eMailValidation(sEmailId)) {
            emailIdEdit.setError(getString(R.string.incorrect_emailid));
            emailIdEdit.requestFocus();
            return false;
        }

        if (filePath.length() == 0) {
            Toast.makeText(this, getString(R.string.please_choose_profile_picture), Toast.LENGTH_SHORT).show();
            profileImage.requestFocus();
            return false;
        }
        if (social_security_no.length() == 0) {
            Toast.makeText(this, getString(R.string.please_enter_your_ssn), Toast.LENGTH_SHORT).show();
            profileImage.requestFocus();
            return false;
        }
        // if all is correct then,

        else if (registrationType.equals(Const.MANUAL) && sPassword.length() < 8) {
            passwordEdit.setError(getString(R.string.password_must_characters));
            passwordEdit.requestFocus();
            return false;
        } else if (sGender.equals("")) {
            AndyUtils.showShortToast(getString(R.string.please_select_gender), this);
            genderRadioGroup.requestFocus();
            return false;

        } else if (sDob.equals("")) {
            dobEdit.requestFocus();
            AndyUtils.showShortToast(getString(R.string.please_select_dob), this);
            return false;
        } else if (sCountryCode.equals("")) {
            countryCode.requestFocus();
            AndyUtils.showShortToast(getString(R.string.please_select_country_code), this);
            return false;
        } else if (sPhoneNumber.length() < 6) {
            phoneEdit.requestFocus();
            phoneEdit.setError(getString(R.string.please_enter_proper_mobile_number));
            return false;
        } else if (sCurrency.equals("--Select Currency--")) {
            currency.requestFocus();
            AndyUtils.showShortToast(getString(R.string.select_currency), this);
            return false;
        } else if (sLanguage.equals("--Select Language--")) {
            language.requestFocus();
            AndyUtils.showShortToast(getString(R.string.select_language), this);
            return false;
        } else if (sNationality.equals("--Nationality--")) {
            nationality.requestFocus();
            AndyUtils.showShortToast(getString(R.string.please_choose_nationality), this);
            return false;
        } else if (sOtp.length() == 0) {
            AndyUtils.showShortToast(getString(R.string.please_enter_otp), this);
            return false;
        } else if (sOtp.length() != 4) {
            otpEdit.setError(getString(R.string.please_enter_correct_otp));
            otpEdit.requestFocus();
            return false;
        } else if (!check_terms_condition.isChecked()) {
            AndyUtils.showShortToast(getString(R.string.agree_for_terms_and_condition), this);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.iv_register_user_icon:
                showPictureDialog();
                break;
            case R.id.bn_register:
                if (validate()) {
                    AndyUtils.appLog("Selected Date", sDob);
                    patientRegistration();
                    registerButton.setEnabled(false);
                }
                break;
            case R.id.tv_register_dob:
                SelectDateFragment selectDateFragment = new SelectDateFragment();
                selectDateFragment.show(getSupportFragmentManager(), Const.DATE_PICKER);
                break;
            case R.id.iv_register_backButton:
                onBackPressed();


                break;
            case R.id.bn_request_otp:
                if (phoneEdit.getText().toString().trim().length() != 0) {
                    sCountryCode = countryCode.getSelectedItem().toString();
                    sPhoneNumber = phoneEdit.getText().toString().trim();
                    generateOtp(sCountryCode, sPhoneNumber);
                } else {
                    AndyUtils.showShortToast(getString(R.string.please_enter_number), this);
                }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Activity Res", "" + requestCode);
        switch (requestCode) {
            case Const.CHOOSE_PHOTO:
                if (data != null) {

                    uri = data.getData();
                    if (uri != null) {

                        beginCrop(uri);
                    } else {
                        AndyUtils.showLongToast("unable to select image", this);
                    }
                }
                break;
            case Const.TAKE_PHOTO:
                if (uri != null) {

                    beginCrop(uri);


                } else {
                    AndyUtils.showLongToast(getString(R.string.unable_to_select_image), this);
                }
                break;
            case Crop.REQUEST_CROP:

                if (data != null)
                    handleCrop(resultCode, data);

                break;
        }

    }

    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getString(R.string.choose_your_option));
        String[] items = {getString(R.string.gallery), getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        choosePhotoFromGallary();
                        break;
                    case 1:
                        takePhotoFromCamera();
                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, Const.CHOOSE_PHOTO);

    }

    private void takePhotoFromCamera() {
        Calendar cal = Calendar.getInstance();
        cameraFile = new File(Environment.getExternalStorageDirectory(),
                (cal.getTimeInMillis() + ".jpg"));
        if (!cameraFile.exists()) {
            try {
                cameraFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {

            cameraFile.delete();
            try {
                cameraFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        uri = Uri.fromFile(cameraFile);
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(i, Const.TAKE_PHOTO
        );
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null,
                null, null, null);

        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void beginCrop(Uri source) {
        // Uri outputUri = Uri.fromFile(new File(registerActivity.getCacheDir(),
        // "cropped"));
        AndyUtils.appLog("ProfileFragment", "beginCrop");
        Uri outputUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), (Calendar.getInstance()
                .getTimeInMillis() + ".jpg")));
        Crop.of(source, outputUri).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        AndyUtils.appLog(TAG, "handleCrop");
        if (resultCode == RESULT_OK) {
            filePath = getRealPathFromURI(Crop.getOutput(result));
//            profileImage.setImageURI(Crop.getOutput(result));
            Glide.with(this).load(Crop.getOutput(result)).into(profileImage);
            AndyUtils.appLog("RegisterActivity", filePath.toString());
        } else if (resultCode == Crop.RESULT_ERROR) {
            AndyUtils.showLongToast(Crop.getError(result).getMessage(), this);
        }

    }


    //  for Patient Registration
    private void patientRegistration() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showShortToast(getResources().getString(R.string.no_internet), this);
            return;
        }
        AndyUtils.showSimpleProgressDialog(this, getString(R.string.please_wait), false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.REGISTER_URL);
        if (registrationType.equals(Const.FACEBOOK) || registrationType.equals(Const.TWITTER) || registrationType.equals(Const.GOOGLE_PLUS)) {
            map.put(Const.Params.SOCIAL_UNIQUE_ID, sSocial_unique_id);
            map.put(Const.Params.LOGIN_BY, registrationType);
            map.put(Const.Params.ACCESS_TOKEN, accessToken);
        } else {
            map.put(Const.Params.PASSWORD, sPassword);
            map.put(Const.Params.LOGIN_BY, Const.MANUAL);

        }
        map.put("current_country", country);
        map.put(Const.Params.FIRST_NAME, sFirstName);
        map.put(Const.Params.LAST_NAME, sLastName);
        map.put(Const.Params.EMAIL, sEmailId);
        map.put(Const.Params.PICTURE, filePath);
        map.put(Const.Params.PHONE, sPhoneNumber);
        map.put(Const.Params.DEVICE_TYPE, Const.ANDROID);
        map.put(Const.Params.DEVICE_TOKEN, new PreferenceHelper(this).getDeviceToken());
        map.put(Const.Params.DOB, sDob);
        map.put(Const.Params.PHONE_COUNTRY_CODE, sCountryCode);
        map.put(Const.Params.GENDER, sGender);
        map.put(Const.Params.OTP, sOtp);
        map.put(Const.Params.NATIONALITY, sNationality);
        map.put(Const.Params.SSN, social_security_no);
        map.put(Const.Params.CURRENCY, sCurrency);
        map.put(Const.Params.LANGUAGE, sLanguage);
        map.put(Const.Params.TIMEZONE, sTimeZone);
        AndyUtils.appLog("Ashutosh", "RegistrationMap" + map);
        new MultiPartRequester(this, map, Const.ServiceCode.REGISTER, this);


    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;

        switch (serviceCode) {
            case Const.ServiceCode.REGISTER:
                registerButton.setEnabled(true);
                AndyUtils.removeProgressDialog();
                AndyUtils.appLog("Ashutosh", "RegisterResponse" + response);
                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.getString("success").equals("true")) {
                        PreferenceHelper.setParam(this, Const.Params.ECOM_TOKEN, jsonObject.optString(Const.Params.ECOM_TOKEN));
                        String session_token = jsonObject.optString("session_token");
                        PreferenceHelper.setParam(this, Const.Params.SESSION_TOKEN, session_token);
                        PreferenceHelper.setParam(this, Const.Params.ID, jsonObject.optString("id"));
                        PreferenceHelper.setParam(this, Const.Params.PICTURE, jsonObject.optString("picture"));
                        PreferenceHelper.setParam(this, Const.LOGIN_FIRST_TIME, true);
                        PreferenceHelper.setParam(this, Const.Params.CURRENCY, jsonObject.optString(Const.Params.CURRENCY));

                        String language1 = jsonObject.optString(Const.Params.LANGUAGE);
                        String language = language1.substring(language1.indexOf("(") + 1, language1.indexOf(")"));

                        PreferenceHelper.setParam(this, Const.Params.LANGUAGE, language);
                        Realm realm = RealmDBController.with(this).getRealm();
                        final PatientProfile patientProfile = new PatientProfile();
                        patientProfile.setPatientId(Integer.parseInt(jsonObject.optString("id")));
                        patientProfile.setFirstName(jsonObject.optString("first_name"));
                        PreferenceHelper.setParam(this, Const.Params.FIRST_NAME, jsonObject.optString("first_name"));
                        patientProfile.setLastName(jsonObject.optString("last_name"));
                        patientProfile.setEmailId(jsonObject.optString("email"));
                        patientProfile.setPictureUrl(jsonObject.optString("picture"));
                        patientProfile.setMobileNumber(jsonObject.optString("phone"));
                        patientProfile.setBillingAddress(jsonObject.optString("billing_address"));
                        patientProfile.setShippingAddress(jsonObject.optString("shipping_address"));
                        patientProfile.setBillingLandMark(jsonObject.optString("blandmark"));
                        patientProfile.setBillingBlockNo(jsonObject.optString("bblock_no"));
                        patientProfile.setShippingLandMark(jsonObject.optString("slandmark"));
                        patientProfile.setShippingBlockNo(jsonObject.optString("sblock_no"));
                        patientProfile.setDob(jsonObject.optString("dob"));
                        patientProfile.setGender(jsonObject.optString("gender"));
                        patientProfile.setGenderTitle(jsonObject.optString("title"));
                        patientProfile.setNationality(jsonObject.optString(Const.Params.NATIONALITY));
                        patientProfile.setCountryCode(jsonObject.optString(Const.Params.PHONE_COUNTRY_CODE));
                        patientProfile.setCurrency(jsonObject.optString(Const.Params.CURRENCY));
                        patientProfile.setLanguage(jsonObject.optString(Const.Params.LANGUAGE));
                        patientProfile.setTimeZone(jsonObject.optString(Const.Params.TIMEZONE));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(patientProfile);
                            }
                        });

                        try {
                            if (!filePath.equals("")) {

                                File file = new File(filePath);
                                file.getAbsoluteFile().delete();

                            }
                            if (cameraFile != null) {
                                cameraFile.getAbsoluteFile().delete();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

//                        registerOnXmpp();
                        AndyUtils.showShortToast(getString(R.string.registration_successfully), this);
                        Intent mainIntent = new Intent(this, MainActivity.class);
                        startActivity(mainIntent);
                        finish();

                    } else {
                        AndyUtils.showShortToast(jsonObject.getString("error_message"), this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
//            case Const.ServiceCode.REGISTER_ON_XMPP:
//                Log.d("mahi","res for xmpp"+response);
////                Commonutils.progressdialog_hide();
//
//                if (response != null)
//                {
//
//                }
//
//                break;
            case Const.ServiceCode.GENERATE_OTP:
                AndyUtils.removeProgressDialog();
                AndyUtils.appLog("Ashutosh", "GenerateOtpResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        AndyUtils.showShortToast("Otp sent successfully", this);

                    } else {
                        AndyUtils.showShortToast(jsonObject.optString("error_message"), this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }

    @Override
    public void onLocationReceived(LatLng latlong) {
        country = getCountryName(this, latlong.latitude, latlong.longitude);
    }

    @Override
    public void onLocationReceived(Location location) {

    }

    @Override
    public void onConntected(Bundle bundle) {

    }

    @Override
    public void onConntected(Location location) {

    }

//    private void registerOnXmpp() {
//
//// AndyUtils.removeCustomProgressDialog();
//        HashMap<String, String> map = new HashMap<String, String>();
//        map.put(Const.Params.URL, Const.XMPP_USER_CREATION + "username="
//                + Const.ServiceType.XMPPDRIVER + String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, ""))
//                + "&password=" + Const.XMPP_PASSWORD + "&name="
//                + "" + "&email="
//                + "");
//        Log.d("mahi", "" + map);
//        new HttpRequester(this,Const.GET,map, Const.ServiceCode.REGISTER_ON_XMPP,
//                this);
//
//    }

    public static class SelectDateFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        Calendar calendar;
        private DatePickerDialog pickerDialog;

        public Dialog onCreateDialog(Bundle savedInstanceState) {

            calendar = Calendar.getInstance();
            yy = calendar.get(Calendar.YEAR) - 17;
            mm = calendar.get(Calendar.MONTH);
            dd = calendar.get(Calendar.DAY_OF_MONTH);
            calendar.set(Calendar.YEAR, yy);
            calendar.set(Calendar.MONTH, mm);
            calendar.set(Calendar.DAY_OF_MONTH, dd);


            pickerDialog = new DatePickerDialog(getActivity(), android.R.style.Theme_Holo_Dialog, this, yy, mm, dd);
            pickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
            return pickerDialog;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            yy = year;
            mm = monthOfYear;
            dd = dayOfMonth;
            populateSetDate(year, monthOfYear + 1, dayOfMonth);
        }


        public void populateSetDate(int year, int month, int day) {

            String priorDate = day + "/" + month + "/" + year;

            AndyUtils.appLog("Populate Date", priorDate);
            if (priorDate != null) {
                sDob = day + "-" + month + "-" + year;
                dobEdit.setText(priorDate);
            } else {
                AndyUtils.showShortToast(getString(R.string.please_select_date), getActivity());
            }
        }


    }

}
