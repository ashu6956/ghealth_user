package com.patient.ghealth.activity;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;

public class DoctorConsultConnectivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskCompleteListener {

    DecimalFormat form = new DecimalFormat("0.00");
    private ImageView backButton, rating;
    private FloatingActionButton callImage, chatImage, videoImage;
    private Bundle bundle;
    private DoctorOnLineDetails doctorOnLineDetails;
    private String doctorType;
    private TextView doctorName, freeCharge, callCharges, videoCharges, chatCharges, doctorHelped, doctorExperience, doctorClinicName, doctorClinicAddress, doctorConsultantFee, doctorRating;
    private ImageView doctorIcon, clinicIcon, addressIcon, consultedIcon, experienceIcon;
    private Dialog progressDialog, debtAmountDialog;
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (progressDialog != null) {
                progressDialog.cancel();
            }
            Log.d("FragmentRequest", "onReceive");
            String notification = intent.getStringExtra("ashutosh");
            Log.d("My Push notification", notification);
            String message = intent.getStringExtra(Const.DOCTOR_INTENT_MESSAGE);
            try {
                JSONObject messageObj = new JSONObject(message);
                AndyUtils.appLog("JsonResponse", messageObj.toString());
                if (messageObj.getString("success").equals("true")) {
                    if (Integer.parseInt(messageObj.getString("status")) == 12) {
                        AndyUtils.appLog("ConsultActivity", "VideoCall");
                        JSONObject jsonObject = messageObj.getJSONObject("data");
                        DoctorOnLineDetails onLineDetails = new DoctorOnLineDetails();
                        onLineDetails.setDoctorOnLineId(jsonObject.getString("doctor_id"));
                        onLineDetails.setDoctorPictureUrl(jsonObject.getString("doctor_picture"));
                        onLineDetails.setDoctorRating(jsonObject.getString("doctor_rating"));
                        onLineDetails.setDoctorName(jsonObject.getString("doctor_name"));
                        onLineDetails.setDoctorStatus(messageObj.getString("status"));
                        onLineDetails.setRequestId(jsonObject.getString("request_id"));
                        onLineDetails.setDoctorStatusTitle(messageObj.getString("title"));
                        Bundle callBundle = new Bundle();
                        callBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, doctorOnLineDetails);
                        callBundle.putString(Const.DOCTOR_CONSULT_CONNECTIVITY, "normal");
                        new PreferenceHelper(getApplicationContext()).putRequestId(jsonObject.getString("request_id"));
                        PreferenceHelper.setParam(DoctorConsultConnectivity.this, Const.Params.DOCTOR_ID, jsonObject.optString("doctor_id"));
                        AndyUtils.appLog("Consult", "DoctorID" + jsonObject.optString("doctor_id"));
                        AndyUtils.appLog("VideoResponse", "DoctorId" + PreferenceHelper.getParam(DoctorConsultConnectivity.this, Const.Params.DOCTOR_ID, ""));
                        new PreferenceHelper(DoctorConsultConnectivity.this).putReq_time(SystemClock.uptimeMillis());
                        Intent callIntent = new Intent(DoctorConsultConnectivity.this, VideoCallActivity.class);
                        callIntent.putExtras(callBundle);
                        startActivity(callIntent);
                        finish();
                    } else if (Integer.parseInt(messageObj.getString("status")) == 11) {
                        JSONObject jsonObject = messageObj.getJSONObject("data");
                        DoctorOnLineDetails onLineDetails = new DoctorOnLineDetails();
                        onLineDetails.setDoctorOnLineId(jsonObject.getString("doctor_id"));
                        onLineDetails.setDoctorPictureUrl(jsonObject.getString("doctor_picture"));
                        onLineDetails.setDoctorRating(jsonObject.getString("doctor_rating"));
                        onLineDetails.setDoctorName(jsonObject.getString("doctor_name"));
                        onLineDetails.setDoctorStatus(messageObj.getString("status"));
                        onLineDetails.setRequestId(jsonObject.getString("request_id"));
                        onLineDetails.setDoctorStatusTitle(messageObj.getString("title"));
                        Bundle callBundle = new Bundle();
                        callBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, doctorOnLineDetails);
                        callBundle.putString(Const.DOCTOR_CONSULT_CONNECTIVITY, "normal");
                        new PreferenceHelper(getApplicationContext()).putRequestId(jsonObject.getString("request_id"));
                        new PreferenceHelper(DoctorConsultConnectivity.this).putReq_time(SystemClock.uptimeMillis());
                        Intent callIntent = new Intent(DoctorConsultConnectivity.this, VoiceCallActivity.class);
                        callIntent.putExtras(callBundle);
                        startActivity(callIntent);
                        finish();
                    } else if (Integer.parseInt(messageObj.getString("status")) == 9) {
                        if (progressDialog != null) {
                            progressDialog.cancel();
                        }
                        AndyUtils.showLongToast(getString(R.string.request_rejected_by_doctor), DoctorConsultConnectivity.this);
                    } else if (Integer.parseInt(messageObj.getString("status")) == 10) {
                        AndyUtils.appLog("ConsultActivity", "MessageCall");
                        JSONObject jsonObject = messageObj.getJSONObject("data");
                        DoctorOnLineDetails onLineDetails = new DoctorOnLineDetails();
                        onLineDetails.setDoctorOnLineId(jsonObject.getString("doctor_id"));
                        onLineDetails.setDoctorPictureUrl(jsonObject.getString("doctor_picture"));
                        onLineDetails.setDoctorRating(jsonObject.getString("doctor_rating"));
                        onLineDetails.setDoctorName(jsonObject.getString("doctor_name"));
                        onLineDetails.setDoctorStatus(messageObj.getString("status"));
                        onLineDetails.setRequestId(jsonObject.getString("request_id"));
                        onLineDetails.setDoctorStatusTitle(messageObj.getString("title"));

                        Bundle callBundle = new Bundle();
                        callBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, doctorOnLineDetails);
                        callBundle.putString(Const.DOCTOR_CONSULT_CONNECTIVITY, "normal");
                        new PreferenceHelper(getApplicationContext()).putRequestId(jsonObject.optString("request_id"));
                        PreferenceHelper.setParam(DoctorConsultConnectivity.this, Const.Params.DOCTOR_ID, jsonObject.optString("doctor_id"));
                        AndyUtils.appLog("MessageResponse", "DoctorId" + PreferenceHelper.getParam(DoctorConsultConnectivity.this, Const.Params.DOCTOR_ID, ""));
                        new PreferenceHelper(DoctorConsultConnectivity.this).putReq_time(SystemClock.uptimeMillis());
                        Intent callIntent = new Intent(DoctorConsultConnectivity.this, MessageActivity.class);
                        callIntent.putExtras(callBundle);
                        startActivity(callIntent);
                        finish();
                    }


                } else {
                    AndyUtils.showLongToast(messageObj.getString("error_message"), DoctorConsultConnectivity.this);
                }
            } catch (JSONException e) {
                if (progressDialog != null) {
                    progressDialog.cancel();
                }
                e.printStackTrace();
            }
        }
    };
    private LinearLayout videoLayout, chatLayout, callLayout;
    private boolean isNetDialogShowing = false;
    private String requestId;
    private String currency = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_consult_connectivity);
        currency = (String) PreferenceHelper.getParam(this, Const.Params.CURRENCY, "");
        doctorName = (TextView) findViewById(R.id.tv_doctor_connection_doctor_name);
        doctorHelped = (TextView) findViewById(R.id.tv_doctor_connection_doctorhelped);
        doctorExperience = (TextView) findViewById(R.id.tv_doctor_connection_doctorExperience);
        doctorIcon = (ImageView) findViewById(R.id.iv_doctor_connection_profile_icon);
        callImage = (FloatingActionButton) findViewById(R.id.iv_doctor_connection_call);
        chatImage = (FloatingActionButton) findViewById(R.id.iv_doctor_connection_chat);
        videoImage = (FloatingActionButton) findViewById(R.id.iv_doctor_connection_video);
        backButton = (ImageView) findViewById(R.id.iv_doctor_connection_back_icon);
        doctorClinicName = (TextView) findViewById(R.id.tv_doctor_connection_clinic_name);
//        doctorConsultantFee = (TextView) findViewById(R.id.tv_doctor_connection_doctorCharges);
        doctorClinicAddress = (TextView) findViewById(R.id.tv_doctor_connection_doctor_address);
        doctorRating = (TextView) findViewById(R.id.tv_doctor_connection_rating);
        rating = (ImageView) findViewById(R.id.iv_rating);
        clinicIcon = (ImageView) findViewById(R.id.iv_doctor_connection_clinic);
        addressIcon = (ImageView) findViewById(R.id.iv_doctor_connection_doctor_address);
        consultedIcon = (ImageView) findViewById(R.id.iv_doctor_connection_consulted);
        experienceIcon = (ImageView) findViewById(R.id.iv_doctor_connection_experience);
        videoLayout = (LinearLayout) findViewById(R.id.ll_video);
        callLayout = (LinearLayout) findViewById(R.id.ll_call);
        chatLayout = (LinearLayout) findViewById(R.id.ll_chat);
        chatCharges = (TextView) findViewById(R.id.tv_doctor_chat_charges);
        videoCharges = (TextView) findViewById(R.id.tv_doctor_video_charges);
        callCharges = (TextView) findViewById(R.id.tv_doctor_call_charges);
        freeCharge = (TextView) findViewById(R.id.tv_free_charge);
        callImage.setOnClickListener(this);
        chatImage.setOnClickListener(this);
        videoImage.setOnClickListener(this);
        backButton.setOnClickListener(this);

        bundle = getIntent().getExtras();
        if (bundle != null) {
            doctorOnLineDetails = (DoctorOnLineDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
            doctorType = bundle.getString(Const.DOCTOR_TYPE);

            setDetailsOnView(doctorOnLineDetails);
        }

    }

    private void setDetailsOnView(DoctorOnLineDetails doctorOnLineDetails) {
        doctorName.setText(doctorOnLineDetails.getDoctorName());
        if (doctorOnLineDetails.getDoctorPictureUrl().equals("")) {
            doctorIcon.setImageResource(R.drawable.profile);
        } else {
            Glide.with(this).load(doctorOnLineDetails.getDoctorPictureUrl()).into(doctorIcon);

        }
        if (!doctorOnLineDetails.getDoctorRating().equals("")) {
            rating.setVisibility(View.VISIBLE);
            doctorRating.setText(doctorOnLineDetails.getDoctorRating());
        }
        if (doctorOnLineDetails.getDoctorClinicName() != null && !doctorOnLineDetails.getDoctorClinicName().equals("")) {
            clinicIcon.setVisibility(View.VISIBLE);
            doctorClinicName.setText(doctorOnLineDetails.getDoctorClinicName());
        }
        if (!doctorOnLineDetails.getDoctorHelpedPeople().equals("")) {
            consultedIcon.setVisibility(View.VISIBLE);
            doctorHelped.setText(getString(R.string.consulted) + " " + doctorOnLineDetails.getDoctorHelpedPeople() + " " + getString(R.string.people));
        }
        if (!doctorOnLineDetails.getDoctorExperience().equals("")) {
            experienceIcon.setVisibility(View.VISIBLE);
            doctorExperience.setText(doctorOnLineDetails.getDoctorExperience() + " " + getString(R.string.year_experience));
        }
        if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1) {
            chatImage.setVisibility(View.VISIBLE);
            chatLayout.setVisibility(View.VISIBLE);
            chatCharges.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + getString(R.string.onwards));

        }
        if (Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1) {
            callLayout.setVisibility(View.VISIBLE);
            callImage.setVisibility(View.VISIBLE);
            callCharges.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) + "\n" + getString(R.string.onwards));
        }
        if (Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
            videoLayout.setVisibility(View.VISIBLE);
            videoImage.setVisibility(View.VISIBLE);
            videoCharges.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + "\n" + getString(R.string.onwards));
        }
        if (doctorOnLineDetails.getClinicAddress() != null && !doctorOnLineDetails.getClinicAddress().equals("")) {

            addressIcon.setVisibility(View.VISIBLE);
            doctorClinicAddress.setText(doctorOnLineDetails.getClinicAddress());
        }
        if (!doctorOnLineDetails.getFreeCharge().equals("")) {
            freeCharge.setText("* " + getString(R.string.initial) + " " + doctorOnLineDetails.getFreeCharge() + " " + getString(R.string.minutes) + " " + getString(R.string.free));
        } else {
            freeCharge.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_doctor_connection_call:
                createVoiceCallRequest();
                break;
            case R.id.iv_doctor_connection_video:
                createVideoCallRequest();
                break;
            case R.id.iv_doctor_connection_chat:
                createChatRequest();
                break;
            case R.id.iv_doctor_connection_back_icon:
                onBackPressed();
                break;
        }

    }

    private void createChatRequest() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        showProgressDialog();
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_REQUEST);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.REQUEST_TYPE, String.valueOf(Const.CHAT));
        map.put(Const.Params.DOCTOR_ID, String.valueOf(doctorOnLineDetails.getDoctorOnLineId()));
        map.put(Const.Params.PAYMENT_MODE, Const.CARD);
        AndyUtils.appLog("Ashutosh", "GetChatRequestMap" + map);
        new HttpRequester(this, Const.POST, map, Const.ServiceCode.CHAT_REQUEST, this);
    }

    private void createVideoCallRequest() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        showProgressDialog();
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_REQUEST);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.REQUEST_TYPE, String.valueOf(Const.VIDEO));
        map.put(Const.Params.DOCTOR_ID, String.valueOf(doctorOnLineDetails.getDoctorOnLineId()));
        map.put(Const.Params.PAYMENT_MODE, Const.CARD);
        AndyUtils.appLog("Ashutosh", "GetVideoRequestMap" + map);
        new HttpRequester(this, Const.POST, map, Const.ServiceCode.VIDEO_REQUEST, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(Const.DOCTOR_REQUEST_STATUS);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
                filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

    }

    private void createVoiceCallRequest() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        showProgressDialog();
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_REQUEST);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.REQUEST_TYPE, String.valueOf(Const.VOICE));
        map.put(Const.Params.DOCTOR_ID, String.valueOf(doctorOnLineDetails.getDoctorOnLineId()));
        map.put(Const.Params.PAYMENT_MODE, Const.CARD);
        AndyUtils.appLog("Ashutosh", "GetVoiceRequestMap" + map);
        new HttpRequester(this, Const.POST, map, Const.ServiceCode.VOICE_REQUEST, this);
    }

    @Override
    public void onBackPressed() {
//        Intent intent=new Intent(DoctorConsultConnectivity.this,DoctorListActivity.class);
//        startActivity(intent);
//        finish();
        super.onBackPressed();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
//        AndyUtils.removeProgressDialog();
        switch (serviceCode) {

            case Const.ServiceCode.VOICE_REQUEST:
//                 AndyUtils.removeProgressDialog();
                AndyUtils.appLog("Ashutosh", "VoiceCallRequestResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        requestId = jsonObject.optString(Const.REQUEST_ID);
                        AndyUtils.appLog("RequestId", requestId);
                    } else {
                        if (progressDialog != null) {
                            progressDialog.cancel();
                        }
                        if (jsonObject.getString("error_message").equalsIgnoreCase("No cards added")) {
                            Intent intent = new Intent(this, AddCardActivity.class);
                            startActivity(intent);
                        } else if (jsonObject.optString("error_message").equalsIgnoreCase("You have previous payment pending")) {
                            String amount = jsonObject.optString("amount");
                            showDebtAmountDialog(amount);

                        } else {
                            AndyUtils.showShortToast(jsonObject.getString("error_message"), this);
                        }
                    }
                } catch (JSONException e) {
                    if (progressDialog != null) {
                        progressDialog.cancel();
                    }
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.VIDEO_REQUEST:
                AndyUtils.appLog("Ashutosh", "VideoCallRequestResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        requestId = jsonObject.optString(Const.REQUEST_ID);
                        AndyUtils.appLog("RequestId", requestId);
                        PreferenceHelper.setParam(this, Const.REQUEST_ID, requestId);
                    } else {
                        if (progressDialog != null) {
                            progressDialog.cancel();
                        }
                        if (jsonObject.getString("error_message").equalsIgnoreCase("No cards added")) {
                            Intent intent = new Intent(this, AddCardActivity.class);
                            startActivity(intent);
                        } else if (jsonObject.optString("error_message").equalsIgnoreCase("You have previous payment pending")) {
                            String amount = jsonObject.optString("amount");
                            showDebtAmountDialog(amount);

                        } else {
                            AndyUtils.showShortToast(jsonObject.getString("error_message"), this);
                        }
                    }
                } catch (JSONException e) {
                    if (progressDialog != null) {
                        progressDialog.cancel();
                    }
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CHAT_REQUEST:
                AndyUtils.appLog("Ashutosh", "ChatRequestResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        requestId = jsonObject.optString(Const.REQUEST_ID);
                        AndyUtils.appLog("RequestId", requestId);
                        PreferenceHelper.setParam(this, Const.REQUEST_ID, requestId);
                    } else {
                        if (progressDialog != null) {
                            progressDialog.cancel();
                        }
                        if (jsonObject.getString("error_message").equalsIgnoreCase("No cards added")) {
                            Intent intent = new Intent(this, AddCardActivity.class);
                            startActivity(intent);
                        } else if (jsonObject.optString("error_message").equalsIgnoreCase("You have previous payment pending")) {
                            String amount = jsonObject.optString("amount");
                            showDebtAmountDialog(amount);

                        } else {
                            AndyUtils.showShortToast(jsonObject.getString("error_message"), this);
                        }
                    }
                } catch (JSONException e) {
                    if (progressDialog != null) {
                        progressDialog.cancel();
                    }
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CREATE_CANCEL_REQUEST:
                try {
                    AndyUtils.appLog("Ashutosh", "CancelRequestResponse" + response);
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        AndyUtils.showShortToast(getString(R.string.request_cancelled_by_user), this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CLEAR_DEBT_AMOUNT:
                try {
                    AndyUtils.appLog("Ashutosh", "DebtClearResponse" + response);
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        AndyUtils.showShortToast(jsonObject.optString("message"), this);
                    } else {
                        AndyUtils.showShortToast(jsonObject.optString("error_message"), this);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


        }
    }

    private void showProgressDialog() {
        progressDialog = new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_custom_progress_layout);
        final ProgressBar progressBar = (ProgressBar) progressDialog.findViewById(R.id.dialog_progressBar);
        Button cancelButton = (Button) progressDialog.findViewById(R.id.bn_cancel_progress_dialog);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.cancel();
                progressBar.setVisibility(View.GONE);
                cancelRequest();

            }
        });

        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    private void cancelRequest() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_CANCEL_REQUEST_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.REQUEST_ID, String.valueOf(requestId));

        AndyUtils.appLog("Ashutosh", "CancelRequestMap" + map);

        new HttpRequester(this, Const.POST, map, Const.ServiceCode.CREATE_CANCEL_REQUEST, this);

    }

    private void showDebtAmountDialog(String amount) {
        isNetDialogShowing = true;
        debtAmountDialog = new Dialog(this);
        debtAmountDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        debtAmountDialog.setContentView(R.layout.dialog_debt_amount_layout);
        TextView debtAmountHeader = (TextView) debtAmountDialog.findViewById(R.id.tv_debt_header);
        debtAmountHeader.setText(getString(R.string.you_have) + " " + currency + form.format(Double.valueOf(amount)) + " " + getString(R.string.amount_pending));
        TextView exit = (TextView) debtAmountDialog.findViewById(R.id.tv_debt_cancel);
        exit.setText(getString(R.string.cancel));
        TextView payNow = (TextView) debtAmountDialog.findViewById(R.id.tv_debt_payNow);
        payNow.setText(getString(R.string.pay_now));

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeInternetDialog();
//                activity.finish();
            }
        });

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeInternetDialog();
                clearDebtAmount();
            }
        });
        debtAmountDialog.setCancelable(false);
        if (!isFinishing()) {
            debtAmountDialog.show();
        }
    }

    private void removeInternetDialog() {
        if (debtAmountDialog != null && debtAmountDialog.isShowing()) {
            debtAmountDialog.dismiss();
            isNetDialogShowing = false;
            debtAmountDialog = null;

        }
    }


    private void clearDebtAmount() {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CLEAR_DEBT_AMOUNT_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "DebtAmountMap" + map);
        new HttpRequester(this, Const.POST, map, Const.ServiceCode.CLEAR_DEBT_AMOUNT, this);
    }

}
