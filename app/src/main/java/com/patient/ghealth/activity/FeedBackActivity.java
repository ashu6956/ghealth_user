package com.patient.ghealth.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.InvoiceDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;

/**
 * Created by user on 9/28/2016.
 */
public class FeedBackActivity extends Activity implements AsyncTaskCompleteListener {

    final String comments = "";
    DecimalFormat form = new DecimalFormat("0.00");
    private Dialog invoiceDialog;
    private ImageView doctorIcon;
    private InvoiceDetails invoiceDetails;
    private Bundle bundle;
    private RatingBar feedBackRatingBar;
    private EditText feedBackComments;
    private ImageButton doneFeedBackButton;
    private TextView doctorName, totalTime, totalDistance;
    private LinearLayout timeDistanceLayout;
    private String currency = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_layout);
        currency = (String) PreferenceHelper.getParam(this, Const.Params.CURRENCY, "");
        doctorIcon = (ImageView) findViewById(R.id.iv_feedback_doctor_icon);
        feedBackRatingBar = (RatingBar) findViewById(R.id.rb_feedBack);
        feedBackComments = (EditText) findViewById(R.id.et_feedback_comments);
        doneFeedBackButton = (ImageButton) findViewById(R.id.ib_feedback_done);
        NotificationManager notifManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notifManager.cancelAll();
        doctorName = (TextView) findViewById(R.id.tv_feedback_doctor_name);
        totalTime = (TextView) findViewById(R.id.tv_feedback_time);
        totalDistance = (TextView) findViewById(R.id.tv_feedback_distance);
        timeDistanceLayout = (LinearLayout) findViewById(R.id.ll_time_distance_layout);
        bundle = getIntent().getExtras();
        if (bundle != null && bundle.getString(Const.DOCTOR_CONSULT_CONNECTIVITY).equals("normal")) {
            invoiceDetails = (InvoiceDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
            showInvoiceDialog(invoiceDetails);
        }
//        } else if (bundle != null && bundle.getString(Const.DOCTOR_CONSULT_CONNECTIVITY).equals("checkStatus")) {
//            invoiceDetails = (InvoiceDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
//            showInvoiceDialog(invoiceDetails);
//        }

        doneFeedBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (feedBackRatingBar.getRating() != 0.0f && invoiceDetails != null) {
                    AndyUtils.appLog("VideoCallActivity", "FeedBackRating" + feedBackRatingBar.getRating());
                    setDoctorRate(invoiceDetails, feedBackComments.getText().toString(), feedBackRatingBar.getRating());
                } else {
                    AndyUtils.showShortToast(getString(R.string.please_give_rating), FeedBackActivity.this);
                }

            }
        });
    }

    private void setDoctorRate(InvoiceDetails details, String comments, float rating) {
        if (!AndyUtils.isNetworkAvailable(this)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), this);
            return;
        }
        AndyUtils.showSimpleProgressDialog(this, getString(R.string.doctor_rating), false);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.DOCTOR_RATE_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(this, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(this, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.REQUEST_ID, String.valueOf(details.getInvoiceRequestId()));
        map.put(Const.Params.RATING, String.valueOf((int) rating));
        map.put(Const.Params.COMMENTS, comments);

        AndyUtils.appLog("Ashutosh", "DoctorRateMap" + map);
        new HttpRequester(this, Const.POST, map, Const.ServiceCode.DOCTOR_RATE, this);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.DOCTOR_RATE:
                AndyUtils.removeProgressDialog();
                AndyUtils.appLog("Ashutosh", "DoctorRateResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {

                        PreferenceHelper.setParam(this, Const.DOCTOR_NAME, "");
                        PreferenceHelper.setParam(this, Const.PICTURE, "");
                        new PreferenceHelper(this).putRequestId("");
                        AndyUtils.showShortToast(getString(R.string.doctor_rated), FeedBackActivity.this);
                        new PreferenceHelper(FeedBackActivity.this).putReq_time(SystemClock.uptimeMillis());
                        Intent homeIntent = new Intent(this, MainActivity.class);
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(homeIntent);
                        finish();
                    } else {
                        AndyUtils.showShortToast(jsonObject.getString("error_message"), this);
                    }
                } catch (JSONException e) {
                    AndyUtils.removeProgressDialog();
                    e.printStackTrace();
                }

        }
    }


    private void showInvoiceDialog(final InvoiceDetails details) {
        invoiceDialog = new Dialog(this);
        invoiceDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        invoiceDialog.setContentView(R.layout.dialog_invoice_layout);
        ImageButton doneInvoiceButton = (ImageButton) invoiceDialog.findViewById(R.id.ib_invoice_done);
        TextView paymentMode = (TextView) invoiceDialog.findViewById(R.id.tv_invoice_payment_mode);
        TextView bookingFee = (TextView) invoiceDialog.findViewById(R.id.tv_invoice_booking_fee);
        TextView feeText = (TextView) invoiceDialog.findViewById(R.id.tv_consult_fee);
        TextView distanceCost = (TextView) invoiceDialog.findViewById(R.id.tv_distance_cost);
        TextView perKm = (TextView) invoiceDialog.findViewById(R.id.tv_per_km_cost);
        TextView distance = (TextView) invoiceDialog.findViewById(R.id.tv_disatnce);
        LinearLayout distanceCostLayout = (LinearLayout) invoiceDialog.findViewById(R.id.ll_distance_cost);
        LinearLayout distanceLayout = (LinearLayout) invoiceDialog.findViewById(R.id.ll_distance_layout);

        if (details.getInvoiceType().equals(Const.BOOKING)) {
            feeText.setText(getString(R.string.booking_fee));
        } else {
            feeText.setText(getString(R.string.consult_fee));
            if (details.getDistanceCost() != null && !details.getDistanceCost().equals("")) {
                distanceCost.setText(currency + " " + details.getDistanceCost());
                distanceCostLayout.setVisibility(View.VISIBLE);
                perKm.setText(currency + details.getPerKm() + "/km");
            }
            if (details.getDistance() != null && !details.getDistance().equals("")) {
                distanceLayout.setVisibility(View.VISIBLE);
//                if(String.valueOf(details.getDistance()).equals("1") || String.valueOf(details.getDistance()).equals("0"))
//                distance.setText(details.getDistance()+" "+"km");
//                else
//                {
//                    distance.setText(details.getDistance()+" "+"kms");
//                }
                distance.setText(details.getDistance());
            }


        }
        TextView promoBonus = (TextView) invoiceDialog.findViewById(R.id.tv_invoice_promo_bonus);
        TextView totalPrice = (TextView) invoiceDialog.findViewById(R.id.tv_invoice_total_amount);
        paymentMode.setText(details.getPaymentMode());
        bookingFee.setText(currency + " " + form.format(Double.valueOf(details.getBasePrice())));
        //treatmentFee.setText(details.getTreatmentFee() + " " + "$");
        //referralBonus.setText(details.getReferralBonus() + " " + "$");
        promoBonus.setText(currency + " " + details.getPromoBonus());
        totalPrice.setText(currency + " " + form.format(Double.valueOf(details.getTotalPrice())));
        //medicineFee.setText(details.getMedicineFee() + " " + "$");
        doneInvoiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                invoiceDialog.cancel();
                setDetailsOnView(details);
            }
        });
        invoiceDialog.setCancelable(false);
        invoiceDialog.show();
    }

    private void setDetailsOnView(final InvoiceDetails details) {
        doctorIcon.setVisibility(View.VISIBLE);
        String pictureUrl = details.getInvoiceDoctorPictureUrl();
        if (pictureUrl != "") {
            Glide.with(this).load(pictureUrl).centerCrop().into(doctorIcon);
        } else {
            doctorIcon.setImageResource(R.drawable.profile);
        }
        doctorName.setText(details.getInvoiceDoctorName());

        if (details.getInvoiceType().equals(Const.BOOKING)) {
            timeDistanceLayout.setVisibility(View.GONE);
        } else {
            timeDistanceLayout.setVisibility(View.VISIBLE);
            int totalDuration = Integer.parseInt(details.getTotalTime());
            if (totalDuration > 60) {
                int hour = totalDuration / 60;
                int mins = totalDuration % 60;
                if (hour == 1) {
                    totalTime.setText(hour + " " + getString(R.string.hour) + " " + mins + " " + getString(R.string.mins));
                } else {
                    totalTime.setText(hour + " " + getString(R.string.hours) + " " + mins + " " + getString(R.string.mins));
                }
            } else if (totalDuration == 60) {
                totalTime.setText(1 + " " + getString(R.string.hour));
            } else if (totalDuration == 1) {
                totalTime.setText(1 + " " + getString(R.string.min));
            } else if (totalDuration == 0) {
                totalTime.setText(totalDuration + " " + getString(R.string.min));
            } else {
                totalTime.setText(totalDuration + " " + getString(R.string.mins));
            }

//            if (details.getDistance().equals("0")) {
//                totalDistance.setText(details.getDistance() + " " + "km");
//            } else {
//                totalDistance.setText(details.getDistance() + " " + "kms");
//            }
            totalDistance.setText(details.getDistance());
        }

    }


    @Override
    public void onBackPressed() {

    }
}

