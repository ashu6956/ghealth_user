package com.patient.ghealth.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.adapter.NotificationPagerAdapter;
import com.patient.ghealth.fragment.NotificationAppointmentFragment;
import com.patient.ghealth.fragment.NotificationAnswerFragment;
import com.patient.ghealth.fragment.NotificationMessageFragment;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar notificationToolbar;
    private TabLayout notificationTabLayout;
    private TextView toolbarHeaderText;
    private ImageView notificationBackImage;
    private ViewPager notificationViewPager;
    private String type;
    private Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        notificationToolbar = (Toolbar) findViewById(R.id.tb_notification);
        notificationTabLayout = (TabLayout) findViewById(R.id.tl_notification);
        setSupportActionBar(notificationToolbar);
        getSupportActionBar().setTitle("");
        notificationViewPager = (ViewPager) findViewById(R.id.vp_notification);
        setWithViewPager(notificationViewPager, type);
        notificationTabLayout.setupWithViewPager(notificationViewPager);
        toolbarHeaderText = (TextView) findViewById(R.id.tb_notification_header_text);
        notificationBackImage = (ImageView) findViewById(R.id.iv_notification_back_icon);
        toolbarHeaderText.setText(getString(R.string.alert));
        PreferenceHelper.setParam(this,Const.NOTIFICATIONCOUNT,0);
        try {
            bundle = getIntent().getExtras();
            if (bundle != null) {
                String notificatioType = bundle.getString(Const.NOTIFICATION);
                AndyUtils.appLog("Type of Notification",notificatioType);
                if (notificatioType.equals(Const.NotificationMessageFragment))
                {
                    notificationTabLayout.getTabAt(0).select();
                } else if (notificatioType.equals(Const.NotificationAppointmentFragment)) {
                    notificationTabLayout.getTabAt(1).select();
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        notificationBackImage.setOnClickListener(this);

    }

    private void setWithViewPager(ViewPager pager, String type) {
        NotificationPagerAdapter pagerAdapter = new NotificationPagerAdapter(getSupportFragmentManager());
        pagerAdapter.addItem(new NotificationMessageFragment(), getString(R.string.message_small));
        pagerAdapter.addItem(new NotificationAppointmentFragment(), getString(R.string.appointment));
        for (int i = 0; i < notificationTabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = notificationTabLayout.getTabAt(i);
            tab.setCustomView(pagerAdapter.getTabView(i,this));
        }
        pager.setAdapter(pagerAdapter);

    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(this,MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_notification_back_icon:
                onBackPressed();
                break;
        }
    }
}

