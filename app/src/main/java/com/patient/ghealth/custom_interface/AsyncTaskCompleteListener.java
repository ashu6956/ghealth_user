package com.patient.ghealth.custom_interface;

/**
 * Created by user on 8/18/2016.
 */
public interface AsyncTaskCompleteListener
{
    void onTaskCompleted(String response, int serviceCode);
}
