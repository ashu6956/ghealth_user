package com.patient.ghealth.custom_interface;

/**
 * Created by user on 11/23/2016.
 */
public interface CartCountListener
{
    void showCount(int count);
}
