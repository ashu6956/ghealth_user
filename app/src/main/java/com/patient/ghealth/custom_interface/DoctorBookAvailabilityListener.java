package com.patient.ghealth.custom_interface;

/**
 * Created by user on 9/1/2016.
 */
public interface DoctorBookAvailabilityListener
{
    void onClickBookAvailability(int position);
}
