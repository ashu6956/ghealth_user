package com.patient.ghealth.custom_interface;

import android.util.Log;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.MultiUserChat;

/**
 * Created by lenovo on 8/23/2015.
 */
public class GroupChatListener implements InvitationListener
{
    String nickname;

    public GroupChatListener(String nick)
    {
        nickname = nick;
    }

    @Override
    public void invitationReceived(XMPPConnection con, String room,String inviter, String reason, String password, Message message)
    {
        System.out.println(" Entered invitation handler... ");
        Log.e("invitation","true");
        try
        {
            MultiUserChat chatRoom = new MultiUserChat(con, room);

            chatRoom.join(nickname);
            chatRoom.addMessageListener(new GroupChatMsgListener());
        }
        catch (SmackException.NoResponseException  e)
        {
            e.printStackTrace();
        }
        catch (XMPPException.XMPPErrorException e)
        {
            e.printStackTrace();
        }
        catch (SmackException.NotConnectedException e)
        {
            e.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        System.out.println(" Invitation Accepted... ");
    }

}
