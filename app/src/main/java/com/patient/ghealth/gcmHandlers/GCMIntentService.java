package com.patient.ghealth.gcmHandlers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.gcm.GcmListenerService;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.activity.NotificationActivity;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by user on 6/29/2015.
 */
public class GCMIntentService extends GcmListenerService {

    public static final int NOTIFICATION_ID = 1;
    NotificationCompat.Builder builder;
    private NotificationManager mNotificationManager;
    private String title;
    private int countAnswer;
    private int countMessage;

    @Override
    public void onMessageReceived(String s, Bundle bundle) {

        Bundle extras = bundle;
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
//        String messageType = gcm.getMessageType(intent);

//        Log.d("Ashutosh", "in gcm intent message " + messageType);


        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM
             * will be extended in the future with new message types, just ignore
             * any message types you're not interested in, or that you don't
             * recognize.
             */
//            if (GoogleCloudMessaging.
//                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
//
//            } else if (GoogleCloudMessaging.
//                    MESSAGE_TYPE_DELETED.equals(messageType)) {
//
//                // If it's a regular GCM message, do some work.
//            } else if (GoogleCloudMessaging.
//                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
//
//
//

            String recieved_message = bundle.getString("message");
            AndyUtils.appLog("PushMessage", recieved_message);

            try {
                JSONObject jsonObject = new JSONObject(recieved_message);
                title = jsonObject.getString("title");
                if (jsonObject.getString("success").equals("true")) {
                    int status_position = Integer.parseInt(jsonObject.getString("status"));
                    AndyUtils.appLog("PushStatusPosition", status_position + "");
                    switch (status_position) {
                        case 1: //accepted
                            sendFirstJsonRequest(recieved_message);
                            sendNotification(title, 0, "", "ashutosh");
                            break;
                        case 2: // on the way or started
                            sendJsonRequest(recieved_message);
                            sendNotification(title, 0, "", "ashutosh");
                            break;
                        case 3:  // arrived
                            sendJsonRequest(recieved_message);
                            sendNotification(title, 0, "", "ashutosh");
                            break;
                        case 4: // start treatment
                            sendJsonRequest(recieved_message);
                            sendNotification(title, 0, "", "ashutosh");
                            break;
                        case 5: // End treatment
                            sendJsonRequest(recieved_message);
                            sendNotification(title, 0, "", "ashutosh");
                            break;
                        case 6:
                            break;
                        case 9://reject the request
                            sendFirstJsonRequest(recieved_message);
                            sendNotification(title, 0, "", "ashutosh");
                            break;
                        case 10: // Accepted Chat Request
                            sendFirstJsonRequest(recieved_message);
                            sendNotification(title, 0, "", "ashutosh");
                            break;
                        case 11: // Accepted Voice Call Request
                            sendFirstJsonRequest(recieved_message);
                            sendNotification(title, 0, "", "ashutosh");
                            break;
                        case 12: // Accepted  Video Call Request
                            sendFirstJsonRequest(recieved_message);
                            sendNotification(title, 0, "", "ashutosh");
                            break;
                        case 13: //End Video Call
                            break;
                        case 25:  // onLine Consult Refreshing With new data
                            break;
                        case 26:  // OnDemand refreshing with new Data
                            break;
                        case 30:  //for booking invoice
                            sendJsonRequest(recieved_message);
                            sendNotification(title, 0, "", "ashutosh");
                            break;
                        case 567: // for Message by Doctor

                            if ((Integer) PreferenceHelper.getParam(getApplicationContext(), Const.NOTIFICATIONCOUNT, 0) > 0) {
                                countMessage = (Integer) PreferenceHelper.getParam(getApplicationContext(), Const.NOTIFICATIONCOUNT, 0);
                            }
                            countMessage++;
                            PreferenceHelper.setParam(getApplicationContext(), Const.NOTIFICATIONCOUNT, countMessage);
                            sendJsonRequest(recieved_message);
                            sendAppointmentNotification(title, 0, "answer", "Ashutosh");
                            break;
                        case 98: // for Appointment Notification before 30 mints
                            sendAppointmentRepeatNotification(title, 0, "appointment", "Ashutosh");
                            break;
                        case 99: //for Answer by Doctor

                            if ((Integer) PreferenceHelper.getParam(getApplicationContext(), Const.ANSWERCOUNT, 0) > 0) {
                                countAnswer = (Integer) PreferenceHelper.getParam(getApplicationContext(), Const.ANSWERCOUNT, 0);
                            }
                            countAnswer++;
                            PreferenceHelper.setParam(getApplicationContext(), Const.ANSWERCOUNT, countAnswer);
                            sendJsonRequest(recieved_message);
                            sendAnswerNotification(title, 0, "answer", "Ashutosh");
                            break;
                        case 700:
                            sendNotification(title, 0, "", "ashutosh");
                            break;


                    }
                } else {
                    AndyUtils.showLongToast(jsonObject.optString("error_message"), this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        // Release the wake lock provided by the WakefulBroadcastReceiver.
//        GcmBroadcastReceiver.completeWakefulIntent(intent);

    }


    private void sendFirstJsonRequest(String recieved_message) {
        Intent newRequestIntent = new Intent(Const.DOCTOR_REQUEST_STATUS);
        newRequestIntent.putExtra(Const.DOCTOR_INTENT_MESSAGE, recieved_message);
        newRequestIntent.putExtra("ashutosh", "pushNotificationByAshutosh");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(newRequestIntent);

    }

    private void sendJsonRequest(String recieved_message) {
        Intent newRequestIntent = new Intent(Const.DOCTOR_NEW_REQUEST_STATUS);
        newRequestIntent.putExtra(Const.DOCTOR_INTENT_MESSAGE, recieved_message);
        newRequestIntent.putExtra("ashutosh", "pushNotificationByAshutosh");
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(newRequestIntent);
    }

    private void sendNotification(String msg, int product_id, String name, String reciever_id) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);


        Intent chat_intent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                chat_intent, 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setVibrate(new long[]{300, 1000})
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setAutoCancel(true)
                        .setContentText(msg);
        mBuilder.setContentIntent(contentIntent);
        Notification mNotification = mBuilder.build();
        mNotificationManager.notify(NOTIFICATION_ID, mNotification);
    }

    private void sendAppointmentNotification(String msg, int product_id, String type, String reciever_id) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent chat_intent = new Intent(this, NotificationActivity.class);
        if (type.equals("answer")) {
            Bundle bundle = new Bundle();
            bundle.putString(Const.NOTIFICATION, Const.NotificationMessageFragment);
            chat_intent.putExtras(bundle);
        } else if (type.equals("appointment")) {
            Bundle bundle = new Bundle();
            bundle.putString(Const.NOTIFICATION, Const.NotificationAppointmentFragment);
            chat_intent.putExtras(bundle);
        }
        PendingIntent contentIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(),
                chat_intent, 0);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setVibrate(new long[]{300, 1000})
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setAutoCancel(true)
                        .setContentText(msg);
        mBuilder.setContentIntent(contentIntent);
        Notification mNotification = mBuilder.build();
        mNotificationManager.notify(NOTIFICATION_ID, mNotification);
    }


    private void sendAppointmentRepeatNotification(String msg, int product_id, String type, String reciever_id) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent chat_intent = new Intent(this, NotificationActivity.class);
        if (type.equals("answer")) {
            Bundle bundle = new Bundle();
            bundle.putString(Const.NOTIFICATION, Const.NotificationMessageFragment);
            chat_intent.putExtras(bundle);
        } else if (type.equals("appointment")) {
            Bundle bundle = new Bundle();
            bundle.putString(Const.NOTIFICATION, Const.NotificationAppointmentFragment);
            chat_intent.putExtras(bundle);
        }
        PendingIntent contentIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(),
                chat_intent, 0);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setVibrate(new long[]{300, 1000})
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setAutoCancel(true)
                        .setContentText(msg);
        mBuilder.setContentIntent(contentIntent);
        Notification mNotification = mBuilder.build();
        mNotification.flags |= Notification.FLAG_INSISTENT;
        mNotificationManager.notify(NOTIFICATION_ID, mNotification);
    }


    private void sendAnswerNotification(String msg, int product_id, String name, String reciever_id) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        Intent chat_intent = new Intent(this, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Const.INCOMING_BUNDLE, Const.NotificationAnswerFragment);
        chat_intent.putExtras(bundle);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                chat_intent, 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getResources().getString(R.string.app_name))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setVibrate(new long[]{300, 1000})
                        .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                        .setAutoCancel(true)
                        .setContentText(msg);
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

}
