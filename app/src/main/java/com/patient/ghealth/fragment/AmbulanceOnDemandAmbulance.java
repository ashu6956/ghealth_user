package com.patient.ghealth.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;

/**
 * Created by user on 9/12/2016.
 */
public class AmbulanceOnDemandAmbulance extends Fragment implements View.OnClickListener
{
    private MapView mapView;
    private boolean mapsSupported = true;
    private GoogleMap mGoogleMap;
    private MainActivity activity;
    private ImageView homeImageView,statusImageView;
    private TextView yourAmbulanceText, ambulanceDepartText, ambulanceName;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_doctor_on_demand_doctor,container,false);
        activity= (MainActivity) getActivity();
        mapView= (MapView) view.findViewById(R.id.mapview_doctor_on_demand_doctor);
        homeImageView= (ImageView) view.findViewById(R.id.iv_doctor_on_demand_home);
        statusImageView= (ImageView) view.findViewById(R.id.iv_doctor_on_demand_doctor_status);
        ambulanceName = (TextView) view.findViewById(R.id.tv_doctor_on_demand_doctor_name);
        ambulanceDepartText = (TextView) view.findViewById(R.id.tv_doctor_on_demand_depart_status);
        yourAmbulanceText = (TextView) view.findViewById(R.id.tv_doctor_on_demand_your);
        yourAmbulanceText.setText(getString(R.string.ambulance));
        ambulanceDepartText.setText(getString(R.string.ambulance_depart));
        ambulanceName.setText("Hospital Rahimah");
        homeImageView.setOnClickListener(this);
        ambulanceDepartText.setOnClickListener(this);
        statusImageView.setOnClickListener(this);
        return view;
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {

        AndyUtils.appLog("DoctorNearByFragment", "onSaveInstanceState");
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            mapsSupported = false;
        }
        if (mapView != null) {
            mapView.onCreate(savedInstanceState);
        }
        initializeMap();

    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        activity.currentFragment= Const.AmbulanceOnDemandAmbulanceFragment;
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    private void initializeMap() {
        if (mGoogleMap == null && mapsSupported) {
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap)
                {
                    mGoogleMap = googleMap;

                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    LatLng sourceLatLng = new LatLng(12.908136,77.647608);

                    CameraPosition sourecCameraPosition = CameraPosition.builder()
                            .target(sourceLatLng)
                            .zoom(16)
                            .build();
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(sourecCameraPosition));
                    googleMap.addMarker(new MarkerOptions().position(sourceLatLng).title("Source")).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                }
            });


//            mGoogleMap.setMyLocationEnabled(false);
        }

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.iv_doctor_on_demand_doctor_status:
                showDoctorStatusDialog();
                break;
            case R.id.iv_doctor_on_demand_home:
//                activity.mainTabLayout.setVisibility(View.VISIBLE);
//                activity.mainToolbar.setVisibility(View.VISIBLE);
//                activity.layoutToolbarOptions.setVisibility(View.VISIBLE);
//                activity.backbuttonImage.setVisibility(View.GONE);
//                activity.addFragment(new HomeFragment(),false,"",Const.HOME_FRAGMENT,true);
                break;
            case R.id.tv_doctor_on_demand_depart_status:
                showInvoiceDialog();
                break;
            default:

        }
    }

    private void showInvoiceDialog()
    {
        final Dialog dialog=new Dialog(getActivity(),R.style.DialogDocotrTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_invoice_layout);
//        LinearLayout treatmentLayout= (LinearLayout) dialog.findViewById(R.id.ll_treatment_fee);
//        LinearLayout medicineLayout= (LinearLayout) dialog.findViewById(R.id.ll_medicine_fee);
//        treatmentLayout.setVisibility(View.GONE);
//        medicineLayout.setVisibility(View.GONE);
        ImageButton doneButton= (ImageButton) dialog.findViewById(R.id.ib_invoice_done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.cancel();
                showFeedBackDialog();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void showFeedBackDialog()
    {
        final Dialog dialog=new Dialog(getActivity(),R.style.DialogDocotrTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_feedback_layout);
        ImageButton doneButton= (ImageButton) dialog.findViewById(R.id.ib_feedback_done);
        TextView ambulanceName= (TextView) dialog.findViewById(R.id.tv_feedback_doctor_name);
        ambulanceName.setText("Hospital Rahimah");
        ImageView ambulanceIcon= (ImageView) dialog.findViewById(R.id.iv_feedback_ambulance_icon);
        ambulanceIcon.setVisibility(View.VISIBLE);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                dialog.cancel();
                activity.mainTabLayout.setVisibility(View.VISIBLE);
                activity.mainToolbar.setVisibility(View.VISIBLE);
                activity.layoutToolbarOptions.setVisibility(View.VISIBLE);
                activity.backbuttonImage.setVisibility(View.GONE);
                activity.addFragment(new HomeFragment(),false,"",Const.HOME_FRAGMENT,true);
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }


    private void showDoctorStatusDialog()
    {
        Dialog dialog=new Dialog(getActivity(),R.style.DialogDocotrTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_doctor_status_layout);
        TextView ambulanceDepart= (TextView) dialog.findViewById(R.id.tv_doctor_depart);
        TextView ambulanceOnTheWay= (TextView) dialog.findViewById(R.id.tv_doctor_on_the_way);
        TextView ambulanceArrived= (TextView) dialog.findViewById(R.id.tv_doctor_arrived);
        TextView pickedUp= (TextView) dialog.findViewById(R.id.tv_start_treatment);
        TextView reachedHospital= (TextView) dialog.findViewById(R.id.tv_end_treatment);

        ambulanceDepart.setText(getString(R.string.ambulance_depart));
        ambulanceOnTheWay.setText(getString(R.string.ambulance_on_the_way));
        ambulanceArrived.setText(getString(R.string.ambulance_arrived));
        pickedUp.setText(getString(R.string.picked_up));
        reachedHospital.setText(getString(R.string.reached_hospital));
        dialog.show();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
        FragmentManager manager=getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.AmbulanceOnDemandAmbulanceFragment);
        if (fragment != null) {
            AndyUtils.appLog("AmbulanceOnDemandAmbulanceFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }

    }
}
