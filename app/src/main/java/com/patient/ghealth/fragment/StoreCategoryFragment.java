package com.patient.ghealth.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.HealthStoreActivity;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.StoreCategoryAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.ProductCategory;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 11/18/2016.
 */
public class StoreCategoryFragment extends Fragment implements AsyncTaskCompleteListener
{

    private RecyclerView categoryRecyclerView;
    private ProgressBar categoryProgressBar;
    private MainActivity activity;
    private List<ProductCategory> productCategoryList;
    private StoreCategoryAdapter categoryAdapter;
    private int count=0;
    private TextView noCategoryText;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productCategoryList = new ArrayList<>();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_store_category_layout,container,false);
        activity= (MainActivity) getActivity();
        categoryRecyclerView= (RecyclerView) view.findViewById(R.id.rv_category);
        noCategoryText= (TextView) view.findViewById(R.id.tv_noCategory);
        categoryProgressBar= (ProgressBar) view.findViewById(R.id.category_progressBar);
        categoryAdapter = new StoreCategoryAdapter(getActivity(),productCategoryList,activity);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        categoryRecyclerView.setLayoutManager(mLayoutManager);
        categoryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        categoryRecyclerView.setAdapter(categoryAdapter);
        getCategoryDetails();
        return view;
    }

    private  void getCategoryDetails()
    {
        if(!AndyUtils.isNetworkAvailable(getActivity()))
        {
            AndyUtils.showLongToast(getString(R.string.no_internet),getActivity());
            return;
        }
        categoryProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL,Const.ServiceType.GET_CATEGORY_URL);

        AndyUtils.appLog("Ashutosh","CategoryMap" +map);
        new HttpRequester(getActivity(),Const.GET,map,Const.ServiceCode.GET_CATEGORY,this);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment= Const.StoreCategoryFragment;
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode)
    {
        categoryProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject;
        switch (serviceCode)
        {
            case Const.ServiceCode.GET_CATEGORY:
                AndyUtils.appLog("Ashutosh","CategoryResponse" +response);
                try {
                    jsonObject=new JSONObject(response);
                    if(jsonObject.optString("status").equals("success"))
                    {
                        JSONObject responseJsonObject=jsonObject.optJSONObject("response");
                        if(responseJsonObject!=null)
                        {
                            JSONArray categoryArray = responseJsonObject.optJSONArray("categories");
                            if(categoryArray!=null && categoryArray.length()>0)
                            {
                                for (int i = 0; i < categoryArray.length(); i++) {
                                    JSONObject categoryObject = categoryArray.getJSONObject(i);
                                    ProductCategory mProductCategory = new ProductCategory();
                                    mProductCategory.setCategoriesId(categoryObject.optString(Const.Params._ID));
                                    mProductCategory.setCategories(categoryObject.optString(Const.Params.NAME));
                                    JSONObject imageObject = categoryObject.optJSONObject("image");
                                    if (imageObject == null) {
                                        mProductCategory.setImageUrl("");
                                    } else {
                                        JSONObject cdnObject = imageObject.getJSONObject("cdn");
                                        mProductCategory.setImageUrl(cdnObject.optString("url"));
                                    }
                                    productCategoryList.add(mProductCategory);
                                }
                                categoryAdapter.notifyDataSetChanged();
                            }
                            else
                            {
                                noCategoryText.setVisibility(View.VISIBLE);
                            }

                        }
                        else
                        {
                            noCategoryText.setVisibility(View.VISIBLE);
                        }


                    }
                    else
                    {
                        noCategoryText.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


        }

    }
}
