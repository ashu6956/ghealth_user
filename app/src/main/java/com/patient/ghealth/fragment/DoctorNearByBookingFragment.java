package com.patient.ghealth.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.DoctorListActivity;
import com.patient.ghealth.adapter.DoctorNearByAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.location.LocationHelper;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by getit on 8/12/2016.
 */
public class DoctorNearByBookingFragment extends Fragment implements AsyncTaskCompleteListener, LocationHelper.OnLocationReceived {
    private static final String TAG = DoctorNearByOnlineConsultFragment.class.getSimpleName();
    private MapView mapView;
    private boolean mapsSupported = true;
    private GoogleMap mGoogleMap = null;
    private int clickedPosition;
    private LocationHelper locationHelper;
    private Bundle mBundle;
    private View view;
    private Marker markerpatient;
    private CurrentLocationAndUpdate locationAndUpdate;
    private ImageView doctorsIcon, aboutImages_1, aboutImages_2, aboutImages_3, call_icon, chat_icon, video_icon;
    private CardView cv_aboutImages_1, cv_aboutImages_2, cv_aboutImages_3;
    private TextView consultText, booktext, doctorRating, doctorName, doctorClinicName, doctorAddress, doctorExperience, doctorHelpedPeople, doctorConsultantFee;
    private EditText sourceEditText, destinationEditTExt;
    private Location myLocation;
    private StringBuilder doctorAddressStrongBuilder;
    private HashMap<LatLng, Integer> markerHashMap;
    private List<DoctorOnLineDetails> onLineDetailsList;
    private int ClickedPosition = 0;
    private LatLngBounds bounds;
    private LatLngBounds.Builder builder;
    private String type;
    private ProgressDialog nearByOnLineProgressDialog;
    private TextView betweenDistance;
    private Handler googleMatrixHandler;
    private Marker lastSelectedMarker;
    private RelativeLayout doctorNearByLayout;
    private ViewPager viewPager;
    private DoctorListActivity activity;
    private ImageButton currentLocationButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
        activity = (DoctorListActivity) getActivity();

        if (!TextUtils.isEmpty((CharSequence) PreferenceHelper.getParam(activity, Const.Params.LANGUAGE, ""))) {
            String languageToLoad = (String) PreferenceHelper.getParam(activity, Const.Params.LANGUAGE, ""); // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config2 = new Configuration();
            config2.locale = locale;
            activity.getResources().updateConfiguration(config2,
                    activity.getResources().getDisplayMetrics());

        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_doctor_near_by, container, false);
        mapView = (MapView) view.findViewById(R.id.mapview);
        viewPager = (ViewPager) view.findViewById(R.id.vp_nearby);
        currentLocationButton = (ImageButton) view.findViewById(R.id.ib_current_location);
        doctorNearByLayout = (RelativeLayout) view.findViewById(R.id.near_by_layout);
        viewPager.setClipToPadding(false);
        viewPager.setPageMargin(15);
        sourceEditText = (EditText) view.findViewById(R.id.et_doctor_near_by_source_address);
        destinationEditTExt = (EditText) view.findViewById(R.id.et_doctor_near_by_destination_address);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {

            }

            @Override
            public void onPageSelected(int position) {
                AndyUtils.appLog("SelectedPagePosition", position + "");
                destinationEditTExt.setText(onLineDetailsList.get(position).getClinicAddress());

                if (mGoogleMap != null) {
                    mGoogleMap.clear();
//                    if (position == 1) {
//                        LatLng coordinate = new LatLng(12.2958, 76.6394); //Store these lat lng values somewhere. These should be constant.
//                        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
//                                coordinate, 15);
//
//                        MarkerOptions currentOption = new MarkerOptions();
//                        currentOption.position(coordinate);
//                        currentOption.title(onLineDetailsList.get(position).getDoctorName());
//                        currentOption.icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_marker));
//                        mGoogleMap.addMarker(currentOption);
//                        mGoogleMap.animateCamera(location);
                    if (!onLineDetailsList.get(position).getdLatitude().equals("") && !onLineDetailsList.get(position).getdLongitude().equals("")) {
                        LatLng coordinate = new LatLng(Double.valueOf(onLineDetailsList.get(position).getdLatitude()), Double.valueOf(onLineDetailsList.get(position).getdLongitude())); //Store these lat lng values somewhere. These should be constant.
                        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                                coordinate, 15);

                        MarkerOptions currentOption = new MarkerOptions();
                        currentOption.position(coordinate);
                        currentOption.title(onLineDetailsList.get(position).getDoctorName());
                        currentOption.icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_marker));
                        mGoogleMap.addMarker(currentOption);
                        mGoogleMap.animateCamera(location);
                        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
                    }
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        currentLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (markerdoctor.getPosition() != null)
//                    mGoogleMap.animateCamera(CameraUpdateFactory
//                            .newLatLng(markerdoctor.getPosition()));
                try {
                    if (mGoogleMap != null && myLocation != null) {

                        LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                        mGoogleMap.animateCamera(cameraUpdate);
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        locationHelper = new LocationHelper(activity);
        locationHelper.setLocationReceivedLister(this);
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            mapsSupported = false;
        }
        if (mapView != null) {
            mapView.onCreate(mBundle);
        }
        setUpMap();
    }

    @Override
    public void onResume() {
        AndyUtils.appLog("Ashutosh", "onResume");
        super.onResume();
        mapView.onResume();
//        activity.currentFragment = Const.DoctorOnDemandFragment;
//        IntentFilter filter = new IntentFilter(Const.DOCTOR_REQUEST_STATUS);
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver,
//                filter);

    }


    @Override
    public void onPause() {
        AndyUtils.appLog("Ashutosh", "onPause");
        super.onPause();
//        locationAndUpdate.disconnect();
        mapView.onPause();

    }


    @Override
    public void onStop() {
        super.onStop();
    }

    private void setUpMap() {
        // map.
        if (mGoogleMap == null) {
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mapView.invalidate();
                    mGoogleMap = googleMap;
                }
            });
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_DOCTOR_NEARBY_BOOKING:
//                AndyUtils.removeProgressDialog();
                AndyUtils.appLog("Ashutosh", "DoctorNearByBookingResponse" + response);
                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.getString("success").equals("true")) {
                        String sourceAddress = jsonObject.optString(Const.SOURCE_ADDRESS);
                        onLineDetailsList = new ArrayList<DoctorOnLineDetails>();
                        JSONArray jsonArray = jsonObject.getJSONArray("doctors");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject onLineJsonObject = jsonArray.getJSONObject(i);
                            if (Integer.parseInt(onLineJsonObject.optString("c_count")) > 0) {
                                DoctorOnLineDetails doctorOnLineDetails = new DoctorOnLineDetails();
                                doctorOnLineDetails.setClinicAddress(onLineJsonObject.optString("c_address"));
                                doctorOnLineDetails.setDoctor_clinic_image_first(onLineJsonObject.optString("c_pic1"));
                                doctorOnLineDetails.setDoctor_clinic_image_second(onLineJsonObject.optString("c_pic2"));
                                doctorOnLineDetails.setDoctor_clinic_image_third(onLineJsonObject.optString("c_pic3"));
                                doctorOnLineDetails.setDoctorClinicName(onLineJsonObject.optString("c_name"));
                                doctorOnLineDetails.setdLatitude(onLineJsonObject.optString("d_latitude"));
                                doctorOnLineDetails.setdLongitude(onLineJsonObject.optString("d_longitude"));
                                doctorOnLineDetails.setBetweenDistance(onLineJsonObject.optString("eta"));
                                doctorOnLineDetails.setClinicId(onLineJsonObject.optString("c_id"));
                                doctorOnLineDetails.setClinicNoOfDoctor(onLineJsonObject.optString("c_count"));
                                onLineDetailsList.add(doctorOnLineDetails);
                            }
                        }

                        if (onLineDetailsList.size() > 0) {
                            sourceEditText.setVisibility(View.VISIBLE);
                            destinationEditTExt.setVisibility(View.VISIBLE);
                            sourceEditText.setText(sourceAddress);
                            sourceEditText.setEnabled(false);
                            doctorNearByLayout.setVisibility(View.VISIBLE);
                            DoctorNearByAdapter pagerAdapter = new DoctorNearByAdapter(getActivity(), onLineDetailsList, activity, myLocation, Const.BOOKING);
                            viewPager.setAdapter(pagerAdapter);
                            setMarkerListOnMap(onLineDetailsList);
                        } else {
                            doctorNearByLayout.setVisibility(View.GONE);
                        }

                    } else {

                        doctorNearByLayout.setVisibility(View.GONE);
//                        AndyUtils.showShortToast(jsonObject.getString("error_message"), getActivity());
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
                break;


        }
    }

    private String getDoctorAddress(DoctorOnLineDetails doctorOnLineDetails) {

        return doctorAddressStrongBuilder.toString();
    }

    private void setMarkerListOnMap(final List<DoctorOnLineDetails> doctorOnLineDetailsList) {

        if (myLocation != null) {
            destinationEditTExt.setText(doctorOnLineDetailsList.get(0).getClinicAddress());
            destinationEditTExt.setEnabled(false);
        }
        if (doctorOnLineDetailsList.size() > 0) {
            markerHashMap = new HashMap<>();
            AndyUtils.appLog("AfetrAddingDoctorNearBySize", doctorOnLineDetailsList.size() + "");
            if (!doctorOnLineDetailsList.get(0).getdLatitude().equals("") && !doctorOnLineDetailsList.get(0).getdLongitude().equals("")) {
                LatLng latLng = new LatLng(Double.valueOf(doctorOnLineDetailsList.get(0).getdLatitude()), Double.valueOf(doctorOnLineDetailsList.get(0).getdLongitude()));
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                        latLng, 15);
                mGoogleMap.clear();
                MarkerOptions currentOption = new MarkerOptions();
                currentOption.position(latLng);
                currentOption.title(doctorOnLineDetailsList.get(0).getDoctorName());
                currentOption.icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_marker));
                mGoogleMap.addMarker(currentOption);
                mGoogleMap.animateCamera(location);
                mGoogleMap.getUiSettings().setMapToolbarEnabled(true);

            }

        }
    }


    @Override
    public void onLocationReceived(LatLng latlong) {

    }

    @Override
    public void onLocationReceived(Location location) {
        if (location != null) {
            // drawTrip(latlong);
            myLocation = location;
            LatLng latLang = new LatLng(location.getLatitude(),
                    location.getLongitude());


        }

    }

    @Override
    public void onConntected(Bundle bundle) {

    }

    @Override
    public void onConntected(Location location) {


        if (location != null) {
            myLocation = location;
            if (myLocation != null) {
//                LatLng latLang = new LatLng(myLocation.getLatitude(),
//                        myLocation.getLongitude());
//
//                AndyUtils.appLog("onConnectedCurrentLatLng", myLocation.getLatitude() + " " + myLocation.getLongitude());
//                CameraPosition sourecCameraPosition = CameraPosition.builder()
//                        .target(latLang)
//                        .zoom(15)
//                        .build();
//                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(sourecCameraPosition));
//                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                String doctorSpecialistsId = (String) PreferenceHelper.getParam(getActivity(), Const.SPECIALITIES_DOCTOR_ID, "");
                AndyUtils.appLog(TAG, "DoctorSpecialistId" + doctorSpecialistsId);
                if (myLocation != null) {
                    AndyUtils.appLog("MyLoction", myLocation.toString());
                    getDoctorNearByBookingDetails(doctorSpecialistsId, myLocation);

                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.DoctorNearByOnlineConsultFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("HealthFeedsFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }

    public String getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(",");
                }
                strAdd = strReturnedAddress.toString();
                Log.d("MyCurrentLoctionAddress", strReturnedAddress.toString());
            } else {
                Log.w("MyCurrentLoctionAddress", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("MyCurrentLoctionAddress", "Canont get Address!");
        }
        return strAdd;
    }

    private void getDoctorNearByBookingDetails(String sId, Location currentLocation) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_DOCTOR_NEARBY_BOOKING_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.SPECIALITY_ID + "="
                + String.valueOf(sId) + "&" + Const.Params.LATITUDE + "="
                + String.valueOf(currentLocation.getLatitude()) + "&" + Const.Params.LONGITUDE + "="
                + String.valueOf(currentLocation.getLongitude()));

        AndyUtils.appLog("Ashutosh", "DoctorNearByBookingMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_DOCTOR_NEARBY_BOOKING, this);

    }

}

