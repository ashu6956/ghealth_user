package com.patient.ghealth.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.OrderDetail;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class RatingFragment extends Fragment implements AsyncTaskCompleteListener {

    private RatingBar ratingBar;
    private Button button;
    private EditText comment;
    private OrderDetail orderDetail;
    private MainActivity activity;

    public RatingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rating, container, false);
        activity = (MainActivity) getActivity();
        ratingBar = (RatingBar) view.findViewById(R.id.ratingStar);
        button = (Button) view.findViewById(R.id.submit_button);
        comment = (EditText) view.findViewById(R.id.comment_box);
        orderDetail = (OrderDetail) getArguments().getSerializable("orderDetail");

        if (!orderDetail.getProductDetail().getReviewList().isEmpty()) {
            ratingBar.setRating((float) orderDetail.getProductDetail().getReviewList().get(0).getStar());
            comment.setText(orderDetail.getProductDetail().getReviewList().get(0).getComment());
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ratingBar.getRating() == 0)
                {
                    AndyUtils.showShortToast("Please give rating", activity);
                    return;
                } else if (comment.getText().toString().trim().length() == 0) {
                    AndyUtils.showShortToast("Please enter a feedback", activity);
                    return;
                } else {
                    sendRating();
                }
            }
        });

        return view;
    }

    private void sendRating() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GIVE_PRODUCT_REVIEW_URL + orderDetail.getProductDetail().getProductId() + "/" + orderDetail.getOrderId());
        map.put("stars", String.valueOf(ratingBar.getRating()));
        map.put("comment", comment.getText().toString());
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "ProductRatingMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.GIVE_PRODUCT_REVIEW, this, headerMap);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GIVE_PRODUCT_REVIEW:
                AndyUtils.appLog("Ashutosh", "ProductReviewResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        AndyUtils.showShortToast("Product rated successfully", getActivity());
                        activity.mainTabLayout.getTabAt(0).select();
                        activity.mainTabLayout.setVisibility(View.VISIBLE);
                        activity.layoutToolbarOptions.setVisibility(View.VISIBLE);
                        activity.backbuttonImage.setVisibility(View.GONE);
                        activity.appLogo.setVisibility(View.VISIBLE);
                        activity.addFragment(new HomeFragment(), false, "", Const.HOME_FRAGMENT, false);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }

    }
}
