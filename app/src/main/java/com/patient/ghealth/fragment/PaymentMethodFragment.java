package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.CheckoutActivity;
import com.patient.ghealth.utils.Const;


/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentMethodFragment extends Fragment implements View.OnClickListener {

    private CardView paypal, PaybyCard, PaybyPayU;
    private CheckoutActivity activity;

    public PaymentMethodFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_method, container, false);
        activity = (CheckoutActivity) getActivity();
        paypal = (CardView) view.findViewById(R.id.PayPal);
        PaybyCard = (CardView) view.findViewById(R.id.PaybyCard);
        PaybyPayU = (CardView) view.findViewById(R.id.PaybyPayU);
        PaybyPayU.setOnClickListener(this);
        PaybyCard.setOnClickListener(this);
        paypal.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        SummaryFragment summaryFragment = new SummaryFragment();
        switch (v.getId()) {
            case R.id.PayPal:
                CheckoutActivity.paymentMethod = 0;
                gotoFragment(summaryFragment);
                break;
            case R.id.PaybyCard:
                CheckoutActivity.paymentMethod = 1;
//                gotoFragment(summaryFragment);
                break;
            case R.id.PaybyPayU:
                CheckoutActivity.paymentMethod = 2;
//                gotoFragment(summaryFragment);
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment= Const.PaymentMethodFragment;
    }

    private void gotoFragment(Fragment fragment) {
        activity.addFragment(fragment, false, getString(R.string.checkout), "", false);

    }
}
