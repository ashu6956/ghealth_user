package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.DoctorListActivity;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

/**
 * Created by user on 9/20/2016.
 */
public class DoctorBookingProfileDetailsFragment extends Fragment
{

    private TextView doctorClinicName,doctorClinicAddress,doctorConsultantFee,doctorConsult;
    private ImageView doctorLocationStaticMap,chat_icon,video_icon,call_icon;
    private DoctorOnLineDetails doctorOnLineDetails;
    private Bundle bundle;
    private String type;
//    private DoctorListActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_online_booking_doctor_profile_details_layout,container,false);
//        activity= (DoctorListActivity) getActivity();
        doctorClinicName= (TextView) view.findViewById(R.id.tv_profile_doctor_clinic_name);
        doctorClinicName= (TextView) view.findViewById(R.id.tv_profile_doctor_clinic_name);
        doctorConsultantFee= (TextView) view.findViewById(R.id.tv_profile_doctor_ConsultFee);
        doctorLocationStaticMap= (ImageView) view.findViewById(R.id.iv_profile_doctor_static_location);
        doctorClinicAddress= (TextView) view.findViewById(R.id.tv_profile_clinic_address);
        doctorConsult= (TextView) view.findViewById(R.id.tv_profile_doctor_consult);
        doctorConsult.setText(getString(R.string.booking));
        bundle=getArguments();
        if(bundle!=null)
        {
            type=bundle.getString(Const.DOCTOR_TYPE);
            doctorOnLineDetails= (DoctorOnLineDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
            if(doctorOnLineDetails!=null && type.equals(Const.BOOKING))
            {
                setDataOnViews(doctorOnLineDetails);
            }
        }

        doctorConsult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(doctorOnLineDetails!=null) {
                    String doctorId = doctorOnLineDetails.getDoctorOnLineId();
                    AndyUtils.appLog("DoctorBookingFragment", "BookDoctorId" + doctorId);
                    DoctorAvailabilityDialogFragment availability = new DoctorAvailabilityDialogFragment();
                    PreferenceHelper.setParam(getActivity(), Const.DOCTOR_ID, doctorId);
                    availability.show(getChildFragmentManager(), "DoctorAvailabilityDialogFragment");
                }
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
//        activity.currentFragment=Const.BookingDoctorProfileDetailsFragment;

    }

    private void setDataOnViews(DoctorOnLineDetails onLineDetails)
    {
        doctorClinicName.setText(onLineDetails.getDoctorClinicName());

        doctorClinicAddress.setText(doctorOnLineDetails.getClinicAddress());
        doctorConsultantFee.setText("MYR "+doctorOnLineDetails.getDoctorChatConsultFee()+" "+getString(R.string.onwards_consultation_fee));
        if (!doctorOnLineDetails.getdLatitude().equals("") && !doctorOnLineDetails.getdLongitude().equals("")) {
            Glide.with(getActivity()).load(getGoogleMapThumbnail(Double.valueOf(doctorOnLineDetails.getdLatitude()), Double.valueOf(doctorOnLineDetails.getdLongitude()))).into(doctorLocationStaticMap);
        }
    }

    public static String getGoogleMapThumbnail(double lati, double longi){
        String staticMapUrl="http://maps.google.com/maps/api/staticmap?center="+lati+","+longi+"&markers="+lati+","+longi+"&zoom=14&size=400x250&sensor=false";
        return staticMapUrl;
    }

//    @Override
//    public void onDestroyView() {
//
//        Fragment fragment = (getFragmentManager()
//                .findFragmentById(R.id.fl_doctor_list));
//        if (fragment!=null && fragment.isResumed()) {
//            getFragmentManager().beginTransaction().remove(fragment)
//                    .commitAllowingStateLoss();
//        }
//        super.onDestroyView();
//    }
}
