package com.patient.ghealth.fragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;

/**
 * Created by user on 9/12/2016.
 */
public class AmbulanceOnDemandFragment extends Fragment implements View.OnClickListener
{

    private MapView mapView;
    private boolean mapsSupported = true;
    private GoogleMap mGoogleMap;
    private MainActivity activity;
    private TextView needAmbulanceText, pickNowText,bookingText;
    private ImageView pickNowIcon,bookingIcon;
    private RelativeLayout pickNowLayout,bookingLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctor_on_demand, container, false);
        activity = (MainActivity) getActivity();
        mapView = (MapView) view.findViewById(R.id.mapview_doctor_on_demand);
        ImageView doctorNext = (ImageView) view.findViewById(R.id.iv_dialog_on_demand_doctor);
        ImageView bookingNext = (ImageView) view.findViewById(R.id.iv_dialog_on_demand_booking);
        needAmbulanceText = (TextView) view.findViewById(R.id.tv_on_demand_need);
        pickNowText = (TextView) view.findViewById(R.id.tv_doctor_on_demand_doctor);
        bookingText= (TextView) view.findViewById(R.id.tv_doctor_on_demand_booking);
        pickNowIcon = (ImageView) view.findViewById(R.id.iv_doctor_on_demand_doctor);
        bookingIcon= (ImageView) view.findViewById(R.id.iv_doctor_on_demand_calendar);
        pickNowLayout = (RelativeLayout) view.findViewById(R.id.rl_doctor_on_demand_doctor);
        bookingLayout= (RelativeLayout) view.findViewById(R.id.rl_doctor_on_demand_booking);
        pickNowLayout.setOnClickListener(this);
        bookingLayout.setOnClickListener(this);
        pickNowIcon.setImageResource(R.drawable.ambulance_blue);
        pickNowText.setText(getString(R.string.pick_now));
        bookingText.setText(getString(R.string.booking));
        needAmbulanceText.setText(getString(R.string.need_ambulance));
        doctorNext.setOnClickListener(this);
        bookingNext.setOnClickListener(this);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        AndyUtils.appLog("DoctorNearByFragment", "onSaveInstanceState");
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            mapsSupported = false;
        }
        if (mapView != null) {
            mapView.onCreate(savedInstanceState);
        }
        initializeMap();


    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
        activity.currentFragment = Const.AmbulanceOnDemandFragment;
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    private void initializeMap() {
        if (mGoogleMap == null && mapsSupported) {
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mGoogleMap = googleMap;

                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }

                    LatLng sourceLatLng = new LatLng(12.908136,77.647608);




                    CameraPosition sourecCameraPosition = CameraPosition.builder()
                            .target(sourceLatLng)
                            .zoom(16)
                            .build();

//            mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(sourecCameraPosition));
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(sourecCameraPosition));
                    googleMap.addMarker(new MarkerOptions().position(sourceLatLng).title("Source")).setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                }
            });


        }

    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.iv_dialog_on_demand_doctor:
//                activity.mainToolbar.setVisibility(View.GONE);
//                activity.mainTabLayout.setVisibility(View.GONE);
//                activity.addFragment(new AmbulanceOnDemandAmbulance(), false, getString(R.string.ambulance_on_demand), Const.AmbulanceOnDemandAmbulanceFragment, false);
                break;

            case R.id.rl_doctor_on_demand_doctor:
//                activity.mainToolbar.setVisibility(View.GONE);
//                activity.mainTabLayout.setVisibility(View.GONE);
//                activity.addFragment(new AmbulanceOnDemandAmbulance(), false, getString(R.string.ambulance_on_demand), Const.AmbulanceOnDemandAmbulanceFragment, false);
                break;
            case R.id.iv_dialog_on_demand_booking:
//                DoctorAvailabilityDialogFragment availability = new DoctorAvailabilityDialogFragment(0);
//                availability.show(getChildFragmentManager(), "DoctorAvailabilityDialogFragment");
                break;
            case R.id.rl_doctor_on_demand_booking:
//                DoctorAvailabilityDialogFragment availability_layout = new DoctorAvailabilityDialogFragment(0);
//                availability_layout.show(getChildFragmentManager(), "DoctorAvailabilityDialogFragment");
                break;

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.DoctorOnDemandFragment);
        if (fragment != null) {
            AndyUtils.appLog("DoctorOnDemandFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }

    }
