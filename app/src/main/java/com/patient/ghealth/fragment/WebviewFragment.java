package com.patient.ghealth.fragment;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.HealthStoreActivity;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class WebviewFragment extends Fragment {

    private WebView webView;
    private HealthStoreActivity activity;

    public WebviewFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!TextUtils.isEmpty((CharSequence) PreferenceHelper.getParam(getActivity(), Const.Params.LANGUAGE, ""))) {
            String languageToLoad = (String) PreferenceHelper.getParam(getActivity(), Const.Params.LANGUAGE, ""); // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config2 = new Configuration();
            config2.locale = locale;
            getResources().updateConfiguration(config2,
                    getResources().getDisplayMetrics());

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_webview, container, false);
        activity = (HealthStoreActivity) getActivity();
        String htmlContent = getArguments().getString(Const.WEB_VIEW_INTENT_KEY);
        webView = (WebView) view.findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        //   webView.getSettings().setUseWideViewPort(true);
        //  webView.getSettings().setLoadWithOverviewMode(true);
        //   webView.getSettings().setSupportZoom(true);
        //  webView.getSettings().setBuiltInZoomControls(true);
        //  webView.getSettings().setDisplayZoomControls(false);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                getActivity().setProgress(progress * 1000);
            }
        });
        webView.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}</style>" + htmlContent, "text/html", "charset=utf-8", null);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.WebViewFragment;
    }
}
