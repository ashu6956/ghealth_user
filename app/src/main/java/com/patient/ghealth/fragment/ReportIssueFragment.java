package com.patient.ghealth.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.networking.MultiPartRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by user on 9/8/2016.
 */
public class ReportIssueFragment extends Fragment implements View.OnClickListener, AsyncTaskCompleteListener {

    private static final String TAG = "ReportIssueFragment";
    private TextView sendText, viewheder;
    private EditText edit_report_issue;
    private ImageView addImageView, camera;
    private Uri uri;
    private RelativeLayout screenshotLayout;
    private String filePath;
    private MainActivity activity;
    private TextView screenShotText;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report_issue_latout, container, false);
        activity = (MainActivity) getActivity();
        sendText = (TextView) view.findViewById(R.id.tv_report_issues_send);
        viewheder = (TextView) view.findViewById(R.id.view_header);
        edit_report_issue = (EditText) view.findViewById(R.id.et_report_issues);
        addImageView = (ImageView) view.findViewById(R.id.iv_report_issues_add_images);
        screenshotLayout = (RelativeLayout) view.findViewById(R.id.rl_screenshot);
        camera = (ImageView) view.findViewById(R.id.iv_camera);
        screenShotText = (TextView) view.findViewById(R.id.tv_screenshot);
        screenshotLayout.setOnClickListener(this);
        sendText.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_screenshot:
                showPictureDialog();
                break;

            case R.id.tv_report_issues_send:

                if (edit_report_issue.getText().toString().length() != 0) {
                    createReportIssue();
                } else {
                    AndyUtils.showShortToast(getString(R.string.enter_report_issue), getActivity());
                }
                break;
        }
    }


    private void createReportIssue() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showShortToast(getString(R.string.no_internet), getActivity());
            return;
        }
        AndyUtils.showSimpleProgressDialog(getActivity(), getString(R.string.please_wait), false);

        HashMap<String, String> map = new HashMap();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_REPORT_ISSUE_URl);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));

        if (filePath == null) {
            map.put(Const.Params.CONTENT, edit_report_issue.getText().toString());
            map.put(Const.Params.PICTURE, "");
            AndyUtils.appLog("Ashutosh", "CreateReportIssueMap" + map);
            new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.CREATE_REPORT_ISSUE, this);
        } else {
            map.put(Const.Params.CONTENT, edit_report_issue.getText().toString());
            map.put(Const.Params.PICTURE, filePath);
            AndyUtils.appLog("Ashutosh", "CreateReportIssueMap" + map);
            new MultiPartRequester(getActivity(), map, Const.ServiceCode.CREATE_REPORT_ISSUE, this);
        }


    }


    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.ReportIssueFragment;

    }

    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(getString(R.string.choose_your_option));
        String[] items = {getString(R.string.gallery), getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        choosePhotoFromGallary();
                        break;
                    case 1:
                        takePhotoFromCamera();
                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, Const.CHOOSE_PHOTO);

    }

    private void takePhotoFromCamera() {
        Calendar cal = Calendar.getInstance();
        File file = new File(Environment.getExternalStorageDirectory(),
                (cal.getTimeInMillis() + ".jpg"));
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {

            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        uri = Uri.fromFile(file);
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(i, Const.TAKE_PHOTO
        );
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null,
                null, null, null);

        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Activity Res", "" + requestCode);
        switch (requestCode) {
            case Const.CHOOSE_PHOTO:
                if (data != null) {

                    uri = data.getData();
                    if (uri != null) {

                        beginCrop(uri);
                    } else {
                        AndyUtils.showLongToast(getString(R.string.unable_to_select_image), getActivity());
                    }
                }
                break;
            case Const.TAKE_PHOTO:
                if (uri != null) {

                    beginCrop(uri);


                } else {
                    AndyUtils.showLongToast(getString(R.string.unable_to_select_image), getActivity());
                }
                break;
            case Crop.REQUEST_CROP:

                if (data != null)
                    handleCrop(resultCode, data);

                break;

        }

    }

    private void beginCrop(Uri source) {
        // Uri outputUri = Uri.fromFile(new File(registerActivity.getCacheDir(),
        // "cropped"));
        AndyUtils.appLog("ProfileFragment", "beginCrop");
        Uri outputUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), (Calendar.getInstance()
                .getTimeInMillis() + ".jpg")));
        Crop.of(source, outputUri).asSquare().start(getActivity(), this);
    }

    private void handleCrop(int resultCode, Intent result) {
        AndyUtils.appLog(TAG, "handleCrop");
        if (resultCode == getActivity().RESULT_OK) {
            filePath = getRealPathFromURI(Crop.getOutput(result));
            addImageView.setVisibility(View.VISIBLE);
            camera.setVisibility(View.GONE);
            screenShotText.setVisibility(View.GONE);
            Glide.with(activity).load(Crop.getOutput(result)).into(addImageView);
            AndyUtils.appLog("ReportissueFragment", filePath.toString());
        } else if (resultCode == Crop.RESULT_ERROR) {
            AndyUtils.showLongToast(Crop.getError(result).getMessage(), getActivity());
        }
    }

    @Override
    public void onDestroyView() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.ReportIssueFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("ReportIssueFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onDestroyView();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        AndyUtils.removeProgressDialog();
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.CREATE_REPORT_ISSUE:
                AndyUtils.appLog("Ashutosh", "CreateIssueResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        AndyUtils.showShortToast("Report Issue Created Successfully", getActivity());
                        activity.addFragment(new UserSettingsFragment(), false, "", Const.UserSettingsFragment, true);
                        activity.appLogo.setVisibility(View.VISIBLE);
                        viewheder.setVisibility(View.GONE);
                        activity.mainTabLayout.setVisibility(View.VISIBLE);
                        activity.layoutToolbarOptions.setVisibility(View.VISIBLE);
                        activity.backbuttonImage.setVisibility(View.GONE);
                    } else {
                        AndyUtils.showShortToast(jsonObject.getString("error_message"), getActivity());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
    }
}
