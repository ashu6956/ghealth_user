package com.patient.ghealth.fragment;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.FollowMoreTopicsAdapter;
import com.patient.ghealth.adapter.HealthFeedsAdapter;
import com.patient.ghealth.adapter.RecyclerViewItemClickListener;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.HealthFeedArticles;
import com.patient.ghealth.model.TopicsInfo;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 9/2/2016.
 */
public class FollowMoreTopicsFragment extends Fragment implements AsyncTaskCompleteListener
{
    private MainActivity activity;
    private RecyclerView followMoreTopicsRecyclerView;
    private FollowMoreTopicsAdapter topicsAdapter;
    private Context mContext;
   private EditText autoCompleteTextView;
    private ProgressBar topicsProgressbar;
    private TextView noTopicsText;
    private List<HealthFeedArticles> feedArticlesList;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext=context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_follow_more_topics_layout,container,false);
        activity= (MainActivity) getActivity();
        topicsProgressbar= (ProgressBar) view.findViewById(R.id.topics_progressbar);
        noTopicsText= (TextView) view.findViewById(R.id.tv_no_topics);
        autoCompleteTextView=activity.editSearchTopic;
        followMoreTopicsRecyclerView= (RecyclerView)view.findViewById(R.id.rv_follow_more_topics);
        followMoreTopicsRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(mContext, new RecyclerViewItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position)
            {
                AndyUtils.appLog("Clicked Position",position+"");
                activity.layoutToolbarOptions.setVisibility(View.VISIBLE);
                activity.backbuttonImage.setVisibility(View.GONE);
                activity.mainTabLayout.setVisibility(View.VISIBLE);
                activity.layoutSearchTopic.setVisibility(View.GONE);
                activity.appLogo.setVisibility(View.VISIBLE);
                HealthFeedFragment healthFeedFragment=new HealthFeedFragment();
                Bundle bundle=new Bundle();
                bundle.putString(Const.FollowMoreTopicsFragment,Const.CATEGORY);
                bundle.putString(Const.TOPICS_ARTICLE_ID,feedArticlesList.get(position).getArticleId());
                healthFeedFragment.setArguments(bundle);
                activity.addFragment(healthFeedFragment,false,"",Const.HealthFeedsFragment,true);
            }
        }));

        getFollowMoreTopicsDetails();
        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                String text = autoCompleteTextView.getText().toString().toLowerCase(Locale.getDefault());
                if(topicsAdapter!=null) {
                    topicsAdapter.filter(text);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return view;
    }


    private void getFollowMoreTopicsDetails()
    {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        topicsProgressbar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_ARTICLES_CATEGORY_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "GetArticlesCategoryMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_ARTICLES_CATEGORY, this);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity.mainTabLayout.setVisibility(View.GONE);
        activity.layoutSearchTopic.setVisibility(View.VISIBLE);
        activity.toolbarHeader.setText(getString(R.string.follow_more_topics));
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment=Const.FollowMoreTopicsFragment;
    }

    @Override
    public void onDestroyView() {

        autoCompleteTextView.setText("");
        FragmentManager manager=getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.FollowMoreTopicsFragment);
        if (fragment != null) {
            AndyUtils.appLog("FollowMoreTopics", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }

        super.onDestroyView();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode)
    {
        topicsProgressbar.setVisibility(View.GONE);
        JSONObject jsonObject;
        switch (serviceCode)
        {
            case Const.ServiceCode.GET_ARTICLES_CATEGORY:
                AndyUtils.appLog("Ashutosh","GetArticlesCategoryResponse" +response);

                try {
                    jsonObject=new JSONObject(response);
                    if(jsonObject.getString("success").equals("true"))
                    {
                        feedArticlesList=new ArrayList<>();
                        JSONArray feedJsonArray=jsonObject.optJSONArray("article_category");
                        if(feedJsonArray!=null && feedJsonArray.length()>0) {
                            for (int i = 0; i < feedJsonArray.length(); i++) {
                                JSONObject feedJsonObject = feedJsonArray.optJSONObject(i);
                                HealthFeedArticles healthFeedArticles = new HealthFeedArticles();
                                healthFeedArticles.setArticleId(feedJsonObject.optString("id"));
                                healthFeedArticles.setArticlePictureUrl(feedJsonObject.optString("picture"));
                                healthFeedArticles.setCategory(feedJsonObject.optString("category"));
                                feedArticlesList.add(healthFeedArticles);
                            }

                            LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
                            followMoreTopicsRecyclerView.setLayoutManager(layoutManager);
                            followMoreTopicsRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(30));
                            topicsAdapter=new FollowMoreTopicsAdapter(getActivity(),feedArticlesList);
                            followMoreTopicsRecyclerView.setAdapter(topicsAdapter);
                            noTopicsText.setVisibility(View.GONE);
                        }
                        else
                        {
                            noTopicsText.setVisibility(View.VISIBLE);
                        }


                    }
                    else
                    {
                        noTopicsText.setVisibility(View.VISIBLE);
                        AndyUtils.showShortToast(jsonObject.getString("error_message"),getActivity());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


        }

    }


    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int mVerticalSpaceHeight;

        public VerticalSpaceItemDecoration(int mVerticalSpaceHeight) {
            this.mVerticalSpaceHeight = mVerticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = mVerticalSpaceHeight;
        }
    }
}
