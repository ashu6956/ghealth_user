package com.patient.ghealth.fragment;

import android.app.Dialog;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.AddressDetailsAdapter;
import com.patient.ghealth.adapter.PlacesAutoCompleteAdapter;
import com.patient.ghealth.adapter.VerticalSpaceItemDecoration;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.AddressDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 11/15/2016.
 */
public class ShippingAddressFragment extends Fragment implements AsyncTaskCompleteListener
{

    private RecyclerView shippingRecyclerView;
    private ProgressBar  shippingProgressBar;
    private TextView noShippingText;
    private Dialog shippingDialog;
    private String sShippingAddress="";
    private LatLng shippingLatLng;
    private FloatingActionButton addAddressFAB;
    private MainActivity activity;
    private List<AddressDetails> addressDetailsList;
    public static AddressDetailsAdapter addressAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_shipping_address_layout,container,false);
        activity= (MainActivity) getActivity();
        shippingRecyclerView= (RecyclerView) view.findViewById(R.id.rv_shipping_address);
        shippingProgressBar= (ProgressBar) view.findViewById(R.id.shipping_progressBar);
        noShippingText= (TextView) view.findViewById(R.id.tv_no_shipping_text);
        addAddressFAB= (FloatingActionButton) view.findViewById(R.id.fb_add_shipping_address);

        getShippingAddress();

        addAddressFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addShippingAddressDialog();
            }
        });

        return view;
    }


    private void getShippingAddress()
    {
        if(!AndyUtils.isNetworkAvailable(getActivity()))
        {
            AndyUtils.showLongToast(getString(R.string.no_internet),getActivity());
            return;
        }
        shippingProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_ADDRESS_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "GetAddressMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_ADDRESS, this);

    }


    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment= Const.ShippingAddressFragment;
    }

    private void addShippingAddressDialog()
    {
        shippingDialog=new Dialog(getActivity(),R.style.DialogDocotrTheme);
        shippingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        shippingDialog.setContentView(R.layout.dialog_add_shipping_address_layout);
        final AutoCompleteTextView shippingAddress= (AutoCompleteTextView) shippingDialog.findViewById(R.id.et_dialog_shipping_address);
        Button addAddressButton= (Button) shippingDialog.findViewById(R.id.bn_add_shipping_address);

        addAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(shippingLatLng!=null && sShippingAddress!=null && !sShippingAddress.equals(""))
                {
                    shippingDialog.cancel();
                    addAddressDetails(shippingLatLng,sShippingAddress);
                }

            }
        });

        final PlacesAutoCompleteAdapter autoCompleteAdapter=new PlacesAutoCompleteAdapter(getActivity(),R.layout.autocomplete_list_text);
        shippingAddress.setAdapter(autoCompleteAdapter);
        autoCompleteAdapter.notifyDataSetChanged();

        shippingAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideKeyboard();
                if(!AndyUtils.isNetworkAvailable(getActivity()))
                {
                    AndyUtils.showShortToast(getResources().getString(R.string.no_internet), getActivity());
                    return;
                }
                sShippingAddress= autoCompleteAdapter.getItem(position);
                Handler handler = new Handler();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        shippingLatLng = getLocationFromAddress(getActivity(),sShippingAddress);
                        if(shippingLatLng!=null)
                        {
//                            PreferenceHelper.saveObject(sourceLatLng,mContext,Const.SOURCE_LATLNG);
                        }
                        else
                        {
                            AndyUtils.showShortToast(getString(R.string.address_invalid), getActivity());
                        }
                    }
                });

            }
        });
        shippingDialog.show();

    }

    private void addAddressDetails(LatLng latLng,String address)
    {
        if(!AndyUtils.isNetworkAvailable(getActivity()))
        {
            AndyUtils.showLongToast(getString(R.string.no_internet),getActivity());
            return;
        }
        shippingProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.ADD_ADDRESS_URL);
        map.put( Const.Params.ID , String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.SHIPPING_ADDRESS,address);
        map.put(Const.Params.SHIPPING_LATITUDE,String.valueOf(latLng.latitude));
        map.put(Const.Params.SHIPPING_LONGITUDE,String.valueOf(latLng.longitude));
        map.put(Const.Params.IS_DEFAULT,String.valueOf("1"));

        AndyUtils.appLog("Ashutosh", "AddAddressMap" + map);

        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.GET_ADDRESS, this);
    }


    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());


            AndyUtils.appLog("LatLng",String.valueOf(p1));

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }


    private void hideKeyboard() {
        // Check if no view has focus:
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode)
    {
        JSONObject jsonObject;
        switch (serviceCode)
        {
            case Const.ServiceCode.GET_ADDRESS:
                shippingProgressBar.setVisibility(View.GONE);
                AndyUtils.appLog("Ashutosh","AddressResponse" +response);
                try {
                    jsonObject=new JSONObject(response);
                    if(jsonObject.optString("success").equals("true"))
                    {
                        JSONArray jsonArray=jsonObject.optJSONArray("data");
                        if(jsonArray!=null && jsonArray.length()>0)
                        {
                            addressDetailsList=new ArrayList<>();
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject addressJsonObject=jsonArray.optJSONObject(i);
                                AddressDetails addressDetails = new AddressDetails();
                                addressDetails.setAddressId(addressJsonObject.optString("address_id"));
                                addressDetails.setAddress(addressJsonObject.optString("shipping_address"));
                                addressDetails.setIsDefault(addressJsonObject.optString("is_default"));
                                addressDetails.setLandMark(jsonObject.optString("slandmark"));
                                addressDetails.setBlockNo(jsonObject.optString("sblock_no"));
                                addressDetails.setPhoneNumber(jsonObject.optString("sphone_no"));
                                addressDetailsList.add(addressDetails);
                            }
                            addressAdapter=new AddressDetailsAdapter(getActivity(),addressDetailsList);
                            shippingRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(12));
                            LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
                            shippingRecyclerView.setLayoutManager(layoutManager);
                            shippingRecyclerView.setAdapter(addressAdapter);
                            addressAdapter.notifyDataSetChanged();
                            noShippingText.setVisibility(View.GONE);
                        }
                        else
                        {
                            noShippingText.setVisibility(View.VISIBLE);
                        }
                    }
                    else
                    {
                        noShippingText.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }
}
