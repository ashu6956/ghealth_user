package com.patient.ghealth.fragment;


import android.animation.Animator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.animation.Interpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.CheckoutActivity;
import com.patient.ghealth.activity.HealthStoreActivity;
import com.patient.ghealth.adapter.ImagesViewPagerAdapter;
import com.patient.ghealth.adapter.RelatedProductListAdapter;
import com.patient.ghealth.adapter.ReviewListAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.ProductDetail;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductDetailFragment extends Fragment implements ViewPager.OnPageChangeListener, View.OnClickListener, AsyncTaskCompleteListener {

    private static final Interpolator INTERPOLATOR = new FastOutSlowInInterpolator();
    private static ProductDetail productDetail;
    private int START = 0, incrementRate = 10;
    private boolean isMoreDataAvailable = false, isLoadingData = false;
    private ReviewListAdapter mReviewListAdapter;
    private int variantNumber = 0, licenseNumber = 0;
    private int qty = 1;
    private String productId;
    private boolean mIsShowing;
    private boolean mIsHiding;
    private ViewPager product_images;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private List<ProductDetail> mRelatedProductDetails = new ArrayList<>();
    private RelatedProductListAdapter mRelatedProductListAdapter;

    private ImageView[] dots;
    private ImagesViewPagerAdapter mAdapter;
    private TextView seller_name, productTitle;
    private LinearLayout list_price_layout, price_layout, you_save_layout;
    private TextView price_text, list_price_text, you_save_text, price_string_text, shipping_fee;
    private RatingBar ratingBar;
    private TextView ratingCount, delivery_text, shortDescription;
    private RecyclerView mRecyclerView, related_product_list;
    private TextView total_reviews, total_rating;
    private RatingBar total_ratingBar;
    private ImageView nextDescription, nextTopReview;
    private Button add_to_cart, buy_now_button;
    private CardView variant_card, license_card, total_review_card, related_products_card;
    private TextView variant_option, license_option;
    private Button quantity;
    private ProgressBar license_progress;
    private NestedScrollView productScrollView;
    private LinearLayout cartNowLayout;
    private Button loadMoreButton;
    private HealthStoreActivity activity;

    public ProductDetailFragment() {
        // Required empty public constructor
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_detail, container, false);
        activity = (HealthStoreActivity) getActivity();
        try {
            if (null != getArguments()) {
                productDetail = (ProductDetail) getArguments().getSerializable("ProductDetail");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        nextTopReview = (ImageView) view.findViewById(R.id.iv_nextTopReview);
        nextTopReview.setOnClickListener(this);
        product_images = (ViewPager) view.findViewById(R.id.product_images_viewpager);
        pager_indicator = (LinearLayout) view.findViewById(R.id.viewPagerCountDots);
        seller_name = (TextView) view.findViewById(R.id.product_detail_seller);
        productTitle = (TextView) view.findViewById(R.id.product_detail_title);
        list_price_layout = (LinearLayout) view.findViewById(R.id.list_price_layout);
        price_layout = (LinearLayout) view.findViewById(R.id.price_layout);
        you_save_layout = (LinearLayout) view.findViewById(R.id.you_save_layout);
        price_text = (TextView) view.findViewById(R.id.price_text);
        list_price_text = (TextView) view.findViewById(R.id.list_price_text);
        you_save_text = (TextView) view.findViewById(R.id.you_save_text);
        price_string_text = (TextView) view.findViewById(R.id.price_text_string);
        ratingBar = (RatingBar) view.findViewById(R.id.rating);
        ratingCount = (TextView) view.findViewById(R.id.ratingCount);
        shipping_fee = (TextView) view.findViewById(R.id.shipping_fee);
        delivery_text = (TextView) view.findViewById(R.id.delivery_text);
        shortDescription = (TextView) view.findViewById(R.id.shortDescription);
        shortDescription.setOnClickListener(this);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.review_list);
        total_rating = (TextView) view.findViewById(R.id.total_rating);
        total_ratingBar = (RatingBar) view.findViewById(R.id.total_ratingBar);
        total_reviews = (TextView) view.findViewById(R.id.total_reviews);
        nextDescription = (ImageView) view.findViewById(R.id.nextDescription);
        add_to_cart = (Button) view.findViewById(R.id.add_to_cart);
        nextDescription.setOnClickListener(this);
        add_to_cart.setOnClickListener(this);
        variant_card = (CardView) view.findViewById(R.id.variant_card);
        variant_card.setOnClickListener(this);
        variant_option = (TextView) view.findViewById(R.id.variant_option);
        license_card = (CardView) view.findViewById(R.id.license_card);
        license_card.setOnClickListener(this);
        license_option = (TextView) view.findViewById(R.id.license_option);
        buy_now_button = (Button) view.findViewById(R.id.buy_now_button);
        buy_now_button.setOnClickListener(this);
        quantity = (Button) view.findViewById(R.id.quantity);
        quantity.setOnClickListener(this);
        quantity.setText(getString(R.string.qty_dot) + " " + 1);
        license_progress = (ProgressBar) view.findViewById(R.id.license_progress);
        total_review_card = (CardView) view.findViewById(R.id.total_review_card);
        productScrollView = (NestedScrollView) view.findViewById(R.id.productScrollView);
        related_products_card = (CardView) view.findViewById(R.id.related_products_card);
        cartNowLayout = (LinearLayout) view.findViewById(R.id.cartNowLayout);
        related_product_list = (RecyclerView) view.findViewById(R.id.related_product_list);
        loadMoreButton = (Button) view.findViewById(R.id.loadMoreButton);
        loadMoreButton.setOnClickListener(this);
        productScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                Rect scrollBounds = new Rect();
                productScrollView.getHitRect(scrollBounds);
                if (related_products_card.getLocalVisibleRect(scrollBounds)) {
                    // imageView is within the visible window
                    //Log.d("Amal","Visible");
                    if (!mIsHiding)
                        hide(cartNowLayout);
                } else {
                    // imageView is not within the visible window
                    //Log.d("Amal","Not Visible");
                    if (!mIsShowing)
                        show(cartNowLayout);
                }

            }
        });

        total_review_card.setOnClickListener(this);

        if (productDetail != null || !PreferenceHelper.getParam(getActivity(), Const.Params.PRODUCT_ID, "").equals("")) {
//            setUpViews();
            productId = (String) PreferenceHelper.getParam(getActivity(), Const.Params.PRODUCT_ID, "");
            AndyUtils.appLog("ProductDetailFragment", "Product ID" + productId);
            getSingleProductDetails(productId);
            getRelatedProducts();
        } else {
            getSingleProductDetails(productId);
            related_products_card.setVisibility(View.GONE);
        }


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.ProductDetailFragment;
    }

    private void getRelatedProducts() {
        mRelatedProductListAdapter = new RelatedProductListAdapter(mRelatedProductDetails, getActivity(), activity);
        final RecyclerView.LayoutManager mLayoutManager = new StaggeredGridLayoutManager(2, 1);
        related_product_list.setLayoutManager(mLayoutManager);
        related_product_list.setItemAnimator(new DefaultItemAnimator());
        related_product_list.setAdapter(mRelatedProductListAdapter);
        related_product_list.setNestedScrollingEnabled(false);

//        if (productDetail.getCategories() != null && !productDetail.getCategories().isEmpty())
//
//        else related_products_card.setVisibility(View.GONE);
        getProducts();

    }


    private void getSingleProductDetails(String id) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.GET_SINGLE_PRODUCT_URL + id);

        AndyUtils.appLog("Ashutosh", "GetSingleProductDetailsMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_SINGLE_PRODUCT, this);
    }

    private void getProducts() {
        String catId = (String) PreferenceHelper.getParam(getActivity(), Const.Params.CATEGORY_ID, "");
        if (!catId.equals("")) {
            if (!AndyUtils.isNetworkAvailable(getActivity())) {
                AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
                return;
            }
            isLoadingData = true;
            HashMap<String, String> map = new HashMap<String, String>();
            map.put(Const.Params.URL, Const.ServiceType.GET_STORE_PRODUCT_URL + Const.Params.CATEGORY + "=" + catId + Const.Params.START + "=" + START);
            AndyUtils.appLog("Ashutosh", "RelatedProductProductMap" + map);
            new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_STORE_PRODUCT, this);
        }
    }


    private void hide(final View view) {
        mIsHiding = true;
        ViewPropertyAnimator animator = view.animate()
                .translationY(view.getHeight())
                .setInterpolator(INTERPOLATOR)
                .setDuration(200);

        animator.setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                // Prevent drawing the View after it is gone
                mIsHiding = false;
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                // Canceling a hide should show the view
                mIsHiding = false;
                if (!mIsShowing) {
                    show(view);
                }
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });

        animator.start();
    }

    /**
     * Show the quick return view.
     * <p/>
     * Animates showing the view, with the view sliding up from the bottom of the screen.
     * After the view has reappeared, its visibility will change to VISIBLE.
     *
     * @param view The quick return view
     */
    private void show(final View view) {
        mIsShowing = true;
        ViewPropertyAnimator animator = view.animate()
                .translationY(0)
                .setInterpolator(INTERPOLATOR)
                .setDuration(200);

        animator.setListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                mIsShowing = false;
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                // Canceling a show should hide the view
                mIsShowing = false;
                if (!mIsHiding) {
                    hide(view);
                }
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });

        animator.start();
    }

    private void setUpViews() {
        if (productDetail.getVariantList().isEmpty())
            variant_card.setVisibility(View.GONE);
        else {
            variant_card.setVisibility(View.VISIBLE);
            variant_option.setText(productDetail.getVariantList().get(0).getName());
        }
        if (productDetail.getLicensesList().isEmpty())
            license_card.setVisibility(View.GONE);
        else {
            license_card.setVisibility(View.VISIBLE);
            license_option.setVisibility(View.GONE);
            license_progress.setVisibility(View.VISIBLE);
        }

        if (productDetail.getQuantity() <= 0) {
            qty = 0;
            quantity.setText(getString(R.string.qty_dot) + " " + qty);
        }

        setUpSellerName();
        setUpProductName();
        setUpProductAfterDiscountPrice();
        setUpDeliveryCharge();
        SetUpDeliverDays();
        setUpPriceAndSave();
        setUpRatings();
        setUpProductImages();
        setUpDescription();
        setUpTotalReviewCard();

    }

    private void setUpTotalReviewCard() {
        if (productDetail.getRating().getTotalReviews() == 1)
//            total_reviews.setText(productDetail.getRating().getTotalRatings() + " " + getString(R.string.customer_reviews));
            total_reviews.setText(getString(R.string.customer_reviews));
        else
            total_reviews.setText(getString(R.string.customer_reviews));
        float rating = 0;
        if (productDetail.getRating().getTotalRatings() != 0)
            rating = productDetail.getRating().getTotalStar() / productDetail.getRating().getTotalRatings();
        total_ratingBar.setRating(rating);
        total_rating.setText(rating + " " + getString(R.string.out_if_5));

    }

    private void setUpReviewRecylerView(ProductDetail detail) {

        mReviewListAdapter = new ReviewListAdapter(detail.getReviewList(), getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mReviewListAdapter);
    }


//    private void parseLongDescRatingLicenseFromResponse(JsonObject result) {
//        try {
//            JSONObject mainObj = new JSONObject(result.toString());
//            if (mainObj.optInt(Const.Params.STATUS_CODE) == 200) {
//                JSONObject responseObj = mainObj.getJSONObject(Const.Params.RESPONSE);
//                JSONArray productJsonArray = responseObj.getJSONArray("product");
//                for (int i = 0; i < productJsonArray.length(); i++) {
//                    JSONObject productObj = productJsonArray.getJSONObject(i);
//                    productDetail.setLongDescription(productObj.optString("long_description"));
//                    JSONArray licenseJsonArray = productObj.optJSONArray("licenses");
//                    if (licenseJsonArray != null) {
//                        for (int k = 0; k < licenseJsonArray.length(); k++) {
//                            JSONObject licenseObj = licenseJsonArray.getJSONObject(k);
//                            JSONObject secondLicenseObj = licenseObj.getJSONObject("license");
//                            productDetail.getLicensesList().get(k).setName(secondLicenseObj.optString(Const.Params.NAME));
//                        }
//                    }
//                    JSONArray reviewJsonArray = productObj.optJSONArray("ratings");
//                    ArrayList<ProductDetail.Reviews> reviewList = new ArrayList<ProductDetail.Reviews>();
//                    if (reviewJsonArray != null) {
//                        for (int k = 0; k < reviewJsonArray.length(); k++) {
//                            JSONObject reviewObject = reviewJsonArray.getJSONObject(k);
//                            ProductDetail.Reviews review = new ProductDetail.Reviews();
//                            review.setComment(reviewObject.optString("comment"));
//                            review.setStar(reviewObject.optDouble("stars"));
//                            JSONObject userObj = reviewObject.getJSONObject("user");
//                            review.setName(userObj.optString(Const.Params.NAME));
//                            JSONObject logoObj = userObj.optJSONObject("logo");
//                            if (logoObj != null) {
//                                JSONObject cdnObj = logoObj.getJSONObject("cdn");
//                                review.setImageUrl(cdnObj.optString("url"));
//                            } else review.setImageUrl("");
//                            reviewList.add(review);
//                        }
//                    }
//                    productDetail.setReviewList(reviewList);
//                    setUpReviewRecylerView();
//                    setUpDescription();
//                    if (!productDetail.getLicensesList().isEmpty())
//                        setUpLicenseOption();
//                }
//            }
//        } catch (JSONException e1) {
//            e1.printStackTrace();
//        }


    private void setUpLicenseOption() {
        license_progress.setVisibility(View.GONE);
        license_option.setVisibility(View.VISIBLE);
        license_option.setText(productDetail.getLicensesList().get(0).getName());
    }

    private void setUpDescription() {
        if (!productDetail.getDescription().isEmpty())
            nextDescription.setVisibility(View.VISIBLE);
        else nextDescription.setVisibility(View.GONE);
        shortDescription.setText(productDetail.getDescription());
    }

    private void setUpProductImages() {
        // Set product images
        mAdapter = new ImagesViewPagerAdapter(getActivity(), productDetail.getCat_images());
        product_images.setAdapter(mAdapter);
        product_images.setCurrentItem(0);
        product_images.setOnPageChangeListener(this);
        setUiPageViewIndicator();
    }

    private void setUpRatings() {
        // Set Rating
        if (productDetail.getRating().getTotalRatings() == 0) {
            ratingBar.setRating(0);
        } else {
            ratingBar.setRating((productDetail.getRating().getTotalStar() / productDetail.getRating().getTotalRatings()));
        }
        ratingCount.setText("(" + productDetail.getRating().getTotalRatings() + ")");

    }

    private void setUpPriceAndSave() {
        ProductDetail.Price price;
        if (productDetail.getVariantList().isEmpty()) {
            if (!productDetail.getLicensesList().isEmpty()) {
                price = productDetail.getLicensesList().get(licenseNumber).getPrice();
            } else
                price = productDetail.getPrice();
        } else {
            price = productDetail.getVariantList().get(variantNumber).getPrice();
        }

        try {
            // Set product original price and you save amount
            if (price.getOriginal_price() != -1 && (price.getOriginal_price() != price.getAfter_discount())) {
                list_price_layout.setVisibility(View.VISIBLE);
                you_save_layout.setVisibility(View.VISIBLE);
                price_string_text.getLayoutParams().width = (int) convertDpToPixel(80, getActivity());
                list_price_text.setText(Const.RM + price.getOriginal_price());
                list_price_text.setPaintFlags(list_price_text.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                double save_amount = price.getOriginal_price() - price.getAfter_discount();
                float discount_price = (float) ((save_amount / price.getOriginal_price()) * 100);
                you_save_text.setText("(" + String.valueOf(new DecimalFormat("##.#").format(discount_price)) + "%)");
//
            } else {
//            list_price_layout.setVisibility(View.GONE);
//            you_save_layout.setVisibility(View.GONE);
                list_price_layout.setVisibility(View.VISIBLE);
                you_save_layout.setVisibility(View.VISIBLE);
                price_string_text.getLayoutParams().width = (int) convertDpToPixel(80, getActivity());
                list_price_text.setText(Const.RM + price.getOriginal_price());
//            list_price_text.setPaintFlags(list_price_text.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                double save_amount = price.getOriginal_price() - price.getAfter_discount();
                float discount_price = (float) ((save_amount / price.getOriginal_price()) * 100);
                you_save_text.setText("(" + String.valueOf(new DecimalFormat("##.#").format(discount_price)) + "%)");


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void SetUpDeliverDays() {
        // Set delivery days
        Spannable word1 = new SpannableString(getString(R.string.guaranteed));
        word1.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.dark_green)), 0, word1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        word1.setSpan(new StyleSpan(Typeface.BOLD), 0, word1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        delivery_text.setText(word1);
        try {

            String duration[] = productDetail.getShippingDetails().getDuration().split(" ");
            if (duration != null && duration[0] != null) {
                Spannable word2 = new SpannableString(" " + getString(R.string.delivery_within) + " " + duration[0] + " " + "day(s)");
                word2.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.secondary_text_color)), 0, word2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                delivery_text.append(word2);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpDeliveryCharge() {
        // Set delivery charge
        if (productDetail.isPaidByBuyer()) {
            if (productDetail.getShippingDetails().getFee() != 0) {
                Spannable word1 = new SpannableString(Const.RM + productDetail.getShippingDetails().getFee());
                word1.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.colorPrimary)), 0, word1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                shipping_fee.setText(word1);
                Spannable word2 = new SpannableString(" " + getString(R.string.shipping_cost));
                word2.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.secondary_text_color)), 0, word2.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                shipping_fee.append(word2);
            } else {
                Spannable word1 = new SpannableString(" " + getString(R.string.free_delivery));
                word1.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.colorPrimary)), 0, word1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                shipping_fee.setText(word1);
            }
        } else {
            Spannable word1 = new SpannableString(" " + getString(R.string.free_delivery));
            word1.setSpan(new ForegroundColorSpan(activity.getResources().getColor(R.color.colorPrimary)), 0, word1.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            shipping_fee.setText(word1);
        }
    }

    private void setUpProductAfterDiscountPrice() {
        // Set product after discount price
        ProductDetail.Price price;
        if (productDetail.getVariantList().isEmpty()) {
            price = productDetail.getPrice();
        } else {
            price = productDetail.getVariantList().get(variantNumber).getPrice();
        }

        if (price.getAfter_discount() != -1) {
            price_text.setText(Const.RM + price.getAfter_discount());
        } else {
            price_layout.setVisibility(View.GONE);
        }
    }

    private void setUpProductName() {
        // Set product name
        productTitle.setText(productDetail.getProductName());
    }

    private void setUpSellerName() {
        // Set seller name
        seller_name.setText(productDetail.getSellerInfo().getSeller_name());
    }

    private void setUiPageViewIndicator() {
        dotsCount = mAdapter.getCount();
        dots = new ImageView[dotsCount];

        if (dotsCount == 0)
            return;

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);
            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
        }
        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.shortDescription:
                if (null != productDetail.getLongDescription() && !productDetail.getLongDescription().equals("")) {
                    gotoWebviewFragment(productDetail.getLongDescription());
                } else {
                    gotoWebviewFragment(productDetail.getDescription());
                }
                break;
            case R.id.nextDescription:
                if (null != productDetail.getLongDescription() && !productDetail.getLongDescription().equals("")) {
                    gotoWebviewFragment(productDetail.getLongDescription());
                } else {
                    gotoWebviewFragment(productDetail.getDescription());
                }
                break;
            case R.id.add_to_cart:
                if (productDetail.getQuantity() <= 0) {
                    AndyUtils.showShortToast("Sorry, the product is currently out of stock", activity);
                } else {
                    addToCartApi();
                }
                break;
            case R.id.variant_card:
                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(getString(R.string.select_vatiant));

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                        getActivity(),
                        android.R.layout.select_dialog_singlechoice);
                for (int i = 0; i < productDetail.getVariantList().size(); i++) {
                    arrayAdapter.add(productDetail.getVariantList().get(i).getName());
                }
                builder.setSingleChoiceItems(arrayAdapter, variantNumber, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        variantNumber = which;
                        dialog.dismiss();
                        variant_option.setText(productDetail.getVariantList().get(which).getName());
                        setUpPriceAndSave();
                        setUpProductAfterDiscountPrice();
                    }
                });
                builder.setCancelable(true);
                builder.show();

                break;
            case R.id.license_card:
                if (license_option.getVisibility() == View.GONE)
                    return;


                final AlertDialog.Builder license_builder = new AlertDialog.Builder(getActivity());
                license_builder.setTitle(getString(R.string.select_license));

                final ArrayAdapter<String> licenseArrayAdapter = new ArrayAdapter<String>(
                        getActivity(),
                        android.R.layout.select_dialog_singlechoice);
                for (int i = 0; i < productDetail.getLicensesList().size(); i++) {
                    licenseArrayAdapter.add(productDetail.getLicensesList().get(i).getName());
                }
                license_builder.setSingleChoiceItems(licenseArrayAdapter, licenseNumber, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        licenseNumber = which;
                        dialog.dismiss();
                        license_option.setText(productDetail.getLicensesList().get(which).getName());
                        setUpPriceAndSave();
                        setUpProductAfterDiscountPrice();
                    }
                });
                license_builder.setCancelable(true);
                license_builder.show();

                break;
            case R.id.buy_now_button:
                if (productDetail.getQuantity() <= 0) {
                    AndyUtils.showShortToast("Sorry, the product is currently out of stock", activity);
                } else {
                    Intent intent = new Intent(getActivity(), CheckoutActivity.class);
                    intent.putExtra(Const.Params.PRODUCT_ID, productDetail.getProductId());
                    intent.putExtra(Const.QUANTITY, qty);
                    if (productDetail.getVariantList() != null && !productDetail.getVariantList().isEmpty())
                        intent.putExtra("variant", productDetail.getVariantList().get(variantNumber).getID());
                    if (productDetail.getLicensesList() != null && !productDetail.getLicensesList().isEmpty())
                        intent.putExtra("license", productDetail.getLicensesList().get(licenseNumber).getID());
                    intent.putExtra(Const.CHECKOUT_METHOD_KEY, 1);
                    startActivity(intent);
                }
                break;
            case R.id.quantity:
                final AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                builder1.setTitle(getString(R.string.select_quantity));

                final ArrayAdapter<String> arrayAdapter1 = new ArrayAdapter<String>(
                        getActivity(),
                        android.R.layout.select_dialog_singlechoice);
                for (int i = 1; i <= productDetail.getQuantity(); i++) {
                    arrayAdapter1.add(String.valueOf(i));
                }
                builder1.setSingleChoiceItems(arrayAdapter1, qty - 1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        qty = which + 1;
                        dialog.dismiss();
                        quantity.setText(getString(R.string.qty_dot) + " " + qty);
                    }
                });
                builder1.setCancelable(true);
                builder1.show();
                break;
            case R.id.total_review_card:
                ProductAllReviewsFragment productAllReviewsFragment = new ProductAllReviewsFragment();
                Bundle args = new Bundle();
                args.putSerializable("ProductDetail", productDetail);
                args.putString("top", "nTop");
                productAllReviewsFragment.setArguments(args);
                activity.addFragment(productAllReviewsFragment, false, getString(R.string.health_store), "", false);
                break;
            case R.id.loadMoreButton:
                START = START + incrementRate;
                getProducts();
                break;
            case R.id.iv_nextTopReview:
                ProductAllReviewsFragment top5ReviewFragment = new ProductAllReviewsFragment();
                Bundle topArgs = new Bundle();
                topArgs.putSerializable("ProductDetail", productDetail);
                topArgs.putString("top", "top");
                top5ReviewFragment.setArguments(topArgs);
                activity.addFragment(top5ReviewFragment, false, getString(R.string.health_store), "", false);
                break;

        }

    }

    private void addToCartApi() {

        String catId = (String) PreferenceHelper.getParam(getActivity(), Const.Params.CATEGORY_ID, "");
        if (!catId.equals("")) {
            if (!AndyUtils.isNetworkAvailable(getActivity())) {
                AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
                return;
            }
//            productProgressBar.setVisibility(View.VISIBLE);
            isLoadingData = true;
            HashMap<String, String> map = new HashMap<String, String>();
            map.put(Const.Params.URL, Const.ServiceType.ADD_CART_PRODUCT_URL);
            map.put(Const.Params.PRODUCT_ID, productDetail.getProductId());
            map.put(Const.QUANTITY, String.valueOf(qty));
            HashMap<String, String> headerMap = new HashMap<>();
            headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
            AndyUtils.appLog("Ashutosh", "CartAddedMap" + map);
            AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
            new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.ADD_CART_PRODUCT, this, headerMap);
        }
    }

    private void setCartCount() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CART_COUNT_URL);

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "CartCountMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_CART_COUNT, this, headerMap);
    }

    private void gotoWebviewFragment(String desc) {
        WebviewFragment webViewFragment = new WebviewFragment();
        Bundle args = new Bundle();
        args.putString(Const.WEB_VIEW_INTENT_KEY, productDetail.getDescription());
        webViewFragment.setArguments(args);
        activity.addFragment(webViewFragment, false, getString(R.string.health_store), Const.WebViewFragment, false);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_SINGLE_PRODUCT:
                AndyUtils.appLog("Ashutosh", "getSingleProductResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    parseAllResponse(jsonObject);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_STORE_PRODUCT:

                AndyUtils.appLog("Ashutosh", "GetStoreProductResponse" + response);
                isLoadingData = false;
                try {
                    JSONObject mainObj = new JSONObject(response);
                    if (mainObj.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        JSONObject responseObj = mainObj.getJSONObject(Const.RESPONSE);
                        JSONArray productJsonArray = responseObj.getJSONArray("product");
                        if (productJsonArray.length() == 0) {
                            isMoreDataAvailable = false;
                            loadMoreButton.setVisibility(View.GONE);
                        } else isMoreDataAvailable = true;
                        for (int i = 0; i < productJsonArray.length(); i++) {
                            JSONObject productObj = productJsonArray.getJSONObject(i);
                            ProductDetail productDetail = new ProductDetail();
                            productDetail.setProductId(productObj.optString(Const.Params._ID));
                            productDetail.setProductName(productObj.optString("name"));
                            productDetail.setQuantity(productObj.optInt("quantity"));
                            productDetail.setDescription(productObj.optString("description"));
                            productDetail.setPaidByBuyer(productObj.optBoolean("paid_by_buyer"));
                            productDetail.setLongDescription(productObj.optString("long_description"));
                            // get the pricing object information
                            JSONObject productPricingObj = productObj.getJSONObject("pricing");
                            ProductDetail.Price price = new ProductDetail.Price();
                            price.setAfter_discount(productPricingObj.optDouble("after_discount", -1));
                            price.setCommission(productPricingObj.optDouble("commission", -1));
                            price.setOriginal_price(productPricingObj.optDouble("original", -1));
                            price.setService_tax(productPricingObj.optDouble("service_tax", -1));
                            // add the pricing obj to main obj
                            productDetail.setPrice(price);
                            // get categories
                            JSONArray categoryJsonArray = productObj.getJSONArray("categories");
                            ArrayList<String> categoryArray = new ArrayList<String>();
                            for (int g = 0; g < categoryJsonArray.length(); g++) {
                                categoryArray.add(categoryJsonArray.optString(g));
                            }
                            productDetail.setCategories(categoryArray);
                            // get variants
                            ArrayList<ProductDetail.Variants> variantsArrayList = new ArrayList<ProductDetail.Variants>();
                            JSONArray VariantJsonArray = productObj.optJSONArray("variants");
                            for (int l = 0; l < VariantJsonArray.length(); l++) {
                                JSONObject variantObj = VariantJsonArray.getJSONObject(l);
                                ProductDetail.Variants variants = new ProductDetail.Variants();
                                ProductDetail.Price price1 = new ProductDetail.Price();
                                variants.setID(variantObj.optString(Const.Params._ID));
                                variants.setName(variantObj.optString(Const.Params.NAME));
                                variants.setQuantity(variantObj.optString(Const.QUANTITY));
                                price1.setAfter_discount(variantObj.optDouble("after_discount", -1));
                                price1.setCommission(variantObj.optDouble("commission", -1));
                                price1.setOriginal_price(variantObj.optDouble("original", -1));
                                price1.setService_tax(variantObj.optDouble("service_tax", -1));
                                variants.setPrice(price1);
                                variantsArrayList.add(variants);
                            }
                            productDetail.setVariantList(variantsArrayList);
                            // get Lincense
                            ArrayList<ProductDetail.License> licenseArrayList = new ArrayList<ProductDetail.License>();
                            JSONArray LicenseJsonArray = productObj.optJSONArray("licenses");
                            for (int l = 0; l < LicenseJsonArray.length(); l++) {
                                JSONObject licenseObj = LicenseJsonArray.getJSONObject(l);
                                ProductDetail.License licenses = new ProductDetail.License();
                                ProductDetail.Price price2 = new ProductDetail.Price();
                                licenses.setID(licenseObj.optString(Const.Params._ID));
                                licenses.setQuantity(licenseObj.optString(Const.QUANTITY));
                                price2.setAfter_discount(licenseObj.optDouble("after_discount", -1));
                                price2.setCommission(licenseObj.optDouble("commission", -1));
                                price2.setOriginal_price(licenseObj.optDouble("original", -1));
                                price2.setService_tax(licenseObj.optDouble("service_tax", -1));
                                licenses.setPrice(price2);
                                licenseArrayList.add(licenses);
                            }
                            productDetail.setLicensesList(licenseArrayList);
                            // get seller info obj
                            // JSONObject sellerInfoObj = productObj.getJSONObject("created_by");
                            ProductDetail.Seller_info seller_info = new ProductDetail.Seller_info();
                            seller_info.set_id(productObj.optString("seller_id"));
                            seller_info.setSeller_name(productObj.optString("seller_name"));
                            // add seller obj to main obj
                            productDetail.setSellerInfo(seller_info);
                            // add rating
                            ProductDetail.Rating rating_info = new ProductDetail.Rating();
                            rating_info.setTotalRatings(productObj.optInt("total_ratings"));
                            rating_info.setTotalStar(productObj.optInt("total_star"));
                            rating_info.setTotalReviews(productObj.optInt("total_reviews"));
                            // add rating obj to main obj
                            productDetail.setRating(rating_info);
                            //add shipping info
                            JSONObject shippingObject = productObj.optJSONObject("shipping_details");
                            ProductDetail.ShippingDetails shippingDetails = new ProductDetail.ShippingDetails();
                            shippingDetails.setDuration(shippingObject.optString("duration"));
                            shippingDetails.setFee(shippingObject.optDouble("fee"));
                            shippingDetails.setUnit(shippingObject.optString("unit"));
                            shippingDetails.setWeight(shippingObject.optDouble("weight"));
                            productDetail.setShippingDetails(shippingDetails);

                            //get the image object information
                            JSONArray productImageJsonArray = productObj.getJSONArray("images");
                            ArrayList<ProductDetail.Cat_image> catImageArrayList = new ArrayList<ProductDetail.Cat_image>();
                            for (int j = 0; j < productImageJsonArray.length(); j++) {
                                JSONObject productImageObj = productImageJsonArray.getJSONObject(j);
                                JSONObject productImageCdnObj = productImageObj.getJSONObject("cdn");
                                ProductDetail.Cat_image catImage = new ProductDetail.Cat_image();
                                catImage.set_id(productImageCdnObj.optString(Const.Params._ID));
                                catImage.setImage_url(productImageCdnObj.optString("url"));
                                catImageArrayList.add(catImage);
                            }
                            // add category image list to main obj
                            productDetail.setCat_images(catImageArrayList);
                            JSONArray reviewJsonArray = productObj.optJSONArray("ratings");
                            ArrayList<ProductDetail.Reviews> reviewList = new ArrayList<ProductDetail.Reviews>();
                            if (reviewJsonArray != null) {
                                for (int k = 0; k < reviewJsonArray.length(); k++) {
                                    JSONObject reviewObject = reviewJsonArray.getJSONObject(k);
                                    ProductDetail.Reviews review = new ProductDetail.Reviews();
                                    review.setComment(reviewObject.optString("comment"));
                                    review.setStar(Double.valueOf(reviewObject.optDouble("stars")));
                                    JSONObject userObj = reviewObject.optJSONObject("user");
                                    if (null != userObj) {
                                        review.setName(userObj.optString(Const.Params.NAME));
                                        JSONObject logoObj = userObj.optJSONObject("logo");
                                        if (logoObj != null) {
                                            JSONObject cdnObj = logoObj.getJSONObject("cdn");
                                            review.setImageUrl(cdnObj.optString("url"));
                                        } else review.setImageUrl("");
                                    }
                                    reviewList.add(review);
                                }
                            }
                            productDetail.setReviewList(reviewList);
                            mRelatedProductDetails.add(productDetail);
                        }

//                        setUpReviewRecylerView(mRelatedProductDetails.get(0));

                        if (mRelatedProductDetails.isEmpty()) {
                            //no_product_found_text.setVisibility(View.VISIBLE);
                            related_products_card.setVisibility(View.GONE);
                        } else {
                            related_products_card.setVisibility(View.VISIBLE);
                        }
                        mRelatedProductListAdapter.notifyDataSetChanged();

                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                break;
            case Const.ServiceCode.ADD_CART_PRODUCT:
                AndyUtils.appLog("Ashutosh", "AddedCartResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        AndyUtils.showShortToast(getString(R.string.added_cart), getActivity());
                        setCartCount();

                    } else {
                        if (jsonObject.optString(Const.Params.STATUS_MESSAGE).equalsIgnoreCase(getString(R.string.added_in_cart))) {
                            AndyUtils.showLongToast(getString(R.string.added_already_in_cart), getActivity());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_CART_COUNT:
                AndyUtils.appLog("Ashutosh", "CartCountResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        JSONObject mainObj = jsonObject.optJSONObject(Const.RESPONSE);
                        int count = mainObj.optInt("count");
                        activity.cartCount.setText(count + "");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;


        }

    }

    private void parseAllResponse(JSONObject mainObj) {
        productDetail = new ProductDetail();
        AndyUtils.appLog("Ashutosh", "ParseAllResponse" + mainObj.toString());
        try {

            if (mainObj.optString(Const.STATUS).equals(Const.SUCCESS)) {
                JSONObject responseObj = mainObj.getJSONObject("response");
                JSONArray productJsonArray = responseObj.getJSONArray("product");
                for (int i = 0; i < productJsonArray.length(); i++) {
                    JSONObject productObj = productJsonArray.getJSONObject(i);
                    productDetail.setProductId(productObj.optString(Const.Params._ID));
                    productDetail.setProductName(productObj.optString("name"));
                    productDetail.setQuantity(productObj.optInt("quantity"));
                    productDetail.setDescription(productObj.optString("description"));
                    productDetail.setPaidByBuyer(productObj.optBoolean("paid_by_buyer"));
                    productDetail.setLongDescription(productObj.optString("long_description"));
                    // get the pricing object information
                    JSONObject productPricingObj = productObj.getJSONObject("pricing");
                    ProductDetail.Price price = new ProductDetail.Price();
                    price.setAfter_discount(productPricingObj.optDouble("after_discount", -1));
                    price.setCommission(productPricingObj.optDouble("commission", -1));
                    price.setOriginal_price(productPricingObj.optDouble("original", -1));
                    price.setService_tax(productPricingObj.optDouble("service_tax", -1));
                    // add the pricing obj to main obj
                    productDetail.setPrice(price);
                    // get categories
                    JSONArray categoryJsonArray = productObj.getJSONArray("categories");
                    ArrayList<String> categoryArray = new ArrayList<String>();
                    for (int g = 0; g < categoryJsonArray.length(); g++) {
                        categoryArray.add(categoryJsonArray.optString(g));
                    }
                    productDetail.setCategories(categoryArray);
                    // get variants
                    ArrayList<ProductDetail.Variants> variantsArrayList = new ArrayList<ProductDetail.Variants>();
                    JSONArray VariantJsonArray = productObj.optJSONArray("variants");
                    for (int l = 0; l < VariantJsonArray.length(); l++) {
                        JSONObject variantObj = VariantJsonArray.getJSONObject(l);
                        ProductDetail.Variants variants = new ProductDetail.Variants();
                        ProductDetail.Price price1 = new ProductDetail.Price();
                        variants.setID(variantObj.optString(Const.Params._ID));
                        variants.setName(variantObj.optString(Const.Params.NAME));
                        variants.setQuantity(variantObj.optString(Const.QUANTITY));
                        price1.setAfter_discount(variantObj.optDouble("after_discount", -1));
                        price1.setCommission(variantObj.optDouble("commission", -1));
                        price1.setOriginal_price(variantObj.optDouble("original", -1));
                        price1.setService_tax(variantObj.optDouble("service_tax", -1));
                        variants.setPrice(price1);
                        variantsArrayList.add(variants);
                    }
                    productDetail.setVariantList(variantsArrayList);
                    // get seller info obj
                    // JSONObject sellerInfoObj = productObj.getJSONObject("created_by");
                    ProductDetail.Seller_info seller_info = new ProductDetail.Seller_info();
                    seller_info.set_id(productObj.optString("seller_id"));
                    seller_info.setSeller_name(productObj.optString("seller_name"));
                    // add seller obj to main obj
                    productDetail.setSellerInfo(seller_info);
                    // add rating
                    ProductDetail.Rating rating_info = new ProductDetail.Rating();
                    rating_info.setTotalRatings(productObj.optInt("total_ratings"));
                    rating_info.setTotalStar(productObj.optInt("total_star"));
                    rating_info.setTotalReviews(productObj.optInt("total_reviews"));
                    // add rating obj to main obj
                    productDetail.setRating(rating_info);
                    //add shipping info
                    JSONObject shippingObject = productObj.optJSONObject("shipping_details");
                    ProductDetail.ShippingDetails shippingDetails = new ProductDetail.ShippingDetails();
                    shippingDetails.setDuration(shippingObject.optString("duration"));
                    shippingDetails.setFee(shippingObject.optDouble("fee"));
                    shippingDetails.setUnit(shippingObject.optString("unit"));
                    shippingDetails.setWeight(shippingObject.optDouble("weight"));
                    productDetail.setShippingDetails(shippingDetails);

                    //get the image object information
                    JSONArray productImageJsonArray = productObj.getJSONArray("images");
                    ArrayList<ProductDetail.Cat_image> catImageArrayList = new ArrayList<ProductDetail.Cat_image>();
                    for (int j = 0; j < productImageJsonArray.length(); j++) {
                        JSONObject productImageObj = productImageJsonArray.getJSONObject(j);
                        JSONObject productImageCdnObj = productImageObj.getJSONObject("cdn");
                        ProductDetail.Cat_image catImage = new ProductDetail.Cat_image();
                        catImage.set_id(productImageCdnObj.optString(Const.Params._ID));
                        catImage.setImage_url(productImageCdnObj.optString("url"));
                        catImageArrayList.add(catImage);
                    }
                    // add category image list to main obj
                    productDetail.setCat_images(catImageArrayList);

                    ArrayList<ProductDetail.License> licenseArrayList = new ArrayList<ProductDetail.License>();
                    JSONArray licenseJsonArray = productObj.optJSONArray("licenses");
                    if (licenseJsonArray != null) {
                        for (int k = 0; k < licenseJsonArray.length(); k++) {
                            JSONObject licenseObj = licenseJsonArray.getJSONObject(k);
                            ProductDetail.License licenses = new ProductDetail.License();
                            ProductDetail.Price price2 = new ProductDetail.Price();
                            licenses.setID(licenseObj.optString(Const.Params._ID));
                            licenses.setQuantity(licenseObj.optString(Const.QUANTITY));
                            price2.setAfter_discount(licenseObj.optDouble("after_discount", -1));
                            price2.setCommission(licenseObj.optDouble("commission", -1));
                            price2.setOriginal_price(licenseObj.optDouble("original", -1));
                            price2.setService_tax(licenseObj.optDouble("service_tax", -1));
                            licenses.setPrice(price2);

                            JSONObject secondLicenseObj = licenseObj.getJSONObject("license");
                            licenses.setName(secondLicenseObj.optString(Const.Params.NAME));

                            licenseArrayList.add(licenses);
                        }
                    }
                    productDetail.setLicensesList(licenseArrayList);
//                    JSONArray reviewJsonArray = productObj.optJSONArray("ratingsratings");
//                    ArrayList<ProductDetail.Reviews> reviewList = new ArrayList<ProductDetail.Reviews>();
//                    if (reviewJsonArray != null) {
//                        for (int k = 0; k < reviewJsonArray.length(); k++) {
//                            JSONObject reviewObject = reviewJsonArray.getJSONObject(k);
//                            ProductDetail.Reviews review = new ProductDetail.Reviews();
//                            review.setComment(reviewObject.optString("comment"));
//                            review.setStar(Double.valueOf(reviewObject.optDouble("stars")));
//                            JSONObject userObj = reviewObject.getJSONObject("user");
//                            review.setName(userObj.optString(Const.Params.NAME));
//                            JSONObject logoObj = userObj.optJSONObject("logo");
//                            if (logoObj != null) {
//                                JSONObject cdnObj = logoObj.getJSONObject("cdn");
//                                review.setImageUrl(cdnObj.optString("url"));
//                            } else review.setImageUrl("");
//                            reviewList.add(review);
//                        }
//                    }
//                    productDetail.setReviewList(reviewList);
                    setUpViews();
//                    setUpReviewRecylerView();
                    setUpDescription();
                    if (!productDetail.getLicensesList().isEmpty())
                        setUpLicenseOption();
                }
            }

        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }
}





