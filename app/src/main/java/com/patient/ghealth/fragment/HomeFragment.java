package com.patient.ghealth.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.LoginActivity;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.HomeAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.DoctorSpecialities;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.realmDB.RealmDBController;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;

/**
 * Created by getit on 8/5/2016.
 */
public class HomeFragment extends Fragment implements AdapterView.OnItemClickListener, AsyncTaskCompleteListener {
    ProgressBar specialist_progress;
    private Context mContext;
    private MainActivity activity;
    private GridView specialitiesGridView;
    private TextView no_speciality_text;
    private List<DoctorSpecialities> doctorSpecialitiesList;
    private String title;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = (MainActivity) getActivity();
        View view = inflater.inflate(R.layout.fragment_home_layout, container, false);
        specialist_progress = (ProgressBar) view.findViewById(R.id.specialist_progress);
        no_speciality_text = (TextView) view.findViewById(R.id.no_speciality_text);
        specialitiesGridView = (GridView) view.findViewById(R.id.gv_home_specialities);

        specialitiesGridView.setOnItemClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDoctorSpecialities();


    }

//    private void getMysch() {
//        String url = "https://ghealth.net/doctorapi/myschedulenew?id=8&session_token=2y10Q5dIt66NPblEpHaM208OWApW43cbHOdG1hAN65P2hnTlJ142DG&date=2017-08-10";
//
//        HashMap<String, String> map = new HashMap<>();
//        map.put(Const.Params.URL, url);
//
//
//        AndyUtils.appLog("Mahesh", "CheckMap" + map);
//        new HttpRequester(mContext, Const.GET, map, 101, this);
//    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.HOME_FRAGMENT;

    }

    private void getDoctorSpecialities() {
        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(getString(R.string.no_internet), mContext);
            return;
        }
        //   AndyUtils.showSimpleProgressDialog(getActivity(), "fetching specialities...", false);
        specialist_progress.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.GET_SPECIALITIES_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(mContext, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Session Token", (String) PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "GetSpecialitiesMap" + map);
        new HttpRequester(mContext, Const.GET, map, Const.ServiceCode.GET_SPECIALITIES, this);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        AndyUtils.appLog("Item Clicked Position", position + "");
        String specialist_DoctorId = doctorSpecialitiesList.get(position).getDoctorId();
        AndyUtils.appLog("HomeFragment", "DoctorSpecialistID" + specialist_DoctorId);
        PreferenceHelper.setParam(getActivity(), Const.SPECIALITIES_DOCTOR_ID, specialist_DoctorId);
        PreferenceHelper.setParam(getActivity(), Const.SPECIALITIES_DOCTOR_TYPE, doctorSpecialitiesList.get(position).getDoctorSpeciality());
        DoctorsAvailabilityFragment availabilityFragment = new DoctorsAvailabilityFragment();
        title = doctorSpecialitiesList.get(position).getDoctorSpeciality();
        activity.currentToolbarHeaderText = title;
        activity.appLogo.setVisibility(View.GONE);
        activity.addFragment(availabilityFragment, false, title, Const.DoctorAvailabilityFragment, true);
        activity.backbuttonImage.setVisibility(View.VISIBLE);
        activity.layoutToolbarOptions.setVisibility(View.GONE);

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        specialist_progress.setVisibility(View.GONE);
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_SPECIALITIES:

                AndyUtils.appLog("Ashutosh", "GetSpecialitiesResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    doctorSpecialitiesList = new ArrayList<DoctorSpecialities>();
                    if (jsonObject.getString("success").equals("true")) {
                        JSONArray jsonArray = jsonObject.optJSONArray("speciality");

                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject specialitiesJsonObject = jsonArray.getJSONObject(i);
                                DoctorSpecialities doctorSpecialities = new DoctorSpecialities();
                                doctorSpecialities.setDoctorId(specialitiesJsonObject.getString("id"));
                                doctorSpecialities.setDoctorSpeciality(specialitiesJsonObject.getString("speciality"));
                                doctorSpecialities.setDoctorPictureUrl(specialitiesJsonObject.getString("picture"));
                                doctorSpecialitiesList.add(doctorSpecialities);
                            }
                            Realm realm = RealmDBController.with(getActivity()).getRealm();


                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.clear(DoctorSpecialities.class);
                                    realm.copyToRealm(doctorSpecialitiesList);
                                }
                            });
                            HomeAdapter homeAdapter = new HomeAdapter(mContext, doctorSpecialitiesList);
                            specialitiesGridView.setAdapter(homeAdapter);
                            no_speciality_text.setVisibility(View.GONE);

                        } else
                            no_speciality_text.setVisibility(View.VISIBLE);

                    } else {
                        no_speciality_text.setVisibility(View.VISIBLE);
                        //   AndyUtils.removeProgressDialog();
                        if (jsonObject.optString("error_message").equalsIgnoreCase(Const.INVALID_TOKEN)) {
                            AndyUtils.showShortToast(getString(R.string.you_have_logged), getActivity());
                            PreferenceHelper.setParam(getActivity(), Const.LOGIN_FIRST_TIME, false);
                            Intent homeIntent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(homeIntent);
                            getActivity().finish();
                        }
//                        AndyUtils.showShortToast(jsonObject.optString("error_message"), mContext);
                    }
                } catch (JSONException e) {
                    //   AndyUtils.removeProgressDialog();
                    e.printStackTrace();
                }

                break;
//            case 101:
//                AndyUtils.appLog("Mashesh", "CheckResponse" + response);
//                break;

        }

    }

    @Override
    public void onDestroyView() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.HOME_FRAGMENT);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("HomeFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onDestroyView();
    }
}
