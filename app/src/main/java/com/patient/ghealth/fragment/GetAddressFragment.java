package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.CheckoutActivity;
import com.patient.ghealth.adapter.AddressListAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.Address;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class GetAddressFragment extends Fragment implements AsyncTaskCompleteListener {

    public static boolean isFromSummary = false, forBillingAddress = false, isFromAccount = false;
    private RecyclerView mRecyclerView;
    private AddressListAdapter mAddressListAdapter;
    private List<Address> mAddressList;
    private CheckoutActivity activity;

    public GetAddressFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAddressList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_get_address, container, false);
        activity = (CheckoutActivity) getActivity();
        try {
            isFromSummary = getArguments().getBoolean("isFromSummary", false);
            forBillingAddress = getArguments().getBoolean("forBillingAddress", false);
//            isFromAccount = getArguments().getBoolean("isFromAccount", false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mRecyclerView = (RecyclerView) view.findViewById(R.id.address_list);
        mAddressList.clear();
        mAddressList.add(new Address());
        setupRecyclerView();
        getUserAddress();
        return view;
    }

    private void getUserAddress() {

        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_USER_ADDRESS_URL);

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "GetAddressMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_USER_ADDRESS, this, headerMap);
    }

    private void setupRecyclerView() {
        mAddressListAdapter = new AddressListAdapter(mAddressList, getActivity(), activity);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mAddressListAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.GetAddressFragment;
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_USER_ADDRESS:
                AndyUtils.appLog("Ashutosh", "GetAddressResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        JSONObject mainObj = jsonObject.optJSONObject(Const.RESPONSE);
                        JSONArray shippingJsonArray = mainObj.optJSONArray("address");
                        if (shippingJsonArray != null) {
                            mAddressList.clear();
                            for (int i = 0; i < shippingJsonArray.length(); i++) {
                                JSONObject shippingObject = shippingJsonArray.getJSONObject(i);
                                Address address = new Address();
                                address.set_ID(shippingObject.optString(Const.Params._ID));
                                address.setNUMBER(shippingObject.optString("phone"));
                                address.setADDRESS(shippingObject.optString("address"));
                                address.setPINCODE(shippingObject.optString("pincode"));
                                address.setSTATE(shippingObject.optString("state"));
                                address.setCITY(shippingObject.optString("city"));
                                address.setNAME(shippingObject.optString("name"));
                                address.setCOUNTRY(shippingObject.optString("country"));
                                mAddressList.add(address);
                            }
                        }
                        mAddressList.add(new Address());
                        mAddressListAdapter.notifyDataSetChanged();
                    } else
                        Toast.makeText(getActivity(), jsonObject.optString(Const.Params.STATUS_MESSAGE), Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }
}
