package com.patient.ghealth.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.NotificationAnswerAdapter;
import com.patient.ghealth.adapter.RecyclerViewItemClickListener;
import com.patient.ghealth.adapter.SimpleDividerItemDecoration;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.AnswerData;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 8/22/2016.
 */
public class NotificationAnswerFragment extends Fragment implements AsyncTaskCompleteListener
{

    private RecyclerView notificationMessageRecyclerView;
    private NotificationAnswerAdapter messageAdapter;
    private List<AnswerData> answerDataList;
    private ProgressBar notificationProgressBar;
    private TextView noNotification;
    private MainActivity activity;
    private TextView border;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_notification_answer,container,false);
        activity= (MainActivity) getActivity();
        notificationMessageRecyclerView= (RecyclerView) view.findViewById(R.id.rv_notification_message);
        border= (TextView) view.findViewById(R.id.border);
        PreferenceHelper.setParam(getActivity(),Const.ANSWERCOUNT,0);
        border.setVisibility(View.VISIBLE);
        notificationProgressBar = (ProgressBar) view.findViewById(R.id.notification_progressbar);
        noNotification= (TextView) view.findViewById(R.id.tv_no_notification);
        noNotification.setText(R.string.no_question_available);
        notificationMessageRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(getActivity(), new RecyclerViewItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position)
            {
                AndyUtils.appLog("ClickedPosition",position+"");
                AnswerData answerData=answerDataList.get(position);
                showAnswerDialog(answerData);
            }
        }));

        getAnswerDetails();
        return view;
    }

    private void showAnswerDialog(AnswerData data)
    {
        Dialog answerDialog=new Dialog(getActivity());
        answerDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        answerDialog.setContentView(R.layout.dialog_answer_layout);
        TextView subject= (TextView) answerDialog.findViewById(R.id.tv_dialog_subject);
        TextView question= (TextView) answerDialog.findViewById(R.id.tv_dialog_question);
        TextView answer= (TextView) answerDialog.findViewById(R.id.tv_dialog_answer);
        subject.setText(data.getSubject());
        question.setText(data.getQuestion());
        answer.setText(data.getAnswer());
        answerDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment=Const.NotificationAnswerFragment;
    }

    private void getAnswerDetails()
    {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }

        notificationProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.REQUEST_VIEW_ANSWER_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") );

        AndyUtils.appLog("Ashutosh", "ViewAnswerMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.REQUEST_VIEW_ANSWER, this);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode)
    {
        notificationProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject;
        switch (serviceCode)
        {
            case Const.ServiceCode.REQUEST_VIEW_ANSWER:
                AndyUtils.appLog("Ashutosh","ViewAnswerResponse" +response);
                try {
                    jsonObject=new JSONObject(response);
                    if(jsonObject.optString("success").equals("true"))
                    {
                        answerDataList=new ArrayList<>();
                        JSONArray jsonArray=jsonObject.optJSONArray("questionArray");
                        if(jsonArray!=null && jsonArray.length()>0)
                        {
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                AnswerData data=new AnswerData();
                                JSONObject answerObject=jsonArray.optJSONObject(i);
                                data.setAnswerId(answerObject.optString("id"));
                                data.setSubject(answerObject.optString("subject"));
                                data.setQuestion(answerObject.optString("content"));
                                data.setAnswer(answerObject.optString("answer"));
                                data.setDate(answerObject.optString("date"));
                                data.setTime(answerObject.optString("time"));
                                data.setDoctorName(answerObject.optString("doctor_name"));
                                data.setDoctorPicture(answerObject.optString("doctor_picture"));
                                answerDataList.add(data);
                            }

                            Collections.reverse(answerDataList);
                            LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
                            notificationMessageRecyclerView.setLayoutManager(layoutManager);
                            messageAdapter=new NotificationAnswerAdapter(getActivity(),answerDataList);
                            notificationMessageRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
                            notificationMessageRecyclerView.setAdapter(messageAdapter);

                        }
                        else
                        {
                            noNotification.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }
}
