package com.patient.ghealth.fragment;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.VideoCallActivity;
import com.patient.ghealth.adapter.ChatAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.ChatObject;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.realmDB.DatabaseHandler;
import com.patient.ghealth.service.ImageUploadService;
import com.patient.ghealth.service.XmppService;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.Helper;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import id.zelory.compressor.Compressor;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.facebook.FacebookSdk.getApplicationContext;

public class VideoChatFragment extends Fragment implements OnClickListener, XmppService.Callbacks, AsyncTaskCompleteListener {
    private static final int CAPTURE_IMAGE = 1, GET_FROM_GALLERY = 2;
    public static List<ChatObject> messages;
    public TextView doctorName;
    private ListView message_listview;
    private ImageView videoChatClose, attachment;
    private FloatingActionButton send;
    private EditText messageEdit;
    private DatabaseHandler db;
    private String request_id = "", sendermsg;
    private MediaUploadReceiver mediaReceiver;
    private ChatAdapter messageAdapter;
    private VideoCallActivity activity;
    private Uri uri;
    private IntentFilter filter1;
    private XmppService myService;
    private Socket mSocket;
    private boolean isConnected = true;
    private PreferenceHelper pHelper;
    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            AndyUtils.appLog("MessageActivity", "onServiceConnected called");
            // We've binded to LocalService, cast the IBinder and get LocalService instance
            XmppService.LocalBinder binder = (XmppService.LocalBinder) service;
            myService = binder.getServiceInstance(); //Get instance of your service!
            myService.registerVideoClient(activity, Const.VIDEO_CHAT); //Activity register in the service as client for callabcks!
//            tvServiceState.setText("Connected to service...");
//            tbStartTask.setEnabled(true);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
//            Toast.makeText(activity, "onServiceDisconnected called", Toast.LENGTH_SHORT).show();
//            tvServiceState.setText("Service disconnected");
//            tbStartTask.setEnabled(false);
        }
    };
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isConnected) {

                        try {

                            JSONObject object = new JSONObject();
                            object.put("request_id", pHelper.getRequestId());
                            object.put("type", "user");

                            Log.e("update_object", "" + object);
                            mSocket.emit("online_chat_consult", object);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Toast.makeText(getApplicationContext(),
                                R.string.connect, Toast.LENGTH_LONG).show();
                        isConnected = true;
                    }
                    if (isConnected) {


                        try {

                            JSONObject object = new JSONObject();
                            object.put("request_id", pHelper.getRequestId());
                            object.put("type", "user");
                            Log.e("update_object", "" + object);
                            mSocket.emit("online_chat_consult", object);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }
            });
        }
    };
    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isConnected = false;
                    Toast.makeText(getApplicationContext(),
                            R.string.disconnect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity,
                            R.string.error_connect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];


                    String data_type;
                    String message;
                    String type;
                    try {

                        data_type = data.getString("data_type");
                        message = data.getString("message");
                        type = data.getString("type");

                        showChat(type, data_type, message);


                    } catch (JSONException e) {
                        return;
                    }


                }
            });
        }
    };
    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d("mahi", "user joined");
        }
    };
    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        return;
                    }

                }
            });
        }
    };
    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d("mahi", "on typing");
        }
    };
    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    try {
                        username = data.getString("username");
                    } catch (JSONException e) {
                        return;
                    }

                }
            });
        }
    };
    private Emitter.Listener onsent = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.d("mahi", "calling on sent");
        }
    };

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHandler(getActivity());
        activity = (VideoCallActivity) getActivity();
        Intent serviceIntent = new Intent(activity, XmppService.class);
        activity.bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE);
        pHelper = new PreferenceHelper(activity);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_layout, container, false);
        AndyUtils.appLog("OnCreate", "DoctorId" + PreferenceHelper.getParam(getActivity(), Const.Params.DOCTOR_ID, ""));
//        XmppService.setupAndConnect(getActivity(),
//                Const.SERVER, "", Const.ServiceType.XMPPDRIVER
//                        + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")), Const.XMPP_PASSWORD);

        initiateSokect();
        attachment = (ImageView) view.findViewById(R.id.attachment);
        doctorName = (TextView) view.findViewById(R.id.chat_video_name);
        if (!PreferenceHelper.getParam(getActivity(), Const.DOCTOR_NAME, "").equals("")) {
            doctorName.setText((CharSequence) PreferenceHelper.getParam(getActivity(), Const.DOCTOR_NAME, ""));
        }
        videoChatClose = (ImageView) view.findViewById(R.id.iv_video_chat_close);
        attachment.setOnClickListener(this);
        request_id = new PreferenceHelper(getActivity()).getRequestId();
        getChatHistory(request_id);
        filter1 = new IntentFilter("media_upload_receiver");
        mediaReceiver = new MediaUploadReceiver();
        message_listview = (ListView) view.findViewById(R.id.listView_chat_messages);
        AndyUtils.appLog("VideoChatFragment", "RequestId" + request_id);
        messages = db.get_Chat("" + request_id);
        if (messages != null && messages.size() > 0) {
            AndyUtils.appLog("Ashutosh", messages.size() + "");
            messageAdapter = new ChatAdapter(activity, R.layout.chat_adapter, messages);
            message_listview.setAdapter(messageAdapter);
        }

        send = (FloatingActionButton) view.findViewById(R.id.send);
        send.setOnClickListener(this);
        messageEdit = (EditText) view.findViewById(R.id.et_message);

//        recieve_chat=new ReceiverChat();
        videoChatClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                AndyUtils.appLog("Close", "Attempt");

                activity.hideFragment();
            }
        });

        return view;
    }

    private void getChatHistory(String request_id) {


        if (!AndyUtils.isNetworkAvailable(activity)) {
            AndyUtils.showShortToast(getString(R.string.no_internet), activity);
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CHAT_HISTORY_MESSAGE_URL + Const.Params.REQUEST_ID + "="
                + request_id);

        AndyUtils.appLog("VideoChatFragment", "ChatHistory" + map);
        new HttpRequester(activity, Const.GET, map, Const.ServiceCode.GET_CHAT_HISTORY_MESSAGE, this);

    }

    private void initiateSokect() {

        try {
            mSocket = IO.socket(Const.ServiceType.SOCKET_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("online_chat_message", onNewMessage);
        mSocket.on("user_joined_chat", onUserJoined);
        mSocket.on("user left", onUserLeft);
        mSocket.on("online_chat_start_typing", onTyping);
        mSocket.on("stop typing", onStopTyping);
        mSocket.on("online_chat_consult", onsent);
        mSocket.connect();
    }

    @Override
    public void getMessage(final String data, final String dataType) {

        AndyUtils.appLog("MessageActivity", data);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showChat("recieve", dataType, data);
                if (data.contains("http")) {
//                    ("Image");
                } else {
//                    notifyMsg(data);
                }
            }
        });
    }


//    class ReceiverChat extends BroadcastReceiver
//    {
//
//        @Override
//        public void onReceive(Context context, Intent intent) {
//
//            String message = intent.getStringExtra("message");
//            String data_type = intent.getStringExtra("data_type");
//            showChat("recieve", data_type, message);
//
//        }
//
//    }


    public void close() {
        this.close();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send:
                sendermsg = messageEdit.getText().toString();
                if (messageEdit.getText().toString().length() == 0) {

                    Toast.makeText(getActivity(),
                            "Enter text before sending", Toast.LENGTH_LONG).show();

                } else {

                    if (mSocket.connected()) {
                        attemptSend(sendermsg, "TEXT");
                    }
                    messageEdit.setText("");

//                    AndyUtils.appLog("DoctorId", String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.DOCTOR_ID, "")));
//                    XmppService.sendMessage(getActivity(),
//                            Const.ServiceType.XMPPUSER + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.DOCTOR_ID, ""))
//                                    + Const.SUFFIX_CHAT,
//                            Message.Type.chat, AndyUtils.message_json("0", Const.Params.TEXT, String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.DOCTOR_ID, "")),
//                                    String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")), "" + new PreferenceHelper(getActivity()).getRequestId(), sendermsg));
//
//                    messageEdit.setText("");

//                    showChat("sent", Const.Params.TEXT, sendermsg);
                }
                break;
            case R.id.attachment:
                openGallery();
                break;

        }
    }


    private void showChat(String type, String data_type, String message) {

        if (messages == null || messages.size() == 0) {

            messages = new ArrayList<ChatObject>();
        }
//
        AndyUtils.appLog("VideoChatFragment", "DoctorId" + PreferenceHelper.getParam(activity, Const.Params.DOCTOR_ID, "") + "");
        AndyUtils.appLog("VideoChatFragment", "InsertChat" + String.valueOf(PreferenceHelper.getParam(activity, Const.Params.DOCTOR_ID, "")) + "");

        messages.add(new ChatObject(message, type, data_type, ""
                + request_id, String.valueOf(PreferenceHelper.getParam(activity, Const.Params.ID, "")), String.valueOf(PreferenceHelper.getParam(activity, Const.Params.DOCTOR_ID, ""))));

        ChatAdapter chatAdabter = new ChatAdapter(activity, R.layout.chat_adapter, messages);
        message_listview.setAdapter(chatAdabter);
        // chatAdabter.notifyDataSetChanged();
        if (type.equals("sent")) {
            AndyUtils.appLog("MessageActivity", "InsertChat" + request_id);
            ChatObject chat = new ChatObject(message, type, data_type, ""
                    + request_id, String.valueOf(PreferenceHelper.getParam(activity, Const.Params.ID, "")), String.valueOf(PreferenceHelper.getParam(activity, Const.Params.DOCTOR_ID, "")));

            db.insertChat(chat);
        }
        // chatAdabter.notifyDataSetChanged();

    }


    private void showPictureDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Select Option");
        String[] items = {"Gallery", "Camera"};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        openGallery();
                        break;
                    case 1:
                        openCamera();
                        break;


                }
            }
        });
        dialog.show();
    }

    private void openGallery() {

        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        activity.startActivityForResult(intent, GET_FROM_GALLERY, "VideoChat");
    }

    public void openCamera() {

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        Calendar cal = Calendar.getInstance();
        File file = new File(Environment.getExternalStorageDirectory(),
                (cal.getTimeInMillis() + ".jpg"));
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {

            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        uri = Uri.fromFile(file);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(cameraIntent, CAPTURE_IMAGE);

    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Intent intent;
        if (resultCode == getActivity().RESULT_OK) {
            switch (requestCode) {

                case CAPTURE_IMAGE:

                    if (uri != null) {
                        String file = Helper.getRealPathFromURI(uri, getActivity());
                        Bitmap compressedImageFile = Compressor.getDefault(getActivity()).compressToBitmap(new File(file));
                        Uri uri = getImageUri(getActivity(), compressedImageFile);
                        String selectedImagePath = Helper.getRealPathFromURI(uri, getActivity());
                        sendermsg = selectedImagePath;

                        Intent captureUploadService = new Intent(getActivity(), ImageUploadService.class);

                        captureUploadService.putExtra("image_path", selectedImagePath);
                        // imageUploadService.putExtra("key", key);
                        captureUploadService.putExtra("request_id", request_id);
                        getActivity().startService(captureUploadService);
                    }

                    // mHandler.sendEmptyMessage(2);

                    break;


                case GET_FROM_GALLERY:

                    if (data != null) {
                        Uri selectedImage = data.getData();
                        String file = Helper.getRealPathFromURI(selectedImage, getActivity());
                        Bitmap compressedImageFile = Compressor.getDefault(getActivity()).compressToBitmap(new File(file));
                        Uri uri = getImageUri(getActivity(), compressedImageFile);
                        String picturePath = Helper.getRealPathFromURI(uri, getActivity());
//                        picturePath = Helper.compressImage(getActivity(), picturePath);
                        sendermsg = picturePath;

                        //mHandler.sendEmptyMessage(2);

                        BitmapFactory.Options options = new BitmapFactory.Options();

                        options.inJustDecodeBounds = false;
                        options.inPreferredConfig = Bitmap.Config.RGB_565;
                        options.inDither = true;

                        Bitmap bitmapgallery = BitmapFactory.decodeFile(picturePath, options);

                        Intent imageUploadService = new Intent(getActivity(), ImageUploadService.class);

                        imageUploadService.putExtra("image_path", picturePath);
                        // imageUploadService.putExtra("key", key);
                        imageUploadService.putExtra("request_id", request_id);
                        getActivity().startService(imageUploadService);
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.unable_to_select_image),
                                Toast.LENGTH_LONG).show();
                    }

                    break;
                default:
                    break;
            }
        }


    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().registerReceiver(mediaReceiver, filter1);
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(recieve_chat,
//                new IntentFilter("message_recieved"));
    }


    @Override
    public void onPause() {
        super.onPause();
//        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(recieve_chat);
//        getActivity().unregisterReceiver(mediaReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(mediaReceiver);
//        LocalBroadcastManager.getInstance(activity).unregisterReceiver(recieve_chat);
        mSocket.disconnect();

        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("online_chat_message", onNewMessage);
        mSocket.off("user_joined_chat", onUserJoined);
        mSocket.off("online_chat_start_typing", onTyping);
        mSocket.off("stop typing", onStopTyping);
        mSocket.off("online_chat_consult", onsent);
    }


    private void parseMediaAndSendMessage(String response) {

        sendermsg = response;

        attemptSend(sendermsg, "IMAGE");
        AndyUtils.appLog("DoctorId", String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.DOCTOR_ID, "")));
//        XmppService.sendMessage(getActivity(),
//                Const.ServiceType.XMPPUSER + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.DOCTOR_ID, ""))
//                        + Const.SUFFIX_CHAT,
//                Message.Type.chat, AndyUtils.message_json("0", Const.Params.IMAGE, String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.DOCTOR_ID, "")),
//                        String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")), "" + new PreferenceHelper(getActivity()).getRequestId(), sendermsg));
//
//
//        showChat("sent", Const.Params.IMAGE, sendermsg);

    }


    private void attemptSend(String sendermsg, String type) {
        if (!mSocket.connected()) return;

        JSONObject messageObj = new JSONObject();
        try {
            messageObj.put("message", sendermsg);
            messageObj.put("message_type", type);

            Log.e("mahi", "message socket in chat" + messageObj.toString());
//showChat("sent", type, sendermsg);
            mSocket.emit("online_chat_message", messageObj);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode) {
            case Const.ServiceCode.GET_CHAT_HISTORY_MESSAGE:
                Log.d("VideoChatFragment", "message response" + response);

                try {
                    JSONObject job = new JSONObject(response);
                    if (job.getString("success").equals("true")) {
                        JSONArray jarray = job.getJSONArray("chatArray");
                        messages = new ArrayList<ChatObject>();
                        for (int i = 0; i < jarray.length(); i++) {
                            JSONObject chatobj = jarray.getJSONObject(i);
                            messages.add(new ChatObject(chatobj.getString("message"), chatobj.getString("type"), chatobj.getString("data_type"), ""
                                    + request_id, String.valueOf(PreferenceHelper.getParam(activity, Const.Params.ID, "")), String.valueOf(PreferenceHelper.getParam(activity, Const.Params.DOCTOR_ID, ""))));

                        }

                        if (messages != null && messages.size() > 0) {
                            messageAdapter = new ChatAdapter(activity, R.layout.chat_adapter, messages);
                            message_listview.setAdapter(messageAdapter);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                break;

        }
    }

    class MediaUploadReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            String response = intent.getStringExtra("response");

            parseMediaAndSendMessage(response);
        }

    }


}
