package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.CheckoutActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.Address;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by amal on 21/09/16.
 */
public class UpdateAddressFragment extends Fragment implements AsyncTaskCompleteListener {

    private EditText fullName, address1, city, state, pincode, number, country;
    private TextView add_address_text;
    private Button updateAddress;
    private CheckoutActivity activity;

    public UpdateAddressFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_address_add, container, false);
        activity = (CheckoutActivity) getActivity();
        final Address address = (Address) getArguments().getSerializable("address");
        fullName = (EditText) view.findViewById(R.id.full_name);
        fullName.setText(address.getNAME());
        address1 = (EditText) view.findViewById(R.id.address1);
        address1.setText(address.getADDRESS());
        city = (EditText) view.findViewById(R.id.city);
        city.setText(address.getCITY());
        state = (EditText) view.findViewById(R.id.state);
        state.setText(address.getSTATE());
        pincode = (EditText) view.findViewById(R.id.pincode);
        pincode.setText(address.getPINCODE());
        number = (EditText) view.findViewById(R.id.phone_number);
        number.setText(address.getNUMBER());
        updateAddress = (Button) view.findViewById(R.id.add_address);
        updateAddress.setText(R.string.update_address);
        add_address_text = (TextView) view.findViewById(R.id.add_address_text);
        country = (EditText) view.findViewById(R.id.country);
        if (address.getCOUNTRY() != null && !address.getCOUNTRY().equals("")) {
            country.setText(address.getCOUNTRY());
            country.setEnabled(true);
        } else {
            country.setEnabled(true);
        }
        add_address_text.setText(getString(R.string.update_your_existing_address));
        updateAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkIfFieldsEmpty()) {

                    updateAddressDetails(address.get_ID());

                }
            }
        });

        return view;
    }


    private void updateAddressDetails(String addId) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.UPDATE_USER_ADDRESS_URL + addId);
        map.put("phone", number.getText().toString());
        map.put("address", address1.getText().toString());
        map.put("state", state.getText().toString());
        map.put("pincode", pincode.getText().toString());
        map.put("city", city.getText().toString());
        map.put("name", fullName.getText().toString());
        map.put("country", country.getText().toString());
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "CartCountMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.PUT, map, Const.ServiceCode.UPDATE_USER_ADDRESS, this, headerMap);
    }

    private boolean checkIfFieldsEmpty() {
        if (fullName.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), R.string.enter_name, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (address1.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), R.string.enter_address, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (city.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), R.string.enter_city, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (state.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), R.string.enter_state, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (pincode.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), R.string.enter_pincode, Toast.LENGTH_SHORT).show();
            return false;
        }
        if (number.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(), R.string.enter_number, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.UPDATE_USER_ADDRESS:
                AndyUtils.appLog("Ashutosh", "UpdateAddressResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        AndyUtils.showShortToast(getString(R.string.address_updated), getActivity());
                        activity.addFragment(new GetAddressFragment(), false, getString(R.string.health_store), "", false);
                    } else if (jsonObject.optInt(Const.Params.STATUS_CODE) == 500) {
                        Toast.makeText(getActivity(), jsonObject.optString(Const.Params.STATUS_MESSAGE), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }
}
