package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

/**
 * Created by user on 8/17/2016.
 */
public class HealthFeedsDescriptionFragment extends Fragment implements AsyncTaskCompleteListener {
    private TextView description_header, border, description;
    private MainActivity activity;
    private Bundle bundle;
    private ImageView articlePicture, bookmarkIcon;
    private boolean isClicked = false;
    private boolean isBooked;
    private String bookMarkArticleId;
    private String backPressType = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_health_feeds_description_layout, container, false);
        description_header = (TextView) view.findViewById(R.id.tv_health_feeds_description_header);
        border = (TextView) view.findViewById(R.id.tv_description_border);
        description = (TextView) view.findViewById(R.id.tv_health_feeds_description);
        articlePicture = (ImageView) view.findViewById(R.id.iv_healths_feed_description_icon);
        bookmarkIcon = (ImageView) view.findViewById(R.id.iv_healths_feed_favourite_icon);

        bookmarkIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isBooked) {
                    bookmarkIcon.setImageResource(R.drawable.no_rated);
                    isBooked = false;
                    addBookMark(0);
                } else {
                    bookmarkIcon.setImageResource(R.drawable.rating_star);
                    isBooked = true;
                    addBookMark(1);
                }

            }
        });

        return view;
    }


    private void addBookMark(int bookMarked) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        if (bookMarked == 0) {
            AndyUtils.showSimpleProgressDialog(getActivity(), getActivity().getString(R.string.removing_bookMark), false);
        } else if (bookMarked == 1) {
            AndyUtils.showSimpleProgressDialog(getActivity(), getActivity().getString(R.string.requesting_bookMark), false);
        }

        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_ADD_BOOKMARK_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.ARTICLE_ID, String.valueOf(bookMarkArticleId));
        map.put(Const.Params.IS_BOOKMARKED, String.valueOf(bookMarked));

        AndyUtils.appLog("Ashutosh", "BookMarkMap" + map);

        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.CREATE_ADD_BOOKMARK, this);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bundle = getArguments();
        if (bundle != null) {
            String articleId = bundle.getString(Const.ARTICLE_CLICKED_POSITION);
            backPressType = bundle.getString(Const.BookMarkFragment);
            AndyUtils.appLog("DescriptionArticleId", articleId);
            getArticleDescription(articleId);

        }
    }

    private void getArticleDescription(String articleId) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        AndyUtils.showSimpleProgressDialog(getActivity(), getActivity().getString(R.string.requesting_desc), false);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_ARTICLE_DESCRIPTION_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.ARTICLE_ID + "="
                + String.valueOf(articleId));

        AndyUtils.appLog("Ashutosh", "GetArticleDescriptionMap" + map);
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_ARTICLE_DESCRIPTION, this);


    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (backPressType != null && backPressType.equals(Const.BookMarkFragmentDescription)) {
                border.setVisibility(View.VISIBLE);
                activity.currentFragment = Const.BookMarkFragmentDescription;
            } else {
                border.setVisibility(View.GONE);
                activity.currentFragment = Const.HealthFeedsDescriptionFragment;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {

        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.HealthFeedsDescriptionFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("HealthFeedsDescriptionFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onDestroyView();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        AndyUtils.removeProgressDialog();
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_ARTICLE_DESCRIPTION:

                AndyUtils.appLog("Ashutosh", "GetArticleDescriptionResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        String articlePictureUrl = jsonObject.getString("picture");
                        String heading = jsonObject.getString("heading");
                        String descriptionContent = jsonObject.getString("content");
                        bookMarkArticleId = jsonObject.optString("article_id");
                        if (articlePictureUrl.equals("")) {

                        } else {
                            Glide.with(getActivity()).load(articlePictureUrl).into(articlePicture);
                        }

                        if (Integer.parseInt(jsonObject.optString("is_bookmarked")) == 0) {
                            bookmarkIcon.setImageResource(R.drawable.no_rated);
                            isBooked = false;
                        } else if (Integer.parseInt(jsonObject.optString("is_bookmarked")) == 1) {
                            bookmarkIcon.setImageResource(R.drawable.rating_star);
                            isBooked = true;
                        }

                        description_header.setText(heading);
//                        description_sub_header.setText(heading);


                        try {
                            description.setText(URLDecoder.decode(descriptionContent, "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    } else {
                        AndyUtils.showShortToast(jsonObject.getString("error_message"), getActivity());
                    }
                } catch (JSONException e) {
                    AndyUtils.removeProgressDialog();
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CREATE_ADD_BOOKMARK:
                AndyUtils.appLog("Ashutosh", "BookMarkResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        if (isBooked == false) {
                            AndyUtils.showShortToast(getString(R.string.article_bookmark_removed), getActivity());
                        } else if (isBooked == true) {
                            AndyUtils.showShortToast(getString(R.string.article_bookmark_successfully), getActivity());
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

        }

    }
}
