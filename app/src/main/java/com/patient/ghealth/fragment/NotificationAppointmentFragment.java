package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.adapter.AppointmentNotificationAdapter;
import com.patient.ghealth.adapter.MyScheduleSlotsAdapter;
import com.patient.ghealth.adapter.SimpleDividerItemDecoration;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.MyDateSchedule;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 9/2/2016.
 */
public class NotificationAppointmentFragment extends Fragment implements AsyncTaskCompleteListener {
    DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    private RecyclerView appointmentRecyclerView;
    private AppointmentNotificationAdapter appointmentAdapter;
    private List<MyDateSchedule> myDateScheduleList;
    private ProgressBar notificationProgressBar;
    private TextView noNotification;
    private MyScheduleSlotsAdapter myScheduleSlotsAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notification_answer, container, false);
        appointmentRecyclerView = (RecyclerView) view.findViewById(R.id.rv_notification_message);
        notificationProgressBar = (ProgressBar) view.findViewById(R.id.notification_progressbar);
        noNotification = (TextView) view.findViewById(R.id.tv_no_notification);
        noNotification.setText(getString(R.string.no_notification_available));
        Calendar c = Calendar.getInstance();
        String currentFormattedDate = FORMATTER.format(c.getTime());
        AndyUtils.appLog("CurrentDate", currentFormattedDate);
        getMySchedulesDateSlots(currentFormattedDate);



//        appointmentRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(getActivity(), new RecyclerViewItemClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//                AndyUtils.appLog("ClickedPosition",position+"");
//                Intent intent=new Intent(getActivity(), MainActivity.class);
//                intent.putExtra(Const.NotificationAppointmentFragment,Const.NotificationAppointmentFragment);
//                startActivity(intent);
//                getActivity().finish();
//            }
//        }));

        return view;
    }

    private void getMySchedulesDateSlots(String selectedDate) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.please_wait), getActivity());
            return;
        }
        notificationProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_MY_SCHEDULE_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.DATE + "="
                + selectedDate);

        AndyUtils.appLog("Ashutosh", "GetMyScheduleSlotsMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_MY_SCHEDULE, this);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {

        notificationProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_MY_SCHEDULE:
                AndyUtils.appLog("Ashutosh", "GetMyScheduleSlotsResponse" + response);
                try {

                    jsonObject = new JSONObject(response);
                    myDateScheduleList = new ArrayList<>();
                    if (jsonObject.getString("success").equals("true")) {
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject scheduleJsonObject = jsonArray.getJSONObject(i);
                                MyDateSchedule myDateSchedule = new MyDateSchedule();
                                myDateSchedule.setScheduleDate(scheduleJsonObject.optString("date"));
                                myDateSchedule.setScheduleDoctorName(scheduleJsonObject.optString("doctor_name"));
                                myDateSchedule.setScheduleClinicStreet(scheduleJsonObject.optString("c_street"));
                                myDateSchedule.setScheduleClinicCity(scheduleJsonObject.optString("c_city"));
                                myDateSchedule.setScheduleClinicState(scheduleJsonObject.optString("c_state"));
                                myDateSchedule.setScheduleClinicCountry(scheduleJsonObject.optString("c_country"));
                                myDateSchedule.setScheduleClinicPostal(scheduleJsonObject.optString("c_postal"));
                                myDateSchedule.setScheduleStartTime(scheduleJsonObject.optString("start_time"));
                                myDateSchedule.setScheduleEndTime(scheduleJsonObject.optString("end_time"));
                                myDateSchedule.setScheduleClinicName(scheduleJsonObject.optString("c_name"));
                                myDateSchedule.setTimeFormat(scheduleJsonObject.optString("ampm"));
                                myDateScheduleList.add(myDateSchedule);
                            }

                            myScheduleSlotsAdapter=new MyScheduleSlotsAdapter(getActivity(),myDateScheduleList);
                            LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
                            appointmentRecyclerView.setLayoutManager(layoutManager);
                            appointmentRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
                            appointmentRecyclerView.setAdapter(myScheduleSlotsAdapter);

                        } else {
                            noNotification.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }

    }
}
