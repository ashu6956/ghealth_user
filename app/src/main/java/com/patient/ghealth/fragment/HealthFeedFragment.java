package com.patient.ghealth.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.LoginActivity;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.HealthFeedsAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.HealthFeedArticles;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by getit on 8/5/2016.
 */
public class HealthFeedFragment extends Fragment implements View.OnClickListener,AsyncTaskCompleteListener {


    ListView healthsListView;
    TextView askAQuestionText,headerText;
    FloatingActionButton healthsFeedButton;
    private MainActivity activity;
    private List<HealthFeedArticles> feedArticlesList;
    private ProgressBar health_feed_progress;
    private TextView noArticlesText;
    private Bundle bundle;
    private String type="",articleId="";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_health_feeds_layout, container, false);
        activity= (MainActivity) getActivity();
        healthsListView = (ListView) view.findViewById(R.id.lv_healths_feed);
        headerText= (TextView) view.findViewById(R.id.tv_healths_feed_header);
        health_feed_progress = (ProgressBar) view.findViewById(R.id.health_feed_progress);
        headerText.setText(getString(R.string.health_feed));
        bundle=getArguments();
        if(bundle!=null && bundle.getString(Const.FollowMoreTopicsFragment).equals(Const.CATEGORY))
        {
           type=bundle.getString(Const.FollowMoreTopicsFragment);
           articleId=bundle.getString(Const.TOPICS_ARTICLE_ID);
           AndyUtils.appLog("ArtcileId",articleId);

        }
        noArticlesText= (TextView) view.findViewById(R.id.tv_no_articles);
        healthsFeedButton= (FloatingActionButton) view.findViewById(R.id.fabButton_health_feeds);
        healthsFeedButton.setOnClickListener(this);
        askAQuestionText= (TextView) view.findViewById(R.id.tv_healths_feed_ask_questions);
        askAQuestionText.setOnClickListener(this);
        healthsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                HealthFeedsDescriptionFragment descriptionFragment=new HealthFeedsDescriptionFragment();
                Bundle bundle=new Bundle();
                HealthFeedArticles feedArticles=feedArticlesList.get(position);
                bundle.putString(Const.ARTICLE_CLICKED_POSITION,feedArticles.getArticleId());
                descriptionFragment.setArguments(bundle);
                activity.addFragment(descriptionFragment,false,getString(R.string.health_feed), Const.HealthFeedsDescriptionFragment,true);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.appLogo.setVisibility(View.GONE);
            }

        });
        getArticles();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }


    private void getArticles()
    {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        health_feed_progress.setVisibility(View.VISIBLE);
        AndyUtils.appLog("ArticleId" ,articleId);
        HashMap<String, String> map = new HashMap<String, String>();
        if(type.equals(Const.CATEGORY))
        {
            map.put(Const.Params.URL, Const.ServiceType.GET_ARTICLES_URL + Const.Params.ID + "="
                    + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                    + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.ARTICLES_CATEGORY_ID+ "="
                    + String.valueOf(articleId));
            AndyUtils.appLog("Ashutosh", "GetArticlesTopicMap" + map);
        }
        else
        {
            map.put(Const.Params.URL, Const.ServiceType.GET_ARTICLES_URL + Const.Params.ID + "="
                    + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                    + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));

            AndyUtils.appLog("Ashutosh", "GetArticlesMap" + map);
        }
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_ARTICLES, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment=Const.HealthFeedsFragment;

    }

    @Override
    public void onDestroyView() {

        FragmentManager manager=getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.HealthFeedsFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("HealthFeedsFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onDestroyView();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.fabButton_health_feeds:
                activity.addFragment(new FollowMoreTopicsFragment(),false,getString(R.string.follow_more_topics),Const.FollowMoreTopicsFragment,true);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.appLogo.setVisibility(View.GONE);
                break;
            case R.id.tv_healths_feed_ask_questions:

                try {

                    activity.layoutToolbarOptions.setVisibility(View.GONE);
                    activity.backbuttonImage.setVisibility(View.VISIBLE);
                    activity.mainTabLayout.setVisibility(View.GONE);
                    activity.appLogo.setVisibility(View.GONE);
                    AskAQuestionFragment askAQuestionFragment = new AskAQuestionFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Const.AskAQuestionFragment, (Serializable) feedArticlesList);
                    askAQuestionFragment.setArguments(bundle);
                    activity.addFragment(askAQuestionFragment, false, getString(R.string.ask_a_question), Const.AskAQuestionFragment, true);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
                break;
        }

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode)
    {
        health_feed_progress.setVisibility(View.GONE);
        JSONObject jsonObject;
        switch (serviceCode)
        {
            case Const.ServiceCode.GET_ARTICLES:
                AndyUtils.appLog("Ashutosh","GetArticlesResponse" +response);

                try {
                    jsonObject=new JSONObject(response);
                    if(jsonObject.getString("success").equals("true"))
                    {
                        feedArticlesList=new ArrayList<>();
                        JSONArray feedJsonArray=jsonObject.optJSONArray("articles");
                        if(feedJsonArray!=null && feedJsonArray.length()>0) {
                            for (int i = 0; i < feedJsonArray.length(); i++) {
                                JSONObject feedJsonObject = feedJsonArray.optJSONObject(i);
                                HealthFeedArticles healthFeedArticles = new HealthFeedArticles();
                                healthFeedArticles.setArticleId(feedJsonObject.optString("article_id"));
                                healthFeedArticles.setArticlePictureUrl(feedJsonObject.optString("picture"));
                                healthFeedArticles.setCategory(feedJsonObject.optString("category"));
                                healthFeedArticles.setTopicHeading(feedJsonObject.optString("heading"));
                                healthFeedArticles.setIsBookMarked(feedJsonObject.optString("is_bookmarked"));
                                feedArticlesList.add(healthFeedArticles);
                            }

                            HealthFeedsAdapter healthFeedsAdapter = new HealthFeedsAdapter(getActivity(), feedArticlesList);
                            healthsListView.setAdapter(healthFeedsAdapter);
                            noArticlesText.setVisibility(View.GONE);
                        }
                        else
                        {
                            noArticlesText.setVisibility(View.VISIBLE);
                        }


                    }
                    else
                    {
                        noArticlesText.setVisibility(View.VISIBLE);

                        if (jsonObject.optString("error_message").equalsIgnoreCase(Const.INVALID_TOKEN)) {
                            AndyUtils.showShortToast(getString(R.string.you_have_logged), getActivity());
                            PreferenceHelper.setParam(getActivity(), Const.LOGIN_FIRST_TIME, false);
                            Intent homeIntent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(homeIntent);
                            getActivity().finish();
                        }
                        else
                        {
                            AndyUtils.showShortToast(jsonObject.getString("error_message"),getActivity());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


        }

    }
}
