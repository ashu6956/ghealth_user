package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.ReadFile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by user on 9/6/2016.
 */
public class AddFamilyFragment extends Fragment implements View.OnClickListener
{
    private Spinner day_spinner,month_spinner,year_spinner,gender_title_spinner,countryCode_spinner,relationship_spinner,blood_group_spinner;
    private MainActivity activity;
    private ArrayList<String> countryAreaCodes;
    private Button addFamilyButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity= (MainActivity) getActivity();
        countryAreaCodes=parseCountryCodes();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_add_family_layout,container,false);
        day_spinner= (Spinner) view.findViewById(R.id.sp_add_family_day);
        month_spinner= (Spinner) view.findViewById(R.id.sp_add_family_month);
        year_spinner= (Spinner) view.findViewById(R.id.sp_add_family_year);
        gender_title_spinner= (Spinner) view.findViewById(R.id.sp_add_family_gender_title);
        countryCode_spinner= (Spinner) view.findViewById(R.id.sp_add_family_country_code);
        relationship_spinner= (Spinner) view.findViewById(R.id.sp_add_family_relationship);
        blood_group_spinner= (Spinner) view.findViewById(R.id.sp_add_family_blood_group);
        addFamilyButton= (Button) view.findViewById(R.id.bn_add_family_add);
        addFamilyButton.setOnClickListener(this);

        ArrayAdapter<String> bloodGroupAdapter=new ArrayAdapter<String>(getActivity(),R.layout.view_spinner_layout_item,getActivity().getResources().getStringArray(R.array.blood_group));
        bloodGroupAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        blood_group_spinner.setAdapter(bloodGroupAdapter);

        ArrayAdapter<String> realtionshipAdapter=new ArrayAdapter<String>(getActivity(),R.layout.view_spinner_layout_item,getActivity().getResources().getStringArray(R.array.relationShip));
        realtionshipAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        relationship_spinner.setAdapter(realtionshipAdapter);

        ArrayAdapter<String> countryCodeAdapter=new ArrayAdapter<String>(getActivity(),R.layout.view_spinner_layout_item,countryAreaCodes);
        countryCodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryCode_spinner.setAdapter(countryCodeAdapter);

        ArrayAdapter<String> genderTitleAdapter=new ArrayAdapter<String>(getActivity(),R.layout.view_spinner_layout_item,getActivity().getResources().getStringArray(R.array.gender_title));
        genderTitleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender_title_spinner.setAdapter(genderTitleAdapter);

        ArrayAdapter<String> dayAdapter=new ArrayAdapter<String>(getActivity(),R.layout.view_spinner_layout_item,getActivity().getResources().getStringArray(R.array.day));
        dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        day_spinner.setAdapter(dayAdapter);

        ArrayAdapter<String> monthAdapter=new ArrayAdapter<String>(getActivity(),R.layout.view_spinner_layout_item,getActivity().getResources().getStringArray(R.array.month));
        monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        month_spinner.setAdapter(monthAdapter);

        ArrayAdapter<String> yearAdapter=new ArrayAdapter<String>(getActivity(),R.layout.view_spinner_layout_item,getActivity().getResources().getStringArray(R.array.year));
        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        year_spinner.setAdapter(yearAdapter);

        return view;
    }

    public ArrayList<String> parseCountryCodes() {
        String response = "";
        ArrayList<String> list = new ArrayList<String>();
        try {
            response = ReadFile.readRawFileAsString(activity,
                    R.raw.countrycodes);

            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                list.add(object.getString("phone-code"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment= Const.AddFamilyFragment;
    }



    @Override
    public void onDestroyView() {

        FragmentManager manager=getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.AddFamilyFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("AddFamilyFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onDestroyView();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.bn_add_family_add:
                activity.addFragment(new UserSettingsFragment(),false,"",Const.UserSettingsFragment,true);
                activity.layoutToolbarOptions.setVisibility(View.VISIBLE);
                activity.backbuttonImage.setVisibility(View.GONE);
                break;
        }
    }
}
