package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.MyMedicalPagerAdapter;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by getit on 8/5/2016.
 */
public class MyMedicalFragment extends Fragment implements ViewPager.OnPageChangeListener
{

    private ViewPager myMedicalViewPager;
    private CircleIndicator myMedicalCircleIndicator;
    private MainActivity activity;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_my_medical_layout,container,false);
        activity= (MainActivity) getActivity();
        myMedicalViewPager= (ViewPager) view.findViewById(R.id.vp_my_medical_record);
        myMedicalCircleIndicator= (CircleIndicator) view.findViewById(R.id.ci_my_medical_record);
        myMedicalViewPager.addOnPageChangeListener(this);
        setWithPagerAdapter(myMedicalViewPager);
        return view;
    }

    private void setWithPagerAdapter(ViewPager pager)
    {
        MyMedicalPagerAdapter pagerAdapter=new MyMedicalPagerAdapter(getChildFragmentManager());
        pagerAdapter.addItem(new MyMedicalRecordFragment());
        pagerAdapter.addItem(new MyFriendlyDoctorFragment());
        pagerAdapter.addItem(new MyAllergiesFragment());
        pagerAdapter.addItem(new MyMedicationFragment());
        pager.setAdapter(pagerAdapter);
        myMedicalCircleIndicator.setViewPager(pager);

    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment=Const.MyMedicalFragmenmt;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager manager=getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.MyMedicalFragmenmt);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("MyMedicationFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position)
    {
        switch (position)
        {
            case 0:
                activity.currentFragment=Const.MyMedicalFragmenmt;
                break;
            case 1:
                activity.currentFragment=Const.MyFriendlyDoctorFragment;
                break;
            case 2:
                activity.currentFragment=Const.MyAllergiesFragment;
                break;
            case 3:
                activity.currentFragment=Const.MyMedicationFragment;
                break;
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


}
