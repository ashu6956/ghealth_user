package com.patient.ghealth.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.patient.ghealth.R;
import com.patient.ghealth.adapter.DoctorAvailabilityPagerAdapter;


/**
 * Created by user on 9/17/2016.
 */
public class DoctorAvailabilityDialogFragment extends DialogFragment {

    public DoctorAvailabilityDialogFragment()
    {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.dialog_book_doctor_availability_layout,container,false);
        ViewPager viewPager = (ViewPager)view.findViewById(R.id.vp_doctor_availability);
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tl_book_doctor_availability);
        ImageView cancelDialog = (ImageView) view.findViewById(R.id.iv_doctor_availability_cancel);
        setUpWithViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        cancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                getDialog().dismiss();
            }
        });
        getDialog().setCancelable(false);
        getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        return view;
    }

    private void setUpWithViewPager(ViewPager pager) {
        DoctorAvailabilityPagerAdapter pagerAdapter = new DoctorAvailabilityPagerAdapter(getChildFragmentManager());
        pagerAdapter.addItem(new TodayBookDoctorFragment(), getString(R.string.today));
        pagerAdapter.addItem(new TomorrowBookDoctorFragment(),getString(R.string.tomorrow));
        pagerAdapter.addItem(new CalendarBookDoctorFragment(),getString(R.string.calendar));
        pager.setAdapter(pagerAdapter);

    }
}

