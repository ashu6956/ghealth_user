package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

/**
 * Created by getit on 8/10/2016.
 */
public class MyMedicalRecordFragment extends Fragment
{

    private String pictureUrl;
    private MainActivity activity;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_my_medical_record,container,false);
        activity= (MainActivity) getActivity();
        ImageView patientIcon= (ImageView) view.findViewById(R.id.iv_my_medical_patient_icon);
        pictureUrl= (String) PreferenceHelper.getParam(getActivity(), Const.Params.PICTURE,"");
        if(pictureUrl!=""){
            Glide.with(getActivity()).load(pictureUrl).centerCrop().into(patientIcon);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment=Const.MyMedicalFragmenmt;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager manager=getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.MyMedicalRecordFragment);
        if (fragment != null) {
            AndyUtils.appLog("MyMedicalRecordFragment", "fragment");
            manager.beginTransaction().remove(fragment).commit();
        }
    }
}
