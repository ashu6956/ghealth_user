package com.patient.ghealth.fragment;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.InviteFriendAndFamilyAdapter;
import com.patient.ghealth.model.PhoneContactInfo;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 9/7/2016.
 */
public class InviteFriendAndFamilyFragment extends Fragment implements View.OnClickListener {


    private static final String shareUrl = "https://play.google.com/store/apps/details?id=com.patient.ghealth";
    private static final String iosUrl = "https://appsto.re/in/ABOrjb.i";

    private EditText contactAutoCompleteText, inviteMessage;
    private MainActivity activity;
    private List<PhoneContactInfo> contactList;
    private ListView phoneContactsListView;
    private InviteFriendAndFamilyAdapter friendAndFamilyAdapter;
    private Context mContext;
    private Button inviteButton;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_invite_friend_and_family, container, false);
        activity = (MainActivity) getActivity();
        phoneContactsListView = (ListView) view.findViewById(R.id.lv_invite_friend_family);
        contactAutoCompleteText = (EditText) view.findViewById(R.id.auto_text_search_contact);
        inviteMessage = (EditText) view.findViewById(R.id.et_invite_friend_type_message);
        inviteButton = (Button) view.findViewById(R.id.bn_invite_friend_family);
        inviteButton.setOnClickListener(this);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        new FetchContacts().execute();
    }


    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.InviteFriendFamilyFragment;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bn_invite_friend_family:

                if (InviteFriendAndFamilyAdapter.selectedNumberList != null && InviteFriendAndFamilyAdapter.selectedNumberList.size() > 0) {
                    if (inviteMessage.getText().toString().trim().length() != 0) {
                        sendMessageToFriendAndFamily(InviteFriendAndFamilyAdapter.selectedNumberList);

                        activity.addFragment(new UserSettingsFragment(), false, "", Const.UserSettingsFragment, true);
                        activity.layoutToolbarOptions.setVisibility(View.VISIBLE);
                        activity.backbuttonImage.setVisibility(View.GONE);
                        activity.appLogo.setVisibility(View.VISIBLE);
                        activity.mainTabLayout.setVisibility(View.VISIBLE);
                    } else {
                        AndyUtils.showShortToast(getString(R.string.please_enter_message), getActivity());
                    }
                }

                break;
        }
    }

    public List<PhoneContactInfo> readContacts() {
        StringBuffer sb = new StringBuffer();
//        sb.append("......Contact Details.....");

        contactList = new ArrayList<>();
        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
        String phone = null;

        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        PhoneContactInfo contactInfo = new PhoneContactInfo();
                        contactInfo.setContactName(name);
                        contactInfo.setContactNumber(phone);
                        contactInfo.isChecked = false;
                        contactList.add(contactInfo);
                    }
                    pCur.close();
                }

            }
        }

        return contactList;
    }

    private void sendMessageToFriendAndFamily(ArrayList<String> list) {
        StringBuilder numbersBuilder = new StringBuilder("");
        for (int i = 0; i < list.size(); i++) {
            numbersBuilder.append(list.get(i));
            if (i != list.size() - 1)
                numbersBuilder.append(";");
        }
        String numbers = "smsto:" + numbersBuilder.toString();
        AndyUtils.appLog("SelectedNumber", numbers);
        Intent smsIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(numbers));
        smsIntent.putExtra("sms_body", inviteMessage.getText().toString() + " " + "\nAndroid Link:\n" + shareUrl + "\n" + "IOS Link:\n" + iosUrl);
//        smsIntent.putExtra("sms_body", inviteMessage.getText().toString() + " " + iosUrl);
        startActivity(smsIntent);

//        Intent sendIntent = new Intent();
//        sendIntent.setAction(Intent.ACTION_SEND);
//        sendIntent.putExtra(Intent.EXTRA_TEXT,
//                "Hey check out my app at:" + shareUrl);
//        sendIntent.setType("text/plain");
//        startActivity(sendIntent);
    }

    @Override
    public void onDestroyView() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.InviteFriendFamilyFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("InviteFriendAndFamily", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onDestroyView();
    }

    class FetchContacts extends AsyncTask<Void, Void, List<PhoneContactInfo>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            AndyUtils.showSimpleProgressDialog(getActivity(), getString(R.string.please_wait), false);
        }

        @Override
        protected List<PhoneContactInfo> doInBackground(Void... params) {
            return readContacts();
        }

        @Override
        protected void onPostExecute(List<PhoneContactInfo> phoneContactInfos) {
            super.onPostExecute(phoneContactInfos);
            AndyUtils.appLog("Size of ContactMap", phoneContactInfos.size() + "");
            AndyUtils.removeProgressDialog();
            friendAndFamilyAdapter = new InviteFriendAndFamilyAdapter(getActivity(), phoneContactInfos);
            phoneContactsListView.setAdapter(friendAndFamilyAdapter);

            contactAutoCompleteText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String text = contactAutoCompleteText.getText().toString().toLowerCase(Locale.getDefault());
                    friendAndFamilyAdapter.filter(text);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }

    }
}

