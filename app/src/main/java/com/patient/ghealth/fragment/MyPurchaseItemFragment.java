package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.OrderListAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.Address;
import com.patient.ghealth.model.OrderDetail;
import com.patient.ghealth.model.ProductDetail;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 9/6/2016.
 */
public class MyPurchaseItemFragment extends Fragment implements AsyncTaskCompleteListener {

    private MainActivity activity;
    private RecyclerView orderRecyclerView;
    private TextView noOrderText;
    private ProgressBar orderProgressBar;
    private OrderListAdapter mOrderListAdapter;
    private List<OrderDetail> orderDetailList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        orderDetailList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_order_history_layout, container, false);
        activity = (MainActivity) getActivity();
        orderRecyclerView = (RecyclerView) view.findViewById(R.id.rv_order_list);
        noOrderText = (TextView) view.findViewById(R.id.tv_no_order_history);
        orderProgressBar = (ProgressBar) view.findViewById(R.id.order_history_progress);

        setupRecyclerView();

        if (orderDetailList.isEmpty())
            getOrdersHistory();
        return view;
    }


    private void setupRecyclerView() {
        mOrderListAdapter = new OrderListAdapter(getActivity(), orderDetailList, activity);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        orderRecyclerView.setLayoutManager(mLayoutManager);
        orderRecyclerView.setItemAnimator(new DefaultItemAnimator());
        orderRecyclerView.setAdapter(mOrderListAdapter);

    }

    private void getOrdersHistory() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        orderProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_ORDER_HISTORY_URL);
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "OrderHistoryMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_ORDER_HISTORY, this, headerMap);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.MyPurchaseItemFragment;

    }

    @Override
    public void onDestroyView() {
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.MyPurchaseItemFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("MyPurchaseItemFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onDestroyView();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_ORDER_HISTORY:
                orderProgressBar.setVisibility(View.GONE);
                AndyUtils.appLog("Ashutosh", "OrderHistoryResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        JSONArray responseArray = jsonObject.getJSONArray(Const.RESPONSE);
                        for (int i = 0; i < responseArray.length(); i++) {
                            JSONObject cartObject = responseArray.getJSONObject(i);
                            JSONArray productJsonArray = cartObject.getJSONArray("products");
                            for (int j = 0; j < productJsonArray.length(); j++) {
                                JSONObject productObject = productJsonArray.getJSONObject(j);
                                OrderDetail orderDetail = new OrderDetail();
                                JSONObject licenseObj = productObject.optJSONObject("license");
                                if (licenseObj != null)
                                    orderDetail.setLicense(licenseObj.optString("name"));
                                else
                                    orderDetail.setLicense(null);
                                orderDetail.setVariant(productObject.optString("variant"));
                                orderDetail.setDownload_token(productObject.optString("download_token"));
                                orderDetail.setQuantity(productObject.optInt(Const.QUANTITY));
                                orderDetail.setPrice(productObject.optString("price"));
                                orderDetail.setOrderId(cartObject.optString(Const.Params._ID));
                                // get product info
                                JSONObject idObject = productObject.getJSONObject("id");
                                ProductDetail productDetail = new ProductDetail();
                                productDetail.setProductName(idObject.optString(Const.Params.NAME));
                                productDetail.setProductId(idObject.optString(Const.Params._ID));
                                ArrayList<ProductDetail.Cat_image> images = new ArrayList<ProductDetail.Cat_image>();
                                JSONArray imageArray = idObject.getJSONArray("images");
                                for (int k = 0; k < imageArray.length(); k++) {
                                    JSONObject singleImageObj = imageArray.getJSONObject(k);
                                    JSONObject cdnObj = singleImageObj.getJSONObject("cdn");
                                    ProductDetail.Cat_image image = new ProductDetail.Cat_image();
                                    image.setImage_url(cdnObj.optString("url"));
                                    images.add(image);
                                }
                                productDetail.setCat_images(images);
                                // get product rating
                                ArrayList<ProductDetail.Reviews> reviewses = new ArrayList<ProductDetail.Reviews>();
                                JSONArray ratingArray = idObject.getJSONArray("ratings");
                                for (int k = 0; k < ratingArray.length(); k++) {
                                    JSONObject singleRatingObj = ratingArray.getJSONObject(k);
                                    ProductDetail.Reviews review = new ProductDetail.Reviews();
                                    review.setComment(singleRatingObj.optString("comment"));
                                    review.setStar(singleRatingObj.optDouble("stars"));
                                    reviewses.add(review);
                                }
                                productDetail.setReviewList(reviewses);

                                orderDetail.setProductDetail(productDetail);


                                // get address
                                Address address = new Address();
                                JSONObject addressObject = cartObject.getJSONObject("shipping");
                                address.setNAME(addressObject.optString(Const.Params.NAME));
                                address.setADDRESS(addressObject.optString("address"));
                                address.setNUMBER(addressObject.optString("phone"));
                                address.setCITY(addressObject.optString("city"));
                                address.setCOUNTRY(addressObject.optString("country"));
                                address.setSTATE(addressObject.optString("state"));
                                address.setPINCODE(addressObject.optString("pincode"));
                                orderDetail.setAddress(address);

                                // getting tracking info
                                JSONObject trackingObj = productObject.getJSONObject("tracking");
                                OrderDetail.Tracking tracking = new OrderDetail.Tracking();
                                tracking.setCompany(trackingObj.optString("company"));
                                tracking.setEstimated_delivery(trackingObj.optString("estimated_delivery"));
                                tracking.setStatus(trackingObj.optString("status"));
                                tracking.setTracking_number(trackingObj.optString("tracking_number"));
                                orderDetail.setTracking(tracking);

                                //getting shop info

                                JSONObject shopInfoObj = productObject.optJSONObject("shop_id");
                                if (null != shopInfoObj) {
                                    OrderDetail.ShopInfo shopInfo = new OrderDetail.ShopInfo();
                                    shopInfo.setId(shopInfoObj.optString(Const.Params._ID));
                                    shopInfo.setName(shopInfoObj.optString(Const.Params.NAME));
                                    shopInfo.setEmail(shopInfoObj.optString(Const.Params.EMAIL));
                                    shopInfo.setNumber(shopInfoObj.optString(Const.Params.PHONE));
                                    orderDetail.setShopInfo(shopInfo);
                                }

                                orderDetailList.add(orderDetail);

                            }
                        }
                        AndyUtils.appLog("OrderDetails Size", orderDetailList.size() + "");
                        mOrderListAdapter.notifyDataSetChanged();
                        if (orderDetailList.isEmpty())
                            noOrderText.setVisibility(View.VISIBLE);
                        else noOrderText.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }
}
