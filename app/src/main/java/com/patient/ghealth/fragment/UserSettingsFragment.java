package com.patient.ghealth.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.AddCardActivity;
import com.patient.ghealth.activity.LoginActivity;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.UserSettingsAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.UserSettings;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by getit on 8/5/2016.
 */
public class UserSettingsFragment extends Fragment implements AdapterView.OnItemClickListener, AsyncTaskCompleteListener {
    private ListView userSettingsListView;

    private MainActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_settings_layout, container, false);
        activity = (MainActivity) getActivity();
        userSettingsListView = (ListView) view.findViewById(R.id.lv_user_settings);
        UserSettingsAdapter settingsAdapter = new UserSettingsAdapter(getActivity(), getUserSettingsList());
        userSettingsListView.setAdapter(settingsAdapter);
        userSettingsListView.setOnItemClickListener(this);

        return view;
    }

    private List<UserSettings> getUserSettingsList() {
        List<UserSettings> userSettingsList = new ArrayList<>();
        userSettingsList.add(new UserSettings(R.drawable.menu_user, getString(R.string.my_profile)));
        userSettingsList.add(new UserSettings(R.drawable.my_schedule, getString(R.string.my_schedule)));
//        userSettingsList.add(new UserSettings(R.drawable.add_family, getString(R.string.add_family)));
        userSettingsList.add(new UserSettings(R.drawable.card, getString(R.string.add_card)));
        userSettingsList.add(new UserSettings(R.drawable.question, getString(R.string.questions)));
        userSettingsList.add(new UserSettings(R.drawable.invite_friend_family, getString(R.string.invite_friend_family)));
        userSettingsList.add(new UserSettings(R.drawable.menu_bookmark, getString(R.string.book_mark)));
        userSettingsList.add(new UserSettings(R.drawable.my_purchase_item, getString(R.string.my_purchase_item)));
        userSettingsList.add(new UserSettings(R.drawable.consult_history, getString(R.string.consult_history)));
//        userSettingsList.add(new UserSettings(R.drawable.menu_bookmark, getString(R.string.payment_history)));
        userSettingsList.add(new UserSettings(R.drawable.report_issues, getString(R.string.report_issue)));
        userSettingsList.add(new UserSettings(R.drawable.ic_lock, getString(R.string.change_password)));
        userSettingsList.add(new UserSettings(R.drawable.sign_out, getString(R.string.sign_out)));

        return userSettingsList;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.UserSettingsFragment;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        activity.appLogo.setVisibility(View.GONE);
        switch (position) {
            case 0:

                UserProfileFragment profileFragment = new UserProfileFragment();
                activity.addFragment(profileFragment, false, getString(R.string.my_profile), Const.UserProfileFragment, true);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                break;
            case 1:

                MyScheduleFragment scheduleFragment = new MyScheduleFragment();
                activity.addFragment(scheduleFragment, false, getString(R.string.my_schedule), Const.MyScheduleFragment, true);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                break;

            case 2:
                activity.appLogo.setVisibility(View.VISIBLE);
                Intent addCardIntent = new Intent(activity, AddCardActivity.class);
                startActivity(addCardIntent);
                break;
            case 3:
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                activity.addFragment(new NotificationAnswerFragment(), false, getString(R.string.questions), Const.NotificationAnswerFragment, true);
                break;
            case 4:

                activity.addFragment(new InviteFriendAndFamilyFragment(), false, getString(R.string.invite_friend_family), Const.AddFamilyFragment, true);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                break;
            case 5:
                activity.addFragment(new BookMarkFragment(), false, getString(R.string.book_mark), Const.BookMarkFragment, true);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                break;

            case 6:

                activity.addFragment(new MyPurchaseItemFragment(), false, getString(R.string.my_purchase_item), Const.MyPurchaseItemFragment, true);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                break;
            case 7:

                activity.addFragment(new ConsultHistoryFragment(), false, getString(R.string.consult_history), Const.ConsultHistoryFragment, true);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                break;
            case 8:
                activity.addFragment(new ReportIssueFragment(), false, getString(R.string.report_issue), Const.ReportIssueFragment, true);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                break;
            case 9:
                activity.addFragment(new ChangePasswordFragment(), false, getString(R.string.change_password), Const.ChangePassword, true);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                break;
            case 10:
                activity.appLogo.setVisibility(View.VISIBLE);
                showLogoutDialog();
                break;


        }

    }

    private void showLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.logout));
        String message = getString(R.string.logout_text);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                postLogout();
            }
        });

        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    private void postLogout() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.POST_LOGOUT_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(getContext(), Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(getContext(), Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("LogoutMap", map.toString());
        new HttpRequester(getContext(), Const.POST, map, Const.ServiceCode.POST_LOGOUT, this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.UserSettingsFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("UserSettingsFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode) {
            case Const.ServiceCode.POST_LOGOUT:
                AndyUtils.appLog("LogoutResponse", response.toString());
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        onDestroyView();
                        PreferenceHelper.setParam(getActivity(), Const.LOGIN_FIRST_TIME, false);
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                        activity.finish();
                    } else {
                        AndyUtils.showShortToast(jsonObject.optString("error_message"), getContext());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }
}
