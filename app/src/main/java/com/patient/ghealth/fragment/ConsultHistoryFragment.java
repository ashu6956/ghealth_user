package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.ConsultHistoryPagerAdapter;
import com.patient.ghealth.utils.Const;

/**
 * Created by user on 10/4/2016.
 */
public class ConsultHistoryFragment extends Fragment {
    public TabLayout consultHistoryTabLayout;
    private ViewPager consultHistoryViewPager;
    private MainActivity activity;
    private Bundle bundle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_consult_history_layout, container, false);
        activity = (MainActivity) getActivity();
        consultHistoryTabLayout = (TabLayout) view.findViewById(R.id.tl_consult_history);
        consultHistoryViewPager = (ViewPager) view.findViewById(R.id.vp_consult_history);
        setUpWithViewPager(consultHistoryViewPager);
        consultHistoryTabLayout.setupWithViewPager(consultHistoryViewPager);
        consultHistoryTabLayout.getTabAt(0).setIcon(R.drawable.phone);
        consultHistoryTabLayout.getTabAt(1).setIcon(R.drawable.message_processing);
        consultHistoryTabLayout.getTabAt(2).setIcon(R.drawable.video);

        try {
            bundle = getArguments();
            if (bundle != null && bundle.getString(Const.INCOMING_BUNDLE) != null) {
                String type = bundle.getString(Const.INCOMING_BUNDLE);
                if (type.equals(Const.ChatConsultHistoryFragment)) {
                    consultHistoryTabLayout.getTabAt(1).select();
                } else if (type.equals(Const.VideoConsultHistoryFragment)) {
                    consultHistoryTabLayout.getTabAt(2).select();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        consultHistoryTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                consultHistoryViewPager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.ConsultHistoryFragment;
    }

    private void setUpWithViewPager(ViewPager consultHistoryViewPager) {
        ConsultHistoryPagerAdapter pagerAdapter = new ConsultHistoryPagerAdapter(getChildFragmentManager());
        pagerAdapter.addItem(new PhoneConsultHistory());
        pagerAdapter.addItem(new ChatConsultHistoryFragment());
        pagerAdapter.addItem(new VideoConsultHistory());
        consultHistoryViewPager.setAdapter(pagerAdapter);
        consultHistoryViewPager.setOffscreenPageLimit(2);
    }


}
