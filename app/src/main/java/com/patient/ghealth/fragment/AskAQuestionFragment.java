package com.patient.ghealth.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.model.DoctorSpecialities;
import com.patient.ghealth.model.HealthFeedArticles;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.realmDB.RealmDBController;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.realm.Realm;

/**
 * Created by user on 9/6/2016.
 */
public class AskAQuestionFragment extends Fragment implements View.OnClickListener, AsyncTaskCompleteListener {
    private TextView header, description;
    private MainActivity activity;
    private Button submitButton;
    private AutoCompleteTextView autoCompleteTextView;
    private List<HealthFeedArticles> articles;
    private Bundle bundle;
    private ArrayList<String> articleStrings;
    private Spinner specialitiesSpinner, doctorSpinner;
    private ProgressBar askProgressBar;
    private ArrayList<String> stringArrayList, doctorNameList;
    private List<DoctorSpecialities> specialitiesList;
    private List<DoctorOnLineDetails> onLineDetailsList;
    private String doctorId = "", sContent = "", sSubject = "";
    private EditText subject, content;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fargment_ask_a_question_layout, container, false);
        activity = (MainActivity) getActivity();
        specialitiesSpinner = (Spinner) view.findViewById(R.id.specialities_spinner);
        doctorSpinner = (Spinner) view.findViewById(R.id.doctor_spinner);
        onLineDetailsList = new ArrayList<>();
        doctorNameList = new ArrayList<>();
//        header = (TextView) view.findViewById(R.id.tv_ask_a_question_header);
//        description = (TextView) view.findViewById(R.id.tv_ask_a_question_description);
        submitButton = (Button) view.findViewById(R.id.bn_ask_a_question_submit);
        askProgressBar = (ProgressBar) view.findViewById(R.id.ask_progressbar);
        content = (EditText) view.findViewById(R.id.et_ask_a_question_content);
        subject = (EditText) view.findViewById(R.id.et_ask_a_question_subject);
//        autoCompleteTextView = (AutoCompleteTextView) view.findViewById(R.id.auto_search_text);
        try {
            Realm realm = RealmDBController.with(getActivity()).getRealm();
            specialitiesList = realm.where(DoctorSpecialities.class).findAll();
            if (specialitiesList != null && specialitiesList.size() > 0) {
                stringArrayList = new ArrayList<>();
                for (int i = 0; i < specialitiesList.size(); i++) {
                    stringArrayList.add(specialitiesList.get(i).getDoctorSpeciality());
                }
                ArrayAdapter<String> sprcialitiesListAdapter = new ArrayAdapter<String>(getActivity(), R.layout.view_spinner_layout_item, stringArrayList);
                sprcialitiesListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                specialitiesSpinner.setAdapter(sprcialitiesListAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        specialitiesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                String specialityName=stringArrayList.get(position);
//                int specialityPosition=specialitiesList.indexOf(specialityName);
//                String specialistId=s
                String specialistId = specialitiesList.get(position).getDoctorId();
                AndyUtils.appLog("Ask A Question", "SpecialistId" + specialistId);
                getDoctorIdDetails(specialistId);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//            doctorSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                @Override
//                public void onItemClick(AdapterView<?> parent, View view, int position, long id)
//                {
//                    if(doctorNameList!=null && doctorNameList.size()>0) {
//                        doctorId = onLineDetailsList.get(position).getDoctorOnLineId();
//                        AndyUtils.appLog("Ask A question", "DoctorId" + doctorId);
//                    }
//                }
//
//            });


//        bundle = getArguments();
//        try {
//            if (bundle != null) {
//                articles = (List<HealthFeedArticles>) bundle.getSerializable(Const.AskAQuestionFragment);
//                if (articles != null && articles.size() > 0) {
//                    articleStrings = new ArrayList<>();
//                    for (int i = 0; i < articles.size(); i++) {
//                        articleStrings.add(articles.get(i).getTopicHeading());
//                    }
//
//                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, articleStrings);
////                    Getting the instance of AutoCompleteTextView
//
//                    autoCompleteTextView.setThreshold(1);//will start working from first character
//                    autoCompleteTextView.setAdapter(adapter);
//                    adapter.notifyDataSetChanged();
//                }
//
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
//            {
//                int index=articleStrings.indexOf(autoCompleteTextView.getText().toString().trim());
//                AndyUtils.appLog("ClickedPosition",index+"");
//                String articleId = articles.get(index).getArticleId();
//                AndyUtils.appLog("Ask a QuestionarticleId",articleId);
//                hideKeyboard();
//                getArticleDescription(articleId);
//
//            }
//        });

        submitButton.setOnClickListener(this);


        return view;
    }

    private void getDoctorIdDetails(String specilsId) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_DOCTOR_SPECIALITIES_ID_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.SPECIALITY_ID + "="
                + String.valueOf(specilsId));

        AndyUtils.appLog("Ashutosh", "GetDoctorIdDetailsMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_DOCTOR_SPECIALITIES_ID, this);

    }

    private void getArticleDescription(String articleId) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
//        AndyUtils.showSimpleProgressDialog(getActivity(), "Requesting description", false);
        askProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_ARTICLE_DESCRIPTION_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.ARTICLE_ID + "="
                + String.valueOf(articleId));

        AndyUtils.appLog("Ashutosh", "GetArticleDescriptionMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_ARTICLE_DESCRIPTION, this);


    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.AskAQuestionFragment;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bn_ask_a_question_submit:

                if (doctorNameList != null && doctorNameList.size() > 0) {
                    int position = doctorSpinner.getSelectedItemPosition();
                    doctorId = onLineDetailsList.get(position).getDoctorOnLineId();
                    AndyUtils.appLog("Ask A question", "DoctorId" + doctorId);
                }
                if (validateDetails()) {
                    sContent = content.getText().toString();
                    sSubject = subject.getText().toString();
                    createQuestionRequest(doctorId,sContent,sSubject);
                }

                break;
        }
    }

    private boolean validateDetails() {

        if (doctorId.equals("")) {
            AndyUtils.showShortToast(getString(R.string.no_doctor_available), getActivity());
            return false;
        } else if (subject.getText().toString().length() == 0) {
            AndyUtils.showShortToast(getString(R.string.please_enter_subject), getActivity());
            return false;
        } else if (content.getText().toString().length() == 0) {
            AndyUtils.showShortToast(getString(R.string.please_enter_content), getActivity());
            return false;
        } else {
            return true;
        }
    }

    private void createQuestionRequest(String id, String content, String subject) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        AndyUtils.showSimpleProgressDialog(getActivity(), getString(R.string.please_wait), false);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.REQUEST_ASK_QUESTION_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.DOCTOR_ID, String.valueOf(id));
        map.put(Const.Params.CONTENT, content);
        map.put(Const.Params.SUBJECT, subject);


        AndyUtils.appLog("Ashutosh", "CreateQuestionRequestMap" + map);

        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.REQUEST_ASK_QUESTION, this);
    }


    @Override
    public void onDestroyView() {
//        autoCompleteTextView.setText("");
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.AskAQuestionFragment);
        if (fragment != null) {
            AndyUtils.appLog("AskAQuestionFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onDestroyView();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        askProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_ARTICLE_DESCRIPTION:

                AndyUtils.appLog("Ashutosh", "GetArticleDescriptionResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        String heading = jsonObject.getString("heading");
                        String descriptionContent = jsonObject.getString("content");
                        String StrbookMarkArticleId = jsonObject.optString("article_id");
                        header.setText(heading);
                        description.setText(descriptionContent);
                    } else {
                        AndyUtils.showShortToast(jsonObject.getString("error_message"), getActivity());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_DOCTOR_SPECIALITIES_ID:
                AndyUtils.appLog("Ashutosh", "DoctorSpecialidIDResponse" + response);
                try {

                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        onLineDetailsList.clear();
                        doctorNameList.clear();
                        JSONArray jsonArray = jsonObject.optJSONArray("doctor_list");
                        if (jsonArray != null && jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject doctorObject = jsonArray.optJSONObject(i);
                                DoctorOnLineDetails doctorOnLineDetails = new DoctorOnLineDetails();
                                doctorOnLineDetails.setDoctorOnLineId(doctorObject.optString("id"));
                                doctorOnLineDetails.setDoctorName(doctorObject.optString("d_name"));
                                onLineDetailsList.add(doctorOnLineDetails);
                            }

                            for (int k = 0; k < onLineDetailsList.size(); k++) {
                                doctorNameList.add(onLineDetailsList.get(k).getDoctorName());
                            }

                        }
                        ArrayAdapter<String> doctorAdapter = new ArrayAdapter<String>(getActivity(), R.layout.view_spinner_layout_item, doctorNameList);
                        doctorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        doctorSpinner.setAdapter(doctorAdapter);
                        doctorAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.REQUEST_ASK_QUESTION:
                AndyUtils.removeProgressDialog();
                AndyUtils.appLog("Ashutosh", "AskAQuestionResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {

                        AndyUtils.showShortToast(getString(R.string.question_submitted_successfully), getActivity());
                        activity.layoutToolbarOptions.setVisibility(View.VISIBLE);
                        activity.backbuttonImage.setVisibility(View.GONE);
                        activity.mainTabLayout.setVisibility(View.VISIBLE);
                        activity.layoutSearchTopic.setVisibility(View.GONE);
                        activity.appLogo.setVisibility(View.VISIBLE);
                        activity.addFragment(new HealthFeedFragment(), false, "", Const.HealthFeedsFragment, true);
                    } else {
                        AndyUtils.showShortToast(jsonObject.optString("error_message"), getActivity());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }
    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
