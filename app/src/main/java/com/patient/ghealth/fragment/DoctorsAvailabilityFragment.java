package com.patient.ghealth.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.model.LatLng;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.DoctorListActivity;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.location.LocationHelper;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

/**
 * Created by getit on 8/6/2016.
 */
public class DoctorsAvailabilityFragment extends Fragment implements View.OnClickListener,LocationHelper.OnLocationReceived{

    private ImageView doctorListNext,doctorNearByNext,doctorOnDemandNext;
    private LinearLayout doctorListLayout,doctorNearByLayout,doctorOnDemandLayout;
    private LocationHelper locationHelper;

    private MainActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationHelper=new LocationHelper(getActivity());
        locationHelper.setLocationReceivedLister(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_doctors_availability_layout,container,false);
        activity= (MainActivity) getActivity();

        doctorListNext= (ImageView) view.findViewById(R.id.iv_doctor_list_next_icon);
        doctorNearByNext= (ImageView) view.findViewById(R.id.iv_doctor_nearby_next_icon);
        doctorOnDemandNext= (ImageView) view.findViewById(R.id.iv_doctor_on_demand_next_icon);
        doctorListLayout= (LinearLayout) view.findViewById(R.id.ll_doctor_list);
        doctorNearByLayout= (LinearLayout) view.findViewById(R.id.ll_doctor_nearBy);
        doctorOnDemandLayout= (LinearLayout) view.findViewById(R.id.ll_doctor_on_demand);
        doctorListLayout.setOnClickListener(this);
        doctorNearByLayout.setOnClickListener(this);
        doctorOnDemandLayout.setOnClickListener(this);
        doctorListNext.setOnClickListener(this);
        doctorNearByNext.setOnClickListener(this);
        doctorOnDemandNext.setOnClickListener(this);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity.currentFragment=Const.DoctorAvailabilityFragment;
        final LocationManager manager = (LocationManager)getActivity().getSystemService( Context.LOCATION_SERVICE );

        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
            buildAlertMessageNoGps();
        }

    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(getString(R.string.gps_disabled))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View v)
    {
        Intent doctorListIntent=new Intent(getActivity(), DoctorListActivity.class);
        switch (v.getId())
        {

            case R.id.iv_doctor_list_next_icon:
                AndyUtils.appLog("Clicked","fragment");
                doctorListIntent.putExtra(Const.DOCTOR_FRAGMENT_TYPE,Const.DOCTOR_LIST);
                startActivity(doctorListIntent);
//                activity.finish();
                break;
            case R.id.ll_doctor_list:
                AndyUtils.appLog("Clicked","fragment");
                doctorListIntent.putExtra(Const.DOCTOR_FRAGMENT_TYPE,Const.DOCTOR_LIST);
                startActivity(doctorListIntent);
//                activity.finish();
                break;
            case R.id.iv_doctor_nearby_next_icon:
                AndyUtils.appLog("Clicked","fragment");
                doctorListIntent.putExtra(Const.DOCTOR_FRAGMENT_TYPE,Const.DOCTOR_NEAR_BY);
                startActivity(doctorListIntent);
//                activity.finish();
                break;
            case R.id.ll_doctor_nearBy:
                AndyUtils.appLog("Clicked","fragment");
                doctorListIntent.putExtra(Const.DOCTOR_FRAGMENT_TYPE,Const.DOCTOR_NEAR_BY);
                startActivity(doctorListIntent);
//                activity.finish();
                break;
            case R.id.iv_doctor_on_demand_next_icon:
                activity.addFragment(new DoctorOnDemandFragment(),false,getString(R.string.doctor_ondemand),Const.DoctorOnDemandFragment,false);
                break;
            case R.id.ll_doctor_on_demand:
                activity.addFragment(new DoctorOnDemandFragment(),false,getString(R.string.doctor_ondemand),Const.DoctorOnDemandFragment,false);
                break;


        }

    }

    @Override
    public void onLocationReceived(LatLng latlong) {

    }

    @Override
    public void onLocationReceived(Location location) {

    }

    @Override
    public void onConntected(Bundle bundle) {

    }

    @Override
    public void onConntected(Location location)
    {
        if(location!=null)
        {
            try {
                LatLng currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                PreferenceHelper.saveObject(currentLatLng, getActivity(), Const.CURRENT_LATLNG);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }

        }

    }
}
