package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by user on 10/4/2016.
 */
public class ChangePasswordFragment extends Fragment implements AsyncTaskCompleteListener {
    private MainActivity activity;
    private boolean isclicked = false;
    private ImageButton vis_pass_one, vis_pass_two, vis_pass_three;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.change_password, container, false);
        activity = (MainActivity) getActivity();

        /*--------initialization widget --------*/
        vis_pass_one = (ImageButton) view.findViewById(R.id.vis_pass_one);
        vis_pass_two = (ImageButton) view.findViewById(R.id.vis_pass_two);
        vis_pass_three = (ImageButton) view.findViewById(R.id.vis_pass_three);

        final EditText old_pass, new_pass, re_type_pass;
        old_pass = (EditText) view.findViewById(R.id.et_user_profile_old_password);
        new_pass = (EditText) view.findViewById(R.id.et_user_profile_password);
        re_type_pass = (EditText) view.findViewById(R.id.et_user_profile_retype_password);
        Button update = (Button) view.findViewById(R.id.bn_user_profile_update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String old_pass_str = old_pass.getText().toString().trim();
                String new_pass_str = new_pass.getText().toString().trim();
                String re_type_pass_str = re_type_pass.getText().toString().trim();

                if (old_pass_str.length() == 0) {
                    AndyUtils.showLongToast(getString(R.string.enter_your_old_password), getActivity());
                } else if (new_pass_str.length() == 0) {
                    AndyUtils.showLongToast(getString(R.string.enter_your_new_password), getActivity());
                } else if (new_pass_str.length() < 8) {
                    AndyUtils.showLongToast(getString(R.string.password_eight_characters), getActivity());
                } else if (old_pass_str.equals(new_pass_str)) {
                    AndyUtils.showLongToast("New password cannot be same as the old password!", getActivity());
                } else if (re_type_pass_str.length() == 0) {
                    AndyUtils.showLongToast("Please enter Re-type Password.", getActivity());
                } else if (new_pass_str.equals(re_type_pass_str)) {
                    UpdatePassword(old_pass_str, new_pass_str);
                } else {
                    AndyUtils.showLongToast(getString(R.string.password_mismatch), getActivity());
                }
            }
        });

        /*--------showing password --------*/

        old_pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                vis_pass_one.setVisibility(View.VISIBLE);
                AndyUtils.appLog("LoginActivity", "onTextChanged");
                vis_pass_one.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isclicked == false) {
                            AndyUtils.appLog("LoginActivitytrue", "onTextChanged");
                            old_pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            isclicked = true;
                            old_pass.setSelection(old_pass.getText().length());
                            vis_pass_one.setVisibility(View.VISIBLE);

                        } else {
                            AndyUtils.appLog("LoginActivityfalse", "onTextChanged");
                            isclicked = false;
                            old_pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            old_pass.setSelection(old_pass.getText().length());
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        new_pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                vis_pass_two.setVisibility(View.VISIBLE);
                AndyUtils.appLog("LoginActivity", "onTextChanged");
                vis_pass_two.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isclicked == false) {
                            AndyUtils.appLog("LoginActivitytrue", "onTextChanged");
                            new_pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            isclicked = true;
                            new_pass.setSelection(new_pass.getText().length());
                            vis_pass_two.setVisibility(View.VISIBLE);

                        } else {
                            AndyUtils.appLog("LoginActivityfalse", "onTextChanged");
                            isclicked = false;
                            new_pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            new_pass.setSelection(new_pass.getText().length());
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        re_type_pass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                vis_pass_three.setVisibility(View.VISIBLE);
                AndyUtils.appLog("LoginActivity", "onTextChanged");
                vis_pass_three.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isclicked == false) {
                            AndyUtils.appLog("LoginActivitytrue", "onTextChanged");
                            re_type_pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            isclicked = true;
                            re_type_pass.setSelection(re_type_pass.getText().length());
                            vis_pass_three.setVisibility(View.VISIBLE);

                        } else {
                            AndyUtils.appLog("LoginActivityfalse", "onTextChanged");
                            isclicked = false;
                            re_type_pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                            re_type_pass.setSelection(re_type_pass.getText().length());
                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    private void UpdatePassword(String old_pass, String new_pass) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showShortToast(getString(R.string.no_internet), getActivity());
            return;
        }
        AndyUtils.showSimpleProgressDialog(getActivity(), getString(R.string.please_wait), false);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.CHANGE_PASSWORD_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.OLD_PASSWORD, old_pass);
        map.put(Const.Params.NEW_PASSWORD, new_pass);

        AndyUtils.appLog("Change_password", "Change_Password_Fragment" + map);

        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.CHANGE_PASSWORD, this);

    }


    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.ChangePassword;
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        AndyUtils.removeProgressDialog();

        switch (serviceCode) {
            case Const.ServiceCode.CHANGE_PASSWORD:
                AndyUtils.appLog("Ashutosh", "CreateConsultHistoryResponse" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        AndyUtils.showLongToast(jsonObject.optString("message"), getActivity());
                        activity.addFragment(new UserSettingsFragment(), false, "", Const.UserSettingsFragment, true);
                        activity.appLogo.setVisibility(View.VISIBLE);
                        activity.mainTabLayout.setVisibility(View.VISIBLE);
                        activity.layoutToolbarOptions.setVisibility(View.VISIBLE);
                        activity.backbuttonImage.setVisibility(View.GONE);
                    } else {
                        AndyUtils.showShortToast(jsonObject.getString("error_message"), getActivity());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


        }
    }

}



