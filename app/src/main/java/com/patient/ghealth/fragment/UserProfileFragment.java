package com.patient.ghealth.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.PlacesAutoCompleteAdapter;
import com.patient.ghealth.adapter.SpinnerAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.networking.MultiPartRequester;
import com.patient.ghealth.realmDB.PatientProfile;
import com.patient.ghealth.realmDB.RealmDBController;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;
import com.patient.ghealth.utils.ReadFile;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.realm.Realm;

/**
 * Created by user on 8/17/2016.
 */
public class UserProfileFragment extends Fragment implements View.OnClickListener, AsyncTaskCompleteListener {
    private MainActivity activity;
    private Spinner day_spinner, month_spinner, year_spinner, gender_title_spinner, currency_spinner, language_spinner, countryCode_spinner, nationalitySpinner;
    private String languagesType[] = {"English(US)", "English(UK)"};
    private RadioButton maleRadioButton, femaleRadioButton;
    private CheckBox same_as_billing_address;
    private EditText firstNameEdit, billingLandMark, billingBlockNo, shippingLandMark, shippingBlockNo, lastNameEdit, emailIdEdit, oldPasswordEdit, createPasswordEdit, reTypePasswordEdit, mobile_numberEdit, et_register_otp;
    private Button updatePatientProfile, editPatientProfile, bn_request_otp;
    private ArrayList<String> countryAreaCodes;
    private RadioGroup genderRadioGroup;
    private boolean same_as_billing = false;
    private ImageView patientIcon;
    private String sGender = "", sDob, sDaySpinners, sMonthSpinner, sCurrencySpinner, sLanguageSpinner, sYearSpinner, sGenderTitleSpinner, sFirstName, sLastName, sEmailId, sCreatePassword, sReTypePassword, sMobile_number,
            sOldPassword, sBillingAddress = "", sShippingAddress = "", sCountryCode, sShippingLandMark, sShippingBlockNo, sBillingLandMark, sBillingBlockNo;
    private AQuery aQuery;
    private String filePath = "";
    private Uri uri;
    private PatientProfile patientProfile;
    private AutoCompleteTextView billingAddress, shippingAddress;
    private ArrayList<String> countryCodes, countryCodesIso;
    private ImageButton passwordView, oldPasswordView, reTypePasswordView;
    private boolean isOldClicked = false, isCreateClicked = false, isRetypeClicked = false;
    private LinearLayout oldPasswordLayout, createPasswordLayout, reTypePasswordLayout, shippingBillingAddressLayout;
    private File cameraFile;
    private LatLng shippingLatLng, billingLatLng;
    private String[] languages = {"en", "zh", "th", "in", "vi", "my", "fa", "ja"};
    private int selectedLanguagePosition = -1;
    private String selectedLanguage = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
        countryCodes = new ArrayList<>();
        countryCodesIso = new ArrayList<>();
        countryAreaCodes = parseCountryCodes();
        aQuery = new AQuery(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_user_profile_layout, container, false);
        oldPasswordLayout = (LinearLayout) view.findViewById(R.id.ll_old_password);
        createPasswordLayout = (LinearLayout) view.findViewById(R.id.ll_create_password);
        reTypePasswordLayout = (LinearLayout) view.findViewById(R.id.ll_reType_password);
        shippingBillingAddressLayout = (LinearLayout) view.findViewById(R.id.ll_same_as_billing_address);
        billingAddress = (AutoCompleteTextView) view.findViewById(R.id.et_user_profile_billing_address);
        shippingAddress = (AutoCompleteTextView) view.findViewById(R.id.et_user_profile_shipping_address);

        getAllViews(view);
        ArrayAdapter<String> nationalityAdapter = new ArrayAdapter<String>(getActivity(), R.layout.view_spinner_layout_item, new AndyUtils().parseNationality(getActivity()));
        nationalityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nationalitySpinner.setAdapter(nationalityAdapter);

        ArrayAdapter<String> countryCodeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.view_spinner_layout_item, countryAreaCodes);
        countryCodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryCode_spinner.setAdapter(countryCodeAdapter);

        ArrayAdapter<String> genderTitleAdapter = new ArrayAdapter<String>(getActivity(), R.layout.view_spinner_layout_item, getActivity().getResources().getStringArray(R.array.gender_title));
        genderTitleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender_title_spinner.setAdapter(genderTitleAdapter);

        ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.view_spinner_layout_item, getActivity().getResources().getStringArray(R.array.day));
        dayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        day_spinner.setAdapter(dayAdapter);

        ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(getActivity(), R.layout.view_spinner_layout_item, getActivity().getResources().getStringArray(R.array.month_number));
        monthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        month_spinner.setAdapter(monthAdapter);

        ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(getActivity(), R.layout.view_spinner_layout_item, getYearList());
        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        year_spinner.setAdapter(yearAdapter);


        Integer[] language_imageArray = {
                R.drawable.us, R.drawable.china, R.drawable.ic_thailand, R.drawable.indonesia,
                R.drawable.ic_vietnam, R.drawable.ic_myanmar, R.drawable.ic_iran, R.drawable.ic_japan};

        Integer[] currency_imageArray = {R.drawable.my, R.drawable.us, R.drawable.china, R.drawable.singapore,
                R.drawable.indonesia, R.drawable.ic_vietnam, R.drawable.ic_myanmar, R.drawable.ic_iran,
                R.drawable.ic_japan, R.drawable.ic_thailand};
        SpinnerAdapter currencyAdapter = new SpinnerAdapter(getActivity(), R.layout.spinner_value_layout, getResources().getStringArray(R.array.currency), currency_imageArray);
        currencyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currency_spinner.setAdapter(currencyAdapter);

        SpinnerAdapter languageAdapter = new SpinnerAdapter(getActivity(), R.layout.spinner_value_layout, getResources().getStringArray(R.array.language), language_imageArray);
        languageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        language_spinner.setAdapter(languageAdapter);

        genderRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_user_profile_male) {
                    sGender = "male";
                } else if (checkedId == R.id.rb_user_profile_female) {
                    sGender = "female";
                }
            }
        });

        same_as_billing_address.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    getAllPatientDetails();
                    shippingAddress.setText(sBillingAddress);
                    shippingLandMark.setText(sBillingLandMark);
                    shippingBlockNo.setText(sBillingBlockNo);
                    same_as_billing = true;
                } else {
                    shippingAddress.setText("");
                    shippingLandMark.setText("");
                    shippingBlockNo.setText("");
                    same_as_billing = false;
                }
            }
        });

        setAllViews();
        setAllViewsDisabled();
        return view;
    }

    private ArrayList<String> getYearList() {


        ArrayList<String> yearslst = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = 1900; i <= (thisYear - 17); i++) {
            yearslst.add(Integer.toString(i));
        }

        return yearslst;
    }

    private ArrayList<String> getDayList() {

        ArrayList<String> dayList = new ArrayList<String>();
        for (int i = 1; i <= 31; i++) {
            dayList.add(Integer.toString(i));
        }
        return dayList;
    }


    private ArrayList<String> getFebDayList() {

        ArrayList<String> dayList = new ArrayList<String>();
        for (int i = 1; i <= 29; i++) {
            dayList.add(Integer.toString(i));
        }
        return dayList;
    }


    private void getAllViews(View v) {
        patientIcon = (ImageView) v.findViewById(R.id.iv_update_user_icon);
        day_spinner = (Spinner) v.findViewById(R.id.sp_user_profile_day);
        billingLandMark = (EditText) v.findViewById(R.id.et_userProfile_billing_landmark);
        shippingLandMark = (EditText) v.findViewById(R.id.et_userProfile_shipping_landmark);
        billingBlockNo = (EditText) v.findViewById(R.id.et_userProfile_billing_block_floor_no);
        shippingBlockNo = (EditText) v.findViewById(R.id.et_userProfile_shipping_block_floor_no);
        month_spinner = (Spinner) v.findViewById(R.id.sp_user_profile_month);
        year_spinner = (Spinner) v.findViewById(R.id.sp_user_profile_year);
        nationalitySpinner = (Spinner) v.findViewById(R.id.sp_user_profile_nationality);
        gender_title_spinner = (Spinner) v.findViewById(R.id.sp_user_profile_gender_title);
        countryCode_spinner = (Spinner) v.findViewById(R.id.sp_user_profile_country_code);
        language_spinner = (Spinner) v.findViewById(R.id.sp_user_profile_language);
        currency_spinner = (Spinner) v.findViewById(R.id.sp_user_profile_currency);
        genderRadioGroup = (RadioGroup) v.findViewById(R.id.rg_user_profile);
        same_as_billing_address = (CheckBox) v.findViewById(R.id.cb_same_as_billingAddress);
        firstNameEdit = (EditText) v.findViewById(R.id.et_user_profile_first_name);
        lastNameEdit = (EditText) v.findViewById(R.id.et_user_profile_last_name);
        emailIdEdit = (EditText) v.findViewById(R.id.et_user_profile_email);
        emailIdEdit.setEnabled(false);
        oldPasswordView = (ImageButton) v.findViewById(R.id.ib_old_password_vis_pass);
        passwordView = (ImageButton) v.findViewById(R.id.ib_create_password_vis_pass);
        reTypePasswordView = (ImageButton) v.findViewById(R.id.ib_reType_password_vis_pass);
        createPasswordEdit = (EditText) v.findViewById(R.id.et_user_profile_password);
        reTypePasswordEdit = (EditText) v.findViewById(R.id.et_user_profile_retype_password);
        et_register_otp = (EditText) v.findViewById(R.id.et_register_otp);
        et_register_otp.setVisibility(View.GONE);
        mobile_numberEdit = (EditText) v.findViewById(R.id.et_user_profile_mobile_number);
        oldPasswordEdit = (EditText) v.findViewById(R.id.et_user_profile_old_password);
        editPatientProfile = (Button) v.findViewById(R.id.bn_user_profile_edit);
        updatePatientProfile = (Button) v.findViewById(R.id.bn_user_profile_update);
        bn_request_otp = (Button) v.findViewById(R.id.bn_request_otp);
        bn_request_otp.setVisibility(View.GONE);
        bn_request_otp.setOnClickListener(this);
        maleRadioButton = (RadioButton) v.findViewById(R.id.rb_user_profile_male);
        femaleRadioButton = (RadioButton) v.findViewById(R.id.rb_user_profile_female);
        updatePatientProfile.setOnClickListener(this);
        editPatientProfile.setOnClickListener(this);
        patientIcon.setOnClickListener(this);

        oldPasswordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                oldPasswordView.setVisibility(View.VISIBLE);
                AndyUtils.appLog("RegisterActivity", "onTextChanged");
                oldPasswordView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isOldClicked == false) {
                            AndyUtils.appLog("UserProfile", "onTextChanged");
                            oldPasswordEdit.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            isOldClicked = true;
                            oldPasswordEdit.setSelection(oldPasswordEdit.getText().length());
                            oldPasswordView.setVisibility(View.VISIBLE);

                        } else {
                            AndyUtils.appLog("RegisterActivityfalse", "onTextChanged");
                            isOldClicked = false;
                            oldPasswordEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                            vis_pass.setVisibility(View.GONE);
                            oldPasswordEdit.setSelection(oldPasswordEdit.getText().length());

                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }


        });
        createPasswordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordView.setVisibility(View.VISIBLE);
                AndyUtils.appLog("RegisterActivity", "onTextChanged");
                passwordView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isCreateClicked == false) {
                            AndyUtils.appLog("UserProfile", "onTextChanged");
                            createPasswordEdit.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            isCreateClicked = true;
                            createPasswordEdit.setSelection(createPasswordEdit.getText().length());
                            passwordView.setVisibility(View.VISIBLE);

                        } else {
                            AndyUtils.appLog("RegisterActivityfalse", "onTextChanged");
                            isCreateClicked = false;
                            createPasswordEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                            vis_pass.setVisibility(View.GONE);
                            createPasswordEdit.setSelection(createPasswordEdit.getText().length());

                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }


        });
        reTypePasswordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                reTypePasswordView.setVisibility(View.VISIBLE);
                AndyUtils.appLog("RegisterActivity", "onTextChanged");
                reTypePasswordView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isRetypeClicked == false) {
                            AndyUtils.appLog("UserProfile", "onTextChanged");
                            reTypePasswordEdit.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                            isRetypeClicked = true;
                            reTypePasswordEdit.setSelection(reTypePasswordEdit.getText().length());
                            reTypePasswordView.setVisibility(View.VISIBLE);

                        } else {
                            AndyUtils.appLog("RegisterActivityfalse", "onTextChanged");
                            isRetypeClicked = false;
                            reTypePasswordEdit.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                            vis_pass.setVisibility(View.GONE);
                            reTypePasswordEdit.setSelection(reTypePasswordEdit.getText().length());

                        }
                    }
                });
            }

            @Override
            public void afterTextChanged(Editable s) {

            }


        });

    }

    private void showPictureDialog() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle(getString(R.string.choose_your_option));
        String[] items = {getActivity().getResources().getString(R.string.gallery), getResources().getString(R.string.camera)};

        dialog.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                switch (which) {
                    case 0:
                        choosePhotoFromGallary();
                        break;
                    case 1:
                        takePhotoFromCamera();
                        break;

                }
            }
        });
        dialog.show();
    }

    private void choosePhotoFromGallary() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, Const.CHOOSE_PHOTO);

    }

    private void takePhotoFromCamera() {
        Calendar cal = Calendar.getInstance();
        cameraFile = new File(Environment.getExternalStorageDirectory(),
                (cal.getTimeInMillis() + ".jpg"));
        if (!cameraFile.exists()) {
            try {
                cameraFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {

            cameraFile.delete();
            try {
                cameraFile.createNewFile();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        uri = Uri.fromFile(cameraFile);
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(i, Const.TAKE_PHOTO
        );
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getActivity().getContentResolver().query(contentURI, null,
                null, null, null);

        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor
                    .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }


    private void beginCrop(Uri source) {
        // Uri outputUri = Uri.fromFile(new File(registerActivity.getCacheDir(),
        // "cropped"));
        AndyUtils.appLog("ProfileFragment", "beginCrop");
        Uri outputUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), (Calendar.getInstance()
                .getTimeInMillis() + ".jpg")));
        Crop.of(source, outputUri).asSquare().start(getActivity(), this);
    }

    private void handleCrop(int resultCode, Intent result) {
        AndyUtils.appLog("UserProfile", "handleCrop");
        if (resultCode == getActivity().RESULT_OK) {
            filePath = getRealPathFromURI(Crop.getOutput(result));
//            patientIcon.setImageURI(Crop.getOutput(result));
            Glide.with(getActivity()).load(Crop.getOutput(result)).into(patientIcon);
            AndyUtils.appLog("UserProfile", filePath.toString());
        } else if (resultCode == Crop.RESULT_ERROR) {
            AndyUtils.showLongToast(Crop.getError(result).getMessage(), getActivity());
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Activity Res", "" + requestCode);
        switch (requestCode) {
            case Const.CHOOSE_PHOTO:
                if (data != null) {

                    uri = data.getData();
                    if (uri != null) {

                        beginCrop(uri);
                    } else {
                        AndyUtils.showLongToast(getString(R.string.unable_to_select_image), getActivity());
                    }
                }
                break;
            case Const.TAKE_PHOTO:
                if (uri != null) {

                    beginCrop(uri);


                } else {
                    AndyUtils.showLongToast(getString(R.string.unable_to_select_image), getActivity());
                }
                break;
            case Crop.REQUEST_CROP:

                if (data != null)
                    handleCrop(resultCode, data);

                break;
        }
    }


    private void setAllViews() {
        Realm realm = RealmDBController.with(getActivity()).getRealm();
        patientProfile = realm.where(PatientProfile.class).equalTo("patientId", Integer.parseInt((String) PreferenceHelper.getParam(getActivity(), Const.Params.ID, ""))).findFirst();
        if (!patientProfile.getGenderTitle().equals("")) {

            String[] genderTitle = getResources().getStringArray(R.array.gender_title);
            AndyUtils.appLog("genderTitle", genderTitle[0] + " " + genderTitle[1]);
            AndyUtils.appLog("genderTitle", genderTitle[0] + " " + genderTitle[1]);
            List<String> list = new ArrayList<>(Arrays.asList(genderTitle));
            AndyUtils.appLog("listSize", list.size() + "");
            int genderPosition = list.indexOf(patientProfile.getGenderTitle());
            AndyUtils.appLog("GenderPosition", genderPosition + "");
            gender_title_spinner.setSelection(genderPosition);
        }
        if (patientProfile.getPictureUrl().equals("")) {
            patientIcon.setImageResource(R.drawable.profile);
        } else {
            Glide.with(getActivity()).load(patientProfile.getPictureUrl()).into(patientIcon);
        }
        if (!patientProfile.getFirstName().equals("")) {
            firstNameEdit.setText(patientProfile.getFirstName());
        }

        if (!patientProfile.getLastName().equals("")) {
            lastNameEdit.setText(patientProfile.getLastName());
        }
        if (patientProfile.getEmailId() != null) {
            emailIdEdit.setText(patientProfile.getEmailId());
        }
        if (!patientProfile.getDob().equals("") && patientProfile.getDob() != null) {
            String[] dob = patientProfile.getDob().split("-");
            String day = dob[0];
            String month = dob[1];
            String year = dob[2];
            AndyUtils.appLog("Dob", day + " " + month + " " + year);
            String[] dayArray = getResources().getStringArray(R.array.day);
            List<String> dayList = new ArrayList<>(Arrays.asList(dayArray));
            AndyUtils.appLog("DayListSize", dayList.size() + "");
            int dayPosition = dayList.indexOf(day);
            day_spinner.setSelection(dayPosition);

            String[] monthArray = getResources().getStringArray(R.array.month_number);
            List<String> monthList = new ArrayList<>(Arrays.asList(monthArray));
            int monthPosition = monthList.indexOf(month);

            AndyUtils.appLog("MonthListSize", monthList.size() + "" + month + " " + monthPosition);
            month_spinner.setSelection(monthPosition);

            int yearPosition = getYearList().indexOf(year);
            year_spinner.setSelection(yearPosition);
        }
        if (!patientProfile.getCountryCode().equals("")) {
            AndyUtils.appLog("CountryCode", patientProfile.getCountryCode());
            ArrayList<String> countryCodeList = countryCodes;
            int codePosition = parseCountryCodes().indexOf(patientProfile.getCountryCode());
            AndyUtils.appLog("CountryCodePosition", codePosition + "");
            countryCode_spinner.setSelection(codePosition);
        }
        if (patientProfile.getMobileNumber() != null) {
            mobile_numberEdit.setText(patientProfile.getMobileNumber());
        }
        if (!patientProfile.getNationality().equals("")) {

            String nationality = patientProfile.getNationality();
            ArrayAdapter nationAdap = (ArrayAdapter) nationalitySpinner.getAdapter();
            int nationPosition = nationAdap.getPosition(nationality);
            nationalitySpinner.setSelection(nationPosition);
        }
        if (!patientProfile.getGender().equals("")) {
            String[] genderTitleArray = getResources().getStringArray(R.array.gender_title);
            if (patientProfile.getGender().equals("male")) {
                maleRadioButton.setChecked(true);
                gender_title_spinner.setSelection(0);


            } else if (patientProfile.getGender().equals("female")) {
                femaleRadioButton.setChecked(true);
                gender_title_spinner.setSelection(1);
            }
        }
        if (!patientProfile.getBillingAddress().equals("")) {
            billingAddress.setText(patientProfile.getBillingAddress());
        }
        if (!patientProfile.getShippingAddress().equals("")) {
            shippingAddress.setText(patientProfile.getShippingAddress());
        }

        if (!patientProfile.getBillingLandMark().equals("")) {
            billingLandMark.setText(patientProfile.getBillingLandMark());
        }

        if (!patientProfile.getShippingLandMark().equals("")) {
            shippingLandMark.setText(patientProfile.getShippingLandMark());
        }
        if (!patientProfile.getBillingBlockNo().equals("")) {
            billingBlockNo.setText(patientProfile.getBillingBlockNo());
        }
        if (!patientProfile.getShippingBlockNo().equals("")) {
            shippingBlockNo.setText(patientProfile.getShippingBlockNo());
        }

        if (!patientProfile.getCurrency().equals("")) {
            String[] currency = getResources().getStringArray(R.array.currency);
            List<String> currencyList = new ArrayList<>(Arrays.asList(currency));
            int currencyPosition = currencyList.indexOf(patientProfile.getCurrency());
            currency_spinner.setSelection(currencyPosition);
        }
        if (!patientProfile.getLanguage().equals("")) {
            String[] language = getResources().getStringArray(R.array.language);
            List<String> languageList = new ArrayList<>(Arrays.asList(language));
            int languagePosition = languageList.indexOf(patientProfile.getLanguage());
            language_spinner.setSelection(languagePosition);
            language_spinner.setSelection(languagePosition, false);
        }
        language_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                AndyUtils.appLog("Ashutosh", "Selectedlanguage" + " " + languages[position]);
                selectedLanguage = languages[position];

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final PlacesAutoCompleteAdapter autoCompleteAdapter = new PlacesAutoCompleteAdapter(getActivity(), R.layout.autocomplete_list_text);
        shippingAddress.setAdapter(autoCompleteAdapter);
        billingAddress.setAdapter(autoCompleteAdapter);
        autoCompleteAdapter.notifyDataSetChanged();

        shippingAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideKeyboard();
                if (!AndyUtils.isNetworkAvailable(getActivity())) {
                    AndyUtils.showShortToast(getResources().getString(R.string.no_internet), getActivity());
                    return;
                }
                sShippingAddress = autoCompleteAdapter.getItem(position);
                if (shippingAddress != null) {
//                    PreferenceHelper.setParam(mContext,Const.PICK_ME_ADDRESS,pickMeAddress);
                }
                Handler handler = new Handler();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        shippingLatLng = getLocationFromAddress(getActivity(), sShippingAddress);
                        if (shippingLatLng != null) {
//                            PreferenceHelper.saveObject(sourceLatLng,mContext,Const.SOURCE_LATLNG);
                        } else {
                            AndyUtils.showShortToast(getString(R.string.address_invalid), getActivity());
                        }
                    }
                });

            }
        });

        billingAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideKeyboard();

                if (!AndyUtils.isNetworkAvailable(getActivity())) {
                    AndyUtils.showShortToast(getResources().getString(R.string.no_internet), getActivity());
                    return;
                }
                sBillingAddress = autoCompleteAdapter.getItem(position);
                if (sBillingAddress != null) {
//                    PreferenceHelper.setParam(mContext,Const.DROP_ME_ADDRESS,dropMeAddress);
                }
                Handler handler = new Handler();
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        billingLatLng = getLocationFromAddress(getActivity(), sBillingAddress);
                        if (billingLatLng != null) {
//                            PreferenceHelper.saveObject(destLatLng, mContext, Const.DESTINATION_LATLNG);
                        } else {
                            AndyUtils.showShortToast(getString(R.string.address_invalid), getActivity());
                        }
                    }
                });

            }

        });

    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());


            AndyUtils.appLog("LatLng", String.valueOf(p1));

        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return p1;
    }


    private void setAllViewsDisabled() {
        firstNameEdit.setEnabled(false);
        lastNameEdit.setEnabled(false);
        emailIdEdit.setEnabled(false);
        mobile_numberEdit.setEnabled(false);
        oldPasswordEdit.setEnabled(false);
        createPasswordEdit.setEnabled(false);
        reTypePasswordEdit.setEnabled(false);
        billingAddress.setEnabled(false);
        maleRadioButton.setEnabled(false);
        femaleRadioButton.setEnabled(false);
        shippingAddress.setEnabled(false);
        day_spinner.setEnabled(false);
        month_spinner.setEnabled(false);
        year_spinner.setEnabled(false);
        gender_title_spinner.setEnabled(false);
        countryCode_spinner.setEnabled(false);
        same_as_billing_address.setEnabled(false);
        patientIcon.setEnabled(false);
        nationalitySpinner.setEnabled(false);
        billingLandMark.setEnabled(false);
        billingBlockNo.setEnabled(false);
        createPasswordLayout.setVisibility(View.GONE);
        oldPasswordLayout.setVisibility(View.GONE);
        reTypePasswordLayout.setVisibility(View.GONE);
        language_spinner.setEnabled(false);
        currency_spinner.setEnabled(false);

    }

    private void getAllPatientDetails() {
        sFirstName = firstNameEdit.getText().toString().trim();
        sLastName = lastNameEdit.getText().toString().trim();
        sEmailId = emailIdEdit.getText().toString().trim();
        sMobile_number = mobile_numberEdit.getText().toString().trim();
        sCreatePassword = createPasswordEdit.getText().toString().trim();
        sOldPassword = oldPasswordEdit.getText().toString().trim();
        sReTypePassword = reTypePasswordEdit.getText().toString().trim();
        sBillingAddress = billingAddress.getText().toString();
        sShippingAddress = shippingAddress.getText().toString();
        sDaySpinners = day_spinner.getSelectedItem().toString();
        sMonthSpinner = month_spinner.getSelectedItem().toString();
        sYearSpinner = year_spinner.getSelectedItem().toString();
        sGenderTitleSpinner = gender_title_spinner.getSelectedItem().toString();
        sDob = sDaySpinners + "-" + sMonthSpinner + "-" + sYearSpinner;
        sCountryCode = countryCode_spinner.getSelectedItem().toString();
        sShippingBlockNo = shippingBlockNo.getText().toString();
        sBillingBlockNo = billingBlockNo.getText().toString();
        sShippingLandMark = shippingLandMark.getText().toString();
        sBillingLandMark = billingLandMark.getText().toString();
        sCurrencySpinner = currency_spinner.getSelectedItem().toString();
        sLanguageSpinner = language_spinner.getSelectedItem().toString();
        selectedLanguagePosition = language_spinner.getSelectedItemPosition();
    }

    private boolean validatePassword() {
        if (sCreatePassword.length() >= 1) {
            if (sOldPassword.length() == 0) {
                oldPasswordEdit.requestFocus();
                oldPasswordEdit.setError(getString(R.string.please_enter_old_password));
                return false;
            } else if (sCreatePassword.length() < 8) {
                createPasswordEdit.requestFocus();
                createPasswordEdit.setError(getString(R.string.password_must_characters));
                return false;
            } else if (sCreatePassword.equals(sReTypePassword)) {
                return true;
            } else {
                reTypePasswordEdit.requestFocus();
                reTypePasswordEdit.setError(getString(R.string.password_mismatch));
                return false;
            }
        } else if (sOldPassword.length() > 0) {
            if (sCreatePassword.length() == 0) {
                createPasswordEdit.setError(getString(R.string.please_enter_password));
                createPasswordEdit.requestFocus();
                return false;
            }
        } else {
            return true;
        }

        return true;
    }

    public ArrayList<String> parseCountryCodes() {
        String response = "";
        ArrayList<String> list = new ArrayList<String>();
        try {
            response = ReadFile.readRawFileAsString(getActivity(),
                    R.raw.countrycodes);

            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                list.add(object.getString("alpha-2") + " (" + object.getString("phone-code") + ")");
                countryCodes.add(object.getString("phone-code"));
                countryCodesIso.add(object.getString("alpha-2"));
            }
            Collections.sort(list);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }


    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.UserProfileFragment;
    }


    private void updateProfile() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showShortToast(getString(R.string.no_internet), getActivity());
            return;
        }
        String sOtp = et_register_otp.getText().toString().trim();
        AndyUtils.showSimpleProgressDialog(getActivity(), getString(R.string.please_wait), false);

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.UPDATE_PROFILE_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.FIRST_NAME, sFirstName);
        map.put(Const.Params.LAST_NAME, sLastName);
        map.put(Const.Params.DOB, sDob);
        map.put(Const.Params.PHONE, sMobile_number);
        map.put(Const.Params.GENDER, sGender);
        map.put(Const.Params.GENDER_TITLE, sGenderTitleSpinner);
        map.put(Const.Params.EMAIL, sEmailId);
        map.put(Const.Params.OLD_PASSWORD, sOldPassword);
        map.put(Const.Params.NEW_PASSWORD, sCreatePassword);
        map.put(Const.Params.NEW_PASSWORD, sCreatePassword);
        map.put(Const.Params.OTP, sOtp);
        map.put(Const.Params.PHONE_COUNTRY_CODE, sCountryCode);
        map.put(Const.Params.CURRENCY, sCurrencySpinner);
        map.put(Const.Params.LANGUAGE, sLanguageSpinner);
        AndyUtils.appLog("SameAsBillingBoolean", same_as_billing + "");
        if (same_as_billing) {
            map.put(Const.Params.BILLING_ADDRESS, sBillingAddress);
            map.put(Const.Params.BILLING_BLOCK_NO, sBillingBlockNo);
            map.put(Const.Params.BILLING_LANDMARK, sBillingLandMark);
            map.put(Const.Params.SAME, String.valueOf("1"));
            if (shippingLatLng != null && billingLatLng != null) {
                map.put(Const.Params.SHIPPING_LATITUDE, String.valueOf(billingLatLng.latitude));
                map.put(Const.Params.SHIPPING_LONGITUDE, String.valueOf(billingLatLng.longitude));
            }
        } else {
            map.put(Const.Params.BILLING_ADDRESS, sBillingAddress);
            map.put(Const.Params.BILLING_BLOCK_NO, sBillingBlockNo);
            map.put(Const.Params.BILLING_LANDMARK, sBillingLandMark);
            map.put(Const.Params.SHIPPING_ADDRESS, sShippingAddress);
            map.put(Const.Params.SHIPPING_BLOCK_NO, sShippingBlockNo);
            map.put(Const.Params.SHIPPING_LANDMARK, sShippingLandMark);
            if (shippingLatLng != null) {
                map.put(Const.Params.SHIPPING_LATITUDE, String.valueOf(shippingLatLng.latitude));
                map.put(Const.Params.SHIPPING_LONGITUDE, String.valueOf(shippingLatLng.longitude));
            }
        }


        if (filePath.equals("")) {
            map.put(Const.Params.PICTURE, patientProfile.getPictureUrl());
            AndyUtils.appLog("Ashutosh", "UpdateProfileMap" + map);
            new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.UPDATE_PROFILE, this);
        } else {
            map.put(Const.Params.PICTURE, filePath);
            AndyUtils.appLog("Ashutosh", "UpdateProfileMap" + map);
            new MultiPartRequester(getActivity(), map, Const.ServiceCode.UPDATE_PROFILE, this);
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bn_user_profile_update:
                getAllPatientDetails();
                if (validatePassword()) {
                    updateProfile();
                } else {
//                    AndyUtils.showShortToast("Please fill proper data", getActivity());
                }
                break;
            case R.id.iv_update_user_icon:
                showPictureDialog();
                break;

            case R.id.bn_user_profile_edit:
                setAllViewsEnabled();
                bn_request_otp.setVisibility(View.VISIBLE);
                editPatientProfile.setVisibility(View.GONE);
                updatePatientProfile.setVisibility(View.VISIBLE);
                break;
            case R.id.bn_request_otp:
                sMobile_number = mobile_numberEdit.getText().toString().trim();
                sCountryCode = countryCode_spinner.getSelectedItem().toString();

                if (sMobile_number.length() < 6) {
                    Toast.makeText(getActivity(), getString(R.string.please_enter_proper_mobile_number), Toast.LENGTH_SHORT).show();
                } else if (sCountryCode == null) {
                    Toast.makeText(getActivity(), getString(R.string.please_select_country_code), Toast.LENGTH_SHORT).show();
                } else {
                    generateOtp(sCountryCode, sMobile_number);
                }

                break;
        }

    }

    private void hideKeyboard() {
        // Check if no view has focus:
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void generateOtp(String countryCode, String number) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showShortToast(getResources().getString(R.string.no_internet), getActivity());
            return;
        }
        AndyUtils.showSimpleProgressDialog(getActivity(), getString(R.string.request_otp), false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.GENERATE_OTP_URL);
        map.put(Const.Params.PHONE, number);
        map.put(Const.Params.PHONE_COUNTRY_CODE, countryCode);

        AndyUtils.appLog("Ashutosh", "GenerateOtpMap" + map.toString());

        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.GENERATE_OTP, this);
    }

    private void setAllViewsEnabled() {
        patientIcon.setEnabled(true);

        mobile_numberEdit.setEnabled(true);
        mobile_numberEdit.setBackground(getResources().getDrawable(R.drawable.user_profile_editable_bg));
        oldPasswordEdit.setEnabled(true);
        createPasswordEdit.setEnabled(true);
        reTypePasswordEdit.setEnabled(true);
        maleRadioButton.setEnabled(true);
        femaleRadioButton.setEnabled(true);
        day_spinner.setEnabled(true);
        day_spinner.setBackground(getResources().getDrawable(R.drawable.user_profile_editable_bg));
        month_spinner.setEnabled(true);
        month_spinner.setBackground(getResources().getDrawable(R.drawable.user_profile_editable_bg));
        year_spinner.setEnabled(true);
        year_spinner.setBackground(getResources().getDrawable(R.drawable.user_profile_editable_bg));
        countryCode_spinner.setEnabled(true);
        countryCode_spinner.setBackground(getResources().getDrawable(R.drawable.user_profile_editable_bg));
        oldPasswordLayout.setVisibility(View.GONE);
        createPasswordLayout.setVisibility(View.GONE);
        reTypePasswordLayout.setVisibility(View.GONE);
        nationalitySpinner.setEnabled(false);
        currency_spinner.setEnabled(true);
        language_spinner.setEnabled(true);
        currency_spinner.setBackground(getResources().getDrawable(R.drawable.user_profile_editable_bg));
        language_spinner.setBackground(getResources().getDrawable(R.drawable.user_profile_editable_bg));
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.UPDATE_PROFILE:
                AndyUtils.removeProgressDialog();
                AndyUtils.appLog("Ashutosh", "UpdateProfileResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {

                        String languageToLoad2 = selectedLanguage; // your language
                        Locale locale2 = new Locale(languageToLoad2);
                        Locale.setDefault(locale2);
                        Configuration config2 = new Configuration();
                        config2.locale = locale2;
                        activity.getResources().updateConfiguration(config2,
                                activity.getResources().getDisplayMetrics());

                        Realm realm = RealmDBController.with(this).getRealm();
                        final PatientProfile patientProfile = new PatientProfile();
                        patientProfile.setPatientId(Integer.parseInt(jsonObject.optString("id")));
                        patientProfile.setFirstName(jsonObject.optString("first_name"));
                        PreferenceHelper.setParam(getActivity(), Const.Params.CURRENCY, jsonObject.optString(Const.Params.CURRENCY));
                        String language1 = jsonObject.optString(Const.Params.LANGUAGE);
                        String language = language1.substring(language1.indexOf("(") + 1, language1.indexOf(")"));

                        PreferenceHelper.setParam(getActivity(), Const.Params.LANGUAGE, language);
                        PreferenceHelper.setParam(getActivity(), Const.Params.FIRST_NAME, jsonObject.optString("first_name"));
                        patientProfile.setLastName(jsonObject.optString("last_name"));
                        patientProfile.setEmailId(jsonObject.optString("email"));
                        patientProfile.setPictureUrl(jsonObject.optString("picture"));
                        patientProfile.setMobileNumber(jsonObject.optString("phone"));
                        patientProfile.setBillingAddress(jsonObject.optString("billing_address"));
                        patientProfile.setShippingAddress(jsonObject.optString("shipping_address"));
                        patientProfile.setBillingLandMark(jsonObject.optString("blandmark"));
                        patientProfile.setBillingBlockNo(jsonObject.optString("bblock_no"));
                        patientProfile.setShippingLandMark(jsonObject.optString("slandmark"));
                        patientProfile.setShippingBlockNo(jsonObject.optString("sblock_no"));
                        patientProfile.setDob(jsonObject.optString("dob"));
                        patientProfile.setGender(jsonObject.optString("gender"));
                        patientProfile.setGenderTitle(jsonObject.optString("title"));
                        patientProfile.setNationality(jsonObject.optString(Const.Params.NATIONALITY));
                        patientProfile.setCountryCode(jsonObject.optString(Const.Params.PHONE_COUNTRY_CODE));
                        patientProfile.setCurrency(jsonObject.optString(Const.Params.CURRENCY));
                        patientProfile.setLanguage(jsonObject.optString(Const.Params.LANGUAGE));
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(patientProfile);
                            }
                        });


                        try {
                            if (!filePath.equals("")) {

                                File file = new File(filePath);
                                file.getAbsoluteFile().delete();

                            }
                            if (cameraFile != null) {
                                cameraFile.getAbsoluteFile().delete();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        AndyUtils.showShortToast(getString(R.string.profile_updated), getActivity());
                        Intent intent = new Intent(activity, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(Const.INCOMING_BUNDLE, Const.Params.LANGUAGE);
                        activity.finish();
                        activity.startActivity(intent);
                        System.exit(0);
                    } else {
                        AndyUtils.showShortToast(jsonObject.getString("error_message"), getActivity());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GENERATE_OTP:
                AndyUtils.removeProgressDialog();
                et_register_otp.setVisibility(View.VISIBLE);
                break;


        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.UserProfileFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("UserProfileFragment", "fragment");
            manager.beginTransaction().remove(fragment).commit();
        }
    }
}
