package com.patient.ghealth.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.DoctorListActivity;
import com.patient.ghealth.activity.LoginActivity;
import com.patient.ghealth.adapter.DoctorNearByAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.location.LocationHelper;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by getit on 8/12/2016.
 */
public class DoctorNearByOnlineConsultFragment extends Fragment implements AsyncTaskCompleteListener, LocationHelper.OnLocationReceived {
    private static final String TAG = DoctorNearByOnlineConsultFragment.class.getSimpleName();
    private MapView mapView;
    private boolean mapsSupported = true;
    private GoogleMap mGoogleMap = null;
    private LocationHelper locationHelper;
    private Bundle mBundle;
    private View view;
    private EditText sourceEditText, destinationEditTExt;
    private Location myLocation;
    private List<DoctorOnLineDetails> onLineDetailsList;
    private ProgressDialog nearByOnLineProgressDialog;
    private RelativeLayout doctorNearByLayout;
    private ViewPager viewPager;
    private DoctorListActivity activity;
    private ImageButton currentLocationButton;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
        activity = (DoctorListActivity) getActivity();
        getPermission();
        if (!TextUtils.isEmpty((CharSequence) PreferenceHelper.getParam(activity, Const.Params.LANGUAGE, ""))) {
            String languageToLoad = (String) PreferenceHelper.getParam(activity, Const.Params.LANGUAGE, ""); // your language
            Locale locale = new Locale(languageToLoad);
            Locale.setDefault(locale);
            Configuration config2 = new Configuration();
            config2.locale = locale;
            activity.getResources().updateConfiguration(config2,
                    activity.getResources().getDisplayMetrics());

        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_doctor_near_by, container, false);
        mapView = (MapView) view.findViewById(R.id.mapview);
        viewPager = (ViewPager) view.findViewById(R.id.vp_nearby);
        doctorNearByLayout = (RelativeLayout) view.findViewById(R.id.near_by_layout);
        currentLocationButton = (ImageButton) view.findViewById(R.id.ib_current_location);
        viewPager.setClipToPadding(false);
        viewPager.setPageMargin(15);
        sourceEditText = (EditText) view.findViewById(R.id.et_doctor_near_by_source_address);
        destinationEditTExt = (EditText) view.findViewById(R.id.et_doctor_near_by_destination_address);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                AndyUtils.appLog("SelectedPagePosition", position + "");
                destinationEditTExt.setText(onLineDetailsList.get(position).getClinicAddress());
                if (mGoogleMap != null) {
                    mGoogleMap.clear();
                    if (!onLineDetailsList.get(position).getdLatitude().equals("") && !onLineDetailsList.get(position).getdLongitude().equals("")) {
                        LatLng coordinate = new LatLng(Double.valueOf(onLineDetailsList.get(position).getdLatitude()), Double.valueOf(onLineDetailsList.get(position).getdLongitude())); //Store these lat lng values somewhere. These should be constant.
                        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                                coordinate, 15);
                        MarkerOptions currentOption = new MarkerOptions();
                        currentOption.position(coordinate);
                        currentOption.title(onLineDetailsList.get(position).getDoctorName());
                        currentOption.icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_marker));
                        mGoogleMap.addMarker(currentOption);
                        mGoogleMap.animateCamera(location);
                        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
                    }
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        currentLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (markerdoctor.getPosition() != null)
//                    mGoogleMap.animateCamera(CameraUpdateFactory
//                            .newLatLng(markerdoctor.getPosition()));
                try {
                    if (mGoogleMap != null && myLocation != null) {

                        LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
                        mGoogleMap.animateCamera(cameraUpdate);
                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        locationHelper = new LocationHelper(activity);
        locationHelper.setLocationReceivedLister(this);
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            mapsSupported = false;
        }
        if (mapView != null) {
            mapView.onCreate(mBundle);
        }
        setUpMap();
    }

    @Override
    public void onResume() {
        AndyUtils.appLog("Ashutosh", "onResume");
        super.onResume();
        mapView.onResume();
//        activity.currentFragment = Const.DoctorOnDemandFragment;
//        IntentFilter filter = new IntentFilter(Const.DOCTOR_REQUEST_STATUS);
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver,
//                filter);

    }


    @Override
    public void onPause() {
        AndyUtils.appLog("Ashutosh", "onPause");
        super.onPause();
//        locationAndUpdate.disconnect();
        mapView.onPause();

    }


    private void setUpMap() {
        // map.
        if (mGoogleMap == null) {
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mapView.invalidate();
                    mGoogleMap = googleMap;
                }
            });
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_DOCTOR_NEARBY_ONLINE:
                nearByOnLineProgressDialog.cancel();
                AndyUtils.appLog("Ashutosh", "DoctorNearByOnLineResponse" + response);
                try {
                    jsonObject = new JSONObject(response);

                    if (jsonObject.getString("success").equals("true")) {
                        String sourceAddress = jsonObject.optString(Const.SOURCE_ADDRESS);
                        onLineDetailsList = new ArrayList<DoctorOnLineDetails>();
                        JSONArray jsonArray = jsonObject.getJSONArray("doctors");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject onLineJsonObject = jsonArray.getJSONObject(i);
                            if (Integer.parseInt(onLineJsonObject.optString("c_count")) > 0) {
                                DoctorOnLineDetails doctorOnLineDetails = new DoctorOnLineDetails();
                                doctorOnLineDetails.setClinicAddress(onLineJsonObject.optString("c_address"));
                                doctorOnLineDetails.setDoctor_clinic_image_first(onLineJsonObject.optString("c_pic1"));
                                doctorOnLineDetails.setDoctor_clinic_image_second(onLineJsonObject.optString("c_pic2"));
                                doctorOnLineDetails.setDoctor_clinic_image_third(onLineJsonObject.optString("c_pic3"));
                                doctorOnLineDetails.setDoctorClinicName(onLineJsonObject.optString("c_name"));
                                doctorOnLineDetails.setdLatitude(onLineJsonObject.optString("d_latitude"));
                                doctorOnLineDetails.setdLongitude(onLineJsonObject.optString("d_longitude"));
                                doctorOnLineDetails.setBetweenDistance(onLineJsonObject.optString("eta"));
                                doctorOnLineDetails.setClinicId(onLineJsonObject.optString("c_id"));
                                doctorOnLineDetails.setClinicNoOfDoctor(onLineJsonObject.optString("c_count"));
                                onLineDetailsList.add(doctorOnLineDetails);
                            }
                        }

                        if (onLineDetailsList.size() > 0) {
                            sourceEditText.setVisibility(View.VISIBLE);
                            destinationEditTExt.setVisibility(View.VISIBLE);
                            sourceEditText.setText(sourceAddress);
                            sourceEditText.setEnabled(false);
                            doctorNearByLayout.setVisibility(View.VISIBLE);
                            DoctorNearByAdapter pagerAdapter = new DoctorNearByAdapter(getActivity(), onLineDetailsList, activity, myLocation, Const.ONLINE_CONSULT);
                            viewPager.setAdapter(pagerAdapter);
                            setMarkerListOnMap(onLineDetailsList);
                        } else {
                            doctorNearByLayout.setVisibility(View.GONE);
                            AndyUtils.showShortToast(getString(R.string.no_clinic), getActivity());
                        }

                    } else {

                        doctorNearByLayout.setVisibility(View.GONE);
                        if (jsonObject.optString("error_message").equalsIgnoreCase(Const.INVALID_TOKEN)) {
                            AndyUtils.showShortToast(getString(R.string.you_have_logged), getActivity());
                            PreferenceHelper.setParam(getActivity(), Const.LOGIN_FIRST_TIME, false);
                            Intent homeIntent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(homeIntent);
                            getActivity().finish();
                        } else {
                            AndyUtils.showShortToast(jsonObject.getString("error_message"), getActivity());
                        }
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
                break;


        }
    }


    private void setMarkerListOnMap(final List<DoctorOnLineDetails> doctorOnLineDetailsList) {

        if (myLocation != null) {
            destinationEditTExt.setText(doctorOnLineDetailsList.get(0).getClinicAddress());
            destinationEditTExt.setEnabled(false);
        }
        if (doctorOnLineDetailsList.size() > 0) {
            AndyUtils.appLog("AfetrAddingDoctorNearBySize", doctorOnLineDetailsList.size() + "");
            if (!doctorOnLineDetailsList.get(0).getdLatitude().equals("") && !doctorOnLineDetailsList.get(0).getdLongitude().equals("")) {
                LatLng latLng = new LatLng(Double.valueOf(doctorOnLineDetailsList.get(0).getdLatitude()), Double.valueOf(doctorOnLineDetailsList.get(0).getdLongitude()));
                CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                        latLng, 15);
                mGoogleMap.clear();
                MarkerOptions currentOption = new MarkerOptions();
                currentOption.position(latLng);
                currentOption.title(doctorOnLineDetailsList.get(0).getDoctorName());
                currentOption.icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_marker));
                mGoogleMap.addMarker(currentOption);
                mGoogleMap.animateCamera(location);
                mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
            }
        }
    }


    @Override
    public void onLocationReceived(LatLng latlong) {

    }

    @Override
    public void onLocationReceived(Location location) {
        if (location != null) {
            // drawTrip(latlong);
            myLocation = location;
            LatLng latLang = new LatLng(location.getLatitude(),
                    location.getLongitude());


        }

    }

    @Override
    public void onConntected(Bundle bundle) {

    }

    @Override
    public void onConntected(Location location) {
        if (location != null) {
            myLocation = location;
            if (myLocation != null) {
                String doctorSpecialistsId = (String) PreferenceHelper.getParam(getActivity(), Const.SPECIALITIES_DOCTOR_ID, "");
                AndyUtils.appLog(TAG, "DoctorSpecialistId" + doctorSpecialistsId);
                if (myLocation != null) {
                    AndyUtils.appLog("MyLoction", myLocation.toString());
                    getDoctorNearByOnLineDetails(doctorSpecialistsId, myLocation);

                }
            }
        }
    }


    @SuppressLint("NewApi")
    private void getPermission() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion > android.os.Build.VERSION_CODES.LOLLIPOP) {

            String[] permissions_dummy = new String[7];
            int i = 0;

            String permission = "android.permission.ACCESS_COARSE_LOCATION";
            int res = activity.checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;
            }
            permission = "android.permission.ACCESS_FINE_LOCATION";
            res = activity.checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;


            }
            permission = "android.permission.CAMERA";
            res = activity.checkCallingOrSelfPermission(permission);
            if (res != PackageManager.PERMISSION_GRANTED) {


                permissions_dummy[i] = permission;
                i = i + 1;
            }
            String[] permissions = new String[i];

            for (int j = 0; j < i; j++) {

                permissions[j] = permissions_dummy[j];

            }


            int yourRequestId = 1;
            if (i != 0) {


                // Do something for lollipop and above versions
                requestPermissions(permissions, yourRequestId);
            }

        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.DoctorNearByOnlineConsultFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("HealthFeedsFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }

    public String getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append(",");
                }
                strAdd = strReturnedAddress.toString();
                Log.d("MyCurrentLoctionAddress", strReturnedAddress.toString());
            } else {
                Log.w("MyCurrentLoctionAddress", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("MyCurrentLoctionAddress", "Canont get Address!");
        }
        return strAdd;
    }

    private void getDoctorNearByOnLineDetails(String doctorId, Location currentLocation) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        nearByOnLineProgressDialog = AndyUtils.getSimpleProgressDialog(getActivity(), getString(R.string.please_wait), false);
        nearByOnLineProgressDialog.show();
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_DOCTOR_NEARBY_ONLINE_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.SPECIALITY_ID + "="
                + String.valueOf(doctorId) + "&" + Const.Params.LATITUDE + "="
                + String.valueOf(currentLocation.getLatitude()) + "&" + Const.Params.LONGITUDE + "="
                + String.valueOf(currentLocation.getLongitude()));

        AndyUtils.appLog("Ashutosh", "DoctorNearByOnlineMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_DOCTOR_NEARBY_ONLINE, this);

    }

}

