package com.patient.ghealth.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.CheckoutActivity;
import com.patient.ghealth.activity.HealthStoreActivity;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.CartDetailsAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.CartProductDetail;
import com.patient.ghealth.model.ProductDetail;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CartDetailsFragment extends Fragment implements View.OnClickListener, AsyncTaskCompleteListener {

    public static TextView order_cost, total_cart_quantity;
    public static LinearLayout empty_cart_view, cart_view;
    public static double total_cost;
    public static int item_qty = 0;
    private static HealthStoreActivity activity;
    private RecyclerView mRecyclerView;
    private CartDetailsAdapter mCartListAdapter;
    private List<CartProductDetail> mProductDetails;
    private ProgressBar progressBar;
    private Button continue_shopping_button, checkout;
    private String guestToken, sessionToken;
    private Bundle bundle;

    public CartDetailsFragment() {
        // Required empty public constructor
    }

    public static void setOrderCostCountView() {
        order_cost.setText(Const.RM + total_cost);
        String itemDots = activity.getString(R.string.items_dot);
        total_cart_quantity.setText("(" + item_qty + " " + itemDots + ")");

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProductDetails = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart_content, container, false);
        activity = (HealthStoreActivity) getActivity();
        empty_cart_view = (LinearLayout) view.findViewById(R.id.empty_cart_view);
        cart_view = (LinearLayout) view.findViewById(R.id.cart_view);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.cart_list);
        progressBar = (ProgressBar) view.findViewById(R.id.cart_progress);
        order_cost = (TextView) view.findViewById(R.id.order_cost);
        total_cart_quantity = (TextView) view.findViewById(R.id.total_cart_quantity);
        continue_shopping_button = (Button) view.findViewById(R.id.continue_shopping_button);
        continue_shopping_button.setOnClickListener(this);
        checkout = (Button) view.findViewById(R.id.checkout);
        checkout.setOnClickListener(this);

        bundle = getArguments();
        if (bundle != null && bundle.getString(Const.INCOMING_BUNDLE) != null) {
            if (bundle.getString(Const.INCOMING_BUNDLE).equals(Const.StoreCategoryFragment)) {
                activity.referenceFragment = bundle.getString(Const.INCOMING_BUNDLE);
            }
        }
        setupRecyclerView();
        if (mProductDetails.isEmpty()) {
            getCartProducts();
        }
        getCountAndSubtotal();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.CartDetailsFragment;

    }

    private void getCountAndSubtotal() {

        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CART_COUNT_URL);

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "CartCountMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_CART_COUNT, this, headerMap);
    }

    private void setupRecyclerView() {
        mCartListAdapter = new CartDetailsAdapter(mProductDetails, getActivity(), activity);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mCartListAdapter);
    }

    private void getCartProducts() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CART_DETAIL_URL);

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "CartCountMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_CART_DETAIL, this, headerMap);

    }


    private void gotoCategoryFragment() {

        Intent intent = new Intent(activity, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(Const.INCOMING_BUNDLE, Const.StoreProductListFragment);
        intent.putExtras(bundle);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continue_shopping_button:
                gotoCategoryFragment();
                break;
            case R.id.checkout:
                if (item_qty > 0) {
                    Intent intent = new Intent(getActivity(), CheckoutActivity.class);
                    intent.putExtra(Const.CHECKOUT_METHOD_KEY, 0);
                    startActivity(intent);
                }
                break;
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_CART_COUNT:
                AndyUtils.appLog("Ashutosh", "CartCountResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        JSONObject mainObj = jsonObject.optJSONObject(Const.RESPONSE);
                        item_qty = mainObj.optInt("count");
                        total_cost = mainObj.optDouble("total_price");
                        activity.cartCount.setText(item_qty + "");
                        setOrderCostCountView();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_CART_DETAIL:
                progressBar.setVisibility(View.GONE);
                AndyUtils.appLog("Ashutosh", "CartDetailResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        parseGetCartProductResponse(jsonObject);

                    } else {
                        empty_cart_view.setVisibility(View.VISIBLE);
                        cart_view.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }
    }

    private void parseGetCartProductResponse(JSONObject result) {
        if (result == null)
            return;

        try {
            JSONObject responseObj = result.getJSONObject(Const.RESPONSE);
            JSONArray productJsonArray = responseObj.getJSONArray("cart");
            if (productJsonArray != null && productJsonArray.length() > 0) {
                for (int i = 0; i < productJsonArray.length(); i++) {
                    JSONObject cartProductDetail = productJsonArray.getJSONObject(i);
                    CartProductDetail mCartProductDetail = new CartProductDetail();
                    mCartProductDetail.setProductQty(cartProductDetail.optInt("product_quantity"));
                    mCartProductDetail.setCart_id(cartProductDetail.optString(Const.Params._ID));
                    JSONObject productObj = cartProductDetail.getJSONObject("product_id");
                    ProductDetail productDetail = new ProductDetail();
                    productDetail.setProductId(productObj.optString(Const.Params._ID));
                    productDetail.setProductName(productObj.optString("name"));
                    productDetail.setQuantity(productObj.optInt("quantity"));
                    productDetail.setDescription(productObj.optString("description"));
                    productDetail.setPaidByBuyer(productObj.optBoolean("paid_by_buyer"));
                    // get the pricing object information
                    JSONObject productPricingObj = cartProductDetail.getJSONObject("selected_pricing");
                    ProductDetail.Price price = new ProductDetail.Price();
                    price.setAfter_discount(productPricingObj.optDouble("after_discount", -1));
                    price.setCommission(productPricingObj.optDouble("commission", -1));
                    price.setOriginal_price(productPricingObj.optDouble("original", -1));
                    price.setService_tax(productPricingObj.optDouble("service_tax", -1));
                    if (productPricingObj.optString(Const.Params.NAME) != null &&
                            !productPricingObj.optString(Const.Params.NAME).equals("") &&
                            !productPricingObj.optString(Const.Params.NAME).equals("null")) {
                        String name = productDetail.getProductName() + ", " + productPricingObj.optString(Const.Params.NAME);
                        productDetail.setProductName(name); // adding variant name to product title
                    }

                    // add the pricing obj to main obj
                    productDetail.setPrice(price);
                    // get seller info obj
                    JSONObject sellerInfoObj = productObj.getJSONObject("created_by");
                    ProductDetail.Seller_info seller_info = new ProductDetail.Seller_info();
                    seller_info.set_id(sellerInfoObj.optString(Const.Params._ID));
                    seller_info.setSeller_name(sellerInfoObj.optString(Const.Params.NAME));
                    // add seller obj to main obj
                    productDetail.setSellerInfo(seller_info);
                    // add rating
                    /*ProductDetail.Rating rating_info = new ProductDetail.Rating();
                    rating_info.setTotalRatings(productObj.optInt("total_ratings"));
                    rating_info.setTotalStar(productObj.optInt("total_star"));
                    rating_info.setTotalReviews(productObj.optInt("total_reviews"));
                    // add rating obj to main obj
                    productDetail.setRating(rating_info);*/
                    //add shipping info
                    /*JSONObject shippingObject = productObj.optJSONObject("shipping_details");
                    ProductDetail.ShippingDetails shippingDetails = new ProductDetail.ShippingDetails();
                    shippingDetails.setDuration(shippingObject.optString("duration"));
                    shippingDetails.setFee(shippingObject.optDouble("fee"));
                    shippingDetails.setUnit(shippingObject.optString("unit"));
                    shippingDetails.setWeight(shippingObject.optDouble("weight"));
                    productDetail.setShippingDetails(shippingDetails);*/

                    //get the image object information
                    JSONArray productImageJsonArray = productObj.getJSONArray("images");
                    ArrayList<ProductDetail.Cat_image> catImageArrayList = new ArrayList<ProductDetail.Cat_image>();
                    for (int j = 0; j < productImageJsonArray.length(); j++) {
                        JSONObject productImageObj = productImageJsonArray.getJSONObject(j);
                        JSONObject cdnObject = productImageObj.getJSONObject("cdn");
                        ProductDetail.Cat_image catImage = new ProductDetail.Cat_image();
                        catImage.set_id(cdnObject.optString(Const.Params._ID));
                        catImage.setImage_url(cdnObject.optString("url"));
                        catImageArrayList.add(catImage);
                    }
                    // add category image list to main obj
                    productDetail.setCat_images(catImageArrayList);

                    mCartProductDetail.setmProductDetail(productDetail);
                    mProductDetails.add(mCartProductDetail);
                }

                AndyUtils.appLog("CartListSize", mProductDetails.size() + "");
                AndyUtils.appLog("ProductJsonArray", productJsonArray.length() + "");
            } else if (mProductDetails.isEmpty()) {
                empty_cart_view.setVisibility(View.VISIBLE);
                cart_view.setVisibility(View.GONE);
            } else {
                empty_cart_view.setVisibility(View.GONE);
                cart_view.setVisibility(View.VISIBLE);
            }

            mCartListAdapter.notifyDataSetChanged();


        } catch (JSONException e1) {
            e1.printStackTrace();
        }
    }
}
