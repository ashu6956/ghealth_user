package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.ChatHistoryAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.ChatHistoryobjt;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.realmDB.DatabaseHandler;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by user on 11/23/2016.
 */
public class ViewMessageHistoryFragment extends Fragment implements AsyncTaskCompleteListener {
    public static String name;
    public static String request_id;
    public static String picture;
    public static String type = "";
    ListView message_listview;
    ImageView btn_close;
    TextView tv_name, noChatHistoryText;
    CircleImageView iv_img;
    DatabaseHandler db;
    List<ChatHistoryobjt> messages;
    ChatHistoryAdapter messageAdapter;
    private MainActivity activity;
    private ProgressBar chatHistoryProgressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_message_historyview, container, false);
        activity = (MainActivity) getActivity();
        iv_img = (CircleImageView) view.findViewById(R.id.iv_img);
        tv_name = (TextView) view.findViewById(R.id.name);
        message_listview = (ListView) view.findViewById(R.id.message_listview);
        noChatHistoryText = (TextView) view.findViewById(R.id.tv_noChatHistory);
        chatHistoryProgressBar = (ProgressBar) view.findViewById(R.id.chatHistory_progressBar);

        Bundle mBundle = getArguments();
        try {
            if (mBundle != null) {
                name = mBundle.getString("name");
                request_id = mBundle.getString("request_id");
                picture = mBundle.getString("picture");
                tv_name.setText(name);
                type = mBundle.getString(Const.INCOMING_BUNDLE);
                Glide.with(activity).load(picture).centerCrop().crossFade().into(iv_img);
                getMessages(request_id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }


    private void getMessages(String request_id) {


        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showShortToast(getString(R.string.no_internet), getActivity());
            return;
        }
        chatHistoryProgressBar.setVisibility(View.VISIBLE);

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CHAT_HISTORY_MESSAGE_URL + Const.Params.REQUEST_ID + "="
                + request_id);

        AndyUtils.appLog("Ashutosh", "ChatHistoryMessageMap" + map);
        new HttpRequester(activity, Const.GET, map, Const.ServiceCode.GET_CHAT_HISTORY_MESSAGE, this);

    }

    public void onResume() {
        super.onResume();
        try {
            if (type.equals(Const.VideoConsultHistoryFragment)) {
                activity.currentFragment = Const.VideoConsultHistoryFragment;
            } else if (type.equals(Const.ChatConsultHistoryFragment)) {
                activity.currentFragment = Const.ChatConsultHistoryFragment;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode) {
            case Const.ServiceCode.GET_CHAT_HISTORY_MESSAGE:
                chatHistoryProgressBar.setVisibility(View.GONE);
                Log.d("Ashutosh", "message response" + response);

                try {
                    JSONObject job = new JSONObject(response);
                    if (job.getString("success").equals("true")) {
                        JSONArray jarray = job.getJSONArray("chatArray");
                        messages = new ArrayList<ChatHistoryobjt>();
                        for (int i = 0; i < jarray.length(); i++) {
                            ChatHistoryobjt chat = new ChatHistoryobjt();
                            JSONObject chatobj = jarray.getJSONObject(i);
                            chat.setMessage(chatobj.getString("message"));
                            chat.setType(chatobj.getString("type"));
                            chat.setData_type(chatobj.getString("data_type"));
                            messages.add(chat);
                        }

                        if (messages != null && messages.size() > 0) {
                            messageAdapter = new ChatHistoryAdapter(getActivity(), messages, activity);
                            // messageAdapter.notifyDataSetChanged();
                            message_listview.setAdapter(messageAdapter);
                        } else {
                            noChatHistoryText.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                break;

        }
    }

}
