package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.HealthFeedsAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.HealthFeedArticles;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 9/6/2016.
 */
public class BookMarkFragment extends Fragment implements AsyncTaskCompleteListener
{
    ListView healthsListView;
    TextView askAQuestionText,headerText;
    FloatingActionButton healthsFeedButton;
    private MainActivity activity;
    private List<HealthFeedArticles> feedArticlesList;
    private ProgressBar bookMarkProgressBar;
    private TextView noArticlesText,border;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_health_feeds_layout, container, false);
        activity= (MainActivity) getActivity();
        healthsListView = (ListView) view.findViewById(R.id.lv_healths_feed);
        healthsFeedButton= (FloatingActionButton) view.findViewById(R.id.fabButton_health_feeds);
        healthsFeedButton.setVisibility(View.GONE);
        headerText= (TextView) view.findViewById(R.id.tv_healths_feed_header);
        headerText.setText(getString(R.string.my_favourite_articles));
        askAQuestionText= (TextView) view.findViewById(R.id.tv_healths_feed_ask_questions);
        askAQuestionText.setVisibility(View.GONE);
        bookMarkProgressBar= (ProgressBar) view.findViewById(R.id.health_feed_progress);
        noArticlesText= (TextView) view.findViewById(R.id.tv_no_articles);
        border= (TextView) view.findViewById(R.id.header_border);
        border.setVisibility(View.VISIBLE);

        getBookMarkArticleDetails();
        healthsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                activity.mainTabLayout.getTabAt(1).select();
                HealthFeedsDescriptionFragment descriptionFragment=new HealthFeedsDescriptionFragment();
                Bundle bundle=new Bundle();
                HealthFeedArticles feedArticles=feedArticlesList.get(position);
                bundle.putString(Const.ARTICLE_CLICKED_POSITION,feedArticles.getArticleId());
                bundle.putString(Const.BookMarkFragment,Const.BookMarkFragmentDescription);
                descriptionFragment.setArguments(bundle);
                activity.addFragment(descriptionFragment,false,getString(R.string.health_feed), Const.HealthFeedsDescriptionFragment,true);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.appLogo.setVisibility(View.GONE);
            }

        });
        return view;
    }


    private void getBookMarkArticleDetails()
    {
        if(!AndyUtils.isNetworkAvailable(getActivity()))
        {
            AndyUtils.showLongToast(getString(R.string.no_internet),getActivity());
            return;
        }
        bookMarkProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_BOOK_MARK_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "GetMyScheduleDateMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_BOOK_MARK, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment=Const.BookMarkFragment;

    }


    @Override
    public void onDestroyView() {

        FragmentManager manager=getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.BookMarkFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("BookMarkFragment", "fragment");
            manager.beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
        super.onDestroyView();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode)
    {
        bookMarkProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject;
        switch (serviceCode)
        {
            case Const.ServiceCode.GET_BOOK_MARK:
                AndyUtils.appLog("Ashutosh","GetArticlesResponse" +response);

                try {
                    jsonObject=new JSONObject(response);
                    if(jsonObject.getString("success").equals("true"))
                    {
                        feedArticlesList=new ArrayList<>();
                        JSONArray feedJsonArray=jsonObject.optJSONArray("articles");
                        if(feedJsonArray!=null && feedJsonArray.length()>0) {
                            for (int i = 0; i < feedJsonArray.length(); i++) {
                                JSONObject feedJsonObject = feedJsonArray.optJSONObject(i);
                                HealthFeedArticles healthFeedArticles = new HealthFeedArticles();
                                healthFeedArticles.setArticleId(feedJsonObject.optString("article_id"));
                                healthFeedArticles.setArticlePictureUrl(feedJsonObject.optString("picture"));
                                healthFeedArticles.setCategory(feedJsonObject.optString("category"));
                                healthFeedArticles.setTopicHeading(feedJsonObject.optString("heading"));
                                healthFeedArticles.setIsBookMarked(feedJsonObject.optString("is_bookmarked"));
                                feedArticlesList.add(healthFeedArticles);
                            }

                            HealthFeedsAdapter healthFeedsAdapter = new HealthFeedsAdapter(getActivity(), feedArticlesList);
                            healthsListView.setAdapter(healthFeedsAdapter);
                            noArticlesText.setVisibility(View.GONE);
                        }
                        else
                        {
                            noArticlesText.setVisibility(View.VISIBLE);
                        }


                    }
                    else
                    {
                        noArticlesText.setVisibility(View.VISIBLE);
                        AndyUtils.showShortToast(jsonObject.getString("error_message"),getActivity());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }
    }
}
