package com.patient.ghealth.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.DoctorConsultConnectivity;
import com.patient.ghealth.activity.DoctorListActivity;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.utils.Const;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by user on 9/20/2016.
 */
public class DoctorOnLineProfileDetailsFragment extends Fragment
{
    private TextView doctorClinicName,doctorClinicAddress,doctorConsultantFee,doctorConsult;
    private ImageView doctorLocationStaticMap,chat_icon,video_icon,call_icon;
    private DoctorOnLineDetails doctorOnLineDetails;
    private Bundle bundle;
    private String type;
//    private DoctorListActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_online_booking_doctor_profile_details_layout,container,false);
//        activity= (DoctorListActivity) getActivity();
        doctorClinicName= (TextView) view.findViewById(R.id.tv_profile_doctor_clinic_name);
        doctorClinicName= (TextView) view.findViewById(R.id.tv_profile_doctor_clinic_name);
        doctorConsultantFee= (TextView) view.findViewById(R.id.tv_profile_doctor_ConsultFee);
        doctorLocationStaticMap= (ImageView) view.findViewById(R.id.iv_profile_doctor_static_location);
        doctorClinicAddress= (TextView) view.findViewById(R.id.tv_profile_clinic_address);
        chat_icon= (ImageView) view.findViewById(R.id.iv_profile_doctor_chat);
        video_icon= (ImageView) view.findViewById(R.id.iv_profile_doctor_video);
        call_icon= (ImageView) view.findViewById(R.id.iv_profile_doctor_call);
        doctorConsult= (TextView) view.findViewById(R.id.tv_profile_doctor_consult);
        doctorConsult.setText(getString(R.string.consult));

        bundle=getArguments();
        if(bundle!=null)
        {
            type=bundle.getString(Const.DOCTOR_TYPE);
            doctorOnLineDetails= (DoctorOnLineDetails) bundle.getSerializable(Const.DOCTOR_ONLINE_DETAILS);
            if(doctorOnLineDetails!=null && type.equals(Const.ONLINE_CONSULT))
            {
                setDataOnViews(doctorOnLineDetails);
            }
        }

        doctorConsult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(bundle!=null &&doctorOnLineDetails!=null)
                {
                    Intent consultIntent = new Intent(getActivity(), DoctorConsultConnectivity.class);
                    consultIntent.putExtras(bundle);
                    startActivity(consultIntent);
                }
            }
        });
        return view;


    }


    @Override
    public void onResume() {
        super.onResume();
//        activity.currentFragment=Const.DoctorOnLineProfileDetailsFragment;

    }

    private void setDataOnViews(DoctorOnLineDetails onLineDetails) {
        doctorClinicName.setText(onLineDetails.getDoctorClinicName());

        if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1) {
            chat_icon.setVisibility(View.VISIBLE);
        }
        if (Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1) {
            call_icon.setVisibility(View.VISIBLE);
        }
        if (Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
            video_icon.setVisibility(View.VISIBLE);
        }
        doctorClinicAddress.setText(doctorOnLineDetails.getClinicAddress());
        if(Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1)
        {
            if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) && Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {

                doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorChatConsultFee() + "\n"+getString(R.string.onwards));

            } else if (Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {
                doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorPhoneConsultFee() +"\n"+getString(R.string.onwards));
            } else {
                doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorVideoConsultFee() + "\n"+getString(R.string.onwards));
            }
        }
        else if(Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1)
        {
            if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee())) {

                doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorChatConsultFee() + "\n"+getString(R.string.onwards));

            }
            else  {

                doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorPhoneConsultFee() + "\n"+getString(R.string.onwards));

            }
        }
        else if(Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1)
        {
            if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {

                doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorChatConsultFee() + "\n"+getString(R.string.onwards));

            }
            else  {

                doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorVideoConsultFee() + "\n"+getString(R.string.onwards));

            }
        }
        else if(Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1)
        {
            if (Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {

                doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorPhoneConsultFee() + "\n"+getString(R.string.onwards));

            }
            else  {

                doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorVideoConsultFee() + "\n"+getString(R.string.onwards));

            }
        }
        else if(Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1)
        {
            doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorPhoneConsultFee() + "\n"+getString(R.string.onwards));
        }
        else if(Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1)
        {
            doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorChatConsultFee() + "\n"+getString(R.string.onwards));
        }
        else if(Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1)
        {
            doctorConsultantFee.setText("MYR" + " " + doctorOnLineDetails.getDoctorVideoConsultFee() + "\n"+getString(R.string.onwards));
        }
//        if(getGoogleMapThumbnail(doctorOnLineDetails.getdLatitude(),doctorOnLineDetails.get))
        if (!doctorOnLineDetails.getdLatitude().equals("") && !doctorOnLineDetails.getdLongitude().equals("")) {
            Glide.with(getActivity()).load(getGoogleMapThumbnail(Double.valueOf(doctorOnLineDetails.getdLatitude()), Double.valueOf(doctorOnLineDetails.getdLongitude()))).into(doctorLocationStaticMap);
        }
    }

    public static String getGoogleMapThumbnail(double lati, double longi){
        String staticMapUrl="http://maps.google.com/maps/api/staticmap?center="+lati+","+longi+"&markers="+lati+","+longi+"&zoom=14&size=400x250&sensor=false";
     return staticMapUrl;
    }

//    @Override
//    public void onDestroyView() {
//
//        Fragment fragment = (getFragmentManager()
//                .findFragmentById(R.id.fl_doctor_list));
//        if (fragment!=null && fragment.isResumed()) {
//            getFragmentManager().beginTransaction().remove(fragment)
//                    .commitAllowingStateLoss();
//        }
//        super.onDestroyView();
//    }

}
