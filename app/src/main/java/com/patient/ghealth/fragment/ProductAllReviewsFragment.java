package com.patient.ghealth.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.HealthStoreActivity;
import com.patient.ghealth.adapter.ReviewListAdapter;
import com.patient.ghealth.adapter.SimpleDividerItemDecoration;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.ProductDetail;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProductAllReviewsFragment extends Fragment implements AsyncTaskCompleteListener {

    private RecyclerView mRecyclerView;
    private ReviewListAdapter mReviewListAdapter;
    private ArrayList<ProductDetail.Reviews> reviewsArrayList;
    private ProductDetail productDetail;
    private ProgressBar review_progress;
    private TextView no_reviews, reviewHeaderText;
    private HealthStoreActivity activity;
    private String reviewHeader;

    public ProductAllReviewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_product_all_reviews, container, false);
        activity = (HealthStoreActivity) getActivity();
        reviewsArrayList = new ArrayList<>();
        productDetail = (ProductDetail) getArguments().getSerializable("ProductDetail");


        mRecyclerView = (RecyclerView) view.findViewById(R.id.review_list);
        no_reviews = (TextView) view.findViewById(R.id.no_reviews);
        reviewHeaderText = (TextView) view.findViewById(R.id.tv_review_header);
        review_progress = (ProgressBar) view.findViewById(R.id.review_progress);
        setUpReviewRecylerView();
        getAllReviews();

        reviewHeader = getArguments().getString("top");
        if (null != reviewHeader) {
            if (reviewHeader.equals("top")) {
                reviewHeaderText.setText(activity.getString(R.string.top_reviews));
            } else {
                reviewHeaderText.setText(activity.getString(R.string.customer_reviews));
            }
        }
        return view;
    }

    private void getAllReviews() {

        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getActivity().getString(R.string.no_internet), getActivity());
            return;
        }
        review_progress.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_SINGLE_PRODUCT_REVIEW_URL + productDetail.getProductId());

        AndyUtils.appLog("Ashutosh", "SingleReviewMap" + map);
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_SINGLE_PRODUCT_REVIEW, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.ProductAllReviewsFragment;
    }

    private void setUpReviewRecylerView() {
        mReviewListAdapter = new ReviewListAdapter(reviewsArrayList, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setAdapter(mReviewListAdapter);
    }


    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_SINGLE_PRODUCT_REVIEW:
                review_progress.setVisibility(View.GONE);
                AndyUtils.appLog("Ashutosh", "SingleReviewResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        JSONObject responseObject = jsonObject.optJSONObject(Const.RESPONSE);
                        JSONArray ratingJsonArray = responseObject.optJSONArray("rating");
                        if (ratingJsonArray != null) {
                            for (int i = 0; i < ratingJsonArray.length(); i++) {
                                JSONObject ratingObj = ratingJsonArray.getJSONObject(i);
                                ProductDetail.Reviews reviews = new ProductDetail.Reviews();
                                reviews.setStar(ratingObj.optDouble("stars"));
                                reviews.setComment(ratingObj.optString("comment"));
                                JSONObject userObj = ratingObj.optJSONObject("user");
                                if (null != userObj) {
                                    reviews.setName(userObj.optString(Const.Params.NAME));
                                    JSONObject logoObj = userObj.optJSONObject("logo");
                                    if (logoObj != null) {
                                        JSONObject cdnObj = logoObj.getJSONObject("cdn");
                                        reviews.setImageUrl(cdnObj.optString("url"));
                                    } else reviews.setImageUrl("");
                                    reviewsArrayList.add(reviews);
                                }
                            }
                        }

                        if (null != reviewHeader && reviewHeader.equals("top")) {
                            if (null != reviewsArrayList && reviewsArrayList.size() >= 5) {
                                ArrayList<ProductDetail.Reviews> reviewsList = new ArrayList<>();
                                reviewsList.addAll(reviewsArrayList);
                                reviewsArrayList.clear();
                                for (int i = 0; i < 5; i++) {
                                    reviewsArrayList.add(reviewsList.get(i));
                                }
                                mReviewListAdapter.notifyDataSetChanged();
                            } else {
                                mReviewListAdapter.notifyDataSetChanged();
                            }
                        } else {
                            mReviewListAdapter.notifyDataSetChanged();
                        }

                        if (reviewsArrayList.isEmpty())
                            no_reviews.setVisibility(View.VISIBLE);
                        else no_reviews.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }
}
