package com.patient.ghealth.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.FullImagesViewPagerAdapter;
import com.patient.ghealth.utils.Const;

import java.util.List;

import me.relex.circleindicator.CircleIndicator;

/**
 * A simple {@link Fragment} subclass.
 */
public class FullScreenImageFragment extends Fragment implements View.OnClickListener {

    private List<String> imageUrls;
    private int position;
    private ImageView[] dots;
    private FullImagesViewPagerAdapter mAdapter;
    private ViewPager product_images;
    private LinearLayout pager_indicator;
    private int dotsCount;
    private MainActivity activity;
    private CircleIndicator circleIndicator;

    public FullScreenImageFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_full_screen_image, container, false);
        activity = (MainActivity) getActivity();
        imageUrls = getArguments().getStringArrayList("images");
        position = getArguments().getInt("imagePosition");
        product_images = (ViewPager) view.findViewById(R.id.product_full_images_viewpager);
//        pager_indicator = (LinearLayout) view.findViewById(R.id.viewPagerCountDots);
        circleIndicator = (CircleIndicator) view.findViewById(R.id.ci_full_screen_images);
        setUpProductImages();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.FullScreenImageFragment;

    }

    private void setUpProductImages() {

        // Set product images
        mAdapter = new FullImagesViewPagerAdapter(getActivity(), imageUrls);
        product_images.setAdapter(mAdapter);
        circleIndicator.setViewPager(product_images);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_main_back_icon:
                break;
        }
    }


}
