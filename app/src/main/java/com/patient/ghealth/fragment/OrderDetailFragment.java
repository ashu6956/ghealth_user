package com.patient.ghealth.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.model.OrderDetail;
import com.patient.ghealth.utils.Const;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderDetailFragment extends Fragment implements View.OnClickListener {

    private OrderDetail orderDetail;
    private ImageView product_image;
    private TextView product_name, seller_name, variant, quantity, price,
            delivery_name, delivery_address, delivery_city, delivery_state, delivery_pincode, delivery_status, write_rating;
    private RatingBar ratingStar;
    private Button download_button;
    private LinearLayout return_package;
    private MainActivity activity;

    public OrderDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_order_detail, container, false);
        activity = (MainActivity) getActivity();
        orderDetail = (OrderDetail) getArguments().getSerializable("orderDetail");
        product_name = (TextView) view.findViewById(R.id.product_name);
        seller_name = (TextView) view.findViewById(R.id.seller_name);
        variant = (TextView) view.findViewById(R.id.variant);
        quantity = (TextView) view.findViewById(R.id.quantity);
        price = (TextView) view.findViewById(R.id.price);
        delivery_name = (TextView) view.findViewById(R.id.delivery_name);
        delivery_address = (TextView) view.findViewById(R.id.delivery_address);
        delivery_city = (TextView) view.findViewById(R.id.delivery_city);
        delivery_state = (TextView) view.findViewById(R.id.delivery_state);
        delivery_pincode = (TextView) view.findViewById(R.id.delivery_pincode);
        delivery_status = (TextView) view.findViewById(R.id.delivery_status);
        product_image = (ImageView) view.findViewById(R.id.product_image);
        write_rating = (TextView) view.findViewById(R.id.write_rating);
        ratingStar = (RatingBar) view.findViewById(R.id.ratingStar);
        download_button = (Button) view.findViewById(R.id.download_button);
        return_package = (LinearLayout) view.findViewById(R.id.return_package);
        return_package.setOnClickListener(this);

        write_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RatingFragment ratingFragment = new RatingFragment();
                Bundle args = new Bundle();
                args.putSerializable("orderDetail", orderDetail);
                ratingFragment.setArguments(args);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                activity.appLogo.setVisibility(View.GONE);
                activity.addFragment(ratingFragment, false, "Rating", "", false);
            }
        });

        if (!orderDetail.getProductDetail().getReviewList().isEmpty())
            ratingStar.setRating((float) orderDetail.getProductDetail().getReviewList().get(0).getStar());
        Glide.with(getActivity()).load(orderDetail.getProductDetail().getCat_images().get(0).getImage_url()).into(product_image);
        product_name.setText(orderDetail.getProductDetail().getProductName());
        if (orderDetail.getShopInfo() != null) {
            seller_name.setText(orderDetail.getShopInfo().getName());
        }
        if (orderDetail.getTracking().getStatus() != null) {
            delivery_status.setText(orderDetail.getTracking().getStatus());
        }
        if (orderDetail.getQuantity() != 0) {
            quantity.setText("" + orderDetail.getQuantity());
        }
        if (orderDetail.getPrice() != null) {
            price.setText(Const.RM + orderDetail.getPrice());
        }
        if (orderDetail.getVariant() != null && !orderDetail.getVariant().equals("null"))
            variant.setText(getString(R.string.variant_dot) + orderDetail.getVariant());
        else if (orderDetail.getLicense() != null && !orderDetail.getLicense().equals("null"))
            variant.setText(getString(R.string.license_dot) + orderDetail.getLicense());
        else variant.setVisibility(View.GONE);

        if (orderDetail.getAddress().getNAME() != null)
            delivery_name.setText(orderDetail.getAddress().getNAME());
        else delivery_name.setVisibility(View.GONE);

        if (orderDetail.getAddress().getADDRESS() != null)
            delivery_address.setText(orderDetail.getAddress().getADDRESS());
        else delivery_address.setVisibility(View.GONE);

        if (orderDetail.getAddress().getSTATE() != null)
            delivery_state.setText(orderDetail.getAddress().getSTATE());
        else delivery_state.setVisibility(View.GONE);

        if (orderDetail.getAddress().getCITY() != null)
            delivery_city.setText(orderDetail.getAddress().getCITY());
        else delivery_city.setVisibility(View.GONE);

        if (orderDetail.getAddress().getPINCODE() != null)
            delivery_pincode.setText(orderDetail.getAddress().getPINCODE());
        else delivery_pincode.setVisibility(View.GONE);

        if (orderDetail.getDownload_token() != null && !orderDetail.getDownload_token().isEmpty() && !orderDetail.getDownload_token().equals("null")) {
            download_button.setVisibility(View.VISIBLE);
        } else {
            download_button.setVisibility(View.GONE);
        }

        download_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String url = EndPoints.getDigitalDownloadUrl(orderDetail.getProductDetail().get_id(),orderDetail.getDownload_token());
//                Intent i = new Intent(Intent.ACTION_VIEW);
//                i.setData(Uri.parse(url));
//                startActivity(i);
            }
        });

        return view;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.return_package:
//                gotoReturnFragment();
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.ORDER_DETAILS_FRAGMENT;
    }


    //    private void gotoReturnFragment() {
//        ReturnProductFragment returnProductFragment = new ReturnProductFragment();
//        Bundle args = new Bundle();
//        args.putSerializable("orderDetail", orderDetail);
//        returnProductFragment.setArguments(args);
//
//        FragmentTransaction transaction = getActivity().getFragmentManager().beginTransaction();
//        transaction.replace(R.id.order_container, returnProductFragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }
}
