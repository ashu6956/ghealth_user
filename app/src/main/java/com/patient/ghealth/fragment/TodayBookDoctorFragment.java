package com.patient.ghealth.fragment;


import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.AddCardActivity;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.GetCardsAdapter;
import com.patient.ghealth.adapter.SimpleDividerItemDecoration;
import com.patient.ghealth.adapter.TodayAndTomorrowBookDoctorAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.CardDetails;
import com.patient.ghealth.model.DoctorBookingScheduleSlots;
import com.patient.ghealth.model.DoctorBookingSlotsData;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 9/1/2016.
 */
public class TodayBookDoctorFragment extends Fragment implements AsyncTaskCompleteListener {

    public static TodayAndTomorrowBookDoctorAdapter doctorSlotsAdapter;
    DecimalFormat form = new DecimalFormat("0.00");
    private TextView todayDate, no_slots_text;
    private String newFormattedDate, star_time;
    private RecyclerView doctorSlotsRecyclerView;
    private List<DoctorBookingScheduleSlots> scheduleSlotsList;
    private List<DoctorBookingSlotsData> morningBookingSlotsList, afternoonBookingSlotsList, eveningBookingSlotsList;
    private ProgressDialog todayProgressDialog;
    private Button bookNow;
    private ArrayList<String> selectedSlotList;
    private Dialog paymentDialog;
    private DoctorOnLineDetails onLineDetails;
    private List<CardDetails> cardDetailsList;
    private String paymentType = "";
    private ProgressBar bookingProgressBar;
    private String currentTime = "", currentDTime = "", timezone = "";
    private Dialog debtAmountDialog;
    private boolean isNetDialogShowing = false;
    private String currency = "", manufacturer = "";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        DateFormat df = new SimpleDateFormat("hh:mm aa");
//        DateFormat time_zone = new SimpleDateFormat("aa");
//        Date date = new Date();
////        curreenttime = df.format(date);
//        timezone = time_zone.format(date);

        manufacturer = Build.MANUFACTURER;
        AndyUtils.appLog("Device Name", manufacturer);

        currency = (String) PreferenceHelper.getParam(getActivity(), Const.Params.CURRENCY, "");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_today_tomorrow_docotor_book_layout, container, false);
        todayDate = (TextView) view.findViewById(R.id.tv_doctor_availability_date);
        no_slots_text = (TextView) view.findViewById(R.id.no_slots_text);
        doctorSlotsRecyclerView = (RecyclerView) view.findViewById(R.id.rv_today_tomorrow_doctor_slots);
        bookNow = (Button) view.findViewById(R.id.bn_doctor_book_now_slots);
        bookingProgressBar = (ProgressBar) view.findViewById(R.id.booking_progressBar);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd,MMM,yyyy", Locale.getDefault());
        String formattedDate = df.format(c.getTime());
        todayDate.setText(formattedDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        newFormattedDate = dateFormat.format(c.getTime());
        scheduleSlotsList = new ArrayList<>();
        DateFormat current = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
        DateFormat currentD = new SimpleDateFormat("HH:mm aa", Locale.getDefault());
        currentTime = current.format(Calendar.getInstance().getTime());
        currentDTime = currentD.format(Calendar.getInstance().getTime());
        AndyUtils.appLog("CurrentTime", currentTime);
        AndyUtils.appLog("CurrentTime", currentDTime);
        AndyUtils.appLog("NewFormattedDate", newFormattedDate);
        bookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (scheduleSlotsList.size() > 0) {
                    selectedSlotList = new ArrayList<String>();
                    for (int i = 0; i < scheduleSlotsList.size(); i++) {
                        for (int j = 0; j < scheduleSlotsList.get(i).getDoctorBookingSlotsDataList().size(); j++) {
                            DoctorBookingSlotsData doctorBookingSlotsData = scheduleSlotsList.get(i).getDoctorBookingSlotsDataList().get(j);
                            if (doctorBookingSlotsData.isSelected()) {
                                star_time = doctorBookingSlotsData.getStartTime() + " " + doctorBookingSlotsData.getTimeZone();
                                selectedSlotList.add(doctorBookingSlotsData.getAvailableSlotId());
                                AndyUtils.appLog("SelctedSlotsId", doctorBookingSlotsData.getAvailableSlotId());
                                AndyUtils.appLog("Selected_time", doctorBookingSlotsData.getStartTime() + " " + doctorBookingSlotsData.getTimeZone());
                            }
                        }
                    }
                    if (selectedSlotList.size() == 1) {
                        DateFormat current = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
                        String currentTime = current.format(Calendar.getInstance().getTime());
                        AndyUtils.appLog("time_status", "" + currentTime);
                        boolean time_status = checkBeforeTime(currentTime, star_time);
                        AndyUtils.appLog("time_status", "" + time_status);
                        if (time_status) {
                            getAddedCard();
                        } else {
                            AndyUtils.showShortToast("Time slot was expired.", getActivity());
                        }

                    } else if (selectedSlotList.size() > 1) {
                        AndyUtils.showShortToast(getString(R.string.please_select_one_slot), getActivity());
                    } else {
                        AndyUtils.showShortToast(getString(R.string.please_choose_any_slot), getActivity());
                    }
                } else {
                    AndyUtils.showShortToast(getString(R.string.no_slots), getActivity());
                }

            }
        });

        return view;
    }


    private void getAddedCard() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getActivity().getString(R.string.no_internet), getActivity());
            return;
        }
//        AndyUtils.showSimpleProgressDialog(mContext, "Fetching All Cards...", false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.GET_ADDED_CARDS_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, ""))
                + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "GetAddedCardMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_ADDED_CARDS, this);


    }


    private void createBookingSlotsRequest(ArrayList<String> selectedSlotList) {

        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }

        LatLng currentLatLng = PreferenceHelper.getObject(getActivity(), Const.CURRENT_LATLNG);

//        if (currentLatLng != null) {
        AndyUtils.showSimpleProgressDialog(getActivity(), getString(R.string.please_wait), false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_DOCTOR_SLOTS_BOOKING_REQUEST_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.S_LATITUDE, String.valueOf(currentLatLng.latitude));
        map.put(Const.Params.S_LONGITUDE, String.valueOf(currentLatLng.longitude));
        map.put(Const.Params.PAYMENT_MODE, paymentType);
        map.put(Const.Params.AVAILABLE_SLOTS_IDS, getStringLine(selectedSlotList));

        AndyUtils.appLog("Ashutosh", "CreateBookingSlotsMap" + map);

        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.CREATE_DOCTOR_SLOTS_BOOKING_REQUEST, this);


//        }
//        else
//        {
//            AndyUtils.appLog("CurrentLatLng","Not Available");
//        }


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String doctorId = (String) PreferenceHelper.getParam(getActivity(), Const.DOCTOR_ID, "");
        AndyUtils.appLog("TodayBookDoctorFragment", "doctorId" + doctorId);
        getDoctorTodaySlots(doctorId);
    }

    private void getDoctorTodaySlots(String doctorId) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }

        bookingProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();

        map.put(Const.Params.URL, Const.ServiceType.GET_DOCTOR_BOOKING_SLOTS_AVAILABILITY_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.DOCTOR_ID + "="
                + String.valueOf(doctorId) + "&" + Const.Params.DATE + "=" + newFormattedDate);

        AndyUtils.appLog("Ashutosh", "DoctorBookSlotsAvailabilityMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_DOCTOR_BOOKING_SLOTS_AVAILABILITY, this);

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_DOCTOR_BOOKING_SLOTS_AVAILABILITY:
                bookingProgressBar.setVisibility(View.GONE);
                AndyUtils.appLog("Ashutosh", "DoctorBookTodaySLotsResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    List<DoctorBookingSlotsData> bookingSlotsDataList;
                    if (jsonObject.getString("success").equals("true")) {
                        bookingSlotsDataList = new ArrayList<>();
                        morningBookingSlotsList = new ArrayList<>();
                        afternoonBookingSlotsList = new ArrayList<>();
                        eveningBookingSlotsList = new ArrayList<>();
                        JSONArray slotsArray = jsonObject.optJSONArray("slots");
                        if (slotsArray != null) {
                            if (slotsArray.length() != 0) {
                                for (int i = 0; i < slotsArray.length(); i++) {
                                    JSONObject slotsObject = slotsArray.getJSONObject(i);
                                    DoctorBookingSlotsData slotsData = new DoctorBookingSlotsData();
                                    slotsData.setAvailableSlotId(slotsObject.getString("available_slot_id"));
                                    slotsData.setStartTime(slotsObject.getString("start_time"));
                                    slotsData.setEndTime(slotsObject.getString("end_time"));
                                    slotsData.setShiftType(slotsObject.getString("type"));
                                    slotsData.setSlotBooked(slotsObject.getString("booked"));
                                    bookingSlotsDataList.add(slotsData);
                                }


                                AndyUtils.appLog("Size of today slotsList", bookingSlotsDataList.size() + "");
                                for (int i = 0; i < bookingSlotsDataList.size(); i++) {
                                    if (bookingSlotsDataList.get(i).getShiftType().equals("m")) {
                                        if (manufacturer.equalsIgnoreCase(Const.SAMSUNG)) {
                                            bookingSlotsDataList.get(i).setTimeZone("AM");
                                            if (checkMorningTimings(currentTime, bookingSlotsDataList.get(i).getStartTime() + " " + "AM")) {
                                                morningBookingSlotsList.add(bookingSlotsDataList.get(i));
                                            }
                                        } else {
                                            String morningTimeZone = getActivity().getResources().getString(R.string.time_zone_morning);
                                            bookingSlotsDataList.get(i).setTimeZone(morningTimeZone);
                                            if (checkMorningTimings(currentTime, bookingSlotsDataList.get(i).getStartTime() + " " + morningTimeZone)) {
                                                morningBookingSlotsList.add(bookingSlotsDataList.get(i));
                                            }
                                        }
                                    } else if (bookingSlotsDataList.get(i).getShiftType().equals("a")) {
                                        if (manufacturer.equalsIgnoreCase(Const.SAMSUNG)) {
                                            bookingSlotsDataList.get(i).setTimeZone("PM");
                                            if (checktimings(currentTime, bookingSlotsDataList.get(i).getStartTime() + " " + "PM")) {
                                                afternoonBookingSlotsList.add(bookingSlotsDataList.get(i));
                                            }
                                        } else {
                                            timezone = getActivity().getResources().getString(R.string.time_zone_evening);
                                            bookingSlotsDataList.get(i).setTimeZone(timezone);
                                            if (checktimings(currentTime, bookingSlotsDataList.get(i).getStartTime() + " " + timezone)) {
                                                afternoonBookingSlotsList.add(bookingSlotsDataList.get(i));
                                            }
                                        }
                                    } else if (bookingSlotsDataList.get(i).getShiftType().equals("e")) {
                                        if (manufacturer.equalsIgnoreCase(Const.SAMSUNG)) {
                                            bookingSlotsDataList.get(i).setTimeZone("PM");
                                            if (checktimings(currentTime, bookingSlotsDataList.get(i).getStartTime() + " " + "PM")) {
                                                eveningBookingSlotsList.add(bookingSlotsDataList.get(i));
                                            }
                                        } else {
                                            timezone = getActivity().getResources().getString(R.string.time_zone_evening);
                                            bookingSlotsDataList.get(i).setTimeZone(timezone);
                                            if (checktimings(currentTime, bookingSlotsDataList.get(i).getStartTime() + " " + timezone)) {
                                                eveningBookingSlotsList.add(bookingSlotsDataList.get(i));
                                            }
                                        }
                                    }
                                }

                                if (morningBookingSlotsList.size() > 0) {
//                                    Collections.sort(morningBookingSlotsList,new SlotsComparator());
                                    DoctorBookingScheduleSlots scheduleSlots = new DoctorBookingScheduleSlots();
                                    scheduleSlots.setType("Morning");
                                    scheduleSlots.setDoctorBookingSlotsDataList(morningBookingSlotsList);
                                    scheduleSlotsList.add(scheduleSlots);
                                }
                                if (afternoonBookingSlotsList.size() > 0) {
//                                    Collections.sort(afternoonBookingSlotsList,new SlotsComparator());
                                    DoctorBookingScheduleSlots scheduleSlots = new DoctorBookingScheduleSlots();
                                    scheduleSlots.setType("Afternoon");
                                    scheduleSlots.setDoctorBookingSlotsDataList(afternoonBookingSlotsList);
                                    scheduleSlotsList.add(scheduleSlots);
                                }
                                if (eveningBookingSlotsList.size() > 0) {
//                                    Collections.sort(eveningBookingSlotsList,new SlotsComparator());
                                    DoctorBookingScheduleSlots scheduleSlots = new DoctorBookingScheduleSlots();
                                    scheduleSlots.setType("Evening");
                                    scheduleSlots.setDoctorBookingSlotsDataList(eveningBookingSlotsList);
                                    scheduleSlotsList.add(scheduleSlots);

                                }

                                JSONArray doctorJsonArray = jsonObject.optJSONArray("doctor_data");
                                JSONObject doctorJsonObject = doctorJsonArray.getJSONObject(0);
                                onLineDetails = new DoctorOnLineDetails();
                                onLineDetails.setDoctorOnLineId(doctorJsonObject.optString("doctor_id"));
                                onLineDetails.setDoctorName(doctorJsonObject.getString("doctor_name"));
                                onLineDetails.setDoctorClinicName(doctorJsonObject.getString("c_name"));
                                onLineDetails.setClinicAddress(doctorJsonObject.optString("c_street"));
                                onLineDetails.setDoctorChatConsultFee(doctorJsonObject.getString("booking_fee"));
                                onLineDetails.setDoctorNationality(doctorJsonObject.optString("nationality"));
                            }

                            doctorSlotsAdapter = new TodayAndTomorrowBookDoctorAdapter(getActivity(), scheduleSlotsList);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                            doctorSlotsRecyclerView.setLayoutManager(layoutManager);
                            doctorSlotsRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
                            doctorSlotsRecyclerView.setAdapter(doctorSlotsAdapter);
                            if (scheduleSlotsList.size() > 0)
                                no_slots_text.setVisibility(View.GONE);
                            else no_slots_text.setVisibility(View.VISIBLE);
                        } else {
                            no_slots_text.setVisibility(View.VISIBLE);
//                            AndyUtils.showShortToast(jsonObject.getString("error_message"), getActivity());
                            return;
                        }
                    } else {
                        AndyUtils.showShortToast(jsonObject.getString("error_message"), getActivity());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

            case Const.ServiceCode.CREATE_DOCTOR_SLOTS_BOOKING_REQUEST:
                AndyUtils.appLog("Ashutosh", "CREATEDOCTORSLOTSRESPONSE" + response);
                try {
                    AndyUtils.removeProgressDialog();
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        AndyUtils.showLongToast(getString(R.string.slots_booked), getActivity());
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();

                    } else {
                        AndyUtils.removeProgressDialog();
                        if (jsonObject.optString("error_message").equalsIgnoreCase("You have previous payment pending")) {
                            String amount = jsonObject.optString("amount");
                            showDebtAmountDialog(amount);

                        } else {
                            AndyUtils.showShortToast(jsonObject.optString("error_message"), getActivity());
                        }
                    }
                } catch (JSONException e) {
                    AndyUtils.removeProgressDialog();
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_ADDED_CARDS:
                AndyUtils.appLog("Ashutosh", "GetAddedCardResponse" + response);
                try {

                    cardDetailsList = new ArrayList<>();
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {

                        JSONArray jsonArray = jsonObject.getJSONArray("cards");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject cardObject = jsonArray.getJSONObject(i);
                                CardDetails cardDetails = new CardDetails();
                                cardDetails.setCardId(cardObject.optString("id"));
                                cardDetails.setCardNumber(cardObject.optString("last_four"));
                                cardDetails.setIsDefault(cardObject.optString("is_default"));
                                cardDetails.setType(cardObject.optString("card_type"));
                                cardDetailsList.add(cardDetails);
                            }

                        }
                        showPaymentDialog(cardDetailsList);
                    } else {
                        showPaymentDialog(cardDetailsList);
//                        AndyUtils.showShortToast(jsonObject.getString("error_message"), getActivity());
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CLEAR_DEBT_AMOUNT:
                try {
                    AndyUtils.appLog("Ashutosh", "DebtClearResponse" + response);
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        AndyUtils.showShortToast(jsonObject.optString("message"), getActivity());
                    } else {
                        AndyUtils.showShortToast(jsonObject.optString("error_message"), getActivity());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


        }

    }

    private boolean checkMorningTimings(String time, String startTime) {

        try {
            String pattern = "hh:mm aa";
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);

            try {
                Date date1 = sdf.parse(startTime);
                Date date2 = sdf.parse(time);

                if (date1.after(date2)) {
                    return true;
                } else {

                    return false;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean checkBeforeTime(String time, String startTime) {

        try {
            String pattern = "hh:mm aa";
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);

            AndyUtils.appLog(" checkBefore Time And StartTime", time + " " + startTime);

            try {
                Date date1 = sdf.parse(startTime);
                Date date2 = sdf.parse(time);

                if (date1.before(date2)) {
                    return false;
                } else {
                    return true;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean checktimings(String time, String startTime) {

        try {
            String pattern = "hh:mm aa";
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);

            AndyUtils.appLog(" check Time And StartTime", time + " " + startTime);

            try {
                Date date1 = sdf.parse(startTime);
                Date date2 = sdf.parse(time);
                AndyUtils.appLog("Date1 and Date2", date1.toString() + " " + date2.toString());

                if (date1.after(date2)) {
                    return true;
                } else {
                    return false;
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private String getStringLine(ArrayList<String> stringList) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < stringList.size(); i++) {
            sb.append(stringList.get(i));
            if (i != stringList.size() - 1)
                sb.append(",");

        }

        return sb.toString();
    }

    private void showPaymentDialog(final List<CardDetails> cardList) {
        paymentType = "";
        paymentDialog = new Dialog(getActivity(), R.style.DialogDocotrTheme);
        paymentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        paymentDialog.setContentView(R.layout.dialog_payment_mode_layout);
        final RecyclerView paymentRecyclerView = (RecyclerView) paymentDialog.findViewById(R.id.rv_card_details);
        paymentRecyclerView.setVisibility(View.GONE);
        final LinearLayout paymentLinearLayout = (LinearLayout) paymentDialog.findViewById(R.id.ll_no_card_added);
        paymentLinearLayout.setVisibility(View.GONE);
        final Button addCardButton = (Button) paymentDialog.findViewById(R.id.bn_payment_add_card);
        addCardButton.setVisibility(View.GONE);
        final ImageButton cashButton = (ImageButton) paymentDialog.findViewById(R.id.ib_payment_cash);
        final ImageButton cardButton = (ImageButton) paymentDialog.findViewById(R.id.ib_payment_card);
        TextView appointmentCharge = (TextView) paymentDialog.findViewById(R.id.tv_payment_appointment_charge);
        TextView doctorName = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_name);
        TextView clinicName = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_clinic_name);
        TextView clinicAddress = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_clinic_address);
        final Button paymentButton = (Button) paymentDialog.findViewById(R.id.bn_payment_confirm);
        doctorName.setText(onLineDetails.getDoctorName());
        clinicName.setText(onLineDetails.getDoctorClinicName());

        clinicAddress.setText(onLineDetails.getClinicAddress());
        appointmentCharge.setText(getString(R.string.charge) + " " + currency + onLineDetails.getDoctorChatConsultFee() + " " + getString(R.string.per_appointment));
        paymentDialog.show();

        paymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (paymentType.equals("")) {
                    AndyUtils.showShortToast(getString(R.string.please_choose_payment_mode), getActivity());
                } else {
                    paymentDialog.cancel();
                    createBookingSlotsRequest(selectedSlotList);
                }
            }
        });
        cardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                paymentType = Const.CARD;
                cardButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.doctor_booked_bg));
                cashButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.doctor_available_bg));
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                paymentRecyclerView.setLayoutManager(layoutManager);

                if (cardList.size() > 0) {
                    paymentRecyclerView.setVisibility(View.VISIBLE);
                    GetCardsAdapter adapter = new GetCardsAdapter(getActivity(), cardList);
                    paymentRecyclerView.setAdapter(adapter);
                } else {
                    paymentRecyclerView.setVisibility(View.GONE);
                    paymentLinearLayout.setVisibility(View.VISIBLE);
                    addCardButton.setVisibility(View.VISIBLE);
                    addCardButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), AddCardActivity.class);
                            startActivity(intent);
                            paymentDialog.cancel();
                        }
                    });
                    paymentButton.setEnabled(false);
                    paymentButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.disable_book_consult_bg));

                }

            }
        });
        cashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentType = Const.CASH;
                paymentRecyclerView.setVisibility(View.GONE);
                cardButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.doctor_available_bg));
                cashButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.doctor_booked_bg));
                paymentLinearLayout.setVisibility(View.GONE);
                addCardButton.setVisibility(View.GONE);
                paymentButton.setEnabled(true);
                paymentButton.setBackgroundDrawable(getResources().getDrawable(R.drawable.book_consult_bg));
            }
        });

    }


    private void showDebtAmountDialog(String amount) {
        isNetDialogShowing = true;
        debtAmountDialog = new Dialog(getActivity());
        debtAmountDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        debtAmountDialog.setContentView(R.layout.dialog_debt_amount_layout);
        TextView debtAmountHeader = (TextView) debtAmountDialog.findViewById(R.id.tv_debt_header);
        debtAmountHeader.setText(getString(R.string.you_have) + " " + currency + form.format(Double.valueOf(amount)) + " " + getString(R.string.amount_pending));
        TextView exit = (TextView) debtAmountDialog.findViewById(R.id.tv_debt_cancel);
        exit.setText(getString(R.string.cancel));
        TextView payNow = (TextView) debtAmountDialog.findViewById(R.id.tv_debt_payNow);
        payNow.setText(getString(R.string.pay_now));

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeInternetDialog();
//                activity.finish();
            }
        });

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeInternetDialog();
                clearDebtAmount();
            }
        });
        debtAmountDialog.setCancelable(false);
        debtAmountDialog.show();
    }

    private void removeInternetDialog() {
        if (debtAmountDialog != null && debtAmountDialog.isShowing()) {
            debtAmountDialog.dismiss();
            isNetDialogShowing = false;
            debtAmountDialog = null;

        }
    }


    private void clearDebtAmount() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getActivity().getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CLEAR_DEBT_AMOUNT_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "DebtAmountMap" + map);
        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.CLEAR_DEBT_AMOUNT, this);
    }


    class SlotsComparator implements Comparator<DoctorBookingSlotsData> {

        @Override
        public int compare(DoctorBookingSlotsData tv1, DoctorBookingSlotsData tv2) {
            try {

                return tv1.getStartTime().compareTo(tv2.getStartTime());

            } catch (Exception e) {
                e.printStackTrace();
            }

            return 0;
        }
    }


}
