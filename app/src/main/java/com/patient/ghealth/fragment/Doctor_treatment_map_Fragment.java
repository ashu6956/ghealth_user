//package com.patient.ghealth.fragment;
//
//import android.Manifest;
//import android.app.Dialog;
//import android.content.pm.PackageManager;
//import android.location.Location;
//import android.os.Bundle;
//import android.support.v4.app.ActivityCompat;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AutoCompleteTextView;
//import android.widget.Button;
//import android.widget.ImageButton;
//
//import com.doctor.ghealth.Location.LocationHelper;
//import com.doctor.ghealth.R;
//import com.doctor.ghealth.activity.MainActivity;
//import com.doctor.ghealth.adapter.PlacesAutoCompleteAdapter;
//import com.doctor.ghealth.utils.Const;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.MapView;
//import com.google.android.gms.maps.MapsInitializer;
//import com.google.android.gms.maps.model.BitmapDescriptorFactory;
//import com.google.android.gms.maps.model.CameraPosition;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//
///**
// * Created by user on 9/7/2016.
// */
//public class Doctor_treatment_map_Fragment extends BaseFragment implements LocationHelper.OnLocationReceived {
//    private GoogleMap googleMap;
//    private Bundle mBundle;
//    private MapView mMapView;
//    private View view;
//    private AutoCompleteTextView et_clientlocation, et_doctorlocation;
//    private PlacesAutoCompleteAdapter placeadapter;
//    private LocationHelper locHelper;
//    private Location myLocation;
//    private Marker markerpatient, markerdoctor;
//    private ImageButton btnMyLocation;
//    private Button btn_status;
//
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//
//        view = inflater.inflate(R.layout.treatmentfragment, container,
//                false);
//        et_clientlocation = (AutoCompleteTextView) view.findViewById(R.id.et_clientlocation);
//        et_doctorlocation = (AutoCompleteTextView) view.findViewById(R.id.et_doctorlocation);
//        btnMyLocation = (ImageButton) view.findViewById(R.id.btnMyLocation);
//        btn_status = (Button) view.findViewById(R.id.btn_status);
//        btn_status.setOnClickListener(this);
//        btnMyLocation.setOnClickListener(this);
//
//        return view;
//    }
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        mBundle = savedInstanceState;
//
//
//    }
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState) {
//        super.onActivityCreated(savedInstanceState);
//        MainActivity.mainTabLayout.setVisibility(View.GONE);
//        MainActivity.backbuttonImage.setVisibility(View.GONE);
//        MainActivity.iv_main_home_icon.setVisibility(View.VISIBLE);
//        MainActivity.notificationImage.setVisibility(View.GONE);
//        MainActivity.iv_location.setVisibility(View.VISIBLE);
//        MainActivity.toolbarHeader.setText("");
//        try {
//            MapsInitializer.initialize(getActivity());
//        } catch (Exception e) {
//        }
//
//        locHelper = new LocationHelper(activity);
//        locHelper.setLocationReceivedLister(this);
//        mMapView = (MapView) view.findViewById(R.id.jobMap);
//        mMapView.onCreate(mBundle);
//        setUpMap();
//        placeadapter = new PlacesAutoCompleteAdapter(activity,
//                R.layout.autocomplete_list_text);
//        et_clientlocation.setAdapter(placeadapter);
//        et_doctorlocation.setAdapter(placeadapter);
//    }
//
//    private void setUpMap() {
//        // map.
//        if (googleMap == null) {
//            googleMap = ((MapView) view.findViewById(R.id.jobMap)).getMap();
//            googleMap.getUiSettings().setMyLocationButtonEnabled(true);
//            if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                // TODO: Consider calling
//                //    ActivityCompat#requestPermissions
//                // here to request the missing permissions, and then overriding
//                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                //                                          int[] grantResults)
//                // to handle the case where the user grants the permission. See the documentation
//                // for ActivityCompat#requestPermissions for more details.
//                return;
//            }
//
//
//        }
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        mMapView.onResume();
//        activity.currentfragment = Const.TREATMENT_FRAGMENT;
//    }
//
//    @Override
//    public void onLocationReceived(LatLng latlong) {
//
//    }
//
//    @Override
//    public void onLocationReceived(Location location) {
//        if (location != null) {
//            // drawTrip(latlong);
//            myLocation = location;
//            LatLng latLang = new LatLng(location.getLatitude(),
//                    location.getLongitude());
//
//
//            if (latLang != null) {
//                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLang, 17));
//
//                CameraPosition cameraPosition = new CameraPosition.Builder()
//                        .target(latLang)      // Sets the center of the map to location user
//                        .zoom(17)                   // Sets the zoom
//                        .bearing(90)                  // Sets the tilt of the camera to 30 degrees
//                        .build();                   // Creates a CameraPosition from the builder
//                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//            }
//
//        }
//
//    }
//
//    @Override
//    public void onConntected(Bundle bundle) {
//
//
//        if (myLocation != null) {
//            LatLng latLang = new LatLng(myLocation.getLatitude(),
//                    myLocation.getLongitude());
//
//            MarkerOptions opt = new MarkerOptions();
//            opt.position(latLang);
//            opt.title("My Location");
//            opt.icon(BitmapDescriptorFactory
//                    .fromResource(R.drawable.doctor_pin));
//            markerdoctor = googleMap.addMarker(opt);
//        }
//    }
//
//    @Override
//    public void onConntected(Location location) {
//
//        if (location != null) {
//            myLocation = location;
//            LatLng latLang = new LatLng(location.getLatitude(),
//                    location.getLongitude());
//
//
//        }
//    }
//
//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.btnMyLocation:
//
//                if (markerdoctor.getPosition() != null)
//                    googleMap.animateCamera(CameraUpdateFactory
//                            .newLatLng(markerdoctor.getPosition()));
//                break;
//            case R.id.btn_status:
//                openinvoice();
//                break;
//            default:
//                break;
//        }
//    }
//
//    private void openinvoice() {
//        Log.d("mahi", "open");
//        final Dialog dialog = new Dialog(activity, R.style.DialogTheme);
//        dialog.setCancelable(true);
//        dialog.setContentView(R.layout.invoice);
//        dialog.setCancelable(true);
//        ImageButton ib_done = (ImageButton) dialog.findViewById(R.id.ib_done);
//        ib_done.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                activity.addFragment(new FeedbackFragment(), false, Const.FEEDBACK_FRAGMENT, true);
//
//            }
//        });
//        dialog.show();
//    }
//
//}
