package com.patient.ghealth.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.sundeepk.compactcalendarview.CompactCalendarView;
import com.github.sundeepk.compactcalendarview.domain.Event;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.MyScheduleSlotsAdapter;
import com.patient.ghealth.adapter.RecyclerViewItemClickListener;
import com.patient.ghealth.adapter.SimpleDividerItemDecoration;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.MyDateSchedule;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.MyAnalogClock;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 8/18/2016.
 */
public class MyScheduleFragment extends Fragment implements AsyncTaskCompleteListener {


    DateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    DateFormat monthFormate = new SimpleDateFormat("yyyy-MM",Locale.ENGLISH);
    String[] user_days;
    private MyAnalogClock scheduleClock;
    private MainActivity activity;
    private CompactCalendarView calendarView;
    private List<MyDateSchedule> myDateScheduleList;
    private SimpleDateFormat dateFormatForMonth = new SimpleDateFormat("MMMM  yyyy", Locale.getDefault());
    private TextView currentMonth, noslotsText;
    private RecyclerView myScheduleRecyclerView;
    private MyScheduleSlotsAdapter myScheduleSlotsAdapter;
    private String selecteddate = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_schedule_layout, container, false);
        activity = (MainActivity) getActivity();
        calendarView = (CompactCalendarView) view.findViewById(R.id.my_schedule_calendar_view);
        myScheduleRecyclerView = (RecyclerView) view.findViewById(R.id.rv_my_schedules);
        currentMonth = (TextView) view.findViewById(R.id.tv_schedule_current_month);
        noslotsText = (TextView) view.findViewById(R.id.tv_no_slots_text);
        user_days = getResources().getStringArray(R.array.days);
        calendarView.setDayColumnNames(user_days);
        currentMonth.setText(dateFormatForMonth.format(calendarView.getFirstDayOfCurrentMonth()));
        final Calendar c = Calendar.getInstance();
        final String currentFormattedDate = FORMATTER.format(c.getTime());
        AndyUtils.appLog("CurrentDate", currentFormattedDate);
        getMyScheduleDate();
        getMySchedulesDateSlots(currentFormattedDate);
        myDateScheduleList = new ArrayList<>();
        myScheduleSlotsAdapter = new MyScheduleSlotsAdapter(getActivity(), myDateScheduleList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        myScheduleRecyclerView.setLayoutManager(layoutManager);
        myScheduleRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
        myScheduleRecyclerView.setAdapter(myScheduleSlotsAdapter);
        calendarView.setListener(new CompactCalendarView.CompactCalendarViewListener() {
            @Override
            public void onDayClick(Date dateClicked) {
                selecteddate = FORMATTER.format(dateClicked);
                calendarView.setCurrentSelectedDayBackgroundColor(ContextCompat.getColor(activity, R.color.trns_white));
                getMySchedulesDateSlots(selecteddate);
            }

            @Override
            public void onMonthScroll(Date firstDayOfNewMonth)
            {
                calendarView.setCurrentSelectedDayBackgroundColor(ContextCompat.getColor(activity, R.color.trans));
                if (myDateScheduleList != null && myScheduleSlotsAdapter != null) {
                    myDateScheduleList.clear();
                    myScheduleSlotsAdapter.notifyDataSetChanged();
                    noslotsText.setVisibility(View.VISIBLE);
                }
                Calendar c = Calendar.getInstance();
                int year = c.get(Calendar.YEAR);
                int month = c.get(Calendar.MONTH) + 1;

                String curent_month = "";
                if (month < 10) {
                    curent_month = year + "-0" + month;
                } else {
                    curent_month = year + "-" + month;
                }
                String selected_month = monthFormate.format(firstDayOfNewMonth);

                Log.d("mahi", "current and scrolled month" + curent_month + " " + selected_month);

                if (curent_month.equals(selected_month)) {
                    Date today = c.getTime();
                    String newdate = FORMATTER.format(today);
                    getMySchedulesDateSlots(newdate);
                }
                currentMonth.setText(dateFormatForMonth.format(firstDayOfNewMonth));
            }
        });


        myScheduleRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(activity, new RecyclerViewItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if (myDateScheduleList != null && myDateScheduleList.size() > 0) {
                    if (myDateScheduleList.get(position).getIsCompleted().equals("1")) {
                        AndyUtils.showShortToast(getString(R.string.schedule_accepted), activity);
                    } else if (myDateScheduleList.get(position).getIsCompleted().equals("0")) {
                        AndyUtils.showShortToast(getString(R.string.waiting_schedule), activity);
                    }
                }

            }
        }));

        return view;
    }

    private void getMyScheduleDate() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        AndyUtils.showSimpleProgressDialog(getActivity(), getString(R.string.please_wait), false);

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_MY_DATE_SCHEDULE_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "GetMyScheduleDateMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_MY_DATE_SCHEDULE, this);


    }

    private void getMySchedulesDateSlots(String selectedDate) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.please_wait), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_MY_SCHEDULE_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.DATE + "="
                + selectedDate);

        AndyUtils.appLog("Ashutosh", "GetMyScheduleSlotsMap" + map);
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_MY_SCHEDULE, this);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.MyScheduleFragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.MyScheduleFragment);
        if (fragment != null && fragment.isResumed()) {
            AndyUtils.appLog("MyScheduleFragment", "fragment");
            manager.beginTransaction().remove(fragment).commit();
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_MY_DATE_SCHEDULE:
                AndyUtils.appLog("Ashutosh", "GetMyScheduleResponse" + response);
                try {

                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        JSONArray dateJsonArray = jsonObject.optJSONArray("data");
                        if (dateJsonArray != null) {
                            if (dateJsonArray.length() > 0) {
                                for (int i = 0; i < dateJsonArray.length(); i++) {
                                    JSONObject dateJsonObject = dateJsonArray.getJSONObject(i);
                                    String date_slot = dateJsonObject.getString("date");
                                    try {

                                        String[] items1 = date_slot.split("-");
                                        String year1 = items1[0];
                                        String month1 = items1[1];
                                        String date1 = items1[2];
                                        String n_date = date1 + "/" + month1 + "/" + year1;
                                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                                        Date date = sdf.parse(n_date);

                                        long startDate = date.getTime();

                                        Event ev1 = new Event(Color.RED, startDate);
                                        calendarView.addEvent(ev1, true);
                                        AndyUtils.appLog("StartDate", startDate + "");

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }

                        AndyUtils.removeProgressDialog();
                    } else {
                        AndyUtils.removeProgressDialog();
                    }
                } catch (JSONException e) {
                    AndyUtils.removeProgressDialog();
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_MY_SCHEDULE:
                AndyUtils.appLog("Ashutosh", "GetMyScheduleSlotsResponse" + response);
                try {

                    jsonObject = new JSONObject(response);
                    myDateScheduleList.clear();
                    if (myScheduleSlotsAdapter != null) {
                        myScheduleSlotsAdapter.notifyDataSetChanged();
                    }
                    if (jsonObject.getString("success").equals("true")) {
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        if (jsonArray != null) {
                            if (jsonArray.length() > 0)
                            {
                                for (int i = 0; i < jsonArray.length(); i++)
                                {
                                    JSONObject scheduleJsonObject = jsonArray.getJSONObject(i);
                                    MyDateSchedule myDateSchedule = new MyDateSchedule();
                                    myDateSchedule.setScheduleDate(scheduleJsonObject.optString("date"));
                                    myDateSchedule.setScheduleDoctorName(scheduleJsonObject.optString("doctor_name"));
                                    myDateSchedule.setScheduleClinicStreet(scheduleJsonObject.optString("c_street"));
                                    myDateSchedule.setScheduleClinicCity(scheduleJsonObject.optString("c_city"));
                                    myDateSchedule.setScheduleClinicState(scheduleJsonObject.optString("c_state"));
                                    myDateSchedule.setScheduleClinicCountry(scheduleJsonObject.optString("c_country"));
                                    myDateSchedule.setScheduleClinicPostal(scheduleJsonObject.optString("c_postal"));
                                    myDateSchedule.setScheduleStartTime(scheduleJsonObject.optString("start_time"));
                                    myDateSchedule.setScheduleEndTime(scheduleJsonObject.optString("end_time"));
                                    myDateSchedule.setScheduleClinicName(scheduleJsonObject.optString("c_name"));
                                    myDateSchedule.setTimeFormat(scheduleJsonObject.optString("ampm"));
                                    myDateSchedule.setIsCompleted(scheduleJsonObject.optString("is_completed"));
                                    myDateScheduleList.add(myDateSchedule);
                                }
                                if (myDateScheduleList.size() > 0)
                                {
                                    noslotsText.setVisibility(View.GONE);
                                }
                                myScheduleSlotsAdapter.notifyDataSetChanged();
                            } else {
                                noslotsText.setVisibility(View.VISIBLE);
                            }
                        } else {
                            noslotsText.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }

    }
}
