package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.adapter.ConsultHistoryAdapter;
import com.patient.ghealth.adapter.RecyclerViewItemClickListener;
import com.patient.ghealth.adapter.SimpleDividerItemDecoration;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.ConsultHistory;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 10/4/2016.
 */
public class ChatConsultHistoryFragment extends Fragment implements AsyncTaskCompleteListener {
    private List<ConsultHistory> consultHistoryList;
    private RecyclerView chatRecyclerView;
    private ConsultHistoryAdapter consultHistoryAdapter;
    private TextView noHistory;
    private MainActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_consult_history_item_layout, container, false);
        activity= (MainActivity) getActivity();
        chatRecyclerView = (RecyclerView) view.findViewById(R.id.rv_consult_history);
        noHistory = (TextView) view.findViewById(R.id.tv_no_consult_history);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        chatRecyclerView.setLayoutManager(layoutManager);
        getConsultHistoryDetails();

        chatRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(getActivity(), new RecyclerViewItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position)
            {
                if(consultHistoryList!=null && consultHistoryList.size()>0)
                {
                    ConsultHistory consultHistory=consultHistoryList.get(position);
                    ViewMessageHistoryFragment fragment = new ViewMessageHistoryFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("name", consultHistory.getDoctorName());
                    bundle.putString("request_id", consultHistory.getHistoryId());
                    bundle.putString("picture", consultHistory.getDoctorPictureUrl());
                    bundle.putString(Const.INCOMING_BUNDLE,Const.ChatConsultHistoryFragment);
                    fragment.setArguments(bundle);
                    activity.addFragment(fragment, false,getString(R.string.chat_history),Const.ViewMessageHistoryFragment, false);

                }

                }


        }));
        return view;
    }

    private void getConsultHistoryDetails() {
        if (!AndyUtils.isNetworkAvailable(activity)) {
            AndyUtils.showShortToast(getString(R.string.no_internet), activity);
            return;
        }
//        AndyUtils.showSimpleProgressDialog(getActivity(), getString(R.string.please_wait), false);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_CHAT_HISTORY_URl + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(activity, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(activity, Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "CreateConsultHistoryMap" + map);
        new HttpRequester(activity, Const.GET, map, Const.ServiceCode.CREATE_CHAT_HISTORY, this);


    }


    @Override
    public void onTaskCompleted(String response, int serviceCode) {
//        AndyUtils.removeProgressDialog();
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.CREATE_CHAT_HISTORY:
                AndyUtils.appLog("Ashutosh", "CreateConsultHistoryResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true"))
                    {
                        consultHistoryList = new ArrayList<>();
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0)
                        {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject historyJsonObject = jsonArray.optJSONObject(i);
                                ConsultHistory history = new ConsultHistory();
                                history.setHistoryId(historyJsonObject.optString("id"));
                                history.setDoctorName(historyJsonObject.optString("doctor_name"));
                                history.setHistoryType(historyJsonObject.optString("type"));
                                history.setConsultationFee(historyJsonObject.optString("fee"));
                                history.setDuration(historyJsonObject.optString("duration"));
                                history.setHistoryDate(historyJsonObject.optString("date"));
                                history.setDoctorNumber(historyJsonObject.optString("phone"));
                                history.setDoctorPictureUrl(historyJsonObject.optString("doctor_picture"));
                                consultHistoryList.add(history);
                            }
                            if (consultHistoryList.size() > 0) {
                                chatRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(activity.getResources()));
                                consultHistoryAdapter = new ConsultHistoryAdapter(activity, consultHistoryList);
                                Collections.reverse(consultHistoryList);
                                chatRecyclerView.setAdapter(consultHistoryAdapter);

                            } else {
                                chatRecyclerView.setVisibility(View.GONE);
                                noHistory.setVisibility(View.VISIBLE);
                            }
                        }
                        else {
                            chatRecyclerView.setVisibility(View.GONE);
                            noHistory.setVisibility(View.VISIBLE);
                        }
                    } else {
                        chatRecyclerView.setVisibility(View.GONE);
                        noHistory.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }
}
