package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.HealthStoreActivity;
import com.patient.ghealth.adapter.ProductListAdapter;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.ProductCategory;
import com.patient.ghealth.model.ProductDetail;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 11/21/2016.
 */
public class StoreProductListFragment extends Fragment implements AsyncTaskCompleteListener
{

    private RecyclerView productRecyclerView;
    private int START = 0, incrementRate = 10;
    private boolean isMoreDataAvailable = false, isLoadingData = false;
    private List<ProductDetail> productDetailList;
    private ProductListAdapter mProductListAdapter;
    private ProgressBar productProgressBar;
    private TextView noProductText;
    private HealthStoreActivity activity;
    private String categoryId;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productDetailList = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_product_list_layout,container,false);
        activity= (HealthStoreActivity) getActivity();
        productRecyclerView = (RecyclerView)view.findViewById(R.id.rv_product_details);
        productProgressBar = (ProgressBar)view. findViewById(R.id.product_progressBar);
        noProductText = (TextView)view. findViewById(R.id.tv_no_product);
        categoryId = (String) PreferenceHelper.getParam(getActivity(),Const.Params.CATEGORY_ID,"");
        setupRecyclerView();
        if(!categoryId.equals(""))
        {
            getCartCount();
            getProductDetails(categoryId);

        }

        return view;
    }


    private void  getCartCount()
    {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CART_COUNT_URL);

        HashMap<String,String> headerMap =new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(),Const.Params.ECOM_TOKEN,""));
        AndyUtils.appLog("Ashutosh","CartCountMap" +map);
        AndyUtils.appLog("Ashutosh","HeaderMap" +headerMap);
        new HttpRequester(getActivity(),Const.GET,map,Const.ServiceCode.GET_CART_COUNT,this,headerMap);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment=Const.StoreProductListFragment;
    }

    private void setupRecyclerView() {
        mProductListAdapter = new ProductListAdapter(getActivity(), productDetailList,activity);
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        productRecyclerView.setLayoutManager(mLayoutManager);
        productRecyclerView.setItemAnimator(new DefaultItemAnimator());
        productRecyclerView.setAdapter(mProductListAdapter);
        productRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int[] firstVisibleItemPositions = new int[2];
                    int pastVisiblesItems = ((LinearLayoutManager) productRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        if (!isLoadingData && isMoreDataAvailable) {
                            START = START + incrementRate;
                            if (!categoryId.equals("")) {
                                getProductDetails(categoryId);
                            }
                        }
                    }

                }
            }
        });
    }

    private void getProductDetails(String catId)
    {

        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        isLoadingData = true;
        productProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_STORE_PRODUCT_URL + Const.Params.CATEGORY + "=" + catId + "&start=" + START);

        AndyUtils.appLog("Ashutosh", "ProductMap" + map);
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_STORE_PRODUCT, this);
    }


    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        productProgressBar.setVisibility(View.GONE);
        JSONObject jsonObject = null;
        switch (serviceCode) {
            case Const.ServiceCode.GET_STORE_PRODUCT:
                AndyUtils.appLog("ashutosh", "StoreProductResponse" + response);
                try {
                    isLoadingData = false;
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS))
                    {
                        JSONObject responseObject = jsonObject.optJSONObject("response");
                        JSONArray productJsonArray = responseObject.getJSONArray("product");
                        if (productJsonArray.length() == 0) {
                            isMoreDataAvailable = false;
                        }
                        else isMoreDataAvailable = true;

                        if (productJsonArray != null && productJsonArray.length() > 0)
                            for (int i = 0; i < productJsonArray.length(); i++)
                            {
                                JSONObject productObj = productJsonArray.getJSONObject(i);
                                ProductDetail productDetail = new ProductDetail();
                                productDetail.setProductId(productObj.optString(Const.Params._ID));
                                productDetail.setProductName(productObj.optString("name"));
                                productDetail.setQuantity(productObj.optInt("quantity"));
                                productDetail.setDescription(productObj.optString("description"));
                                productDetail.setPaidByBuyer(productObj.optBoolean("paid_by_buyer"));
                                // get the pricing object information
                                JSONObject productPricingObj = productObj.getJSONObject("pricing");
                                ProductDetail.Price price = new ProductDetail.Price();
                                price.setAfter_discount(productPricingObj.optDouble("after_discount", -1));
                                price.setCommission(productPricingObj.optDouble("commission", -1));
                                price.setOriginal_price(productPricingObj.optDouble("original", -1));
                                price.setService_tax(productPricingObj.optDouble("service_tax", -1));
                                // add the pricing obj to main obj
                                productDetail.setPrice(price);
                                // get categories
//                        JSONArray categoryJsonArray = productObj.getJSONArray("categories");
//                        ArrayList<String> categoryArray = new ArrayList<String>();
//                        for (int g = 0; g < categoryJsonArray.length(); g++) {
//                            categoryArray.add(categoryJsonArray.optString(g));
//                        }
//                        productDetail.setCategories(categoryArray);
                                // get variants
                                ArrayList<ProductDetail.Variants> variantsArrayList = new ArrayList<ProductDetail.Variants>();
                                JSONArray VariantJsonArray = productObj.optJSONArray("variants");
                                for (int l = 0; l < VariantJsonArray.length(); l++) {
                                    JSONObject variantObj = VariantJsonArray.getJSONObject(l);
                                    ProductDetail.Variants variants = new ProductDetail.Variants();
                                    ProductDetail.Price price1 = new ProductDetail.Price();
                                    variants.setID(variantObj.optString(Const.Params._ID));
                                    variants.setName(variantObj.optString(Const.Params.NAME));
                                    variants.setQuantity(variantObj.optString(Const.QUANTITY));
                                    price1.setAfter_discount(variantObj.optDouble("after_discount", -1));
                                    price1.setCommission(variantObj.optDouble("commission", -1));
                                    price1.setOriginal_price(variantObj.optDouble("original", -1));
                                    price1.setService_tax(variantObj.optDouble("service_tax", -1));
                                    variants.setPrice(price1);
                                    variantsArrayList.add(variants);
                                }
                                productDetail.setVariantList(variantsArrayList);
                                // get Lincense
                                ArrayList<ProductDetail.License> licenseArrayList = new ArrayList<ProductDetail.License>();
                                JSONArray LicenseJsonArray = productObj.optJSONArray("licenses");
                                for (int l = 0; l < LicenseJsonArray.length(); l++) {
                                    JSONObject licenseObj = LicenseJsonArray.getJSONObject(l);
                                    ProductDetail.License licenses = new ProductDetail.License();
                                    ProductDetail.Price price2 = new ProductDetail.Price();
                                    licenses.setID(licenseObj.optString(Const.Params._ID));
                                    licenses.setQuantity(licenseObj.optString(Const.QUANTITY));
                                    price2.setAfter_discount(licenseObj.optDouble("after_discount", -1));
                                    price2.setCommission(licenseObj.optDouble("commission", -1));
                                    price2.setOriginal_price(licenseObj.optDouble("original", -1));
                                    price2.setService_tax(licenseObj.optDouble("service_tax", -1));
                                    licenses.setPrice(price2);
                                    licenseArrayList.add(licenses);
                                }
                                productDetail.setLicensesList(licenseArrayList);
                                // get seller info obj
                                // JSONObject sellerInfoObj = productObj.getJSONObject("created_by");
                                ProductDetail.Seller_info seller_info = new ProductDetail.Seller_info();
                                seller_info.set_id(productObj.optString("seller_id"));
                                seller_info.setSeller_name(productObj.optString("seller_name"));
                                // add seller obj to main obj
                                productDetail.setSellerInfo(seller_info);
                                // add rating
                                ProductDetail.Rating rating_info = new ProductDetail.Rating();
                                rating_info.setTotalRatings(productObj.optInt("total_ratings"));
                                rating_info.setTotalStar(productObj.optInt("total_star"));
                                rating_info.setTotalReviews(productObj.optInt("total_reviews"));
                                // add rating obj to main obj
                                productDetail.setRating(rating_info);
                                //add shipping info
                                JSONObject shippingObject = productObj.optJSONObject("shipping_details");
                                ProductDetail.ShippingDetails shippingDetails = new ProductDetail.ShippingDetails();
                                shippingDetails.setDuration(shippingObject.optString("duration"));
                                shippingDetails.setFee(shippingObject.optDouble("fee"));
                                shippingDetails.setUnit(shippingObject.optString("unit"));
                                shippingDetails.setWeight(shippingObject.optDouble("weight"));
                                productDetail.setShippingDetails(shippingDetails);

                                //get the image object information
                                JSONArray productImageJsonArray = productObj.getJSONArray("images");
                                ArrayList<ProductDetail.Cat_image> catImageArrayList = new ArrayList<ProductDetail.Cat_image>();
                                for (int j = 0; j < productImageJsonArray.length(); j++) {
                                    JSONObject productImageObj = productImageJsonArray.getJSONObject(j);
                                    JSONObject productImageCdnObj = productImageObj.getJSONObject("cdn");
                                    ProductDetail.Cat_image catImage = new ProductDetail.Cat_image();
                                    catImage.set_id(productImageCdnObj.optString(Const.Params._ID));
                                    catImage.setImage_url(productImageCdnObj.optString("url"));
                                    catImageArrayList.add(catImage);
                                }
                                // add category image list to main obj
                                productDetail.setCat_images(catImageArrayList);

                                productDetailList.add(productDetail);
                            }
                        if (productDetailList.isEmpty()) {
                            noProductText.setVisibility(View.VISIBLE);
                        } else
                            noProductText.setVisibility(View.GONE);

                        AndyUtils.appLog("Size of ProductList",productDetailList.size()+"");
                        mProductListAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_CART_COUNT:
                AndyUtils.appLog("Ashutosh","CartCountResponse" +response);
                try {
                    jsonObject=new JSONObject(response);
                    if(jsonObject.optString(Const.STATUS).equals(Const.SUCCESS))
                    {
                        JSONObject mainObj=jsonObject.optJSONObject(Const.RESPONSE);
                        int count=mainObj.optInt("count");
                        activity.cartCount.setText(count+"");

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

        }
    }
}
