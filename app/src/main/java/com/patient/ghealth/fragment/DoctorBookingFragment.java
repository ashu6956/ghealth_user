package com.patient.ghealth.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.DoctorListActivity;
import com.patient.ghealth.adapter.DoctorListAdapter;
import com.patient.ghealth.adapter.SimpleDividerItemDecoration;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.custom_interface.DoctorBookAvailabilityListener;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by getit on 8/9/2016.
 */
public class DoctorBookingFragment extends Fragment implements DoctorBookAvailabilityListener, AsyncTaskCompleteListener {

    private static final String TAG = DoctorBookingFragment.class.getSimpleName();
    public static DoctorListAdapter doctorListAdapter;
    ProgressBar online_progress;
    TextView no_doc_text;
    private RecyclerView doctorListRecyclerView;
    private DoctorListActivity activity;
    private int clickedPosition;
    private List<DoctorOnLineDetails> onLineDetailsList;
    private ProgressDialog bookingProgressDialog;
    private EditText searchEditText;
    private int START = 0, incrementRate = 6;
    private boolean isMoreDataAvailable = false, isLoadingData = false;
    private String doctorSpecialistsId = "";
    private boolean firstTime = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onLineDetailsList = new ArrayList<DoctorOnLineDetails>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_doctor_online_consult, container, false);
        activity = (DoctorListActivity) getActivity();
        online_progress = (ProgressBar) view.findViewById(R.id.online_progress);
        no_doc_text = (TextView) view.findViewById(R.id.no_doc_text);
        doctorListRecyclerView = (RecyclerView) view.findViewById(R.id.rv_doctor_list);
        searchEditText = (EditText) view.findViewById(R.id.et_doctorList_search);
        doctorSpecialistsId = (String) PreferenceHelper.getParam(getActivity(), Const.SPECIALITIES_DOCTOR_ID, "");
        if (!doctorSpecialistsId.equals("")) {
            AndyUtils.appLog(TAG, "DoctorSpecialistId" + doctorSpecialistsId);
            getDoctorBookingConsultDetails(doctorSpecialistsId);
        }
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = searchEditText.getText().toString().toLowerCase(Locale.ENGLISH);
                if (doctorListAdapter != null) {
                    AndyUtils.appLog(TAG, "text" + text);
                    doctorListAdapter.filter(text);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        setupRecyclerView();
        return view;
    }

    private void setupRecyclerView() {
        final RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(activity);
        doctorListRecyclerView.setLayoutManager(mLayoutManager);
        AndyUtils.appLog("BookingFragment", "ChildCountSize" + doctorListRecyclerView.getChildCount() + "");

        doctorListRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    AndyUtils.appLog("TotalCount", +visibleItemCount + " " + totalItemCount + "");
                    int[] firstVisibleItemPositions = new int[2];
                    int pastVisiblesItems = ((LinearLayoutManager) doctorListRecyclerView.getLayoutManager()).findFirstVisibleItemPosition();
                    if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                        if (!isLoadingData && isMoreDataAvailable) {
                            START = START + incrementRate;
                            if (!doctorSpecialistsId.equals("")) {
                                getDoctorBookingConsultDetails(doctorSpecialistsId);
                            }
                        }
                    }

                }
            }
        });
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    private void getDoctorBookingConsultDetails(String doctorSpecialtiesId) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        //  bookingProgressDialog=AndyUtils.getSimpleProgressDialog(getActivity(),getString(R.string.please_wait),false);
        //  bookingProgressDialog.show();
        online_progress.setVisibility(View.VISIBLE);

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_DOCTOR_LIST_BOOKING_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.SPECIALITY_ID + "="
                + String.valueOf(doctorSpecialtiesId) + "&" + Const.Params.SKIP + "=" + String.valueOf(START)
                + "&" + Const.Params.TAKE + "=" + String.valueOf(incrementRate));

        AndyUtils.appLog("Ashutosh", "DoctorListBookingMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_DOCTOR_LIST_BOOKING, this);

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        online_progress.setVisibility(View.GONE);
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_DOCTOR_LIST_BOOKING:
                AndyUtils.appLog("Ashutosh", "DoctorListBookingResponse" + response);

                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true"))
                    {
                        JSONArray jsonArray = jsonObject.getJSONArray("doctor_list");

                        if (jsonArray.length() == 0) {
                            isMoreDataAvailable = false;
                        } else isMoreDataAvailable = true;


                        for (int i = 0; i < jsonArray.length(); i++)
                        {
                            JSONObject onLineJsonObject = jsonArray.getJSONObject(i);
                            DoctorOnLineDetails doctorOnLineDetails = new DoctorOnLineDetails();
                            doctorOnLineDetails.setDoctorOnLineId(onLineJsonObject.getString("id"));
                            doctorOnLineDetails.setDoctorName(onLineJsonObject.getString("d_name"));
                            doctorOnLineDetails.setDoctorPictureUrl(onLineJsonObject.getString("d_photo"));
                            doctorOnLineDetails.setClinicAddress(onLineJsonObject.optString("c_address"));
                            doctorOnLineDetails.setDoctor_clinic_image_first(onLineJsonObject.getString("c_pic1"));
                            doctorOnLineDetails.setDoctor_clinic_image_second(onLineJsonObject.getString("c_pic2"));
                            doctorOnLineDetails.setDoctor_clinic_image_third(onLineJsonObject.getString("c_pic3"));
                            doctorOnLineDetails.setDoctorChatConsultFee(onLineJsonObject.getString("appointment_fee"));
                            doctorOnLineDetails.setDoctorClinicName(onLineJsonObject.getString("c_name"));
                            doctorOnLineDetails.setDoctorRating(onLineJsonObject.getString("rating"));
                            doctorOnLineDetails.setDoctorHelpedPeople(onLineJsonObject.getString("helped"));
                            doctorOnLineDetails.setDoctorExperience(onLineJsonObject.getString("experience"));
                            doctorOnLineDetails.setdLatitude(onLineJsonObject.optString("d_latitude"));
                            doctorOnLineDetails.setdLongitude(onLineJsonObject.getString("d_longitude"));
                            doctorOnLineDetails.setDoctorNationality(onLineJsonObject.optString("nationality"));
                            doctorOnLineDetails.setBetweenDistance("");
                            doctorOnLineDetails.setDegree1(onLineJsonObject.optString("degree1"));
                            doctorOnLineDetails.setEducation1(onLineJsonObject.optString("univ1"));
                            doctorOnLineDetails.setDegree2(onLineJsonObject.optString("degree2"));
                            doctorOnLineDetails.setEducation2(onLineJsonObject.optString("univ2"));
                            doctorOnLineDetails.setIsFavorite(onLineJsonObject.optString(Const.Params.IS_FAVORITE));
                            onLineDetailsList.add(doctorOnLineDetails);
                        }

                        List<DoctorOnLineDetails> favoriteList = new ArrayList<>();
                        List<DoctorOnLineDetails> nonFavoriteList = new ArrayList<>();
                        for (int i = 0; i < onLineDetailsList.size(); i++) {
                            if (onLineDetailsList.get(i).getIsFavorite().equals("1")) {
                                favoriteList.add(onLineDetailsList.get(i));
                            } else if (onLineDetailsList.get(i).getIsFavorite().equals("0")) {
                                nonFavoriteList.add(onLineDetailsList.get(i));
                            }
                        }
                        onLineDetailsList.clear();
                        onLineDetailsList.addAll(favoriteList);
                        onLineDetailsList.addAll(nonFavoriteList);
                        doctorListAdapter = new DoctorListAdapter(getActivity(), onLineDetailsList, Const.BOOKING, activity, this);

                        doctorListRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
                        doctorListRecyclerView.setItemAnimator(new DefaultItemAnimator());
                        doctorListRecyclerView.setAdapter(doctorListAdapter);
                        doctorListAdapter.notifyDataSetChanged();
                        firstTime = true;
                        //  bookingProgressDialog.cancel();
                        if (onLineDetailsList.isEmpty())
                            no_doc_text.setVisibility(View.VISIBLE);
                        else
                            no_doc_text.setVisibility(View.GONE);
                    }
                    else {
                        if (firstTime == false) {
                            firstTime = true;
                            no_doc_text.setVisibility(View.VISIBLE);
                        } else {
                            no_doc_text.setVisibility(View.GONE);
                        }

                        isMoreDataAvailable = false;
                        if(doctorListAdapter!=null) {
                            doctorListAdapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    bookingProgressDialog.cancel();
                    e.printStackTrace();
                }

        }

    }

    @Override
    public void onClickBookAvailability(int position) {
        String doctorId = onLineDetailsList.get(position).getDoctorOnLineId();
        AndyUtils.appLog("DoctorBookingFragment", "BookDoctorId" + doctorId);
        DoctorAvailabilityDialogFragment availability = new DoctorAvailabilityDialogFragment();
        PreferenceHelper.setParam(getActivity(), Const.DOCTOR_ID, doctorId);
        availability.show(getChildFragmentManager(), "DoctorAvailabilityDialogFragment");
    }


//    @Override
//    public void onDestroyView() {
//
//        Fragment fragment = (getFragmentManager()
//                .findFragmentById(R.id.fl_doctor_list));
//        if (fragment!=null && fragment.isResumed()) {
//            getFragmentManager().beginTransaction().remove(fragment)
//                    .commitAllowingStateLoss();
//        }
//        super.onDestroyView();
//    }


}
