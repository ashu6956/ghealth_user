package com.patient.ghealth.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.adapter.NotificationAnswerAdapter;
import com.patient.ghealth.adapter.NotificationMessageAdapter;
import com.patient.ghealth.adapter.RecyclerViewItemClickListener;
import com.patient.ghealth.adapter.SimpleDividerItemDecoration;
import com.patient.ghealth.adapter.VerticalSpaceItemDecoration;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.AnswerData;
import com.patient.ghealth.model.MessageDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 11/30/2016.
 */
public class NotificationMessageFragment extends Fragment implements AsyncTaskCompleteListener
{

    private RecyclerView notificationMessageRecyclerView;
    private NotificationMessageAdapter messageAdapter;
    private ProgressBar notificationProgressBar;
    private TextView noNotification;
    private List<MessageDetails> messageDetailsList;
    private Dialog messageDialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_notification_answer,container,false);
        notificationMessageRecyclerView= (RecyclerView) view.findViewById(R.id.rv_notification_message);
        notificationProgressBar = (ProgressBar) view.findViewById(R.id.notification_progressbar);
        noNotification= (TextView) view.findViewById(R.id.tv_no_notification);
        noNotification.setText(getString(R.string.no_message_available));
        notificationMessageRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(getActivity(), new RecyclerViewItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position)
            {
                AndyUtils.appLog("ClickedPosition",position+"");
                if(messageDetailsList!=null && messageDetailsList.size()>0) {
                    MessageDetails details = messageDetailsList.get(position);
                    showMessageDialog(details);
                }
            }
        }));

        getMessageDetailsList();
        return view;
    }



    private void showMessageDialog(final MessageDetails messageDetails)
    {
        messageDialog=new Dialog(getActivity(),R.style.DialogDocotrTheme);
        messageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        messageDialog.setContentView(R.layout.inbox_msg_popup);
        ImageView doctorIcon= (ImageView) messageDialog.findViewById(R.id.iv_message_dialog);
        TextView doctorName= (TextView) messageDialog.findViewById(R.id.tv_message_dialog_name);
        TextView receivedMsg= (TextView) messageDialog.findViewById(R.id.dialog_received_msg);
        TextView replyMsg= (TextView) messageDialog.findViewById(R.id.dialog_reply_msg);
        final EditText replyEdit= (EditText) messageDialog.findViewById(R.id.et_dialog_message);
        RelativeLayout replyLayout= (RelativeLayout) messageDialog.findViewById(R.id.message_reply_layout);
        FloatingActionButton send= (FloatingActionButton) messageDialog.findViewById(R.id.dialog_send);


        if(!messageDetails.getDoctorName().equals(""))
        {
            doctorName.setText(messageDetails.getDoctorName());
        }
        if(!messageDetails.getPictureUrl().equals(""))
        {
            Glide.with(getActivity()).load(messageDetails.getPictureUrl()).into(doctorIcon);
        }
        if(!messageDetails.getSent().equals(""))
        {
            receivedMsg.setVisibility(View.VISIBLE);
            receivedMsg.setText(messageDetails.getSent());
        }
        if(!messageDetails.getReply().equals(""))
        {
            replyMsg.setVisibility(View.VISIBLE);
            replyMsg.setText(messageDetails.getReply());
            replyLayout.setVisibility(View.GONE);
        }
        else
        {
            replyLayout.setVisibility(View.VISIBLE);
        }

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
               if(replyEdit.getText().toString().equals("") && replyEdit.getText().toString().isEmpty())
               {
                   AndyUtils.showShortToast(getString(R.string.please_enter_text),getActivity());
               }
               else
               {
                   replyMessageToDoctor(messageDetails,replyEdit.getText().toString());
               }
            }
        });

        messageDialog.show();

    }

    private void replyMessageToDoctor(MessageDetails details,String reply)
    {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_REPLY_MESSAGE_URL);
        map.put(Const.Params.ID,String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.MESSAGE_ID,String.valueOf(details.getMessageId()));
        map.put(Const.Params.REPLY,reply);

        AndyUtils.appLog("Ashutosh", "ReplyMsgMap" + map);

        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.CREATE_REPLY_MESSAGE, this);
    }

    private void getMessageDetailsList()
    {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        notificationProgressBar.setVisibility(View.VISIBLE);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_DOCTOR_MESSAGE_lIST_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(getActivity(), Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(getActivity(), Const.Params.SESSION_TOKEN, "") );

        AndyUtils.appLog("Ashutosh", "ViewAnswerMap" + map);

        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.GET_DOCTOR_MESSAGE_lIST_URL, this);
    }


    @Override
    public void onTaskCompleted(String response, int serviceCode)
    {
        JSONObject jsonObject;
        switch(serviceCode)
        {
            case Const.ServiceCode.GET_DOCTOR_MESSAGE_lIST_URL:
                notificationProgressBar.setVisibility(View.GONE);
               AndyUtils.appLog("Ashutosh","MessageListResponse" +response);
                try {
                    jsonObject=new JSONObject(response);
                    if(jsonObject.optString("success").equals("true"))
                    {

                        JSONArray jsonArray=jsonObject.optJSONArray("messageArray");
                        if(jsonArray!=null && jsonArray.length()>0)
                        {
                            messageDetailsList=new ArrayList<>();
                            for(int i=0;i<jsonArray.length();i++)
                            {
                                JSONObject messageObject=jsonArray.optJSONObject(i);
                                MessageDetails details = new MessageDetails();
                                details.setMessageId(messageObject.optString("id"));
                                details.setDoctorId(messageObject.optString("doctor_id"));
                                details.setDoctorName(messageObject.optString("doctor_name"));
                                details.setSent(messageObject.optString("sent"));
                                details.setPictureUrl(messageObject.optString("doctor_picture"));
                                details.setReply(messageObject.optString("reply"));
                                messageDetailsList.add(details);
                            }
                            Collections.reverse(messageDetailsList);
                            LinearLayoutManager layoutManager=new LinearLayoutManager(getActivity());
                            notificationMessageRecyclerView.setLayoutManager(layoutManager);
                            messageAdapter=new NotificationMessageAdapter(getActivity(),messageDetailsList);
                            notificationMessageRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getResources()));
                            notificationMessageRecyclerView.setAdapter(messageAdapter);
                        }
                        else
                        {
                            noNotification.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CREATE_REPLY_MESSAGE:
                AndyUtils.appLog("Ashutosh","Replyresponse" +response);
                try {
                    jsonObject=new JSONObject(response);
                    if(jsonObject.optString("success").equals("true"))
                    {
                        if(messageDialog!=null)
                        {
                            messageDialog.dismiss();
                        }
                        AndyUtils.showShortToast(getString(R.string.message_replied),getActivity());
                        JSONArray jsonArray=jsonObject.optJSONArray("messageArray");
                        if(jsonArray!=null && jsonArray.length()>0) {
                            messageDetailsList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject messageObject = jsonArray.optJSONObject(i);
                                MessageDetails details = new MessageDetails();
                                details.setMessageId(messageObject.optString("id"));
                                details.setDoctorId(messageObject.optString("doctor_id"));
                                details.setDoctorName(messageObject.optString("doctor_name"));
                                details.setSent(messageObject.optString("sent"));
                                details.setPictureUrl(messageObject.optString("doctor_picture"));
                                details.setReply(messageObject.optString("reply"));
                                messageDetailsList.add(details);
                            }
                            Collections.reverse(messageDetailsList);
                            messageAdapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }
}
