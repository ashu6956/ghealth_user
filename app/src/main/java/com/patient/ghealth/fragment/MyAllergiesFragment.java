package com.patient.ghealth.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;

/**
 * Created by getit on 8/10/2016.
 */
public class MyAllergiesFragment extends Fragment
{
    private MainActivity activity;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_alergies_layout,container,false);
        activity= (MainActivity) getActivity();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
//        activity.currentFragment=Const.MyAllergiesFragment;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager manager=getActivity().getSupportFragmentManager();
        Fragment fragment = manager.findFragmentByTag(Const.MyAllergiesFragment);
        if (fragment != null) {
            AndyUtils.appLog("MyAllergiesFragment", "fragment");
            manager.beginTransaction().remove(fragment).commit();
        }
    }
}
