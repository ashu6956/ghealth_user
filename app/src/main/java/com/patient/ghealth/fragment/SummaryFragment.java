package com.patient.ghealth.fragment;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.CheckoutActivity;
import com.patient.ghealth.activity.FinishPaymentActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.model.CouponDetail;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class SummaryFragment extends Fragment implements View.OnClickListener, AsyncTaskCompleteListener {

    private static final int REQUEST_CODE_PAYPAL = 1;


    private double items_cost = 0, total_cost = 0, delivery_cost = 0,
            promotion_applied_cost = 0, final_order_cost = 0, total_savings = 0;

    private CardView payment_method_card, shipping_address_card, billing_address_card;
    private TextView receiver_name, receiver_address, payment_method, itemsCost,
            deliverCost, totalItemsCost, promotionCost, totalOrderCost, totalSavings,
            bill_receiver_name, bill_receiver_address;
    private Button payButton, apply_coupon_button;
    private ProgressDialog loadingDialog;
    private EditText coupon_code;
    private CouponDetail couponDetail;
    private CheckoutActivity activity;

    private String[] paymentMethods = {"PayPal", "Pay by Card", "Pay by PayUMoney"};

    public SummaryFragment() {
        // Required empty public constructor
    }

    public static String hashCal(String type, String str) {
        byte[] hashseq = str.getBytes();
        StringBuffer hexString = new StringBuffer();
        try {
            MessageDigest algorithm = MessageDigest.getInstance(type);
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException nsae) {
        }
        return hexString.toString();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_summary, container, false);
        activity = (CheckoutActivity) getActivity();
        payment_method_card = (CardView) view.findViewById(R.id.payment_info_card);
        payment_method_card.setOnClickListener(this);
        shipping_address_card = (CardView) view.findViewById(R.id.shipping_address_card);
        shipping_address_card.setOnClickListener(this);
        billing_address_card = (CardView) view.findViewById(R.id.billing_address_card);
        billing_address_card.setOnClickListener(this);
        receiver_name = (TextView) view.findViewById(R.id.receiver_name);
        receiver_address = (TextView) view.findViewById(R.id.receiver_address);
        bill_receiver_name = (TextView) view.findViewById(R.id.bill_receiver_name);
        bill_receiver_address = (TextView) view.findViewById(R.id.bill_receiver_address);
        payment_method = (TextView) view.findViewById(R.id.payment_method);
        itemsCost = (TextView) view.findViewById(R.id.itemsCost);
        deliverCost = (TextView) view.findViewById(R.id.deliverCost);
        totalItemsCost = (TextView) view.findViewById(R.id.totalItemsCost);
        promotionCost = (TextView) view.findViewById(R.id.promotionCost);
        totalOrderCost = (TextView) view.findViewById(R.id.totalOrderCost);
        totalSavings = (TextView) view.findViewById(R.id.totalSavings);
        payButton = (Button) view.findViewById(R.id.payButton);
        apply_coupon_button = (Button) view.findViewById(R.id.apply_coupon_button);
        coupon_code = (EditText) view.findViewById(R.id.coupon_code);
        apply_coupon_button.setOnClickListener(this);

        payButton.setOnClickListener(this);
        loadingDialog = new ProgressDialog(getActivity());
        loadingDialog.setIndeterminate(true);
        loadingDialog.setMessage(getString(R.string.please_wait));

        receiver_name.setText(CheckoutActivity.checkoutAddress.getNAME());
        receiver_address.setText(CheckoutActivity.checkoutAddress.getADDRESS());
        payment_method.setText(paymentMethods[CheckoutActivity.paymentMethod]);

        if (CheckoutActivity.billingAddress == null) {
            CheckoutActivity.billingAddress = CheckoutActivity.checkoutAddress;
        }
        bill_receiver_name.setText(CheckoutActivity.billingAddress.getNAME());
        bill_receiver_address.setText(CheckoutActivity.billingAddress.getADDRESS());

        // if 0, then checkout cart
        // if 1, then buy now
        if (CheckoutActivity.checkoutMethod == 0) {
            getCheckoutDetail();
        } else if (CheckoutActivity.checkoutMethod == 1) {
            getBuyNowDetail();
        }

        return view;
    }

    private void getBuyNowDetail() {

        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.BUY_NOW_DETAIL_URL);
        map.put(Const.Params.PRODUCT_ID, CheckoutActivity.productId);
        map.put(Const.QUANTITY, String.valueOf(CheckoutActivity.productQty));

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "BuyDetailMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.BUY_NOW_DETAIL, this, headerMap);

    }

    private void getCheckoutDetail() {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.CART_NOW_DETAIL_URL);

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "CartDetailMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.CART_NOW_DETAIL, this, headerMap);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.payment_info_card:
                PaymentMethodFragment paymentMethodFragment = new PaymentMethodFragment();
                gotoFragment(paymentMethodFragment);
                break;
            case R.id.shipping_address_card:
                GetAddressFragment getAddressFragment = new GetAddressFragment();
                Bundle args = new Bundle();
                args.putBoolean("isFromSummary", true);
                getAddressFragment.setArguments(args);
                gotoAddressFragment(getAddressFragment);
                break;
            case R.id.billing_address_card:
                GetAddressFragment getAddressFragment1 = new GetAddressFragment();
                Bundle args1 = new Bundle();
                args1.putBoolean("isFromSummary", true);
                args1.putBoolean("forBillingAddress", true);
                getAddressFragment1.setArguments(args1);
                gotoAddressFragment(getAddressFragment1);
                break;
            case R.id.payButton:
                if (checkIfAddress()) {
                    switch (CheckoutActivity.paymentMethod) {
                        case 0:
                            payByPayPal();
                            break;
                        case 1:
//                            payByCard();
                            break;
                        case 2:
                            //  payByPayU();
                            break;
                    }

                }
                break;
            case R.id.apply_coupon_button:
                if (!coupon_code.getText().toString().isEmpty())
                    applyCoupon(coupon_code.getText().toString());
                else
                    Toast.makeText(getActivity(), getString(R.string.enter_coupon_code), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void applyCoupon(String couponCode) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.COUPON_VALID_URL + couponCode);

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "ApplyCouponMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.GET, map, Const.ServiceCode.COUPON_VALID, this, headerMap);
    }

    private void calculateNewPrice() {
        // minus
        if (couponDetail.getType() == 0) {
            promotion_applied_cost = couponDetail.getAmount();
            calculateAndDisplay();
        }
        // percentage
        if (couponDetail.getType() == 1) {
            promotion_applied_cost = ((items_cost * couponDetail.getAmount()) / 100);
            calculateAndDisplay();
        }
    }

    /*private void payByPayU() {
        String key = "gtKFFx",
                salt = "eCwWELxi",
                productinfo = "product_name",
                firstname = "piyush",
                email = "test@payu.in",
                txnid = "0nf7" + System.currentTimeMillis();
        double amount = 12.90;

        PayUmoneySdkInitilizer.PaymentParam.Builder builder = new PayUmoneySdkInitilizer.PaymentParam.Builder();
        builder
                .setMerchantId("XXXXXX")
                .setKey("YYYYYYYY")
                .setIsDebug(true) // for Live mode - setIsDebug(false)
                .setAmount(amount)
                .setTnxId(txnid)
                .setPhone("8882434664")
                .setProductName("product_name")
                .setFirstName("piyush")
                .setEmail("test@payu.in")
                .setsUrl("https://www.PayUmoney.com/mobileapp/PayUmoney/success.php")
                .setfUrl("https://www.PayUmoney.com/mobileapp/PayUmoney/failure.php")
        ;
        PayUmoneySdkInitilizer.PaymentParam paymentParam = builder.build();


        String hashSequence = key + "|" + txnid + "|" + amount + "|" + productinfo + "|" + firstname + "|" + email + "|" + salt;
        String serverCalculatedHash = hashCal("SHA-512", hashSequence);
        paymentParam.setMerchantHash(serverCalculatedHash);
        PayUmoneySdkInitilizer.startPaymentActivityForResult(getActivity(), paymentParam);
    }*/

//    private void payByCard() {
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.setContentView(R.layout.card_layout);
//        Button checkout_card = (Button) dialog.findViewById(R.id.checkout_card);
//        final CardForm cardForm = (CardForm) dialog.findViewById(R.id.bt_card_form);
//        cardForm.setRequiredFields(getActivity(), true, true, true, false, "Checkout");
//        checkout_card.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (cardForm.isValid()) {
//                    getStripeToken(cardForm.getCardNumber(), cardForm.getExpirationMonth(), cardForm.getExpirationYear(), cardForm.getCvv());
//                    dialog.dismiss();
//                } else {
//                    cardForm.validate();
//                }
//            }
//        });
//        cardForm.setOnCardFormSubmitListener(new OnCardFormSubmitListener() {
//            @Override
//            public void onCardFormSubmit() {
//                if (cardForm.isValid()) {
//                    getStripeToken(cardForm.getCardNumber(), cardForm.getExpirationMonth(), cardForm.getExpirationYear(), cardForm.getCvv());
//                    dialog.dismiss();
//                } else {
//                    cardForm.validate();
//                }
//            }
//        });
//        dialog.show();
//
//    }

//    private void getStripeToken(String cardNo, String expMonth, String expYear, String cvv) {
//        loadingDialog.show();
//        Card card = new Card(cardNo, Integer.parseInt(expMonth), Integer.parseInt(expYear), cvv);
//        Stripe stripe = null;
//        try {
//            stripe = new Stripe(Const.STRIPE_TOKEN);
//            stripe.createToken(
//                    card,
//                    new TokenCallback() {
//                        public void onSuccess(Token token) {
//                            // Send token to your server
//                            sendPaymentConfirmation(token.getId(), "stripe");
//                        }
//
//                        public void onError(Exception error) {
//                            // Show localized error message
//                            Toast.makeText(getActivity(),
//                                    error.getLocalizedMessage(),
//                                    Toast.LENGTH_LONG
//                            ).show();
//                        }
//                    }
//            );
//        } catch (AuthenticationException e) {
//            e.printStackTrace();
//        }


//    }

    private void payByPayPal() {
        PayPalPayment payment = new PayPalPayment(new BigDecimal(final_order_cost), "MYR", "Total bill",
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(getActivity(), PaymentActivity.class);
        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, CheckoutActivity.config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, REQUEST_CODE_PAYPAL);
    }

    private void gotoFragment(Fragment fragment) {
        activity.addFragment(fragment, false, getString(R.string.checkout), "", false);
    }

    private void gotoAddressFragment(Fragment fragment) {
        activity.addFragment(fragment, false, getString(R.string.shipping_address), "", false);
    }

    private boolean checkIfAddress() {
        if (CheckoutActivity.checkoutAddress == null) {
            Toast.makeText(getActivity(), getString(R.string.enter_shipping_address), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (CheckoutActivity.billingAddress == null) {
            Toast.makeText(getActivity(), getString(R.string.enter_billing_address), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYPAL) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.i("paymentExample", confirm.toJSONObject().toString(4));

                        // TODO: send 'confirm' to your server for verification.
                        // see https://developer.paypal.com/webapps/developer/docs/integration/mobile/verify-mobile-payment/
                        // for more details.
                        String paymentId = confirm.getProofOfPayment().getPaymentId();
                        sendPaymentConfirmation(paymentId, "paypal");
                        loadingDialog.show();

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
       /* if (requestCode == PayUmoneySdkInitilizer.PAYU_SDK_PAYMENT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Log.i("amal", "Success - Payment ID : " +
                        data.getStringExtra(SdkConstants.PAYMENT_ID));
                String paymentId =
                        data.getStringExtra(SdkConstants.PAYMENT_ID);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("amal", "cancelled");
            } else if (resultCode == PayUmoneySdkInitilizer.RESULT_FAILED) {
                Log.i("amal", "failure");
            } else if (resultCode == PayUmoneySdkInitilizer.RESULT_BACK) {
                Log.i("amal", "User returned without login");
            }
        }*/
    }

    private void sendPaymentConfirmation(String paymentId, String method) {
        if (!AndyUtils.isNetworkAvailable(getActivity())) {
            AndyUtils.showLongToast(getString(R.string.no_internet), getActivity());
            return;
        }

        HashMap<String, String> map = new HashMap<>();

        map.put("payment_method", method);
        map.put("payment_status", "completed");
        map.put("payment_id", paymentId);
        map.put("shipping", CheckoutActivity.checkoutAddress.get_ID());
        map.put("billing", CheckoutActivity.billingAddress.get_ID());

        if (couponDetail != null)
            map.put("coupon", couponDetail.getCodeName());


        if (CheckoutActivity.checkoutMethod == 1) {
            map.put(Const.Params.PRODUCT_ID, CheckoutActivity.productId);
            map.put(Const.QUANTITY, String.valueOf(CheckoutActivity.productQty));
//            if (CheckoutActivity.productVariant != null)
//                json.addProperty("variant", CheckoutActivity.productVariant);
//            if (CheckoutActivity.productLicense != null)
//                json.addProperty("license", CheckoutActivity.productLicense);
            completedBuyPayment(map);
        } else {
            completedCartPayment(map);
        }
    }


    private void completedBuyPayment(HashMap<String, String> map) {
        map.put(Const.Params.URL, Const.ServiceType.BUY_NOW_COMPLETED_URL);
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "CompletedBuyMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.BUY_NOW_COMPLETED, this, headerMap);
    }

    private void completedCartPayment(HashMap<String, String> map) {
        map.put(Const.Params.URL, Const.ServiceType.CART_NOW_COMPLETED_URL);
        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(getActivity(), Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "CompletedBuyMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(getActivity(), Const.POST, map, Const.ServiceCode.CART_NOW_COMPLETED, this, headerMap);
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.currentFragment = Const.SummaryFragment;

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.BUY_NOW_DETAIL:
                AndyUtils.appLog("Ashutosh", "BuyDetailResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        JSONObject responseObj = jsonObject.optJSONObject(Const.RESPONSE);
                        items_cost = responseObj.optDouble("total_after_discount");
                        delivery_cost = responseObj.optDouble("total_shipping");
                        promotion_applied_cost = responseObj.optDouble("promo_discount");
                        total_savings = responseObj.optDouble("total_discount");
                        calculateAndDisplay();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CART_NOW_DETAIL:
                AndyUtils.appLog("Ashutosh", "CartDetailResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        JSONObject responseObj = jsonObject.getJSONObject(Const.RESPONSE);
                        items_cost = responseObj.optDouble("total_after_discount");
                        delivery_cost = responseObj.optDouble("total_shipping");
                        promotion_applied_cost = responseObj.optDouble("promo_discount");
                        total_savings = responseObj.optDouble("total_discount");
                        calculateAndDisplay();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.COUPON_VALID:
                AndyUtils.appLog("Ashutosh", "CouponValidResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        JSONObject responseObject = jsonObject.optJSONObject(Const.RESPONSE);
                        CouponDetail mCouponDetail = new CouponDetail();
                        mCouponDetail.setID(responseObject.optString(Const.Params._ID));
                        mCouponDetail.setAmount(responseObject.optDouble("amount"));
                        mCouponDetail.setCodeName(responseObject.optString("code"));
                        mCouponDetail.setType(responseObject.optInt("type"));
                        couponDetail = mCouponDetail;
                        calculateNewPrice();
                    } else {
                        AndyUtils.showShortToast(getString(R.string.coupon_invalid), getActivity());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.BUY_NOW_COMPLETED:
                AndyUtils.appLog("Ashutosh", "BuyNowREsponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        AndyUtils.showShortToast(getString(R.string.payment_successfull), getActivity());
                        JSONObject responseObj = jsonObject.optJSONObject(Const.RESPONSE);
                        Intent intent = new Intent(getActivity(), FinishPaymentActivity.class);
                        intent.putExtra("orderId", responseObj.optString("order_id"));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            case Const.ServiceCode.CART_NOW_COMPLETED:
                AndyUtils.appLog("Ashutosh", "BuyNowREsponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        AndyUtils.showShortToast(getString(R.string.payment_successfull), getActivity());
                        JSONObject responseObj = jsonObject.optJSONObject(Const.RESPONSE);
                        Intent intent = new Intent(getActivity(), FinishPaymentActivity.class);
                        intent.putExtra("orderId", responseObj.optString("order_id"));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        getActivity().finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }

    private void calculateAndDisplay() {
        total_cost = items_cost + delivery_cost;
        final_order_cost = total_cost - promotion_applied_cost;
        itemsCost.setText(Const.RM + items_cost);
        deliverCost.setText(Const.RM + delivery_cost);
        totalItemsCost.setText(Const.RM + total_cost);
        promotionCost.setText(Const.RM + promotion_applied_cost);
        totalOrderCost.setText(Const.RM + final_order_cost);
        int discount_percentage = (int) ((total_savings / items_cost) * 100);
        totalSavings.setText(Const.RM + total_savings + " (" + discount_percentage + "%)");
    }
}
