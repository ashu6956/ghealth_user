package com.patient.ghealth.networking;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.patient.ghealth.app.AppController;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;


import java.util.Map;

/**
 * Created by user on 7/7/2015.
 */
public class MultiPartRequester {

    Context activity;
    private AsyncTaskCompleteListener asyncTaskCompleteListener;
    int service_code;


    public MultiPartRequester(Context activity, Map<String, String> map, int service_code, AsyncTaskCompleteListener asyncTaskCompleteListener) {

        this.activity = activity;
        this.service_code = service_code;
        this.asyncTaskCompleteListener = asyncTaskCompleteListener;
        String url = map.get(Const.Params.URL);
        map.remove(Const.Params.URL);
        multipart_volley_requester(url, map);
    }

    void multipart_volley_requester(String url, Map<String, String> map) {
        HttpsTrustManager.allowAllSSL();
        MultipartRequest mr = new MultipartRequest(url, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Ashutosh", error.toString());
                String msg = "No network connection.Please check your internet";
                AndyUtils.showLongToast(msg,activity);
                AndyUtils.removeProgressDialog();
            }

        }, new Response.Listener<String>() {

            @Override
            public void onResponse(String response)
            {
                if(response!=null) {
                    AndyUtils.appLog("Ashutosh", "MultipartRequesterResponse" + response);
                    asyncTaskCompleteListener.onTaskCompleted(response.toString(), service_code);
                }
            }

        }, map);

        mr.setRetryPolicy(new DefaultRetryPolicy(Const.TIMEOUT,
                Const.MAX_RETRY,
                Const.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(mr);
    }
}
