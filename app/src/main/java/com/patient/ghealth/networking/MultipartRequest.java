package com.patient.ghealth.networking;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;


import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by user on 7/7/2015.
 */
public class MultipartRequest extends Request<String> {

    private MultipartEntity entity = new MultipartEntity();

    private static final String FILE_PART_NAME = "image";

    private final Response.Listener<String> mListener;
    private final File mFilePart;
    // private final File mFilePart_invoice;
    private final Map<String, String> mStringPart;

    public MultipartRequest(String url, Response.ErrorListener errorListener,
                            Response.Listener<String> listener,
                            Map<String, String> mStringPart) {
        super(Method.POST, url, errorListener);
        HttpsTrustManager.allowAllSSL();
        AndyUtils.appLog("Ashutosh", "MultiPartRequestUrl " + url);


        mListener = listener;
        if (mStringPart.get(Const.Params.PICTURE) != null && !mStringPart.get(Const.Params.PICTURE).equals(""))
            mFilePart = new File(mStringPart.get(Const.Params.PICTURE));
        else
            mFilePart = null;


        mStringPart.remove(Const.Params.PICTURE);
        this.mStringPart = mStringPart;
        buildMultipartEntity();
        //}
    }

    private void buildMultipartEntity() {

        if (mFilePart != null)
            entity.addPart(Const.Params.PICTURE, new FileBody(mFilePart));

        try {
            for (String key : mStringPart.keySet()) {
                entity.addPart(key, new StringBody(mStringPart.get(key)));
            }
        } catch (UnsupportedEncodingException e) {
            VolleyLog.e("UnsupportedEncodingException");
        }
    }

    @Override
    public String getBodyContentType() {
        return entity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            entity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }


    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        String parsed;
        try {
            parsed = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            parsed = new String(response.data);
        }
        return Response.success(parsed, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(String response) {
        mListener.onResponse(response);
    }
}
