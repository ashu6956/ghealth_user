package com.patient.ghealth.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.patient.ghealth.gcmHandlers.GcmBroadcastReceiver;

import java.util.Calendar;

/**
 * Created by user on 12/30/2016.
 */
public class BacKGroundReceiver extends BroadcastReceiver
{
    private AlarmManager alarmManager;

    private static final int REQUEST_ID_FIRST = 247;


    private PendingIntent firstPendingIntent;

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Log.d("JunkFileReceiver", "onReceive");

        Calendar calendar=Calendar.getInstance();
        Intent alarmIntent = new Intent(context, GcmBroadcastReceiver.class);

        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        firstPendingIntent = PendingIntent.getService(context.getApplicationContext(), 0, alarmIntent, 0);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), 5*1000, firstPendingIntent);

    }


}
