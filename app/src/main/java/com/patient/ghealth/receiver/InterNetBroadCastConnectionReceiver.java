package com.patient.ghealth.receiver;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.utils.AndyUtils;

/**
 * Created by user on 9/21/2016.
 */
public class InterNetBroadCastConnectionReceiver extends BroadcastReceiver {
    private Activity activity;
    private boolean isNetDialogShowing = false;
    private Dialog internetDialog;
    private Context mContext;

    public InterNetBroadCastConnectionReceiver(Activity activity) {
        this.activity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo activeWIFIInfo = connectivityManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (activeWIFIInfo.isConnected() || activeNetInfo.isConnected()) {
            removeInternetDialog();
        } else {
            if (isNetDialogShowing) {
                return;
            }
            showInternetDialog();
        }
    }


    private void removeInternetDialog() {
        if (internetDialog != null && internetDialog.isShowing()) {
            internetDialog.dismiss();
            isNetDialogShowing = false;
            internetDialog = null;

        }
    }

    private void showInternetDialog()
    {

        isNetDialogShowing = true;
        internetDialog = new Dialog(mContext);
        internetDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        internetDialog.setContentView(R.layout.internet_dialog_layout);
        TextView wifi = (TextView) internetDialog.findViewById(R.id.tv_wifi);
        TextView exit = (TextView) internetDialog.findViewById(R.id.tv_exit);
        TextView enable3g = (TextView) internetDialog.findViewById(R.id.tv_mobile_network);

        wifi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                removeInternetDialog();
            }
        });
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeInternetDialog();
                activity.finish();
            }
        });

        enable3g.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                mContext.startActivity(intent);
                removeInternetDialog();

            }
        });
        internetDialog.setCancelable(false);
        internetDialog.show();
    }

}
