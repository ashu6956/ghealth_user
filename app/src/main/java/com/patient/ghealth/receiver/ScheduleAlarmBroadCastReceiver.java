package com.patient.ghealth.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.patient.ghealth.service.ScheduleAlarmService;

import java.util.Calendar;

/**
 * Created by getit on 3/18/2016.
 */
public class ScheduleAlarmBroadCastReceiver extends BroadcastReceiver {

    private AlarmManager alarmManager;

    private static final int REQUEST_ID_FIRST = 247;

    private static final int REQUEST_ID_SECOND = 245;

    private static final int REQUEST_ID_THIRD = 249;
    private PendingIntent firstPendingIntent;
    private PendingIntent secondPendingIntent,thirdPendingIntent;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("JunkFileReceiver", "onReceive");

        Calendar firstUpdatedTime = Calendar.getInstance();
        firstUpdatedTime.set(Calendar.DAY_OF_WEEK, 1);
        firstUpdatedTime.set(Calendar.HOUR_OF_DAY, 9);
        firstUpdatedTime.set(Calendar.MINUTE, 30);
        firstUpdatedTime.set(Calendar.SECOND, 0);
        Calendar secondUpdatedTime=Calendar.getInstance();
        secondUpdatedTime.set(Calendar.DAY_OF_WEEK,5);
        secondUpdatedTime.set(Calendar.HOUR_OF_DAY,9);
        secondUpdatedTime.set(Calendar.MINUTE, 30);
        secondUpdatedTime.set(Calendar.SECOND,0);

        Calendar thirdUpdatedTime=Calendar.getInstance();
        thirdUpdatedTime.set(Calendar.DAY_OF_WEEK, 2);
        thirdUpdatedTime.set(Calendar.HOUR_OF_DAY, 15);
        thirdUpdatedTime.set(Calendar.MINUTE, 0);
        thirdUpdatedTime.set(Calendar.SECOND, 0);


        Intent alarmIntent = new Intent(context, ScheduleAlarmService.class);


        for (int id = 0; id < 3; id++) {
            // Define pendingintent
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, id,alarmIntent, 0);
            // set() schedules an alarm
//            alarmManager.set(AlarmManager.RTC_WAKEUP, alertTime, pendingIntent);
        }



        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        firstPendingIntent = PendingIntent.getService(context.getApplicationContext(), REQUEST_ID_FIRST, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, thirdUpdatedTime.getTimeInMillis(), (7 * 24 * 60 * 60 * 1000), firstPendingIntent);
        secondPendingIntent=PendingIntent.getService(context.getApplicationContext(),REQUEST_ID_SECOND,alarmIntent,PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,secondUpdatedTime.getTimeInMillis(),(7 * 24 * 60 * 60 * 1000),secondPendingIntent);

        thirdPendingIntent = PendingIntent.getService(context.getApplicationContext(), REQUEST_ID_THIRD, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, thirdUpdatedTime.getTimeInMillis(), (7 * 24 * 60 * 60 * 1000), thirdPendingIntent);

    }
}




