package com.patient.ghealth.model;

/**
 * Created by user on 10/5/2016.
 */
public class CardDetails
{
    private String cardNumber;
    private String cardTypeUrl;
    private String isDefault;
    private String type;
    private String cardId;

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCardTypeUrl() {
        return cardTypeUrl;
    }

    public void setCardTypeUrl(String cardTypeUrl) {
        this.cardTypeUrl = cardTypeUrl;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
