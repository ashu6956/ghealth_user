package com.patient.ghealth.model;

/**
 * Created by ad on 8/27/2015.
 */
public class ChatGroup {

    public   String groupId;
    public   String created_by_user_id;
    public   String group_name;
    public   String group_pic_url;
    public   String group_deleted_at;
    public   String group_created_at;
    public   String group_updated_at;
    public   String is_admin;

    public ChatGroup(String groupId, String created_by_user_id, String group_name, String group_pic_url, String group_deleted_at, String group_created_at, String group_updated_at, String is_admin) {
        this.groupId = groupId;
        this.created_by_user_id = created_by_user_id;
        this.group_name = group_name;
        this.group_pic_url = group_pic_url;
        this.group_deleted_at = group_deleted_at;
        this.group_created_at = group_created_at;
        this.group_updated_at = group_updated_at;
        this.is_admin = is_admin;
    }

    public ChatGroup()
    {

    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getCreated_by_user_id() {
        return created_by_user_id;
    }

    public void setCreated_by_user_id(String created_by_user_id) {
        this.created_by_user_id = created_by_user_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getGroup_pic_url() {
        return group_pic_url;
    }

    public void setGroup_pic_url(String group_pic_url) {
        this.group_pic_url = group_pic_url;
    }

    public String getGroup_deleted_at() {
        return group_deleted_at;
    }

    public void setGroup_deleted_at(String group_deleted_at) {
        this.group_deleted_at = group_deleted_at;
    }

    public String getGroup_created_at() {
        return group_created_at;
    }

    public void setGroup_created_at(String group_created_at) {
        this.group_created_at = group_created_at;
    }

    public String getGroup_updated_at() {
        return group_updated_at;
    }

    public void setGroup_updated_at(String group_updated_at) {
        this.group_updated_at = group_updated_at;
    }

    public String getIs_admin() {
        return is_admin;
    }

    public void setIs_admin(String is_admin) {
        this.is_admin = is_admin;
    }
}
