package com.patient.ghealth.model;

import java.io.Serializable;

/**
 * Created by user on 9/23/2016.
 */
public class InvoiceDetails implements Serializable {
    private int invoiceDoctorId;
    private String totalPrice;
    private String basePrice;
    private String medicineFee;
    private String paymentMode;
    private String referralBonus;
    private String totalDistance;
    private String totalTime;
    private String promoBonus;
    private String invoiceRequestId;
    private String invoiceDuration;
    private String invoiceType;
    private String DistanceCost;
    private String distance;
    private String invoiceDoctorPictureUrl;
    private String invoiceDoctorName;
    private String invoiceDoctorStatus;
    private String treatmentFee;

    public String getPerKm() {
        return perKm;
    }

    public void setPerKm(String perKm) {
        this.perKm = perKm;
    }

    private String perKm;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDistanceCost() {
        return DistanceCost;
    }

    public void setDistanceCost(String distanceCost) {
        DistanceCost = distanceCost;
    }

    public String getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(String invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getInvoiceDuration() {
        return invoiceDuration;
    }

    public void setInvoiceDuration(String invoiceDuration) {
        this.invoiceDuration = invoiceDuration;
    }

    public String getInvoiceDoctorPictureUrl() {
        return invoiceDoctorPictureUrl;
    }

    public void setInvoiceDoctorPictureUrl(String invoiceDoctorPictureUrl) {
        this.invoiceDoctorPictureUrl = invoiceDoctorPictureUrl;
    }

    public String getInvoiceDoctorName() {
        return invoiceDoctorName;
    }

    public void setInvoiceDoctorName(String invoiceDoctorName) {
        this.invoiceDoctorName = invoiceDoctorName;
    }

    public String getInvoiceDoctorStatus() {
        return invoiceDoctorStatus;
    }

    public void setInvoiceDoctorStatus(String invoiceDoctorStatus) {
        this.invoiceDoctorStatus = invoiceDoctorStatus;
    }

    public String getTreatmentFee() {
        return treatmentFee;
    }

    public void setTreatmentFee(String treatmentFee) {
        this.treatmentFee = treatmentFee;
    }

    public String getInvoiceRequestId() {
        return invoiceRequestId;
    }

    public void setInvoiceRequestId(String invoiceRequestId) {
        this.invoiceRequestId = invoiceRequestId;
    }

    public int getInvoiceDoctorId() {
        return invoiceDoctorId;
    }

    public void setInvoiceDoctorId(int invoiceDoctorId) {
        this.invoiceDoctorId = invoiceDoctorId;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getMedicineFee() {
        return medicineFee;
    }

    public void setMedicineFee(String medicineFee) {
        this.medicineFee = medicineFee;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getReferralBonus() {
        return referralBonus;
    }

    public void setReferralBonus(String referralBonus) {
        this.referralBonus = referralBonus;
    }

    public String getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(String totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getPromoBonus() {
        return promoBonus;
    }

    public void setPromoBonus(String promoBonus) {
        this.promoBonus = promoBonus;
    }
}
