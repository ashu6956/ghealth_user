package com.patient.ghealth.model;

/**
 * Created by user on 10/3/2016.
 */
public class MyDateSchedule
{
    private String scheduleSlotsId;
    private String scheduleDate;
    private String scheduleStartTime;
    private String scheduleEndTime;
    private String scheduleDoctorName;
    private String scheduleClinicName;
    private String scheduleClinicStreet;
    private String scheduleClinicCity;
    private String scheduleClinicState;
    private String scheduleClinicCountry;
    private String scheduleClinicPostal;
    private String isCompleted;

    public String getIsCompleted() {
        return isCompleted;
    }

    public void setIsCompleted(String isCompleted) {
        this.isCompleted = isCompleted;
    }

    public String getTimeFormat() {
        return timeFormat;
    }

    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
    }

    private String timeFormat;

    public String getScheduleStartTime() {
        return scheduleStartTime;
    }

    public void setScheduleStartTime(String scheduleStartTime) {
        this.scheduleStartTime = scheduleStartTime;
    }

    public String getScheduleEndTime() {
        return scheduleEndTime;
    }

    public void setScheduleEndTime(String scheduleEndTime) {
        this.scheduleEndTime = scheduleEndTime;
    }

    public String getScheduleDoctorName() {
        return scheduleDoctorName;
    }

    public void setScheduleDoctorName(String scheduleDoctorName) {
        this.scheduleDoctorName = scheduleDoctorName;
    }

    public String getScheduleClinicName() {
        return scheduleClinicName;
    }

    public void setScheduleClinicName(String scheduleClinicName) {
        this.scheduleClinicName = scheduleClinicName;
    }

    public String getScheduleClinicStreet() {
        return scheduleClinicStreet;
    }

    public void setScheduleClinicStreet(String scheduleClinicStreet) {
        this.scheduleClinicStreet = scheduleClinicStreet;
    }

    public String getScheduleClinicCity() {
        return scheduleClinicCity;
    }

    public void setScheduleClinicCity(String scheduleClinicCity) {
        this.scheduleClinicCity = scheduleClinicCity;
    }

    public String getScheduleClinicState() {
        return scheduleClinicState;
    }

    public void setScheduleClinicState(String scheduleClinicState) {
        this.scheduleClinicState = scheduleClinicState;
    }

    public String getScheduleClinicCountry() {
        return scheduleClinicCountry;
    }

    public void setScheduleClinicCountry(String scheduleClinicCountry) {
        this.scheduleClinicCountry = scheduleClinicCountry;
    }

    public String getScheduleClinicPostal() {
        return scheduleClinicPostal;
    }

    public void setScheduleClinicPostal(String scheduleClinicPostal) {
        this.scheduleClinicPostal = scheduleClinicPostal;
    }

    public String getScheduleSlotsId() {
        return scheduleSlotsId;
    }

    public void setScheduleSlotsId(String scheduleSlotsId) {
        this.scheduleSlotsId = scheduleSlotsId;
    }

    public String getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(String scheduleDate) {
        this.scheduleDate = scheduleDate;
    }
}
