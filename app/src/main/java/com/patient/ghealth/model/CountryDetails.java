package com.patient.ghealth.model;

/**
 * Created by user on 12/7/2016.
 */
public class CountryDetails
{
    private int imageId;
    private String country;
    private String currency;

    public CountryDetails(int imageId, String country, String currency) {
        this.imageId = imageId;
        this.country = country;
        this.currency = currency;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }


}
