package com.patient.ghealth.model;

/**
 * Created by user on 11/15/2016.
 */
public class QuestionAnswerDetails
{

    private String answer;
    private String question;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {

        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
