package com.patient.ghealth.model;

/**
 * Created by user on 9/7/2016.
 */
public class PhoneContactInfo
{
    private String contactName;
    private String contactNumber;
    public boolean isChecked=false;

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public PhoneContactInfo()
    {

    }

    public PhoneContactInfo(String contactName, String contactNumber) {
        this.contactName = contactName;
        this.contactNumber = contactNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }
}
