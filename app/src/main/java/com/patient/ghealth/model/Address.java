package com.patient.ghealth.model;

import java.io.Serializable;

/**
 * Created by amal on 30/08/16.
 */
public class Address implements Serializable {
    private String _ID, NAME, NUMBER, STATE, COUNTRY, ADDRESS, PINCODE, CITY;

    public String get_ID() {
        return _ID;
    }

    public void set_ID(String _ID) {
        this._ID = _ID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getNUMBER() {
        return NUMBER;
    }

    public void setNUMBER(String NUMBER) {
        this.NUMBER = NUMBER;
    }

    public String getSTATE() {
        return STATE;
    }

    public void setSTATE(String STATE) {
        this.STATE = STATE;
    }

    public String getCOUNTRY() {
        return COUNTRY;
    }

    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getPINCODE() {
        return PINCODE;
    }

    public void setPINCODE(String PINCODE) {
        this.PINCODE = PINCODE;
    }

    public String getCITY() {
        return CITY;
    }



    public void setCITY(String CITY) {
        this.CITY = CITY;
    }


    @Override
    public String toString() {
        return "Address{" +
                "_ID='" + _ID + '\'' +
                ", NAME='" + NAME + '\'' +
                ", NUMBER='" + NUMBER + '\'' +
                ", STATE='" + STATE + '\'' +
                ", COUNTRY='" + COUNTRY + '\'' +
                ", ADDRESS='" + ADDRESS + '\'' +
                ", PINCODE='" + PINCODE + '\'' +
                ", CITY='" + CITY + '\'' +
                '}';
    }
}
