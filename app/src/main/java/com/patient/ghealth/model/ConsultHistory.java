package com.patient.ghealth.model;

/**
 * Created by user on 10/5/2016.
 */
public class ConsultHistory {

    private String historyId;
    private String historyType;
    private String doctorName;
    private String historyDate;
    private String time;
    private String duration;
    private String consultationFee;
    private String doctorPictureUrl;
    private String doctorNumber;

    public String getDoctorNumber() {
        return doctorNumber;
    }

    public void setDoctorNumber(String doctorNumber) {
        this.doctorNumber = doctorNumber;
    }

    public String getDoctorPictureUrl() {
        return doctorPictureUrl;
    }

    public void setDoctorPictureUrl(String doctorPictureUrl) {
        this.doctorPictureUrl = doctorPictureUrl;
    }

    public String getHistoryId() {
        return historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getHistoryType() {
        return historyType;
    }

    public void setHistoryType(String historyType) {
        this.historyType = historyType;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getHistoryDate() {
        return historyDate;
    }

    public void setHistoryDate(String historyDate) {
        this.historyDate = historyDate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getConsultationFee() {
        return consultationFee;
    }

    public void setConsultationFee(String consultationFee) {
        this.consultationFee = consultationFee;
    }
}
