package com.patient.ghealth.model;

import java.io.Serializable;

/**
 * Created by user on 11/14/2016.
 */
public class ProductCategory implements Serializable
{
    private String categoriesId;
    private String categories;
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getCategoriesId() {

        return categoriesId;
    }

    public void setCategoriesId(String categoriesId) {
        this.categoriesId = categoriesId;
    }
}
