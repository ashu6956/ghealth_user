package com.patient.ghealth.model;

import android.widget.TextView;

import java.io.Serializable;

/**
 * Created by user on 9/30/2016.
 */
public class DoctorBookingSlotsData implements Serializable
{


    private String availableSlotId;
    private String startTime;
    private String endTime;
    private String shiftType;
    private String slotBooked;
    private boolean isSelected=false;

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    private String timeZone;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getShiftType() {
        return shiftType;
    }

    public void setShiftType(String shiftType) {
        this.shiftType = shiftType;
    }


    public String getAvailableSlotId() {
        return availableSlotId;
    }

    public void setAvailableSlotId(String availableSlotId) {
        this.availableSlotId = availableSlotId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSlotBooked() {
        return slotBooked;
    }

    public void setSlotBooked(String slotBooked) {
        this.slotBooked = slotBooked;
    }
}
