package com.patient.ghealth.model;

import java.io.Serializable;

/**
 * Created by amal on 27/08/16.
 */
public class CartProductDetail implements Serializable {
    private String cart_id;
    private ProductDetail mProductDetail;
    private int productQty;

    public ProductDetail getmProductDetail() {
        return mProductDetail;
    }

    public void setmProductDetail(ProductDetail mProductDetail) {
        this.mProductDetail = mProductDetail;
    }

    public int getProductQty() {
        return productQty;
    }

    public void setProductQty(int productQty) {
        this.productQty = productQty;
    }


    public String getCart_id() {
        return cart_id;
    }

    public void setCart_id(String cart_id) {
        this.cart_id = cart_id;
    }
}
