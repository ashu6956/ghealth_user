package com.patient.ghealth.model;

import java.io.Serializable;

/**
 * Created by user on 9/15/2016.
 */
public class DoctorOnLineDetails implements Serializable {

    private String doctorOnLineId;
    private String doctorName;
    private String doctorPictureUrl;
    private String doctorRating;
    private String doctorClinicName;
    private String clinicAddress;
    private String doctor_clinic_image_first;
    private String doctor_clinic_image_second;
    private String doctor_clinic_image_third;
    private String doctorChatConsult;
    private String doctorVideoConsult;
    private String doctorPhoneConsult;
    private String doctorChatConsultFee;
    private String doctorPhoneConsultFee;
    private String doctorVideoConsultFee;
    private String doctorExperience;
    private String doctorHelpedPeople;
    private String latitude;
    private String longitude;
    private String doctorStatus;
    private String requestId;
    private String doctorStatusTitle;
    private String dLatitude;
    private String dLongitude;
    private String doctorNationality;
    private String doctorQualification;
    private String betweenDistance;
    private String clinicId;
    private String clinicNoOfDoctor;
    private String degree1;
    private String degree2;
    private String education1;
    private String education2;
    private String etaTime;
    private String sourceAddress;
    private String freeCharge;

    public String getFreeCharge() {
        return freeCharge;
    }

    public void setFreeCharge(String freeCharge) {
        this.freeCharge = freeCharge;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    private String isFavorite;

    public String getSourceAddress() {
        return sourceAddress;
    }

    public void setSourceAddress(String sourceAddress) {
        this.sourceAddress = sourceAddress;
    }

    public String getEtaTime() {
        return etaTime;
    }

    public void setEtaTime(String etaTime) {
        this.etaTime = etaTime;
    }

    public String getClinicAddress() {
        return clinicAddress;
    }

    public void setClinicAddress(String clinicAddress) {
        this.clinicAddress = clinicAddress;
    }

    public String getEducation2() {
        return education2;
    }

    public void setEducation2(String education2) {
        this.education2 = education2;
    }

    public String getEducation1() {
        return education1;
    }

    public void setEducation1(String education1) {
        this.education1 = education1;
    }

    public String getDegree1() {
        return degree1;
    }

    public void setDegree1(String degree1) {
        this.degree1 = degree1;
    }

    public String getDegree2() {
        return degree2;
    }

    public void setDegree2(String degree2) {
        this.degree2 = degree2;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getClinicNoOfDoctor() {
        return clinicNoOfDoctor;
    }

    public void setClinicNoOfDoctor(String clinicNoOfDoctor) {
        this.clinicNoOfDoctor = clinicNoOfDoctor;
    }

    public String getBetweenDistance() {
        return betweenDistance;
    }

    public void setBetweenDistance(String betweenDistance) {
        this.betweenDistance = betweenDistance;
    }

    public String getDoctorOnLineId() {
        return doctorOnLineId;
    }

    public void setDoctorOnLineId(String doctorOnLineId) {
        this.doctorOnLineId = doctorOnLineId;
    }

    public String getDoctorQualification() {
        return doctorQualification;
    }

    public void setDoctorQualification(String doctorQualification) {
        this.doctorQualification = doctorQualification;
    }

    public String getDoctorNationality() {
        return doctorNationality;
    }

    public void setDoctorNationality(String doctorNationality) {
        this.doctorNationality = doctorNationality;
    }

    public String getDoctorStatusTitle() {
        return doctorStatusTitle;
    }

    public void setDoctorStatusTitle(String doctorStatusTitle) {
        this.doctorStatusTitle = doctorStatusTitle;
    }

    public String getdLatitude() {
        return dLatitude;
    }

    public void setdLatitude(String dLatitude) {
        this.dLatitude = dLatitude;
    }

    public String getdLongitude() {
        return dLongitude;
    }

    public void setdLongitude(String dLongitude) {
        this.dLongitude = dLongitude;
    }


    public String getDoctorStatus() {
        return doctorStatus;
    }

    public void setDoctorStatus(String doctorStatus) {
        this.doctorStatus = doctorStatus;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }


    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getDoctorPictureUrl() {
        return doctorPictureUrl;
    }

    public void setDoctorPictureUrl(String doctorPictureUrl) {
        this.doctorPictureUrl = doctorPictureUrl;
    }

    public String getDoctorRating() {
        return doctorRating;
    }

    public void setDoctorRating(String doctorRating) {
        this.doctorRating = doctorRating;
    }

    public String getDoctorClinicName() {
        return doctorClinicName;
    }

    public void setDoctorClinicName(String doctorClinicName) {
        this.doctorClinicName = doctorClinicName;
    }

    public String getDoctor_clinic_image_first() {
        return doctor_clinic_image_first;
    }

    public void setDoctor_clinic_image_first(String doctor_clinic_image_first) {
        this.doctor_clinic_image_first = doctor_clinic_image_first;
    }

    public String getDoctor_clinic_image_second() {
        return doctor_clinic_image_second;
    }

    public void setDoctor_clinic_image_second(String doctor_clinic_image_second) {
        this.doctor_clinic_image_second = doctor_clinic_image_second;
    }

    public String getDoctor_clinic_image_third() {
        return doctor_clinic_image_third;
    }

    public void setDoctor_clinic_image_third(String doctor_clinic_image_third) {
        this.doctor_clinic_image_third = doctor_clinic_image_third;
    }

    public String getDoctorChatConsult() {
        return doctorChatConsult;
    }

    public void setDoctorChatConsult(String doctorChatConsult) {
        this.doctorChatConsult = doctorChatConsult;
    }

    public String getDoctorVideoConsult() {
        return doctorVideoConsult;
    }

    public void setDoctorVideoConsult(String doctorVideoConsult) {
        this.doctorVideoConsult = doctorVideoConsult;
    }

    public String getDoctorPhoneConsult() {
        return doctorPhoneConsult;
    }

    public void setDoctorPhoneConsult(String doctorPhoneConsult) {
        this.doctorPhoneConsult = doctorPhoneConsult;
    }

    public String getDoctorChatConsultFee() {
        return doctorChatConsultFee;
    }

    public void setDoctorChatConsultFee(String doctorChatConsultFee) {
        this.doctorChatConsultFee = doctorChatConsultFee;
    }

    public String getDoctorPhoneConsultFee() {
        return doctorPhoneConsultFee;
    }

    public void setDoctorPhoneConsultFee(String doctorPhoneConsultFee) {
        this.doctorPhoneConsultFee = doctorPhoneConsultFee;
    }

    public String getDoctorVideoConsultFee() {
        return doctorVideoConsultFee;
    }

    public void setDoctorVideoConsultFee(String doctorVideoConsultFee) {
        this.doctorVideoConsultFee = doctorVideoConsultFee;
    }

    public String getDoctorExperience() {
        return doctorExperience;
    }

    public void setDoctorExperience(String doctorExperience) {
        this.doctorExperience = doctorExperience;
    }

    public String getDoctorHelpedPeople() {
        return doctorHelpedPeople;
    }

    public void setDoctorHelpedPeople(String doctorHelpedPeople) {
        this.doctorHelpedPeople = doctorHelpedPeople;
    }
}
