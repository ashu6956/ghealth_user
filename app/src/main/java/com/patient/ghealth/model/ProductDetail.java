package com.patient.ghealth.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by user on 11/14/2016.
 */
public class ProductDetail implements Serializable {
    private String productId;
    private String productPictureUrl1;
    private String productPictureUrl2;
    private String productPictureUrl3;
    private String oldPrice;
    private String newPrice;
    private String productName;
    private String currency;
    private Price price;
    private ArrayList<Cat_image> cat_images;
    private ArrayList<Reviews> reviewList = new ArrayList<>();
    private ArrayList<Variants> variantList = new ArrayList<>();
    private ArrayList<License> licensesList = new ArrayList<>();
    private ArrayList<String> categories = new ArrayList<>();
    private int quantity;
    private String isBookMarked;
    private String description;
    private boolean paidByBuyer;
    private Seller_info sellerInfo;
    private ShippingDetails shippingDetails;
    private Rating rating;

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    private String longDescription;

    public Seller_info getSellerInfo() {
        return sellerInfo;
    }

    public void setSellerInfo(Seller_info sellerInfo) {
        this.sellerInfo = sellerInfo;
    }

    public ShippingDetails getShippingDetails() {
        return shippingDetails;
    }

    public void setShippingDetails(ShippingDetails shippingDetails) {
        this.shippingDetails = shippingDetails;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public ArrayList<Cat_image> getCat_images() {
        return cat_images;
    }

    public void setCat_images(ArrayList<Cat_image> cat_images) {
        this.cat_images = cat_images;
    }

    public ArrayList<Reviews> getReviewList() {
        return reviewList;
    }

    public void setReviewList(ArrayList<Reviews> reviewList) {
        this.reviewList = reviewList;
    }

    public ArrayList<Variants> getVariantList() {
        return variantList;
    }

    public void setVariantList(ArrayList<Variants> variantList) {
        this.variantList = variantList;
    }

    public ArrayList<License> getLicensesList() {
        return licensesList;
    }

    public void setLicensesList(ArrayList<License> licensesList) {
        this.licensesList = licensesList;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public boolean isPaidByBuyer() {
        return paidByBuyer;
    }

    public void setPaidByBuyer(boolean paidByBuyer) {
        this.paidByBuyer = paidByBuyer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductPictureUrl3() {
        return productPictureUrl3;
    }

    public void setProductPictureUrl3(String productPictureUrl3) {
        this.productPictureUrl3 = productPictureUrl3;
    }

    public String getProductPictureUrl2() {
        return productPictureUrl2;
    }

    public void setProductPictureUrl2(String productPictureUrl2) {
        this.productPictureUrl2 = productPictureUrl2;
    }

    public String getProductPictureUrl1() {
        return productPictureUrl1;
    }

    public void setProductPictureUrl1(String productPictureUrl1) {
        this.productPictureUrl1 = productPictureUrl1;
    }


    public String getIsBookMarked() {
        return isBookMarked;
    }

    public void setIsBookMarked(String isBookMarked) {
        this.isBookMarked = isBookMarked;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }


    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }


    public static class Price implements Serializable {
        private double original_price;
        private double after_discount;
        private double commission;
        private double service_tax;

        public double getOriginal_price() {
            return original_price;
        }

        public void setOriginal_price(double original_price) {
            this.original_price = original_price;
        }

        public double getAfter_discount() {
            return after_discount;
        }

        public void setAfter_discount(double after_discount) {
            this.after_discount = after_discount;
        }

        public double getCommission() {
            return commission;
        }

        public void setCommission(double commission) {
            this.commission = commission;
        }

        public double getService_tax() {
            return service_tax;
        }

        public void setService_tax(double service_tax) {
            this.service_tax = service_tax;
        }
    }

    public static class Cat_image implements Serializable {
        private String _id;
        private String Image_url;

        public String getImage_url() {
            return Image_url;
        }

        public void setImage_url(String image_url) {
            Image_url = image_url;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }
    }

    public static class Seller_info implements Serializable {
        private String _id;
        private String seller_name;

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public String getSeller_name() {
            return seller_name;
        }

        public void setSeller_name(String seller_name) {
            this.seller_name = seller_name;
        }
    }

    public static class ShippingDetails implements Serializable {
        private double weight;
        private double fee;
        private String duration;
        private String unit;

        public double getWeight() {
            return weight;
        }

        public void setWeight(double weight) {
            this.weight = weight;
        }

        public double getFee() {
            return fee;
        }

        public void setFee(double fee) {
            this.fee = fee;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }
    }

    public static class Rating implements Serializable {
        private int totalRatings;
        private int totalReviews;
        private int totalStar;

        public int getTotalRatings() {
            return totalRatings;
        }

        public void setTotalRatings(int totalRatings) {
            this.totalRatings = totalRatings;
        }

        public int getTotalReviews() {
            return totalReviews;
        }

        public void setTotalReviews(int totalReviews) {
            this.totalReviews = totalReviews;
        }

        public int getTotalStar() {
            return totalStar;
        }

        public void setTotalStar(int totalStar) {
            this.totalStar = totalStar;
        }
    }

    public static class Reviews implements Serializable {
        private double star;
        private String comment;
        private String name;
        private String title;
        private String imageUrl;

        public double getStar() {
            return star;
        }

        public void setStar(double star) {
            this.star = star;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
    }

    public static class Variants implements Serializable {
        private String ID, name, quantity;
        private Price price;

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public Price getPrice() {
            return price;
        }

        public void setPrice(Price price) {
            this.price = price;
        }
    }

    public static class License implements Serializable {
        private String ID, quantity, name;
        private Price price;

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public Price getPrice() {
            return price;
        }

        public void setPrice(Price price) {
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
