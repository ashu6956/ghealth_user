package com.patient.ghealth.model;

import java.io.Serializable;

/**
 * Created by user on 9/24/2016.
 */
public class HealthFeedArticles implements Serializable
{
    private String articleId;
    private String doctorName;
    private String category;
    private String articlePictureUrl;
    private String articleDescription;
    private String topicHeading;

    private String isBookMarked;

    public String getIsBookMarked() {
        return isBookMarked;
    }

    public void setIsBookMarked(String isBookMarked) {
        this.isBookMarked = isBookMarked;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getArticlePictureUrl() {
        return articlePictureUrl;
    }

    public void setArticlePictureUrl(String articlePictureUrl) {
        this.articlePictureUrl = articlePictureUrl;
    }

    public String getArticleDescription() {
        return articleDescription;
    }

    public void setArticleDescription(String articleDescription) {
        this.articleDescription = articleDescription;
    }

    public String getTopicHeading() {
        return topicHeading;
    }

    public void setTopicHeading(String topicHeading) {
        this.topicHeading = topicHeading;
    }
}
