package com.patient.ghealth.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by user on 9/15/2016.
 */
public class DoctorSpecialities extends RealmObject
{

    private String doctorId;
    private String doctorSpeciality;
    private String doctorPictureUrl;

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorSpeciality() {
        return doctorSpeciality;
    }

    public void setDoctorSpeciality(String doctorSpeciality) {
        this.doctorSpeciality = doctorSpeciality;
    }

    public String getDoctorPictureUrl() {
        return doctorPictureUrl;
    }

    public void setDoctorPictureUrl(String doctorPictureUrl) {
        this.doctorPictureUrl = doctorPictureUrl;
    }
}


