package com.patient.ghealth.model;

/**
 * Created by user on 9/8/2016.
 */
public class TopicsInfo
{
    private int imageId;
    private String title;

    public TopicsInfo(int imageId, String title) {
        this.imageId = imageId;
        this.title = title;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
