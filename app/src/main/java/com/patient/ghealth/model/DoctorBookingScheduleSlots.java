package com.patient.ghealth.model;

import java.util.List;

/**
 * Created by user on 10/2/2016.
 */
public class DoctorBookingScheduleSlots
{
    private String type;
    private List<DoctorBookingSlotsData> doctorBookingSlotsDataList;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<DoctorBookingSlotsData> getDoctorBookingSlotsDataList() {
        return doctorBookingSlotsDataList;
    }

    public void setDoctorBookingSlotsDataList(List<DoctorBookingSlotsData> doctorBookingSlotsDataList) {
        this.doctorBookingSlotsDataList = doctorBookingSlotsDataList;
    }
}
