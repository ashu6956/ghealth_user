package com.patient.ghealth.model;

/**
 * Created by ad on 8/27/2015.
 */
public class GroupUserInfo {
    private  String group_user_id;//user id
    private  String group_username;//ex:919008990419
    private  String group_person_name;
    private  String group_useremail;//
    private  String group_userphone;
    private  String group_user_country_code;
    private  String group_user_status;
    private  String group_user_push_token;
    private  String group_user_api_token;
    private  String group_user_verification_code;
    private  String group_user_profile_pic_url;
    private  String group_user_active;
    private  String group_user_verified;
    private  String group_user_status_changed_at;
    private  String group_user_profile_pic_change_at;
    private  String group_user_deleted_at;
    private  String group_user_created_at;
    private  String group_user_updated_at;
    private String group_id;

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public GroupUserInfo()
    {

    }

    public GroupUserInfo(String group_user_id, String group_username, String group_person_name, String group_useremail, String group_userphone, String group_user_country_code, String group_user_status, String group_user_push_token, String group_user_api_token, String group_user_verification_code, String group_user_profile_pic_url, String group_user_active, String group_user_verified, String group_user_status_changed_at, String group_user_profile_pic_change_at, String group_user_deleted_at, String group_user_created_at, String group_user_updated_at, String user_group_id, String group_user_is_admin,String group_id) {
        this.group_user_id = group_user_id;
        this.group_username = group_username;
        this.group_person_name = group_person_name;
        this.group_useremail = group_useremail;
        this.group_userphone = group_userphone;
        this.group_user_country_code = group_user_country_code;
        this.group_user_status = group_user_status;
        this.group_user_push_token = group_user_push_token;
        this.group_user_api_token = group_user_api_token;
        this.group_user_verification_code = group_user_verification_code;
        this.group_user_profile_pic_url = group_user_profile_pic_url;
        this.group_user_active = group_user_active;
        this.group_user_verified = group_user_verified;
        this.group_user_status_changed_at = group_user_status_changed_at;
        this.group_user_profile_pic_change_at = group_user_profile_pic_change_at;
        this.group_user_deleted_at = group_user_deleted_at;
        this.group_user_created_at = group_user_created_at;
        this.group_user_updated_at = group_user_updated_at;
        this.user_group_id = user_group_id;
        this.group_user_is_admin = group_user_is_admin;
        this.group_id = group_id;
    }

    private  String user_group_id;//groupid

    public String getGroup_user_id() {
        return group_user_id;
    }

    public void setGroup_user_id(String group_user_id) {
        this.group_user_id = group_user_id;
    }

    public String getGroup_username() {
        return group_username;
    }

    public void setGroup_username(String group_username) {
        this.group_username = group_username;
    }

    public String getGroup_person_name() {
        return group_person_name;
    }

    public void setGroup_person_name(String group_person_name) {
        this.group_person_name = group_person_name;
    }

    public String getGroup_useremail() {
        return group_useremail;
    }

    public void setGroup_useremail(String group_useremail) {
        this.group_useremail = group_useremail;
    }

    public String getGroup_userphone() {
        return group_userphone;
    }

    public void setGroup_userphone(String group_userphone) {
        this.group_userphone = group_userphone;
    }

    public String getGroup_user_country_code() {
        return group_user_country_code;
    }

    public void setGroup_user_country_code(String group_user_country_code) {
        this.group_user_country_code = group_user_country_code;
    }

    public String getGroup_user_status() {
        return group_user_status;
    }

    public void setGroup_user_status(String group_user_status) {
        this.group_user_status = group_user_status;
    }

    public String getGroup_user_push_token() {
        return group_user_push_token;
    }

    public void setGroup_user_push_token(String group_user_push_token) {
        this.group_user_push_token = group_user_push_token;
    }

    public String getGroup_user_api_token() {
        return group_user_api_token;
    }

    public void setGroup_user_api_token(String group_user_api_token) {
        this.group_user_api_token = group_user_api_token;
    }

    public String getGroup_user_verification_code() {
        return group_user_verification_code;
    }

    public void setGroup_user_verification_code(String group_user_verification_code) {
        this.group_user_verification_code = group_user_verification_code;
    }

    public String getGroup_user_profile_pic_url() {
        return group_user_profile_pic_url;
    }

    public void setGroup_user_profile_pic_url(String group_user_profile_pic_url) {
        this.group_user_profile_pic_url = group_user_profile_pic_url;
    }

    public String getGroup_user_active() {
        return group_user_active;
    }

    public void setGroup_user_active(String group_user_active) {
        this.group_user_active = group_user_active;
    }

    public String getGroup_user_verified() {
        return group_user_verified;
    }

    public void setGroup_user_verified(String group_user_verified) {
        this.group_user_verified = group_user_verified;
    }

    public String getGroup_user_status_changed_at() {
        return group_user_status_changed_at;
    }

    public void setGroup_user_status_changed_at(String group_user_status_changed_at) {
        this.group_user_status_changed_at = group_user_status_changed_at;
    }

    public String getGroup_user_profile_pic_change_at() {
        return group_user_profile_pic_change_at;
    }

    public void setGroup_user_profile_pic_change_at(String group_user_profile_pic_change_at) {
        this.group_user_profile_pic_change_at = group_user_profile_pic_change_at;
    }

    public String getGroup_user_deleted_at() {
        return group_user_deleted_at;
    }

    public void setGroup_user_deleted_at(String group_user_deleted_at) {
        this.group_user_deleted_at = group_user_deleted_at;
    }

    public String getGroup_user_created_at() {
        return group_user_created_at;
    }

    public void setGroup_user_created_at(String group_user_created_at) {
        this.group_user_created_at = group_user_created_at;
    }

    public String getGroup_user_updated_at() {
        return group_user_updated_at;
    }

    public void setGroup_user_updated_at(String group_user_updated_at) {
        this.group_user_updated_at = group_user_updated_at;
    }

    public String getUser_group_id() {
        return user_group_id;
    }

    public void setUser_group_id(String user_group_id) {
        this.user_group_id = user_group_id;
    }

    public String getGroup_user_is_admin() {
        return group_user_is_admin;
    }

    public void setGroup_user_is_admin(String group_user_is_admin) {
        this.group_user_is_admin = group_user_is_admin;
    }

    private  String group_user_is_admin;

}
