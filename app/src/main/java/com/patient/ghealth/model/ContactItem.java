package com.patient.ghealth.model;

/**
 * Created by Lenovo on 6/18/2015.
 */
public class ContactItem {

    String name,phone;//name=sharan   //username=919008990419
    int image;
    boolean checked;
  String userType;

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    String id;
    String userId;

    public String getMsg_date_time() {
        return msg_date_time;
    }

    public void setMsg_date_time(String msg_date_time) {
        this.msg_date_time = msg_date_time;
    }

    String username,msg_date_time;

    public String getMessage_status_flag() {
        return message_status_flag;
    }

    public void setMessage_status_flag(String message_status_flag) {
        this.message_status_flag = message_status_flag;
    }

    String message_status_flag;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsingapp() {
        return usingapp;
    }

    public void setUsingapp(String usingapp) {
        this.usingapp = usingapp;
    }

    public String getBlockedbyfriend() {
        return blockedbyfriend;
    }

    public void setBlockedbyfriend(String blockedbyfriend) {
        this.blockedbyfriend = blockedbyfriend;
    }

    public String getBlockedbyme() {
        return blockedbyme;
    }

    public void setBlockedbyme(String blockedbyme) {
        this.blockedbyme = blockedbyme;
    }

    public String getProfile_pic_change_at() {

        return profile_pic_change_at;
    }

    public void setProfile_pic_change_at(String profile_pic_change_at) {
        this.profile_pic_change_at = profile_pic_change_at;
    }

    public String getStatus_changed_at() {
        return status_changed_at;
    }

    public void setStatus_changed_at(String status_changed_at) {
        this.status_changed_at = status_changed_at;
    }

    public String getProfile_pic_url() {

        return profile_pic_url;
    }

    public void setProfile_pic_url(String profile_pic_url) {
        this.profile_pic_url = profile_pic_url;
    }

    public String getStatus() {

        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCountry_code() {
        return country_code;
    }

    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    String country_code;
    String status;
    String profile_pic_url,profile_pic_localurl;
    String status_changed_at;
    String profile_pic_change_at;
    String blockedbyme;

    public String getProfile_pic_localurl() {
        return profile_pic_localurl;
    }

    public void setProfile_pic_localurl(String profile_pic_localurl) {
        this.profile_pic_localurl = profile_pic_localurl;
    }

    String blockedbyfriend,usingapp;
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {

        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {

        this.name = name;
    }


    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public ContactItem() {

    }

   /*
    public ContactItem(String name, int image, String phone) {
        this.name = name;
        this.image = image;
        this.phone = phone;
    }*/

    public ContactItem(String id,String name,String username,String phone,String country_code,String status,String profile_pic_url,String profile_pic_localurl,String status_changed_at,String profile_pic_change_at,String blockedbyme,String blockedbyfriend,String usingapp,String message_status_flag ) {
        this.id = id;
        this.name = name;//sharan
        this.username = username;//919008990419
        this.phone = phone;//9008990419
        this.country_code = country_code;
        this.status = status;
        this.profile_pic_url = profile_pic_url;
        this. profile_pic_localurl=profile_pic_localurl;
        this.status_changed_at = status_changed_at;
        this.profile_pic_change_at = profile_pic_change_at;
        this.status_changed_at = status_changed_at;
        this.profile_pic_change_at = profile_pic_change_at;
        this.blockedbyme = blockedbyme;
        this.blockedbyfriend = blockedbyfriend;
        this.usingapp = usingapp;
        this.message_status_flag=message_status_flag;
    }


    public ContactItem(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public ContactItem(String name, int image) {
        this.name = name;
        this.image = image;
    }

}
