package com.patient.ghealth.adapter;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.patient.ghealth.R;

/**
 * Created by user on 10/13/2016.
 */
public class SimpleDividerHorizontalItemDecoration extends RecyclerView.ItemDecoration
{
    private int space;

        public SimpleDividerHorizontalItemDecoration(int space) {
            this.space = space;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.left = space;
            outRect.right = space;
            outRect.bottom = space;
            outRect.top=space;

            // Add top margin only for the first item to avoid double space between items
            if(parent.getChildAdapterPosition(view) == 0)
                outRect.left = space;
        }

}
