package com.patient.ghealth.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.model.UserSettings;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import java.util.List;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by getit on 8/11/2016.
 */
public class UserSettingsAdapter extends BaseAdapter
{
    private Context mContext;
    private List<UserSettings> userSettingsList;
    public UserSettingsAdapter(Context context, List<UserSettings> userSettingsList)
    {
        mContext=context;
        this.userSettingsList=userSettingsList;
    }
    @Override
    public int getCount() {
        return userSettingsList.size();
    }

    @Override
    public Object getItem(int position) {
        return userSettingsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null)
       {
           convertView= LayoutInflater.from(mContext).inflate(R.layout.adpter_user_settings,null);
           holder=new ViewHolder();
           holder.userSettingsIcon= (ImageView) convertView.findViewById(R.id.iv_user_settings_icon);
           holder.userSettingsTitle= (TextView) convertView.findViewById(R.id.tv_user_settings_title);
           holder.answerCount= (TextView) convertView.findViewById(R.id.tv_answer_count);
           convertView.setTag(holder);
       }
        holder= (ViewHolder) convertView.getTag();
        AndyUtils.appLog("UserSettingsAdapter","Position" +position);
        if(position==3)
        {
            holder.answerCount.setVisibility(View.VISIBLE);
            holder.answerCount.setText(PreferenceHelper.getParam(mContext, Const.ANSWERCOUNT,0)+"");
        }
        else
        {
            holder.answerCount.setVisibility(View.GONE);
        }
        holder.userSettingsTitle.setText(userSettingsList.get(position).getUserSettingsText());
        holder.userSettingsIcon.setImageResource(userSettingsList.get(position).getUserSettingsIcon());
        return convertView;
    }

    class ViewHolder
    {
        ImageView userSettingsIcon;
        TextView userSettingsTitle,answerCount;
    }
}
