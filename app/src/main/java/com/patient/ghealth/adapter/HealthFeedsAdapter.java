package com.patient.ghealth.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.model.HealthFeedArticles;

import java.util.List;

/**
 * Created by getit on 8/6/2016.
 */
public class HealthFeedsAdapter extends BaseAdapter
{
    private Context mContext;
    private List<HealthFeedArticles> articlesList;
    public HealthFeedsAdapter(Context context, List<HealthFeedArticles> articlesList)
    {
        mContext=context;
        this.articlesList=articlesList;
    }
    @Override
    public int getCount() {
        return articlesList.size();
    }

    @Override
    public Object getItem(int position) {
        return articlesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView==null)
        {
            convertView= LayoutInflater.from(mContext).inflate(R.layout.adapter_healths_feed_item,null);
            holder=new ViewHolder();
            holder.healthFeedImage= (ImageView) convertView.findViewById(R.id.iv_adapter_healths_feed);
            holder.healthFeedsItemHeading= (TextView) convertView.findViewById(R.id.tv_adapter_health_feeds_topics_name);
            holder.bookMarkIcon= (ImageView) convertView.findViewById(R.id.iv_adapter_feeds_bookmark);
            convertView.setTag(holder);
        }
        holder= (ViewHolder) convertView.getTag();
        if(articlesList.get(position).getArticlePictureUrl().equals("")) {

        }
        else
        {
            Glide.with(mContext).load(articlesList.get(position).getArticlePictureUrl()).into(holder.healthFeedImage);
        }

        holder.healthFeedsItemHeading.setText(articlesList.get(position).getTopicHeading());

        if(Integer.parseInt(articlesList.get(position).getIsBookMarked())==0)
        {
            holder.bookMarkIcon.setImageResource(R.drawable.no_rated);
        }
        else if(Integer.parseInt(articlesList.get(position).getIsBookMarked())==1)
        {
            holder.bookMarkIcon.setImageResource(R.drawable.rating_star);
        }

        return convertView;
    }



    class ViewHolder
    {
        private ImageView healthFeedImage,bookMarkIcon;
        private TextView healthFeedsItemHeading;
    }
}
