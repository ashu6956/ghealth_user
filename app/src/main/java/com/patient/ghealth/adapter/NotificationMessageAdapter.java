package com.patient.ghealth.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.model.MessageDetails;

import java.util.List;

/**
 * Created by user on 11/30/2016.
 */
public class NotificationMessageAdapter extends RecyclerView.Adapter<NotificationMessageAdapter.CustomHolder> {


    private List<MessageDetails> messageDetailsList;
    private Context mContext;
    public NotificationMessageAdapter(Context context, List<MessageDetails> messageDetailsList)
    {
        mContext=context;
        this.messageDetailsList=messageDetailsList;
    }

    @Override
    public NotificationMessageAdapter.CustomHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view= LayoutInflater.from(mContext).inflate(R.layout.adapter_notification_message,null);
        return new CustomHolder(view);
    }

    @Override
    public void onBindViewHolder(NotificationMessageAdapter.CustomHolder holder, int position)
    {
        MessageDetails messageDetails=messageDetailsList.get(position);
        if(!messageDetails.getPictureUrl().equals(""))
        {
            Glide.with(mContext).load(messageDetails.getPictureUrl()).into(holder.doctorIcon);
        }
        holder.doctorName.setText(messageDetails.getDoctorName());
        holder.message.setText(mContext.getString(R.string.message_dot)+" "+messageDetails.getSent());

    }

    @Override
    public int getItemCount() {
        return messageDetailsList.size();
    }

    class CustomHolder extends RecyclerView.ViewHolder
    {
        private TextView doctorName,message;
        private ImageView doctorIcon;
        public CustomHolder(View itemView) {
            super(itemView);
            doctorName= (TextView) itemView.findViewById(R.id.tv_message_doctor_name);
            message= (TextView) itemView.findViewById(R.id.tv_message_doctor_message);
            doctorIcon= (ImageView) itemView.findViewById(R.id.iv_message_doctor_icon);

        }
    }
}
