package com.patient.ghealth.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.model.MyDateSchedule;
import com.patient.ghealth.utils.MyAnalogClock;

import java.util.List;

/**
 * Created by user on 10/3/2016.
 */
public class MyScheduleSlotsAdapter extends RecyclerView.Adapter<MyScheduleSlotsAdapter.CustomScheduleHolder> {

    private Context mContext;
    private List<MyDateSchedule> myDateScheduleList;

    public MyScheduleSlotsAdapter(Context context, List<MyDateSchedule> myDateScheduleList) {
        mContext = context;
        this.myDateScheduleList = myDateScheduleList;

    }

    @Override
    public MyScheduleSlotsAdapter.CustomScheduleHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_my_schedule_item_layout, null);
        CustomScheduleHolder holder = new CustomScheduleHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyScheduleSlotsAdapter.CustomScheduleHolder holder, int position) {
        try {

            StringBuilder doctorAddressStrongBuilder = new StringBuilder();
            holder.doctorName.setText(myDateScheduleList.get(position).getScheduleDoctorName());
            holder.clinicName.setText(myDateScheduleList.get(position).getScheduleClinicName());

            if (!myDateScheduleList.get(position).getScheduleClinicStreet().equals("")) {
                doctorAddressStrongBuilder.append(myDateScheduleList.get(position).getScheduleClinicStreet());
            }
            if (!myDateScheduleList.get(position).getScheduleClinicCity().equals("")) {
                doctorAddressStrongBuilder.append(",").append(myDateScheduleList.get(position).getScheduleClinicCity());
            }
            if (!myDateScheduleList.get(position).getScheduleClinicState().equals("")) {
                doctorAddressStrongBuilder.append(",").append(myDateScheduleList.get(position).getScheduleClinicState());
            }
            if (!myDateScheduleList.get(position).getScheduleClinicCountry().equals("")) {
                doctorAddressStrongBuilder.append(",").append(myDateScheduleList.get(position).getScheduleClinicCountry());
            }
            if (!myDateScheduleList.get(position).getScheduleClinicPostal().equals("")) {
                doctorAddressStrongBuilder.append(",").append(myDateScheduleList.get(position).getScheduleClinicPostal());
            }
            holder.clinicAddress.setText(doctorAddressStrongBuilder.toString());
            holder.scheduleTime.setText(mContext.getString(R.string.time_dot) + myDateScheduleList.get(position).getScheduleStartTime() + " " + myDateScheduleList.get(position).getTimeFormat());
            if (!myDateScheduleList.get(position).getScheduleStartTime().equals("")) {
                String[] starTime = myDateScheduleList.get(position).getScheduleStartTime().split(":");
                int hours = Integer.parseInt(starTime[0]);
                int minutes = Integer.parseInt(starTime[1]);
                holder.analogClock.setTime(hours, minutes, 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return myDateScheduleList.size();
    }

    public class CustomScheduleHolder extends RecyclerView.ViewHolder {

        private TextView doctorName, clinicName, clinicAddress, scheduleTime;
        private MyAnalogClock analogClock;

        public CustomScheduleHolder(View itemView) {
            super(itemView);
            doctorName = (TextView) itemView.findViewById(R.id.tv_schedule_doctor_name);
            clinicName = (TextView) itemView.findViewById(R.id.tv_schedule_clinic_name);
            clinicAddress = (TextView) itemView.findViewById(R.id.tv_schedule_clinic_address);
            scheduleTime = (TextView) itemView.findViewById(R.id.tv_schedule_time);
            analogClock = (MyAnalogClock) itemView.findViewById(R.id.schedule_analogClock);
        }
    }
}
