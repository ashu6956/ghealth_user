package com.patient.ghealth.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.model.ConsultHistory;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by user on 10/5/2016.
 */
public class ConsultHistoryAdapter extends RecyclerView.Adapter<ConsultHistoryAdapter.ConsultViewHolder> {

    private Context mContext;
    private List<ConsultHistory> consultHistoryList;
    DecimalFormat form = new DecimalFormat("0.00");
    private String currency;
    public ConsultHistoryAdapter(Context context, List<ConsultHistory> consultHistoryList) {
         mContext = context;
        this.consultHistoryList = consultHistoryList;
        currency= (String) PreferenceHelper.getParam(mContext, Const.Params.CURRENCY,"");
    }

    @Override
    public ConsultHistoryAdapter.ConsultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_consult_history_item_layout, null);
        ConsultViewHolder holder = new ConsultViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ConsultHistoryAdapter.ConsultViewHolder holder, int position) {
        ConsultHistory consultHistory = consultHistoryList.get(position);
        holder.doctorName.setText(consultHistory.getDoctorName());
        holder.date.setText(mContext.getString(R.string.date_dot) + " " + consultHistory.getHistoryDate());
        holder.duration.setText(mContext.getString(R.string.duration_dot) + " " + consultHistory.getDuration() + " " + mContext.getString(R.string.mins));
        holder.fee.setText(mContext.getString(R.string.fee_dot)+ " " + currency + " " + form.format(Double.valueOf(consultHistory.getConsultationFee())));
        holder.doctorNumber.setText(mContext.getString(R.string.mobile_dot) + " " + consultHistory.getDoctorNumber());


        if (consultHistory.getDoctorPictureUrl().equals("")) {
            holder.doctorIcon.setImageResource(R.drawable.profile);
        } else {
            Glide.with(mContext).load(consultHistory.getDoctorPictureUrl()).into(holder.doctorIcon);
        }
        if (consultHistory.getHistoryType().equalsIgnoreCase(mContext.getString(R.string.chat))) {
            holder.historyIcon.setImageResource(R.drawable.message_processing);
        } else if (consultHistory.getHistoryType().equalsIgnoreCase(mContext.getString(R.string.video))) {
            holder.historyIcon.setImageResource(R.drawable.video);
        } else if (consultHistory.getHistoryType().equalsIgnoreCase(mContext.getString(R.string.phone))) {
            holder.historyIcon.setImageResource(R.drawable.phone);
        }


    }

    @Override
    public int getItemCount() {
        return consultHistoryList.size();
    }

    public class ConsultViewHolder extends RecyclerView.ViewHolder {
        private ImageView doctorIcon, historyIcon;
        private TextView doctorName, date, duration, fee, doctorNumber;

        public ConsultViewHolder(View itemView) {
            super(itemView);
            doctorIcon = (ImageView) itemView.findViewById(R.id.iv_consult_history_profile_icon);
            doctorName = (TextView) itemView.findViewById(R.id.tv_consult_history_doctorName);
            doctorNumber = (TextView) itemView.findViewById(R.id.tv_consult_history_doctorNumber);
            date = (TextView) itemView.findViewById(R.id.tv_consult_history_date);
            duration = (TextView) itemView.findViewById(R.id.tv_consult_history_duration);
            fee = (TextView) itemView.findViewById(R.id.tv_consult_history_fee);
            historyIcon = (ImageView) itemView.findViewById(R.id.iv_history_icon);
        }
    }
}
