package com.patient.ghealth.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.model.AnswerData;
import com.patient.ghealth.utils.AndyUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * Created by user on 8/22/2016.
 */
public class NotificationAnswerAdapter extends RecyclerView.Adapter<NotificationAnswerAdapter.MessageViewHolder> {

    private Context mContext;
    private List<AnswerData> answerDataList;

    public NotificationAnswerAdapter(Context context, List<AnswerData> answerDataList) {
        mContext = context;
        this.answerDataList = answerDataList;
    }

    @Override
    public NotificationAnswerAdapter.MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_notification_answer, null);
        MessageViewHolder messageViewHolder = new MessageViewHolder(view);
        return messageViewHolder;
    }

    @Override
    public void onBindViewHolder(NotificationAnswerAdapter.MessageViewHolder holder, int position) {

        AnswerData answerData = answerDataList.get(position);
        holder.question.setText(answerData.getQuestion());
        holder.answer.setText(answerData.getAnswer());
        holder.subject.setText(answerData.getSubject());
        if (answerData.getDoctorPicture().equals("")) {
            holder.picture.setImageResource(R.drawable.profile);
        } else {
            Glide.with(mContext).load(answerData.getDoctorPicture()).into(holder.picture);
        }
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = dateFormat.format(c.getTime());
        AndyUtils.appLog("CurrentDate", formattedDate);

        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        String yesterdayDate = dateFormat.format(yesterday.getTime());
        AndyUtils.appLog("YesterDayDate", yesterdayDate);
        if (formattedDate.equals(answerData.getDate())) {
            holder.time.setText(answerData.getDate() + " " + answerData.getTime());
        } else if (yesterdayDate.equals(answerData.getDate())) {
            holder.time.setText(answerData.getDate() + " " + answerData.getTime());
        } else {
            holder.time.setText(answerData.getDate() + " " + answerData.getTime());
        }


    }

    @Override
    public int getItemCount() {
        return answerDataList.size();
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView question, subject, answer, time;
        ImageView picture;

        public MessageViewHolder(View itemView) {
            super(itemView);
            question = (TextView) itemView.findViewById(R.id.tv_question);
            subject = (TextView) itemView.findViewById(R.id.tv_subject);
            answer = (TextView) itemView.findViewById(R.id.tv_answer);
            time = (TextView) itemView.findViewById(R.id.tv_answer_time);
            picture = (ImageView) itemView.findViewById(R.id.iv_notification_message_adapter_icon);
        }
    }
}
