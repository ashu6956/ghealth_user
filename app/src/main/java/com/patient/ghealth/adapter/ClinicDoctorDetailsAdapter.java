package com.patient.ghealth.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.AddCardActivity;
import com.patient.ghealth.activity.ClinicDoctorDetailsActivity;
import com.patient.ghealth.activity.DoctorConsultConnectivity;
import com.patient.ghealth.activity.DoctorProfileActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.fragment.DoctorAvailabilityDialogFragment;
import com.patient.ghealth.model.CardDetails;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 11/11/2016.
 */
public class ClinicDoctorDetailsAdapter extends RecyclerView.Adapter<ClinicDoctorDetailsAdapter.CustomHolder> implements AsyncTaskCompleteListener {

    DecimalFormat form = new DecimalFormat("0.00");
    private Context mContext;
    private List<DoctorOnLineDetails> doctorOnLineDetailsList;
    private String type;
    private Activity deatilsActivity;
    private String demandDoctorId = "";
    private Dialog progressDialog, paymentDialog, doctorBookingDialog, debtAmountDialog;
    private String paymentType = "";
    private List<CardDetails> cardDetailsList;
    private String requestId = "";
    private int clickedPosition = 0;
    private boolean isNetDialogShowing = false;
    private String currency = "";

    public ClinicDoctorDetailsAdapter(Activity context, List<DoctorOnLineDetails> doctorOnLineDetailsList, String type, ClinicDoctorDetailsActivity activity) {
        mContext = context;
        this.doctorOnLineDetailsList = doctorOnLineDetailsList;
        this.type = type;
        this.deatilsActivity = context;
        currency = (String) PreferenceHelper.getParam(mContext, Const.Params.CURRENCY, "");
    }

    @Override
    public CustomHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_clinic_doctor_details_layout, null);
        CustomHolder holder = new CustomHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CustomHolder holder, final int position) {
        DoctorOnLineDetails doctorOnLineDetails = doctorOnLineDetailsList.get(position);
        holder.doctorName.setText(doctorOnLineDetails.getDoctorName());
        if (!doctorOnLineDetails.getDoctorRating().equals("")) {
            holder.rating.setVisibility(View.VISIBLE);
            holder.ratingText.setText(doctorOnLineDetails.getDoctorRating());
        }

        if (!doctorOnLineDetails.getDoctorExperience().equals("")) {
            holder.experienceIcon.setVisibility(View.VISIBLE);
            holder.experience.setText(doctorOnLineDetails.getDoctorExperience() + " " + mContext.getString(R.string.year_experience));
        }
        if (!doctorOnLineDetails.getDoctorHelpedPeople().equals("")) {
            holder.consultedIcon.setVisibility(View.VISIBLE);
            holder.helped.setText(mContext.getString(R.string.consulted) + " " + doctorOnLineDetails.getDoctorHelpedPeople() + " " + mContext.getString(R.string.people));
        }

        if (type.equals(Const.ONLINE_CONSULT)) {

            if (doctorOnLineDetails.getDoctorPictureUrl().equals("")) {
                holder.doctorIcon.setImageResource(R.drawable.profile);
            } else {
                Glide.with(mContext).load(doctorOnLineDetails.getDoctorPictureUrl()).into(holder.doctorIcon);

            }
            holder.doctorIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DoctorOnLineDetails doctorOnLineDetails = doctorOnLineDetailsList.get(position);
                    AndyUtils.appLog("DoctorOnLineDetails", doctorOnLineDetails.toString());
                    Intent profileIntent = new Intent(deatilsActivity, DoctorProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, doctorOnLineDetails);
                    bundle.putString(Const.DOCTOR_TYPE, type);
                    profileIntent.putExtras(bundle);
                    deatilsActivity.startActivity(profileIntent);
                }
            });

            holder.consultType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent consultIntent = new Intent(deatilsActivity, DoctorConsultConnectivity.class);
                    DoctorOnLineDetails consultDoctorOnLineDetails = doctorOnLineDetailsList.get(position);
                    Bundle consultBundle = new Bundle();
                    consultBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, consultDoctorOnLineDetails);
                    consultBundle.putString(Const.DOCTOR_TYPE, type);
                    consultIntent.putExtras(consultBundle);
                    deatilsActivity.startActivity(consultIntent);
                }
            });


            holder.consultType.setText(mContext.getString(R.string.consult));
            if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1) {
                holder.chat.setVisibility(View.VISIBLE);
            }
            if (Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1) {
                holder.call.setVisibility(View.VISIBLE);
            }
            if (Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                holder.video.setVisibility(View.VISIBLE);
            }

            if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) && Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                } else if (Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {
                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) + "\n" + mContext.getString(R.string.onwards));
                } else {
                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + "\n" + mContext.getString(R.string.onwards));
                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee())) {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                } else {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                } else {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                } else {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1) {
                holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) + "\n" + mContext.getString(R.string.onwards));
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1) {
                holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + mContext.getString(R.string.onwards));
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + "\n" + mContext.getString(R.string.onwards));
            }
        } else if (type.equals(Const.BOOKING)) {
            if (doctorOnLineDetails.getDoctorPictureUrl().equals("")) {
                holder.doctorIcon.setImageResource(R.drawable.profile);
            } else {
                Glide.with(mContext).load(doctorOnLineDetails.getDoctorPictureUrl()).into(holder.doctorIcon);

            }
            holder.doctorIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DoctorOnLineDetails doctorOnLineDetails = doctorOnLineDetailsList.get(position);
                    AndyUtils.appLog("DoctorOnLineDetails", doctorOnLineDetails.toString());
                    Intent profileIntent = new Intent(deatilsActivity, DoctorProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, doctorOnLineDetails);
                    bundle.putString(Const.DOCTOR_TYPE, type);
                    profileIntent.putExtras(bundle);
                    deatilsActivity.startActivity(profileIntent);
                }
            });

            holder.consultType.setText(mContext.getString(R.string.booking));
            holder.consultType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String doctorId = doctorOnLineDetailsList.get(position).getDoctorOnLineId();
                    AndyUtils.appLog("DoctorBookingFragment", "BookDoctorId" + doctorId);
                    DoctorAvailabilityDialogFragment availability = new DoctorAvailabilityDialogFragment();
                    PreferenceHelper.setParam(mContext, Const.DOCTOR_ID, doctorId);
                    availability.show(((FragmentActivity) deatilsActivity).getSupportFragmentManager(), "DoctorAvailabilityDialogFragment");

                }
            });

            holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + mContext.getString(R.string.onwards));
        } else if (type.equals(Const.ONDEMAND)) {
            if (doctorOnLineDetails.getDoctorPictureUrl().equals("")) {
                holder.doctorIcon.setImageResource(R.drawable.profile);
            } else {
                Glide.with(mContext).load(doctorOnLineDetails.getDoctorPictureUrl()).into(holder.doctorIcon);

            }
            holder.doctorIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DoctorOnLineDetails doctorOnLineDetails = doctorOnLineDetailsList.get(position);
                    AndyUtils.appLog("DoctorOnLineDetails", doctorOnLineDetails.toString());
                    Intent profileIntent = new Intent(deatilsActivity, DoctorProfileActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, doctorOnLineDetails);
                    bundle.putString(Const.DOCTOR_TYPE, type);
                    profileIntent.putExtras(bundle);
                    deatilsActivity.startActivity(profileIntent);
                }
            });
            holder.consultType.setText(mContext.getString(R.string.on_demand));
            holder.consultType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickedPosition = position;
                    showDoctorBookingDialog(doctorOnLineDetailsList.get(position));
                }
            });
            holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + mContext.getString(R.string.onwards));

        }
    }

    @Override
    public int getItemCount() {
        return doctorOnLineDetailsList.size();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_ADDED_CARDS:
                AndyUtils.appLog("Ashutosh", "GetAddedCardResponse" + response);
                try {

                    cardDetailsList = new ArrayList<>();
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true"))
                    {
                        JSONArray jsonArray = jsonObject.getJSONArray("cards");
                        if (jsonArray != null && jsonArray.length() > 0)
                        {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject cardObject = jsonArray.getJSONObject(i);
                                CardDetails cardDetails = new CardDetails();
                                cardDetails.setCardId(cardObject.optString("id"));
                                cardDetails.setCardNumber(cardObject.optString("last_four"));
                                cardDetails.setIsDefault(cardObject.optString("is_default"));
                                cardDetails.setType(cardObject.optString("card_type"));
                                cardDetailsList.add(cardDetails);
                            }

                        }
                        showPaymentDialog(cardDetailsList);
                    } else {
                        showPaymentDialog(cardDetailsList);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CREATE_REQUEST:
                AndyUtils.appLog("Ashutosh", "CreateRequestResponse" + response);
                try {

                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            JSONObject requestObject = jsonArray.getJSONObject(0);
                            requestId = requestObject.optString(Const.REQUEST_ID);
                            AndyUtils.appLog("RequestId", requestId);
                        }
                    } else {
//                        AndyUtils.removeProgressDialog();
                        if (progressDialog != null) {
                            progressDialog.cancel();
                        }
                        if (jsonObject.optString("error_message").equalsIgnoreCase("You have previous payment pending")) {
                            String amount = jsonObject.optString("amount");
                            showDebtAmountDialog(amount);
                        } else {
                            AndyUtils.showShortToast(jsonObject.getString("error_message"), mContext);
                        }
                    }

                } catch (JSONException e) {
                    if (progressDialog != null) {
                        progressDialog.cancel();
                    }
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CREATE_CANCEL_REQUEST:
                try {
                    AndyUtils.appLog("Ashutosh", "CancelRequestResponse" + response);
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        AndyUtils.showShortToast(jsonObject.optString("message"), mContext);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CLEAR_DEBT_AMOUNT:
                try {
                    AndyUtils.appLog("Ashutosh", "DebtClearResponse" + response);
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        AndyUtils.showShortToast(jsonObject.optString("message"), mContext);
                    } else {
                        AndyUtils.showShortToast(jsonObject.optString("error_message"), mContext);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

        }

    }

    private void showDebtAmountDialog(String amount) {
        isNetDialogShowing = true;
        debtAmountDialog = new Dialog(mContext);
        debtAmountDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        debtAmountDialog.setContentView(R.layout.dialog_debt_amount_layout);
        TextView debtAmountHeader = (TextView) debtAmountDialog.findViewById(R.id.tv_debt_header);
        debtAmountHeader.setText(mContext.getString(R.string.you_have) + " " + currency + form.format(Double.valueOf(amount)) + " " + mContext.getString(R.string.amount_pending));
        TextView exit = (TextView) debtAmountDialog.findViewById(R.id.tv_debt_cancel);
        exit.setText(mContext.getString(R.string.cancel));
        TextView payNow = (TextView) debtAmountDialog.findViewById(R.id.tv_debt_payNow);
        payNow.setText(mContext.getString(R.string.pay_now));

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeInternetDialog();
//                activity.finish();
            }
        });

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeInternetDialog();
                clearDebtAmount();
            }
        });
        debtAmountDialog.setCancelable(false);
        debtAmountDialog.show();
    }

    private void removeInternetDialog() {
        if (debtAmountDialog != null && debtAmountDialog.isShowing()) {
            debtAmountDialog.dismiss();
            isNetDialogShowing = false;
            debtAmountDialog = null;

        }
    }


    private void clearDebtAmount() {
        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CLEAR_DEBT_AMOUNT_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(mContext, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "DebtAmountMap" + map);
        new HttpRequester(mContext, Const.POST, map, Const.ServiceCode.CLEAR_DEBT_AMOUNT, this);
    }

    private void showPaymentDialog(final List<CardDetails> cardList) {
        paymentType = "";
        AndyUtils.appLog("onDemandClickedPosition", clickedPosition + "");
        DoctorOnLineDetails onLineDetails = doctorOnLineDetailsList.get(clickedPosition);
        paymentDialog = new Dialog(mContext, R.style.DialogDocotrTheme);
        paymentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        paymentDialog.setContentView(R.layout.dialog_payment_mode_layout);
        final RecyclerView paymentRecyclerView = (RecyclerView) paymentDialog.findViewById(R.id.rv_card_details);
        paymentRecyclerView.setVisibility(View.GONE);
        final LinearLayout paymentLinearLayout = (LinearLayout) paymentDialog.findViewById(R.id.ll_no_card_added);
        paymentLinearLayout.setVisibility(View.GONE);
        final Button addCardButton = (Button) paymentDialog.findViewById(R.id.bn_payment_add_card);
        addCardButton.setVisibility(View.GONE);
        final ImageButton cashButton = (ImageButton) paymentDialog.findViewById(R.id.ib_payment_cash);
        final ImageButton cardButton = (ImageButton) paymentDialog.findViewById(R.id.ib_payment_card);

        TextView appointmentCharge = (TextView) paymentDialog.findViewById(R.id.tv_payment_appointment_charge);
        TextView doctorName = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_name);
        TextView clinicName = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_clinic_name);
        TextView clinicAddress = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_clinic_address);
        final Button paymentButton = (Button) paymentDialog.findViewById(R.id.bn_payment_confirm);
        doctorName.setText(onLineDetails.getDoctorName());
        clinicName.setText(onLineDetails.getDoctorClinicName());
        clinicAddress.setText(onLineDetails.getClinicAddress());
        appointmentCharge.setText(mContext.getString(R.string.charge) + " " + currency + form.format(Double.valueOf(onLineDetails.getDoctorChatConsultFee())) + " " + mContext.getString(R.string.per_appointment));
        paymentDialog.show();

        paymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (paymentType.equals("")) {
                    AndyUtils.showShortToast(mContext.getString(R.string.please_choose_payment_mode), mContext);
                } else {
                    paymentDialog.cancel();
                    createDoctorRequest();
                }
            }
        });
        cardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                paymentType = Const.CARD;
                cardButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.doctor_booked_bg));
                cashButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.doctor_available_bg));
                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
                paymentRecyclerView.setLayoutManager(layoutManager);

                if (cardList.size() > 0) {
                    paymentRecyclerView.setVisibility(View.VISIBLE);
                    GetCardsAdapter adapter = new GetCardsAdapter(mContext, cardList);
                    paymentRecyclerView.setAdapter(adapter);
                } else {
                    paymentRecyclerView.setVisibility(View.GONE);
                    paymentLinearLayout.setVisibility(View.VISIBLE);
                    addCardButton.setVisibility(View.VISIBLE);
                    addCardButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            paymentDialog.cancel();
                            Intent intent = new Intent(mContext, AddCardActivity.class);
                            mContext.startActivity(intent);
                        }
                    });
                    paymentButton.setEnabled(false);
                    paymentButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.disable_book_consult_bg));

                }

            }
        });
        cashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentType = Const.CASH;
                paymentRecyclerView.setVisibility(View.GONE);
                cardButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.doctor_available_bg));
                cashButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.doctor_booked_bg));
                paymentLinearLayout.setVisibility(View.GONE);
                addCardButton.setVisibility(View.GONE);
                paymentButton.setEnabled(true);
                paymentButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.book_consult_bg));
            }
        });

    }

    private void createDoctorRequest() {
        AndyUtils.appLog("CreateDoctorRequest", clickedPosition + "");
        DoctorOnLineDetails doctorDetails = doctorOnLineDetailsList.get(clickedPosition);
        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
        LatLng latLng = PreferenceHelper.getObject(mContext, Const.CURRENT_LATLNG);
        if (latLng != null) {
            showProgressDialog();
            HashMap<String, String> map = new HashMap<>();
            map.put(Const.Params.URL, Const.ServiceType.CREATE_REQUEST);
            map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(mContext, Const.Params.ID, "")));
            map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, ""));
            map.put(Const.Params.REQUEST_TYPE, String.valueOf(4));
            map.put(Const.Params.DOCTOR_ID, String.valueOf(doctorDetails.getDoctorOnLineId()));
            map.put(Const.Params.S_LATITUDE, String.valueOf(latLng.latitude));
            map.put(Const.Params.S_LONGITUDE, String.valueOf(latLng.longitude));
            map.put(Const.Params.PAYMENT_MODE, paymentType);
            AndyUtils.appLog("Ashutosh", "GetRequestMap" + map);
            new HttpRequester(mContext, Const.POST, map, Const.ServiceCode.CREATE_REQUEST, this);
        }
    }

    private void showProgressDialog() {
        progressDialog = new Dialog(mContext);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_custom_progress_layout);
        final ProgressBar progressBar = (ProgressBar) progressDialog.findViewById(R.id.dialog_progressBar);
        Button cancelButton = (Button) progressDialog.findViewById(R.id.bn_cancel_progress_dialog);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.cancel();
                progressBar.setVisibility(View.GONE);
                cancelRequest();
            }
        });

        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void removeProgressDialog() {
        if (progressDialog != null) {
            progressDialog.cancel();
        }
    }

    private void cancelRequest() {
        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_CANCEL_REQUEST_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(mContext, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.REQUEST_ID, String.valueOf(requestId));

        AndyUtils.appLog("Ashutosh", "CancelRequestMap" + map);

        new HttpRequester(mContext, Const.POST, map, Const.ServiceCode.CREATE_CANCEL_REQUEST, this);

    }

    private void showDoctorBookingDialog(final DoctorOnLineDetails details) {
        doctorBookingDialog = new Dialog(mContext);
        doctorBookingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        doctorBookingDialog.setContentView(R.layout.dialog_doctor_booking_layout);
        ImageView doctorNext = (ImageView) doctorBookingDialog.findViewById(R.id.iv_dialog_on_demand_doctor);
        ImageView bookingNext = (ImageView) doctorBookingDialog.findViewById(R.id.iv_dialog_on_demand_booking);
        TextView needDoctorText = (TextView) doctorBookingDialog.findViewById(R.id.tv_on_demand_need);
        TextView doctorText = (TextView) doctorBookingDialog.findViewById(R.id.tv_doctor_on_demand_doctor);
        TextView bookingText = (TextView) doctorBookingDialog.findViewById(R.id.tv_doctor_on_demand_booking);
        ImageView doctorIcon = (ImageView) doctorBookingDialog.findViewById(R.id.iv_doctor_on_demand_doctor);
        ImageView bookingIcon = (ImageView) doctorBookingDialog.findViewById(R.id.iv_doctor_on_demand_calendar);
        RelativeLayout doctorLayout = (RelativeLayout) doctorBookingDialog.findViewById(R.id.rl_doctor_on_demand_doctor);
        RelativeLayout bookingLayout = (RelativeLayout) doctorBookingDialog.findViewById(R.id.rl_doctor_on_demand_booking);

        doctorIcon.setImageResource(R.drawable.stethoscope);
        doctorText.setText(mContext.getString(R.string.doctor));
        bookingText.setText(mContext.getString(R.string.schedule));
        needDoctorText.setText(mContext.getString(R.string.need_doctor));
        doctorNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                getAddedCard();
            }
        });
        bookingNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                demandDoctorId = details.getDoctorOnLineId();
                DoctorAvailabilityDialogFragment availability = new DoctorAvailabilityDialogFragment();
                PreferenceHelper.setParam(mContext, Const.DOCTOR_ID, demandDoctorId);
                AndyUtils.appLog("DoctorOnDemand", "DoctorId" + demandDoctorId);
                availability.show(((FragmentActivity) deatilsActivity).getSupportFragmentManager(), "DoctorAvailabilityDialogFragment");

            }
        });
        doctorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                getAddedCard();

            }
        });
        bookingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                demandDoctorId = details.getDoctorOnLineId();
                DoctorAvailabilityDialogFragment availability = new DoctorAvailabilityDialogFragment();
                PreferenceHelper.setParam(mContext, Const.DOCTOR_ID, demandDoctorId);
                AndyUtils.appLog("DoctorOnDemand", "DoctorId" + demandDoctorId);
                availability.show(((FragmentActivity) deatilsActivity).getSupportFragmentManager(), "DoctorAvailabilityDialogFragment");
            }
        });

        doctorBookingDialog.show();
    }

    private void getAddedCard() {
        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
//        AndyUtils.showSimpleProgressDialog(mContext, "Fetching All Cards...", false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.GET_ADDED_CARDS_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(mContext, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "GetAddedCardMap" + map);

        new HttpRequester(mContext, Const.GET, map, Const.ServiceCode.GET_ADDED_CARDS, this);


    }

    public class CustomHolder extends RecyclerView.ViewHolder {

        private ImageView doctorIcon, chat, video, call, rating, consultedIcon, experienceIcon;
        private TextView doctorName, doctorConsultantFee, consultType, experience, helped, ratingText;


        public CustomHolder(View itemView) {
            super(itemView);
            doctorIcon = (ImageView) itemView.findViewById(R.id.iv_doctor_list_profile_icon);
            doctorName = (TextView) itemView.findViewById(R.id.tv_doctorList_doctorName);
            experienceIcon = (ImageView) itemView.findViewById(R.id.iv_clinic_details_experience);
            consultedIcon = (ImageView) itemView.findViewById(R.id.iv_clinic_details_consulted);
            doctorConsultantFee = (TextView) itemView.findViewById(R.id.tv_doctor_list_doctorConsultFee);
            consultType = (TextView) itemView.findViewById(R.id.tv_doctor_list_consult);
            call = (ImageView) itemView.findViewById(R.id.iv_doctor_list_call);
            chat = (ImageView) itemView.findViewById(R.id.iv_doctor_list_chat);
            video = (ImageView) itemView.findViewById(R.id.iv_doctor_list_video);
            experience = (TextView) itemView.findViewById(R.id.tv_doctorlist_doctorExperience);
            helped = (TextView) itemView.findViewById(R.id.tv_doctorlist_doctorhelped);
            rating = (ImageView) itemView.findViewById(R.id.iv_rating);
            ratingText = (TextView) itemView.findViewById(R.id.tv_doctorList_rating);

        }
    }
}
