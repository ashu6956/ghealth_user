package com.patient.ghealth.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.model.DoctorSpecialities;
import com.patient.ghealth.utils.AndyUtils;

import java.util.List;

/**
 * Created by getit on 8/5/2016.
 */
public class HomeAdapter extends BaseAdapter
{

    private Context mContext;
    List<DoctorSpecialities> doctorSpecialitiesList;
    public HomeAdapter(Context context, List<DoctorSpecialities> doctorSpecialitiesList)
    {
         mContext=context;
        this.doctorSpecialitiesList=doctorSpecialitiesList;
    }
    @Override
    public int getCount() {
        return doctorSpecialitiesList.size();
    }

    @Override
    public Object getItem(int position) {
        return doctorSpecialitiesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        if(convertView==null)
        {
            convertView= LayoutInflater.from(mContext).inflate(R.layout.adapter_home,null);
            holder=new ViewHolder();
            holder.specialitiesIcon= (ImageView) convertView.findViewById(R.id.iv_home_specialities_adapter_icon);
            holder.specialitiesType= (TextView) convertView.findViewById(R.id.tv_home_specialities_adapter_type);
            convertView.setTag(holder);
        }
        holder= (ViewHolder) convertView.getTag();
        if(!doctorSpecialitiesList.get(position).getDoctorPictureUrl().equals("") && doctorSpecialitiesList.get(position).getDoctorPictureUrl()!=null)
        {
            Glide.with(mContext).load(doctorSpecialitiesList.get(position).getDoctorPictureUrl()).into(holder.specialitiesIcon);
        }
        else
        {

        }

        holder.specialitiesType.setText(doctorSpecialitiesList.get(position).getDoctorSpeciality());

        return convertView;
    }

    static class ViewHolder
    {
        ImageView specialitiesIcon;
        TextView specialitiesType;
    }
}
