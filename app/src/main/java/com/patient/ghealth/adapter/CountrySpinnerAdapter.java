package com.patient.ghealth.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.model.CountryDetails;

import java.util.List;

/**
 * Created by user on 12/7/2016.
 */
public class CountrySpinnerAdapter extends BaseAdapter
{

    private Context mContext;
    private List<CountryDetails> countryDetailsList;

    public CountrySpinnerAdapter(Context context, List<CountryDetails> countryDetailsList)
    {
        mContext=context;
        this.countryDetailsList=countryDetailsList;

    }

    @Override
    public int getCount() {
        return countryDetailsList.size();
    }

    @Override
    public Object getItem(int i) {
        return countryDetailsList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        view= LayoutInflater.from(mContext).inflate(R.layout.spinner_adapter_item_layout,null);
        TextView countryName= (TextView) view.findViewById(R.id.tv_country_name);
        ImageView countryFlag= (ImageView) view.findViewById(R.id.iv_country_flag);
        countryName.setText(countryDetailsList.get(i).getCountry());
        countryFlag.setImageResource(countryDetailsList.get(i).getImageId());


        return view;
    }
}
