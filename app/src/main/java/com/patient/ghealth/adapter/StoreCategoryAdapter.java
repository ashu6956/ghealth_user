package com.patient.ghealth.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.HealthStoreActivity;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.model.ProductCategory;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import java.util.List;

/**
 * Created by user on 11/18/2016.
 */
public class StoreCategoryAdapter extends RecyclerView.Adapter<StoreCategoryAdapter.CustomHolder> {

    private Context mContext;
    private List<ProductCategory> productCategoryList;
    private MainActivity activity;

    public StoreCategoryAdapter(Context context, List<ProductCategory> productCategoryList, MainActivity activity) {
        mContext = context;
        this.productCategoryList = productCategoryList;
        this.activity=activity;
    }

    @Override
    public CustomHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_category_item_layout, null);
        return new CustomHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomHolder holder, final int position) {
        ProductCategory category = productCategoryList.get(position);
        Glide.with(mContext).load(category.getImageUrl()).dontAnimate().placeholder(R.drawable.rectangle_place_holder).into(holder.categoryImage);

        holder.categoryImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent storeIntent = new Intent(mContext, HealthStoreActivity.class);
                AndyUtils.appLog("CategoryAdapter","CategoryId" +productCategoryList.get(position).getCategoriesId());
                PreferenceHelper.setParam(mContext,Const.Params.CATEGORY_ID,productCategoryList.get(position).getCategoriesId());
                mContext.startActivity(storeIntent);
                activity.finish();

            }
        });
    }

    @Override
    public int getItemCount() {
        return productCategoryList.size();
    }

    public class CustomHolder extends RecyclerView.ViewHolder {
        private ImageView categoryImage;

        public CustomHolder(View itemView) {
            super(itemView);
            categoryImage = (ImageView) itemView.findViewById(R.id.iv_categoryImage);
        }
    }
}
