package com.patient.ghealth.adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.HealthStoreActivity;
import com.patient.ghealth.fragment.ProductDetailFragment;
import com.patient.ghealth.model.ProductDetail;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amal on 11/08/16.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.MyViewHolder> {

    private List<ProductDetail> mProductDetails;
    private Activity mContext;
    private HealthStoreActivity activity;

    public ProductListAdapter(Activity mContext, List<ProductDetail> mProductDetails, HealthStoreActivity activity) {
        this.mProductDetails = mProductDetails;
        this.mContext = mContext;
        this.activity = activity;

    }

    @Override
    public ProductListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_product_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ProductListAdapter.MyViewHolder holder, int position) {
        final ProductDetail productDetail = mProductDetails.get(position);
        if (productDetail.getCat_images() != null && !productDetail.getCat_images().isEmpty())
            Glide.with(mContext).load(productDetail.getCat_images().get(0).getImage_url()).dontAnimate().placeholder(R.drawable.rectangle_place_holder).into(holder.productImage);
        holder.productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProductDetailFragment(productDetail);
            }
        });
        holder.product_title.setText(productDetail.getProductName());
        holder.product_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProductDetailFragment(productDetail);
            }
        });
        holder.seller_name.setText(mContext.getString(R.string.by) + " " + productDetail.getSellerInfo().getSeller_name());
        if (productDetail.getPrice().getAfter_discount() != -1) {
            holder.final_price.setText(Const.RM + productDetail.getPrice().getAfter_discount());
        } else {
            holder.final_price.setVisibility(View.GONE);
        }
        if (productDetail.getPrice().getOriginal_price() != -1 &&
                (productDetail.getPrice().getOriginal_price() != productDetail.getPrice().getAfter_discount())) {
            holder.original_price.setText(Const.RM + productDetail.getPrice().getOriginal_price());
            holder.original_price.setPaintFlags(holder.original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.original_price.setVisibility(View.GONE);
        }


        ArrayList<String> qtyArrayList = new ArrayList<>();
        for (int k = 0; k <= productDetail.getQuantity(); k++) {
            qtyArrayList.add(String.valueOf(k));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, qtyArrayList);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.qty_spinner.setAdapter(adapter);
        final boolean[] isProductAlreadyAdded = {false};
        if (productDetail.getQuantity() != 0)
            holder.qty_spinner.setSelection(1, false); // false to not call listener on initialise
        holder.qty_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    if (productDetail.getQuantity() != 0)
                        holder.qty_spinner.setSelection(1);
                    holder.qty_spinner.setVisibility(View.GONE);
                    holder.add_button.setVisibility(View.VISIBLE);
                    isProductAlreadyAdded[0] = false;
                    // updateToCart(productDetail, position);
//                    removeFromCart(productDetail.getProductId());
                    return;
                } else {
                        /*holder.qty_spinner.setVisibility(View.VISIBLE);
                        holder.add_button.setVisibility(View.GONE);*/
                    isProductAlreadyAdded[0] = true;
                }

//                if (!isProductAlreadyAdded[0])
//                    addToCart(productDetail, position);
//                else
//                    updateToCart(productDetail, position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // holder.qty_spinner.setVisibility(View.GONE);
        holder.add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                addToCart(productDetail, 1);
                holder.add_button.setVisibility(View.GONE);
                holder.mProgressBar.setVisibility(View.VISIBLE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        holder.mProgressBar.setVisibility(View.GONE);
                        holder.qty_spinner.setVisibility(View.VISIBLE);
                    }
                }, 1000);

            }
        });

        holder.viewOption_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProductDetailFragment(productDetail);
            }
        });

        if (!productDetail.getVariantList().isEmpty() || !productDetail.getLicensesList().isEmpty()) {
            holder.add_button.setVisibility(View.GONE);
            holder.viewOption_button.setVisibility(View.VISIBLE);
        }

        if (productDetail.getQuantity() <= 0) {
            holder.add_button.setVisibility(View.GONE);
            holder.product_ofs_overlay.setVisibility(View.VISIBLE);
        }

        if (productDetail.getRating().getTotalRatings() == 0)
            holder.ratingBar.setRating(0);
        else
            holder.ratingBar.setRating((productDetail.getRating().getTotalStar() / productDetail.getRating().getTotalRatings()));
        holder.ratingCount.setText("(" + productDetail.getRating().getTotalRatings() + ")");


    }

    private void gotoProductDetailFragment(ProductDetail productDetail) {
        ProductDetailFragment detailFragment = new ProductDetailFragment();
        AndyUtils.appLog("ProductListAdapter", "Product Id" + productDetail.getProductId());
        PreferenceHelper.setParam(mContext, Const.Params.PRODUCT_ID, productDetail.getProductId());
        Bundle args = new Bundle();
        args.putSerializable("ProductDetail", productDetail);
        detailFragment.setArguments(args);
        activity.addFragment(detailFragment, false, mContext.getString(R.string.health_store), "", false);
    }

    @Override
    public int getItemCount() {
        return mProductDetails.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView productImage;
        public TextView original_price, final_price, seller_name, product_title, ratingCount;
        public Button add_button, viewOption_button;
        public ProgressBar mProgressBar;
        public Spinner qty_spinner;
        public RelativeLayout product_ofs_overlay;
        public RatingBar ratingBar;

        public MyViewHolder(View view) {
            super(view);
            productImage = (ImageView) view.findViewById(R.id.product_image_preview);
            original_price = (TextView) view.findViewById(R.id.original_price_text);
            final_price = (TextView) view.findViewById(R.id.final_price_text);
            seller_name = (TextView) view.findViewById(R.id.seller_name_text);
            product_title = (TextView) view.findViewById(R.id.product_title);
            add_button = (Button) view.findViewById(R.id.add_button);
            mProgressBar = (ProgressBar) view.findViewById(R.id.item_add_progress);
            qty_spinner = (Spinner) view.findViewById(R.id.qty_spinner);
            viewOption_button = (Button) view.findViewById(R.id.viewOption_button);
            product_ofs_overlay = (RelativeLayout) view.findViewById(R.id.product_ofs_overlay);
            ratingBar = (RatingBar) view.findViewById(R.id.rating);
            ratingCount = (TextView) view.findViewById(R.id.ratingCount);
        }
    }
}
