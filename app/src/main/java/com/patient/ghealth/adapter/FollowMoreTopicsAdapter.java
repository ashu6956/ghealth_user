package com.patient.ghealth.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.model.HealthFeedArticles;
import com.patient.ghealth.model.PhoneContactInfo;
import com.patient.ghealth.model.TopicsInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 9/2/2016.
 */
public class FollowMoreTopicsAdapter extends RecyclerView.Adapter<FollowMoreTopicsAdapter.CustomTopicsViewHolder> {

    private Context mContext;
    private List<HealthFeedArticles> feedArticlesList;
    private ArrayList<HealthFeedArticles> arraylist;

    public FollowMoreTopicsAdapter(Context context, List<HealthFeedArticles> feedArticlesList)
    {

        mContext=context;
        this.feedArticlesList=feedArticlesList;
        this.arraylist=new ArrayList<HealthFeedArticles>();
        this.arraylist.addAll(feedArticlesList);
    }

    @Override
    public FollowMoreTopicsAdapter.CustomTopicsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.adapter_follow_more_topics_item_layout,null);
        CustomTopicsViewHolder topicsViewHolder=new CustomTopicsViewHolder(view);
        return topicsViewHolder;
    }

    @Override
    public void onBindViewHolder(FollowMoreTopicsAdapter.CustomTopicsViewHolder holder, int position)
    {
        holder.followTopicsName.setText(feedArticlesList.get(position).getCategory());
        if(!feedArticlesList.get(position).getArticlePictureUrl().equals(""))
        {
            Glide.with(mContext).load(feedArticlesList.get(position).getArticlePictureUrl()).into(holder.followTopicsImages);
        }


    }

    @Override
    public int getItemCount() {
        return feedArticlesList.size();
    }

    public class CustomTopicsViewHolder extends RecyclerView.ViewHolder
    {
        ImageView followTopicsImages;
        TextView  followTopicsName;
        public CustomTopicsViewHolder(View itemView) {
            super(itemView);
            followTopicsImages= (ImageView) itemView.findViewById(R.id.iv_follow_more_topics_images);
            followTopicsName= (TextView) itemView.findViewById(R.id.tv_follow_more_topics_name);
        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        feedArticlesList.clear();
        if (charText.length() == 0) {
            feedArticlesList.addAll(arraylist);
        } else {
            for (HealthFeedArticles wp : arraylist) {
                if (wp.getCategory().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    feedArticlesList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
