package com.patient.ghealth.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.fragment.ShippingAddressFragment;
import com.patient.ghealth.model.AddressDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 11/15/2016.
 */
public class AddressDetailsAdapter extends RecyclerView.Adapter<AddressDetailsAdapter.CustomHolder> implements AsyncTaskCompleteListener {
    private Context mContext;
    private List<AddressDetails> addressDetailsList;
    private boolean isClicked = false;

    public AddressDetailsAdapter(Context context, List<AddressDetails> addressDetailsList) {
        mContext = context;
        this.addressDetailsList = addressDetailsList;
    }

    @Override
    public CustomHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.adapter_shipping_address_layout, null);
        CustomHolder holder = new CustomHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final CustomHolder holder, final int position) {
        AddressDetails addressDetails = addressDetailsList.get(position);
        if (Integer.parseInt(addressDetails.getIsDefault()) == 1) {
            holder.radioButton.setChecked(true);
        } else if (Integer.parseInt(addressDetails.getIsDefault()) == 0) {
            holder.radioButton.setChecked(false);
        }

        StringBuilder builder=new StringBuilder();
        if(!addressDetails.getBlockNo().equals(""))
        {
            builder.append(addressDetails.getBlockNo()).append(",");
        }
        if(!addressDetails.getLandMark().equals(""))
        {
            builder.append(addressDetails.getLandMark()).append("");
        }
        if (!addressDetails.getAddress().equals(""))
        {
            builder.append(addressDetails.getAddress());
        }

        holder.address.setText(builder.toString());

        if(!addressDetails.getPhoneNumber().equals(""))
        {
            holder.phoneNo.setText(mContext.getString(R.string.phone)+addressDetails.getPhoneNumber());
        }

        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.radioButton.isChecked()) {
                    String addressId = addressDetailsList.get(position).getAddressId();
                    selectAddress(addressId);
                }
            }
        });

    }

    private void selectAddress(String id) {
        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.SELECT_ADDRESS_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(mContext, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.ADDRESS_ID, String.valueOf(id));

        AndyUtils.appLog("Ashutosh", "AddAddressMap" + map);

        new HttpRequester(mContext, Const.POST, map, Const.ServiceCode.SELECT_ADDRESS, this);
    }

    @Override
    public int getItemCount() {
        return addressDetailsList.size();
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.SELECT_ADDRESS:
                AndyUtils.appLog("Ashutosh", "selectAddressResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            if (addressDetailsList != null) {
                                addressDetailsList.clear();
                            }
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject addressJsonObject = jsonArray.optJSONObject(i);
                                AddressDetails addressDetails = new AddressDetails();
                                addressDetails.setAddressId(addressJsonObject.optString("address_id"));
                                addressDetails.setAddress(addressJsonObject.optString("shipping_address"));
                                addressDetails.setIsDefault(addressJsonObject.optString("is_default"));
                                addressDetailsList.add(addressDetails);
                            }

                            ShippingAddressFragment.addressAdapter.notifyDataSetChanged();

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
    }

    public class CustomHolder extends RecyclerView.ViewHolder {
        private RadioButton radioButton;
        private TextView address,phoneNo;
        public CustomHolder(View itemView) {
            super(itemView);
            radioButton = (RadioButton) itemView.findViewById(R.id.rb_shipping_address);
            address = (TextView) itemView.findViewById(R.id.tv_shipping_address);
            phoneNo= (TextView) itemView.findViewById(R.id.tv_shipping_phone);
        }
    }
}
