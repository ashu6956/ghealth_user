package com.patient.ghealth.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.model.DoctorBookingScheduleSlots;

import java.util.List;

/**
 * Created by user on 9/17/2016.
 */
public class TodayAndTomorrowBookDoctorAdapter extends RecyclerView.Adapter<TodayAndTomorrowBookDoctorAdapter.CustomBookingSlotsViewHolder> {

    public static List<DoctorBookingScheduleSlots> doctorBookingScheduleSlotsList;
    public static DoctorBookingShiftSlotsAdapter shiftSlotsAdapter;
    private Context mContext;



    public TodayAndTomorrowBookDoctorAdapter(Context context, List<DoctorBookingScheduleSlots> doctorBookingScheduleSlotsList)
    {
        mContext=context;
        this.doctorBookingScheduleSlotsList=doctorBookingScheduleSlotsList;
    }

    public static int convertDpToPixels(float dp, Context context) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );
    }

    @Override
    public TodayAndTomorrowBookDoctorAdapter.CustomBookingSlotsViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view=LayoutInflater.from(mContext).inflate(R.layout.adapter_doctor_slots_item_layout,null);

        CustomBookingSlotsViewHolder holder=new CustomBookingSlotsViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final TodayAndTomorrowBookDoctorAdapter.CustomBookingSlotsViewHolder holder, int position)
    {
         holder.shiftType.setText(doctorBookingScheduleSlotsList.get(position).getType());
//         int count=doctorBookingScheduleSlotsList.get(position).getDoctorBookingSlotsDataList().size();
//      //  double height=(Math.ceil(count/4.0))*80;
//        int height = convertDpToPixels((float) ((Math.ceil(count/4.0))*80),mContext);
//        ViewGroup.LayoutParams layoutParams = holder.doctorBookingSlotsGridView.getLayoutParams();
//        layoutParams.height = height; //this is in pixels
//        holder.doctorBookingSlotsGridView.setLayoutParams(layoutParams);
        shiftSlotsAdapter=new DoctorBookingShiftSlotsAdapter(mContext,doctorBookingScheduleSlotsList.get(position).getDoctorBookingSlotsDataList(),doctorBookingScheduleSlotsList.get(position).getType());
        holder.doctorBookingSlotsGridView.setAdapter(shiftSlotsAdapter);
        shiftSlotsAdapter.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return doctorBookingScheduleSlotsList.size();
    }


    public class CustomBookingSlotsViewHolder extends RecyclerView.ViewHolder{

        GridView doctorBookingSlotsGridView;
        TextView shiftType;

        public CustomBookingSlotsViewHolder(View itemView)
        {
            super(itemView);
            shiftType= (TextView) itemView.findViewById(R.id.tv_adapter_doctor_slots_shift);
            doctorBookingSlotsGridView = (GridView) itemView.findViewById(R.id.gv_adapter_doctor_slots_shift);
        }
    }
}
