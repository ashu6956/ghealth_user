package com.patient.ghealth.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.model.ProductDetail;

import java.util.List;

/**
 * Created by amal on 30/08/16.
 */
public class ReviewListAdapter extends RecyclerView.Adapter<ReviewListAdapter.MyViewHolder> {

    private List<ProductDetail.Reviews> reviewList;
    private Context mContext;

    public ReviewListAdapter(List<ProductDetail.Reviews> reviewList, Context mContext) {
        this.reviewList = reviewList;
        this.mContext = mContext;
    }

    @Override
    public ReviewListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_review_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReviewListAdapter.MyViewHolder holder, int position) {
        ProductDetail.Reviews review = reviewList.get(position);
        holder.ratingBar.setRating((float) review.getStar());
        holder.comment.setText(review.getComment());
        Glide.with(mContext).load(review.getImageUrl()).placeholder(R.drawable.user).into(holder.image);
        holder.user_name.setText(review.getName());
       /* holder.comment.setTextSize(50);
        holder.comment.setAlpha(.87f);*/
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView comment, user_name;
        public RatingBar ratingBar;
        public ImageView image;

        public MyViewHolder(View view) {
            super(view);
            comment = (TextView) view.findViewById(R.id.comment);
            ratingBar = (RatingBar) view.findViewById(R.id.rating);
            image = (ImageView) view.findViewById(R.id.profile_image);
            user_name = (TextView) view.findViewById(R.id.user_name);
        }
    }
}
