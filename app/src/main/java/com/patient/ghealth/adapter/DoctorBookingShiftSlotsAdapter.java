package com.patient.ghealth.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.custom_interface.DoctorBookSlotsListener;
import com.patient.ghealth.fragment.TodayBookDoctorFragment;
import com.patient.ghealth.model.DoctorBookingSlotsData;
import com.patient.ghealth.utils.AndyUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/2/2016.
 */
public class DoctorBookingShiftSlotsAdapter extends BaseAdapter {
    private Context mContext;
    private List<DoctorBookingSlotsData> bookingSlotsDataList;
    private boolean selectedItem = true;
    private boolean[] selected;
    private int lastClickedPosition = -1;
    private Holder holder;
    private boolean[] slotsBoolean;
    private TextView lastSelectedTextView;
    private boolean clicked = false;
    int pos;

    private ArrayList<String> selectedSlotList;
    private String scheduleShift;




    public DoctorBookingShiftSlotsAdapter(Context context, List<DoctorBookingSlotsData> bookingSlotsDataList,String scheduleShift) {
        mContext = context;
        this.bookingSlotsDataList = bookingSlotsDataList;
        selectedSlotList=new ArrayList<>();
        this.scheduleShift=scheduleShift;

    }

    @Override
    public int getCount() {
        return bookingSlotsDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return bookingSlotsDataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_booking_slots_timing_layout, null);
            holder = new Holder();
            holder.slotsTimingText = (TextView) convertView.findViewById(R.id.tv_doctor_slots_timing);
            convertView.setTag(holder);
        }

        holder = (Holder) convertView.getTag();
        holder.slotsTimingText.setText(bookingSlotsDataList.get(position).getStartTime());

        if(bookingSlotsDataList.get(position).isSelected()==true)
        {
            AndyUtils.appLog("ClickedSlotsId",bookingSlotsDataList.get(position).getAvailableSlotId());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.slotsTimingText.setBackground(mContext.getDrawable(R.drawable.doctor_booked_bg));
            } else {
                holder.slotsTimingText.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.doctor_booked_bg));
            }
        }
        else
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                holder.slotsTimingText.setBackground(mContext.getDrawable(R.drawable.doctor_available_bg));
            }
            else {
                holder.slotsTimingText.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.doctor_available_bg));
            }
        }
        holder.slotsTimingText.setTag(position);
        holder.slotsTimingText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                int clickedPosition = (int) v.getTag();
                String slotsId=bookingSlotsDataList.get(clickedPosition).getAvailableSlotId();
                AndyUtils.appLog("FirstClickedSlotsId",slotsId);

                for(int i=0;i<bookingSlotsDataList.size();i++)
                {
                    if(clickedPosition==i) {
                        if(!bookingSlotsDataList.get(i).isSelected())
                            bookingSlotsDataList.get(i).setSelected(true);
                        else
                            bookingSlotsDataList.get(i).setSelected(false);
                    }
                    else {
                        bookingSlotsDataList.get(i).setSelected(false);
                    }

                }
                notifyDataSetChanged();

            }
        });


        return convertView;
    }

//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }

    class Holder {
        private TextView slotsTimingText;
//        private LinearLayout slotsTimingLayout;
    }


}
