package com.patient.ghealth.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 9/20/2016.
 */
public class DoctorProfilePagerAdapter extends FragmentPagerAdapter
{

    private List<Fragment> fragmentsList=new ArrayList<>();
    private List<String>  titleList=new ArrayList<>();


    public DoctorProfilePagerAdapter(FragmentManager fm)
    {
        super(fm);
    }




    public void addItem(Fragment fragment,String title)
    {
        fragmentsList.add(fragment);
        titleList.add(title);
    }
    @Override
    public Fragment getItem(int position) {
        return fragmentsList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }
}
