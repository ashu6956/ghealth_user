package com.patient.ghealth.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.model.DoctorSpecialities;
import com.patient.ghealth.model.TopicsInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 10/14/2016.
 */
public class SearchAdapter extends BaseAdapter {

   private Context mContext;
    private List<DoctorSpecialities> doctorSpecialitiesList;
    private List<DoctorSpecialities> arraylist;
    public SearchAdapter(Context context, List<DoctorSpecialities> doctorSpecialitiesList)
    {
        mContext=context;
        this.doctorSpecialitiesList=doctorSpecialitiesList;
        this.arraylist=new ArrayList<DoctorSpecialities>();
        this.arraylist.addAll(doctorSpecialitiesList);

    }

    @Override
    public int getCount() {
        return doctorSpecialitiesList.size();
    }

    @Override
    public Object getItem(int position) {
        return doctorSpecialitiesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        SearchHolder holder;
        if(convertView==null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_search_list_item, null);
            holder=new SearchHolder();
            holder.serachText= (TextView) convertView.findViewById(R.id.tv_search_specialities);
            convertView.setTag(holder);

        }
        holder= (SearchHolder) convertView.getTag();
        holder.serachText.setText(doctorSpecialitiesList.get(position).getDoctorSpeciality());
        return convertView;
    }

    class SearchHolder
    {
        private TextView serachText;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        doctorSpecialitiesList.clear();
        if (charText.length() == 0) {
            doctorSpecialitiesList.addAll(arraylist);
        } else {
            for (DoctorSpecialities wp : arraylist) {
                if (wp.getDoctorSpeciality().toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    doctorSpecialitiesList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
