package com.patient.ghealth.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.CheckoutActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.fragment.AddAddressFragment;
import com.patient.ghealth.fragment.GetAddressFragment;
import com.patient.ghealth.fragment.PaymentMethodFragment;
import com.patient.ghealth.fragment.UpdateAddressFragment;
import com.patient.ghealth.model.Address;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * Created by amal on 30/08/16.
 */
public class AddressListAdapter extends RecyclerView.Adapter<AddressListAdapter.MyViewHolder> implements AsyncTaskCompleteListener {

    private List<Address> addressList;
    private Activity mContext;
    private CheckoutActivity activity;

    public AddressListAdapter(List<Address> addressList, Activity mContext, CheckoutActivity activity) {
        this.addressList = addressList;
        this.mContext = mContext;
        this.activity = activity;
    }

    @Override
    public AddressListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_address_item_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AddressListAdapter.MyViewHolder holder, final int position) {

        if (position == addressList.size() - 1) {
            holder.address.setText(mContext.getString(R.string.add_a_new_address));
            holder.pincode.setVisibility(View.GONE);
            holder.number.setVisibility(View.GONE);
            holder.name.setVisibility(View.GONE);
            holder.edit_address.setVisibility(View.GONE);
            holder.remove_address_button.setVisibility(View.GONE);
            holder.address_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AddAddressFragment addAddressFragment = new AddAddressFragment();
                    activity.addFragment(addAddressFragment, false, mContext.getString(R.string.health_store), "", false);
                }
            });
        } else {
            holder.edit_address.setVisibility(View.VISIBLE);
            holder.remove_address_button.setVisibility(View.VISIBLE);

            final Address address = addressList.get(position);
            holder.edit_address.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UpdateAddressFragment updateAddressFragment = new UpdateAddressFragment();
                    Bundle args = new Bundle();
                    args.putSerializable("address", address);
                    updateAddressFragment.setArguments(args);
                    gotoFragment(updateAddressFragment);
                }
            });
//            final int arrayListPosition = position;
            holder.remove_address_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AndyUtils.appLog("Position", position + "");
                    AndyUtils.appLog("AddressId", addressList.get(position).get_ID());
                    new AlertDialog.Builder(mContext)
                            .setMessage(mContext.getString(R.string.dialog_delete_address))
                            .setPositiveButton(mContext.getString(R.string.yes), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    removeAddress(addressList.get(position));
                                    addressList.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, addressList.size());

                                }
                            })
                            .setNegativeButton(mContext.getString(R.string.no), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .show();
                }
            });

            holder.address_layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!GetAddressFragment.isFromAccount) {

                        if (GetAddressFragment.forBillingAddress)
                            CheckoutActivity.billingAddress = addressList.get(position);
                        else
                            CheckoutActivity.checkoutAddress = addressList.get(position);


                        PaymentMethodFragment paymentMethodFragment = new PaymentMethodFragment();
                        gotoFragment(paymentMethodFragment);
                    }
                }
            });


            if (GetAddressFragment.isFromAccount)
                holder.right_icon.setVisibility(View.GONE);

            if (address.getNAME().isEmpty())
                holder.name.setVisibility(View.GONE);
            else {
                holder.name.setVisibility(View.VISIBLE);
                holder.name.setText(address.getNAME());
            }
            if (address.getNUMBER().isEmpty())
                holder.number.setVisibility(View.GONE);
            else {
                holder.number.setVisibility(View.VISIBLE);
                holder.number.setText(address.getNUMBER());
            }
            if (address.getPINCODE().isEmpty())
                holder.pincode.setVisibility(View.GONE);
            else {
                holder.pincode.setVisibility(View.VISIBLE);
                holder.pincode.setText(address.getPINCODE());
            }
            if (address.getADDRESS().isEmpty())
                holder.address.setVisibility(View.GONE);
            else {
                holder.address.setVisibility(View.VISIBLE);
                holder.address.setText(address.getADDRESS());
            }

        }
    }


    @Override
    public int getItemCount() {
        return addressList.size();
    }

    private void removeAddress(Address address) {

        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
        AndyUtils.showSimpleProgressDialog(activity, "Address deleting...", false);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.REMOVE_ADDRESS_URL + address.get_ID());

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(mContext, Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "RemoveAddressMap" + map);
        AndyUtils.appLog("Ashutosh", "RemoveHeaderMap" + headerMap);
        new HttpRequester(mContext, Const.DELETE, map, Const.ServiceCode.REMOVE_ADDRESS, this, headerMap);

    }

    private void gotoFragment(Fragment fragment) {

        activity.addFragment(fragment, false, mContext.getString(R.string.checkout), "", false);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode) {
            case Const.ServiceCode.REMOVE_ADDRESS:
                AndyUtils.removeProgressDialog();
                AndyUtils.appLog("AddresssListAdapter", "RemoveAddressResponse" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.optInt(Const.Params.STATUS_CODE) == 200) {
                        Toast.makeText(mContext, "Address deleted successfully", Toast.LENGTH_SHORT).show();
                        // TODO: go back to get address fragment
                        //mContext.getFragmentManager().popBackStack();
                    } else if (jsonObject.optInt(Const.Params.STATUS_CODE) == 500) {
                        Toast.makeText(mContext, jsonObject.optString(Const.Params.STATUS_MESSAGE), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }

        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView address, pincode, name, number;
        public TextView edit_address, remove_address_button;
        public LinearLayout address_layout;
        public ImageView right_icon;

        public MyViewHolder(View view) {
            super(view);
            address = (TextView) view.findViewById(R.id.address);
            pincode = (TextView) view.findViewById(R.id.pincode);
            name = (TextView) view.findViewById(R.id.name);
            number = (TextView) view.findViewById(R.id.number);
            edit_address = (TextView) view.findViewById(R.id.edit_address_button);
            remove_address_button = (TextView) view.findViewById(R.id.remove_address_button);
            address_layout = (LinearLayout) view.findViewById(R.id.address_layout);
            right_icon = (ImageView) view.findViewById(R.id.right_icon);
        }
    }

}
