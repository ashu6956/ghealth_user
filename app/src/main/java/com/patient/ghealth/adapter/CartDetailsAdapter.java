package com.patient.ghealth.adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.HealthStoreActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.fragment.CartDetailsFragment;
import com.patient.ghealth.model.CartProductDetail;
import com.patient.ghealth.model.ProductDetail;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by amal on 11/08/16.
 */
public class CartDetailsAdapter extends RecyclerView.Adapter<CartDetailsAdapter.MyViewHolder> implements AsyncTaskCompleteListener {

    private List<CartProductDetail> mProductDetails;
    private Activity mContext;
    private HealthStoreActivity activity;

    public CartDetailsAdapter(List<CartProductDetail> mProductDetails, Activity mContext, HealthStoreActivity activity) {
        this.mProductDetails = mProductDetails;
        this.mContext = mContext;
        this.activity = activity;
    }

    @Override
    public CartDetailsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_cart_product_item, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final CartDetailsAdapter.MyViewHolder holder, final int position) {
        final ProductDetail productDetail = mProductDetails.get(position).getmProductDetail();
        int qty_in_cart = mProductDetails.get(position).getProductQty();
        AndyUtils.appLog("Qty In Cart", qty_in_cart + "");
        final String cart_id = mProductDetails.get(position).getCart_id();

        if (productDetail.getCat_images() != null && !productDetail.getCat_images().isEmpty())
            Glide.with(mContext).load(productDetail.getCat_images().get(0).getImage_url()).dontAnimate().placeholder(R.drawable.rectangle_place_holder).into(holder.productImage);
        holder.product_title.setText(productDetail.getProductName());
        holder.product_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProductDetailFragment(productDetail.getProductId());
            }
        });
        holder.seller_name.setText(mContext.getString(R.string.by) + " " + productDetail.getSellerInfo().getSeller_name());
        if (productDetail.getPrice().getAfter_discount() != -1) {
            holder.final_price.setText(Const.RM + productDetail.getPrice().getAfter_discount());
        } else {
            holder.final_price.setVisibility(View.GONE);
        }
        if (productDetail.getPrice().getOriginal_price() != -1 &&
                (productDetail.getPrice().getOriginal_price() != productDetail.getPrice().getAfter_discount())) {
            holder.original_price.setText(Const.RM + productDetail.getPrice().getOriginal_price());
            holder.original_price.setPaintFlags(holder.original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.original_price.setVisibility(View.GONE);
        }

        ArrayList<String> qtyArrayList = new ArrayList<>();
        for (int k = 0; k <= productDetail.getQuantity(); k++) {
            qtyArrayList.add(String.valueOf(k));
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, qtyArrayList);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        holder.qty_spinner.setAdapter(adapter);
        if (productDetail.getQuantity() != 0)
            holder.qty_spinner.setSelection(qty_in_cart, false);
        holder.qty_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {
                    removeFromCart(cart_id);
                    mProductDetails.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, mProductDetails.size());
                } else updateToCart(productDetail, position, cart_id);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        holder.remove_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFromCart(cart_id);
                mProductDetails.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, mProductDetails.size());
            }
        });

    }

    private void updateToCart(ProductDetail productDetail, int qty, String cart_id) {

        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.UPDATE_CART_URL);
        map.put(Const.Params.PRODUCT_ID, productDetail.getProductId());
        map.put(Const.QUANTITY, String.valueOf(qty));
        map.put(Const.Params.CART_ID, cart_id);

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(mContext, Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "UpdateCartMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(mContext, Const.POST, map, Const.ServiceCode.UPDATE_CART, this, headerMap);

    }

    private void removeFromCart(String id) {
        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.DELETE_REMOVE_CART_URL + Const.Params.CART_ID + "=" + id);

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(mContext, Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "CartCountMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(mContext, Const.DELETE, map, Const.ServiceCode.DELETE_REMOVE_CART, this, headerMap);

    }


    private void gotoProductDetailFragment(String productId) {
//        ProductDetailFragment newFragment = new ProductDetailFragment();
//        Bundle args = new Bundle();
//        args.putString("productId", productId);
//        newFragment.setArguments(args);
//        FragmentTransaction transaction = mContext.getFragmentManager().beginTransaction();
//        transaction.replace(R.id.display_container, newFragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
    }

    @Override
    public int getItemCount() {
        return mProductDetails.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.DELETE_REMOVE_CART:
                AndyUtils.appLog("Ashutosh", "RemoveCartResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        setCartCount();
                    } else if (jsonObject.optInt(Const.Params.STATUS_CODE) == 500) {
                        Toast.makeText(mContext, jsonObject.optString(Const.Params.STATUS_MESSAGE), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.GET_CART_COUNT:
                AndyUtils.appLog("Ashutosh", "CartCountResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {
                        parseGetGuestCartCount(jsonObject);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.UPDATE_CART:
                AndyUtils.appLog("Ashutosh", "UpdateCartResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString(Const.STATUS).equals(Const.SUCCESS)) {

                        setCartCount();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
    }


    private void setCartCount() {

        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.GET_CART_COUNT_URL);

        HashMap<String, String> headerMap = new HashMap<>();
        headerMap.put(Const.Params.AUTHORIZATION, (String) PreferenceHelper.getParam(mContext, Const.Params.ECOM_TOKEN, ""));
        AndyUtils.appLog("Ashutosh", "CartCountMap" + map);
        AndyUtils.appLog("Ashutosh", "HeaderMap" + headerMap);
        new HttpRequester(mContext, Const.GET, map, Const.ServiceCode.GET_CART_COUNT, this, headerMap);

    }


    private void parseGetGuestCartCount(JSONObject result) {

        if (result == null)
            return;

        try {
            JSONObject responseObject = result.optJSONObject(Const.RESPONSE);
            AndyUtils.appLog("AShutosh", "Response Object" + responseObject.toString());
            int count = responseObject.optInt("count");
            activity.cartCount.setText(count + "");
//                mContext.invalidateOptionsMenu();
            CartDetailsFragment.total_cost = responseObject.optDouble("total_price");
            CartDetailsFragment.item_qty = count;
            CartDetailsFragment.setOrderCostCountView();
            if (CartDetailsFragment.item_qty == 0) {
                CartDetailsFragment.empty_cart_view.setVisibility(View.VISIBLE);
                CartDetailsFragment.cart_view.setVisibility(View.GONE);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView productImage;
        public TextView original_price, final_price, seller_name, product_title, remove_product;
        public Spinner qty_spinner;


        public MyViewHolder(View view) {
            super(view);
            productImage = (ImageView) view.findViewById(R.id.product_image_preview);
            original_price = (TextView) view.findViewById(R.id.original_price_text);
            final_price = (TextView) view.findViewById(R.id.final_price_text);
            seller_name = (TextView) view.findViewById(R.id.seller_name_text);
            product_title = (TextView) view.findViewById(R.id.product_title);
            qty_spinner = (Spinner) view.findViewById(R.id.qty_spinner);
            remove_product = (TextView) view.findViewById(R.id.remove_from_cart);
        }
    }

}
