package com.patient.ghealth.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.patient.ghealth.activity.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by getit on 8/10/2016.
 */
public class MyMedicalPagerAdapter extends FragmentStatePagerAdapter
{
    List<Fragment> fragmentList=new ArrayList<>();
    List<String>  titleList=new ArrayList<>();
    public MyMedicalPagerAdapter(FragmentManager fm)
    {
        super(fm);
    }
    @Override
    public Fragment getItem(int position)
    {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    public void addItem(Fragment fragment)
    {
        fragmentList.add(fragment);

    }


}
