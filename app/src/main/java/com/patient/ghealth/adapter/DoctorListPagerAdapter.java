package com.patient.ghealth.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.patient.ghealth.fragment.DoctorBookingFragment;
import com.patient.ghealth.fragment.DoctorNearByBookingFragment;
import com.patient.ghealth.fragment.DoctorNearByOnlineConsultFragment;
import com.patient.ghealth.fragment.DoctorOnlineConsultFragment;
import com.patient.ghealth.utils.Const;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by getit on 8/9/2016.
 */
public class DoctorListPagerAdapter extends FragmentStatePagerAdapter
{
        private List<Fragment> fragmentList=new ArrayList<>();
    private List<String> titleList =new ArrayList<>();
    private String type;
    private int tabCount;
    public DoctorListPagerAdapter(FragmentManager fm)
    {
        super(fm);
//        this.tabCount=tabCount;
//        this.type=type;
    }
    @Override
    public Fragment getItem(int position)
    {
      return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    public void addItem(Fragment fragment, String title)
    {
        fragmentList.add(fragment);
        titleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }
}
