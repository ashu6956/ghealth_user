package com.patient.ghealth.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.patient.ghealth.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 8/22/2016.
 */
public class NotificationPagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragmentList=new ArrayList<>();
    private List<String> titleList =new ArrayList<>();

    public NotificationPagerAdapter(FragmentManager fm)
    {
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return titleList.size();
    }

    public void addItem(Fragment fragment, String title)
    {
        fragmentList.add(fragment);
        titleList.add(title);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleList.get(position);
    }

    public View getTabView(int position, Context context) {
        // Given you have a custom layout in `res/layout/custom_tab.xml` with a TextView and ImageView
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab_view_layout, null);
        TextView tv = (TextView) v.findViewById(R.id.tv_tabText);
        TextView count= (TextView) v.findViewById(R.id.tv_no_notification);
        count.setText("2");
        tv.setText(titleList.get(position));

        return v;
    }
}
