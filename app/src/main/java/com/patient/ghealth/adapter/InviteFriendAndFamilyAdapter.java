package com.patient.ghealth.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.model.PhoneContactInfo;
import com.patient.ghealth.utils.AndyUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by user on 9/7/2016.
 */
public class InviteFriendAndFamilyAdapter extends BaseAdapter {
    public static ArrayList<String> selectedNumberList;
    private Context mContext;
    private List<PhoneContactInfo> contactList;
    private ArrayList<PhoneContactInfo> arraylist;
    private boolean[] contactsChecked;

    public InviteFriendAndFamilyAdapter(Context context, List<PhoneContactInfo> contactList) {

        mContext = context;
        this.contactList = contactList;
        this.arraylist = new ArrayList<PhoneContactInfo>();
        contactsChecked = new boolean[contactList.size()];
        this.arraylist.addAll(contactList);
        selectedNumberList = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return contactList.size();
    }

    @Override
    public Object getItem(int position) {
        return contactList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CustomPhoneViewHolder phoneViewHolder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.adapter_invite_friend_family_item_layout, null);
            phoneViewHolder = new CustomPhoneViewHolder();
            phoneViewHolder.contactDisplayName = (TextView) convertView.findViewById(R.id.tv_invite_friend_family_contact_name);
            phoneViewHolder.contactCheckBox = (CheckBox) convertView.findViewById(R.id.cb_invite_friend_family);

            convertView.setTag(phoneViewHolder);
        }

        phoneViewHolder = (CustomPhoneViewHolder) convertView.getTag();
        phoneViewHolder.contactDisplayName.setText(contactList.get(position).getContactName());
        phoneViewHolder.contactCheckBox.setTag(position);
        phoneViewHolder.contactCheckBox.setChecked(contactList.get(position).isChecked);


        if (contactsChecked[position]) {
            phoneViewHolder.contactCheckBox.setChecked(contactList.get(position).isChecked);

        } else {
            phoneViewHolder.contactCheckBox.setChecked(contactList.get(position).isChecked);
        }


        final CustomPhoneViewHolder finalPhoneViewHolder = phoneViewHolder;
        phoneViewHolder.contactCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int clickedPosition = (int) v.getTag();
                if (finalPhoneViewHolder.contactCheckBox.isChecked()) {
                    AndyUtils.appLog("InviteFriendAndFamilyAdapter", "Checked Checkbox");
                    contactsChecked[clickedPosition] = true;
                    contactList.get(clickedPosition).isChecked = true;
                    AndyUtils.appLog("PhoneNumber", contactList.get(clickedPosition).getContactNumber());
                    selectedNumberList.add(contactList.get(clickedPosition).getContactNumber());
                } else {
                    AndyUtils.appLog("InviteFriendAndFamilyAdapter", "UnChecked Checkbox");
                    contactsChecked[clickedPosition] = false;
                    contactList.get(clickedPosition).isChecked = false;
                    selectedNumberList.remove(contactList.get(clickedPosition).getContactNumber());
                }

            }
        });
        return convertView;
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        contactList.clear();
        if (charText.length() == 0) {
            contactList.addAll(arraylist);
        } else {
            for (PhoneContactInfo wp : arraylist) {
                if (wp.getContactName().toLowerCase(Locale.getDefault())
                        .contains(charText) || wp.getContactNumber().contains(charText)) {
                    contactList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    class CustomPhoneViewHolder {
        TextView contactDisplayName;
        CheckBox contactCheckBox;
    }

}
