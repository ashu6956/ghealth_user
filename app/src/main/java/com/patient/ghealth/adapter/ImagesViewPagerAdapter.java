package com.patient.ghealth.adapter;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.model.ProductDetail;

import java.util.ArrayList;

/**
 * Created by amal on 17/08/16.
 */
public class ImagesViewPagerAdapter extends PagerAdapter {
    private Activity mContext;
    private ArrayList<ProductDetail.Cat_image> cat_images;

    public ImagesViewPagerAdapter(Activity mContext, ArrayList<ProductDetail.Cat_image> cat_images) {
        this.mContext = mContext;
        this.cat_images = cat_images;
    }

    @Override
    public int getCount() {
        return cat_images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_pager_item);
        Glide.with(mContext).load(cat_images.get(position).getImage_url()).dontAnimate().placeholder(R.drawable.rectangle_place_holder).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                gotoFragment(position);
            }
        });
        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

//    private void gotoFragment(int position) {
//        ArrayList<String> imageUrls = new ArrayList<>();
//        for (int i = 0; i < cat_images.size(); i++) {
//            imageUrls.add(cat_images.get(i).getImage_url());
//        }
//        FullScreenImageFragment newFragment = new FullScreenImageFragment();
//        Bundle args = new Bundle();
//        args.putStringArrayList("images",imageUrls);
//        args.putInt("imagePosition",position);
//        newFragment.setArguments(args);
//        FragmentTransaction transaction = mContext.getFragmentManager().beginTransaction();
//        transaction.replace(R.id.display_container, newFragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
//    }
}
