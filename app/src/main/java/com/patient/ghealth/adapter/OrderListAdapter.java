package com.patient.ghealth.adapter;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.fragment.OrderDetailFragment;
import com.patient.ghealth.fragment.RatingFragment;
import com.patient.ghealth.model.OrderDetail;


import java.util.List;

/**
 * Created by amal on 30/08/16.
 */
public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.MyViewHolder> {

    private List<OrderDetail> orderDetailList;
    private Activity mContext;
    private MainActivity activity;

    public OrderListAdapter(Activity mContext,List<OrderDetail> orderDetailList,MainActivity activity) {
        this.orderDetailList = orderDetailList;
        this.mContext = mContext;
        this.activity=activity;
    }

    @Override
    public OrderListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_order_history_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrderListAdapter.MyViewHolder holder, int position) {
        final OrderDetail orderDetail = orderDetailList.get(position);
        Glide.with(mContext).load(orderDetail.getProductDetail().getCat_images().get(0).getImage_url()).into(holder.productImage);
        holder.productName.setText(orderDetail.getProductDetail().getProductName());
        holder.deliveryStatus.setText(orderDetail.getTracking().getStatus());
        holder.product_info_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderDetailFragment orderDetailFragment = new OrderDetailFragment();
                Bundle args = new Bundle();
                args.putSerializable("orderDetail", orderDetail);
                orderDetailFragment.setArguments(args);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                activity.appLogo.setVisibility(View.GONE);
                activity.addFragment(orderDetailFragment,false,mContext.getString(R.string.order_details),"",false);

            }
        });
        holder.write_rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RatingFragment ratingFragment = new RatingFragment();
                Bundle args = new Bundle();
                args.putSerializable("orderDetail", orderDetail);
                ratingFragment.setArguments(args);
                activity.layoutToolbarOptions.setVisibility(View.GONE);
                activity.backbuttonImage.setVisibility(View.VISIBLE);
                activity.mainTabLayout.setVisibility(View.GONE);
                activity.appLogo.setVisibility(View.GONE);
                activity.addFragment(ratingFragment,false,mContext.getString(R.string.rating),"",false);
            }
        });
        if (!orderDetail.getProductDetail().getReviewList().isEmpty())
            holder.ratingBar.setRating((float) orderDetail.getProductDetail().getReviewList().get(0).getStar());
    }

    @Override
    public int getItemCount() {
        return orderDetailList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView productImage;
        public TextView deliveryStatus, productName, write_rating;
        public LinearLayout product_info_layout;
        public RatingBar ratingBar;

        public MyViewHolder(View view) {
            super(view);
            productImage = (ImageView) view.findViewById(R.id.product_image);
            productName = (TextView) view.findViewById(R.id.product_name);
            deliveryStatus = (TextView) view.findViewById(R.id.delivery_status);
            product_info_layout = (LinearLayout) view.findViewById(R.id.product_info_layout);
            write_rating = (TextView) view.findViewById(R.id.write_rating);
            ratingBar = (RatingBar) view.findViewById(R.id.ratingStar);
        }
    }
}
