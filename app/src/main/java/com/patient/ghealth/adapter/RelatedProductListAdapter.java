package com.patient.ghealth.adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.HealthStoreActivity;
import com.patient.ghealth.fragment.ProductDetailFragment;
import com.patient.ghealth.model.ProductDetail;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import java.util.List;


/**
 * Created by amal on 11/08/16.
 */
public class RelatedProductListAdapter extends RecyclerView.Adapter<RelatedProductListAdapter.MyViewHolder> {

    private List<ProductDetail> mProductDetails;
    private Activity mContext;
    private HealthStoreActivity activity;

    public RelatedProductListAdapter(List<ProductDetail> mProductDetails, Activity mContext, HealthStoreActivity activity) {
        this.mProductDetails = mProductDetails;
        this.mContext = mContext;
        this.activity = activity;
    }

    @Override
    public RelatedProductListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_related_product_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RelatedProductListAdapter.MyViewHolder holder, int position) {
        final ProductDetail productDetail = mProductDetails.get(position);
        if (productDetail.getCat_images() != null && !productDetail.getCat_images().isEmpty())
            Glide.with(mContext).load(productDetail.getCat_images().get(0).getImage_url()).dontAnimate().placeholder(R.drawable.rectangle_place_holder).into(holder.productImage);
        holder.productImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProductDetailFragment(productDetail);
            }
        });
        holder.product_title.setText(productDetail.getProductName());
        holder.product_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProductDetailFragment(productDetail);
            }
        });
        holder.seller_name.setText(mContext.getString(R.string.by) + " " + productDetail.getSellerInfo().getSeller_name());
        if (productDetail.getPrice().getAfter_discount() != -1) {
            holder.final_price.setText(Const.RM + productDetail.getPrice().getAfter_discount());
        } else {
            holder.final_price.setVisibility(View.GONE);
        }
        if (productDetail.getPrice().getOriginal_price() != -1 &&
                (productDetail.getPrice().getOriginal_price() != productDetail.getPrice().getAfter_discount())) {
            holder.original_price.setText(Const.RM + productDetail.getPrice().getOriginal_price());
            holder.original_price.setPaintFlags(holder.original_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.original_price.setVisibility(View.GONE);
        }


        if (productDetail.getQuantity() == 0) {
            holder.product_ofs_overlay.setVisibility(View.VISIBLE);
        }

        if (productDetail.getRating().getTotalRatings() == 0)
            holder.ratingBar.setRating(0);
        else
            holder.ratingBar.setRating((productDetail.getRating().getTotalStar() / productDetail.getRating().getTotalRatings()));
        holder.ratingCount.setText("(" + productDetail.getRating().getTotalRatings() + ")");


    }


    private void gotoProductDetailFragment(ProductDetail productDetail) {

        ProductDetailFragment detailFragment = new ProductDetailFragment();
        AndyUtils.appLog("RelatedProductListAdapter", "Product Id" + productDetail.getProductId());
        PreferenceHelper.setParam(mContext, Const.Params.PRODUCT_ID, productDetail.getProductId());
        Bundle args = new Bundle();
        args.putSerializable("ProductDetail", productDetail);
        detailFragment.setArguments(args);
        activity.addFragment(detailFragment, false, mContext.getString(R.string.health_store), "", false);
    }

    @Override
    public int getItemCount() {
        return mProductDetails.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView productImage;
        public TextView original_price, final_price, seller_name, product_title, ratingCount;
        public ProgressBar mProgressBar;

        public RelativeLayout product_ofs_overlay;
        public RatingBar ratingBar;

        public MyViewHolder(View view) {
            super(view);
            productImage = (ImageView) view.findViewById(R.id.product_image_preview);
            original_price = (TextView) view.findViewById(R.id.original_price_text);
            final_price = (TextView) view.findViewById(R.id.final_price_text);
            seller_name = (TextView) view.findViewById(R.id.seller_name_text);
            product_title = (TextView) view.findViewById(R.id.product_title);
            mProgressBar = (ProgressBar) view.findViewById(R.id.item_add_progress);
            product_ofs_overlay = (RelativeLayout) view.findViewById(R.id.product_ofs_overlay);
            ratingBar = (RatingBar) view.findViewById(R.id.rating);
            ratingCount = (TextView) view.findViewById(R.id.ratingCount);
        }
    }
}
