package com.patient.ghealth.adapter;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.ClinicDoctorDetailsActivity;
import com.patient.ghealth.activity.DoctorListActivity;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import java.util.List;

/**
 * Created by user on 10/13/2016.
 */
public class DoctorNearByAdapter extends PagerAdapter {
    private Context mContext;
    private List<DoctorOnLineDetails> doctorOnLineDetailsList;
    private StringBuilder doctorAddressStrongBuilder;
    private ImageView clinicImages;
    private TextView betweenDisatnce, clinicName, clinicAddress, noOfDoctorText;
    private int clickedPosition = 0;
    ;
    private DoctorListActivity activity;
    private Location myLocation;
    private String type;

    public DoctorNearByAdapter(Context context, List<DoctorOnLineDetails> doctorOnLineDetailsList, DoctorListActivity activity, Location myLocation, String type) {

        this.activity = activity;
        mContext = context;
        this.doctorOnLineDetailsList = doctorOnLineDetailsList;
        this.myLocation = myLocation;
        this.type = type;
    }

    @Override
    public int getCount() {
        return doctorOnLineDetailsList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        View view = null;
        try {
            view = LayoutInflater.from(mContext).inflate(R.layout.adapter_near_by_item_layout, container, false);
            betweenDisatnce = (TextView) view.findViewById(R.id.tv_clinic_nearby_distance);
            clinicImages = (ImageView) view.findViewById(R.id.iv_near_by_clinic_icon);
            clinicName = (TextView) view.findViewById(R.id.tv_nearby_clinicName);
            clinicAddress = (TextView) view.findViewById(R.id.tv_nearby_clinic_Address);
            noOfDoctorText = (TextView) view.findViewById(R.id.tv_clinic_nearby_number_of_doctors);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent consultIntent = new Intent(activity, ClinicDoctorDetailsActivity.class);
                    DoctorOnLineDetails clinicDoctorDetails = doctorOnLineDetailsList.get(position);
                    AndyUtils.appLog("NearBy ClinicId", clinicDoctorDetails.getClinicId());
                    PreferenceHelper.setParam(mContext, Const.CLINIC_ID, clinicDoctorDetails.getClinicId());
                    Bundle consultBundle = new Bundle();
                    consultBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, clinicDoctorDetails);
                    consultBundle.putString(Const.DOCTOR_TYPE, type);
                    consultIntent.putExtras(consultBundle);
                    mContext.startActivity(consultIntent);
//                    activity.finish();


                }
            });
            setValuesOnView(doctorOnLineDetailsList.get(position), position);
            container.addView(view, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }


    @Override
    public float getPageWidth(int position) {
        if (doctorOnLineDetailsList.size() == 1) {
            return 1;

        } else {
            return 0.97f;
        }
    }

    private void setValuesOnView(final DoctorOnLineDetails doctorOnLineDetails, final int scrollPosition) {


        if (myLocation != null) {
//            findDistanceAndTime(doctorOnLineDetails);
        }
        if (!doctorOnLineDetails.getDoctor_clinic_image_first().equals("")) {
            Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_first()).into(clinicImages);
        } else if (!doctorOnLineDetails.getDoctor_clinic_image_second().equals("")) {
            Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_second()).into(clinicImages);
        } else if (!doctorOnLineDetails.getDoctor_clinic_image_third().equals("")) {
            Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_third()).into(clinicImages);
        }
        clinicName.setText(doctorOnLineDetails.getDoctorClinicName());
        clinicAddress.setText(doctorOnLineDetails.getClinicAddress());
        if (!doctorOnLineDetails.getBetweenDistance().equals("")) {
            String distance[] = doctorOnLineDetails.getBetweenDistance().split(" ");
            StringBuilder sb = new StringBuilder();
            if (distance[0] != null) {
                sb.append(distance[0]);
            }
            if (distance[1] != null) {
                sb.append(" ").append(distance[1]);
            }
            if (distance[2] != null) {
                sb.append(" ").append(mContext.getString(R.string.away));
            }
            betweenDisatnce.setText(sb.toString());
        }
        if (doctorOnLineDetails.getClinicNoOfDoctor().equals("")) {
            noOfDoctorText.setVisibility(View.GONE);
        } else {
            noOfDoctorText.setVisibility(View.VISIBLE);
            noOfDoctorText.setText(doctorOnLineDetails.getClinicNoOfDoctor());
        }

//        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


}
