package com.patient.ghealth.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;


/**
 * Created by amal on 17/08/16.
 */
public class FullImagesViewPagerAdapter extends PagerAdapter {
    private Context mContext;
    private List<String> cat_images;

    public FullImagesViewPagerAdapter(Context mContext, List<String> cat_images) {
        this.mContext = mContext;
        this.cat_images = cat_images;
    }

    @Override
    public int getCount() {
        return cat_images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.full_pager_item, container, false);

        final ImageViewTouch imageView = (ImageViewTouch) itemView.findViewById(R.id.img_pager_item);


        Glide.with(mContext).load(cat_images.get(position)).override(mContext.getResources().getInteger(R.integer.img_width), mContext.getResources().getInteger(R.integer.img_height)).fitCenter().into(imageView);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

}
