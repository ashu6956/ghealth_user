package com.patient.ghealth.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.fragment.FullScreenImageFragment;
import com.patient.ghealth.model.ChatHistoryobjt;
import com.patient.ghealth.utils.Const;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 3/19/2015.
 */
public class ChatHistoryAdapter extends BaseAdapter {

    List<String> imageUrls;
    List<ChatHistoryobjt> chat_data;
    Context context;
    int resource;
    ViewHolder holder = null;
    MainActivity activity;


    public ChatHistoryAdapter(Context context, List<ChatHistoryobjt> chat_data, MainActivity activity) {

        this.chat_data = chat_data;
        this.context = context;
        this.resource = resource;
        this.activity = activity;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public int getCount() {
        return chat_data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.chat_adapter, null);
            holder = new ViewHolder();
            holder.img_lay = (RelativeLayout) convertView.findViewById(R.id.img_lay);
            holder.text_lay = (RelativeLayout) convertView.findViewById(R.id.text_lay);
            holder.textView_left_chat = (TextView) convertView.findViewById(R.id.msg);
            holder.textView_right_chat = (TextView) convertView.findViewById(R.id.received_msg);

            holder.received_msg_image = (ImageView) convertView.findViewById(R.id.received_msg_image);
            holder.sent_msg_image = (ImageView) convertView.findViewById(R.id.sent_msg_image);

            convertView.setTag(holder);
        } else {

            holder = (ViewHolder) convertView.getTag();

        }

        if (chat_data.get(position).getData_type().equals("TEXT")) {
//        Log.d("pavan","type "+chat_data.get(position).getType());
//        Log.d("pavan","message "+chat_data.get(position).getMessage());
            holder.img_lay.setVisibility(View.GONE);
            holder.text_lay.setVisibility(View.VISIBLE);

            if (chat_data.get(position).getType().equals("sent")) {

                try {
                    holder.textView_right_chat.setText(URLDecoder.decode(chat_data.get(position).getMessage(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                holder.textView_left_chat.setVisibility(View.GONE);
                holder.textView_right_chat.setVisibility(View.VISIBLE);


            } else {

                try {
                    holder.textView_left_chat.setText(URLDecoder.decode(chat_data.get(position).getMessage(), "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                holder.textView_right_chat.setVisibility(View.GONE);
                holder.textView_left_chat.setVisibility(View.VISIBLE);
            }
        } else {
            imageUrls = new ArrayList<>();
            holder.img_lay.setVisibility(View.VISIBLE);
            holder.text_lay.setVisibility(View.GONE);


            if (chat_data.get(position).getType().equals("sent")) {
                Glide.with(context).load(chat_data.get(position).getMessage()).into(holder.received_msg_image);
                holder.sent_msg_image.setVisibility(View.GONE);
                holder.received_msg_image.setVisibility(View.VISIBLE);


            } else {
                Glide.with(context).load(chat_data.get(position).getMessage()).into(holder.sent_msg_image);
                holder.sent_msg_image.setVisibility(View.VISIBLE);
                holder.received_msg_image.setVisibility(View.GONE);
            }

            holder.sent_msg_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageUrls.add(chat_data.get(position).getMessage());
                    for (int i = 0; i < chat_data.size(); i++) {
                        if (i != position && chat_data.get(i).getMessage().contains("https:")) {
                            imageUrls.add(chat_data.get(i).getMessage());
                        }
                    }
                    gotoFragment(0);

                }
            });
            holder.received_msg_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageUrls.add(chat_data.get(position).getMessage());
                    for (int i = 0; i < chat_data.size(); i++) {
                        if (i != position && chat_data.get(i).getMessage().contains("https:")) {
                            imageUrls.add(chat_data.get(i).getMessage());
                        }
                    }
                    gotoFragment(0);
                }
            });


        }
        return convertView;
    }

    private void gotoFragment(int position) {
        List<String> clickedImageUrls = new ArrayList<>();
        List<String> temporaryList = imageUrls;
        clickedImageUrls.add(temporaryList.get(position));
        temporaryList.remove(position);
        for (int i = 0; i < temporaryList.size(); i++) {
            clickedImageUrls.add(temporaryList.get(i));
        }
        FullScreenImageFragment newFragment = new FullScreenImageFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("images", (ArrayList<String>) clickedImageUrls);
        args.putInt("imagePosition", position);
        newFragment.setArguments(args);
        activity.addFragment(newFragment, false, activity.getResources().getString(R.string.image_gallery), Const.FullScreenImageFragment, false);
    }

    private class ViewHolder {
        RelativeLayout img_lay, text_lay;
        TextView textView_left_chat;
        TextView textView_right_chat;
        ImageView received_msg_image, sent_msg_image;

    }
}
