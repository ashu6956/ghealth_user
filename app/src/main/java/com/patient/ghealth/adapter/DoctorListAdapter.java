package com.patient.ghealth.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.DoctorConsultConnectivity;
import com.patient.ghealth.activity.DoctorListActivity;
import com.patient.ghealth.activity.DoctorProfileActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.custom_interface.DoctorBookAvailabilityListener;
import com.patient.ghealth.fragment.DoctorBookingFragment;
import com.patient.ghealth.fragment.DoctorOnlineConsultFragment;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by getit on 8/9/2016.
 */
public class DoctorListAdapter extends RecyclerView.Adapter<DoctorListAdapter.CustomDoctorListViewHolder> implements AsyncTaskCompleteListener {

    DecimalFormat form = new DecimalFormat("0.00");
    private Context mContext;
    private List<DoctorOnLineDetails> onLineDetailsList;
    private String type = "";
    private DoctorListActivity activity;
    private DoctorBookAvailabilityListener availabilityListener;
    private List<String> dialogImagesList;
    private ArrayList<DoctorOnLineDetails> arraylist;
    private boolean isBooked;
    private String currency = "";


    public DoctorListAdapter(Context context, List<DoctorOnLineDetails> onLineDetailsList, String type, DoctorListActivity activity, DoctorBookAvailabilityListener availabilityListener) {
        mContext = context;
        this.onLineDetailsList = onLineDetailsList;
        this.type = type;
        this.activity = activity;
        this.availabilityListener = availabilityListener;
        this.arraylist = new ArrayList<DoctorOnLineDetails>();
        this.arraylist.addAll(onLineDetailsList);
        currency = (String) PreferenceHelper.getParam(mContext, Const.Params.CURRENCY, "");

    }


    @Override
    public DoctorListAdapter.CustomDoctorListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_doctor_list_item_layout, parent, false);
        CustomDoctorListViewHolder holder = new CustomDoctorListViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final DoctorListAdapter.CustomDoctorListViewHolder holder, final int position) {

        if (type.equals(Const.BOOKING)) {
            DoctorOnLineDetails doctorOnLineDetails = onLineDetailsList.get(position);
            dialogImagesList = new ArrayList<String>();
            if (doctorOnLineDetails.getDoctorPictureUrl().equals("")) {
                holder.doctorsIcon.setImageResource(R.drawable.profile);
            } else {
                Glide.with(mContext).load(doctorOnLineDetails.getDoctorPictureUrl()).into(holder.doctorsIcon);

            }
            if (doctorOnLineDetails.getIsFavorite().equals("1")) {
                holder.favoriteIcon.setImageResource(R.drawable.wishlist_selected);
            } else if (doctorOnLineDetails.getIsFavorite().equals("0")) {
                holder.favoriteIcon.setImageResource(R.drawable.wishlist_unselected);
            }

            holder.favoriteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isBooked) {
                        holder.favoriteIcon.setImageResource(R.drawable.wishlist_unselected);
                        isBooked = false;
                        addBookMark(0, onLineDetailsList.get(position).getDoctorOnLineId(), type);
                    } else {
                        holder.favoriteIcon.setImageResource(R.drawable.wishlist_selected);
                        isBooked = true;
                        addBookMark(1, onLineDetailsList.get(position).getDoctorOnLineId(), type);
                    }

                }
            });

            if (doctorOnLineDetails.getDoctor_clinic_image_first().equals("")) {
                holder.aboutImages_1.setImageResource(R.drawable.back_ground_image_bg);
            } else {
                Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_first()).into(holder.aboutImages_1);
                dialogImagesList.add(doctorOnLineDetails.getDoctor_clinic_image_first());
                holder.aboutImages_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ;
                        AndyUtils.showDoctorAboutDialog(position, onLineDetailsList, mContext);
                    }
                });


            }
            if (doctorOnLineDetails.getDoctor_clinic_image_second().equals("")) {
                holder.aboutImages_2.setImageResource(R.drawable.back_ground_image_bg);

            } else {
                Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_second()).into(holder.aboutImages_2);
                dialogImagesList.add(doctorOnLineDetails.getDoctor_clinic_image_second());
                holder.aboutImages_2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AndyUtils.showDoctorAboutDialog(position, onLineDetailsList, mContext);
                    }
                });
            }
            if (doctorOnLineDetails.getDoctor_clinic_image_third().equals("")) {
                holder.aboutImages_3.setImageResource(R.drawable.back_ground_image_bg);

            } else {

                Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_third()).into(holder.aboutImages_3);
                dialogImagesList.add(doctorOnLineDetails.getDoctor_clinic_image_third());
                holder.aboutImages_3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AndyUtils.showDoctorAboutDialog(position, onLineDetailsList, mContext);
                    }
                });
            }

            if (!doctorOnLineDetails.getDoctorRating().equals("")) {
                holder.rating.setVisibility(View.VISIBLE);
                holder.doctorRating.setText(doctorOnLineDetails.getDoctorRating());
            }

            holder.doctorName.setText(doctorOnLineDetails.getDoctorName());
            if (!doctorOnLineDetails.getDoctorClinicName().equals("")) {
                holder.clinicIcon.setVisibility(View.VISIBLE);
                holder.doctorClinicName.setText(doctorOnLineDetails.getDoctorClinicName());
            }

            if (!doctorOnLineDetails.getClinicAddress().equals("")) {
                holder.addressIcon.setVisibility(View.VISIBLE);
                holder.doctorAddress.setText(doctorOnLineDetails.getClinicAddress());
            }

            if (!doctorOnLineDetails.getDoctorExperience().equals("")) {
                holder.experienceIcon.setVisibility(View.VISIBLE);
                holder.doctorExperience.setText(doctorOnLineDetails.getDoctorExperience() + " " + mContext.getString(R.string.year_experience));
            } else {
                holder.experienceIcon.setVisibility(View.GONE);
                holder.doctorExperience.setVisibility(View.GONE);
            }
            if (!doctorOnLineDetails.getDoctorHelpedPeople().equals("")) {
                holder.consultedIcon.setVisibility(View.VISIBLE);
                holder.doctorHelpedPeople.setText(mContext.getString(R.string.consulted) + " " + doctorOnLineDetails.getDoctorHelpedPeople() + " " + mContext.getString(R.string.people));
            }
            holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + mContext.getString(R.string.onwards));
            holder.booktext.setVisibility(View.VISIBLE);
            holder.booktext.setText(mContext.getString(R.string.booking));
            holder.consultText.setVisibility(View.GONE);
            holder.connectivityLayout.setVisibility(View.GONE);
            holder.booktext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AndyUtils.appLog("DoctorListAdapter", "BookClickedPosition" + position);
                    availabilityListener.onClickBookAvailability(position);

                }
            });

        } else if (type.equals(Const.ONLINE_CONSULT)) {
            dialogImagesList = new ArrayList<>();
            DoctorOnLineDetails doctorOnLineDetails = onLineDetailsList.get(position);
            if (doctorOnLineDetails.getDoctorPictureUrl().equals("")) {
                holder.doctorsIcon.setImageResource(R.drawable.profile);
            } else {
                Glide.with(mContext).load(doctorOnLineDetails.getDoctorPictureUrl()).into(holder.doctorsIcon);

            }

            if (doctorOnLineDetails.getIsFavorite().equals("1")) {
                holder.favoriteIcon.setImageResource(R.drawable.wishlist_selected);
            } else if (doctorOnLineDetails.getIsFavorite().equals("0")) {
                holder.favoriteIcon.setImageResource(R.drawable.wishlist_unselected);
            }

            holder.favoriteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isBooked) {
                        holder.favoriteIcon.setImageResource(R.drawable.wishlist_unselected);
                        isBooked = false;
                        addBookMark(0, onLineDetailsList.get(position).getDoctorOnLineId(), type);
                    } else {
                        holder.favoriteIcon.setImageResource(R.drawable.wishlist_selected);
                        isBooked = true;
                        addBookMark(1, onLineDetailsList.get(position).getDoctorOnLineId(), type);
                    }

                }
            });

            if (doctorOnLineDetails.getDoctor_clinic_image_first().equals("")) {
                holder.aboutImages_1.setImageResource(R.drawable.back_ground_image_bg);

            } else {
                Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_first()).into(holder.aboutImages_1);
                dialogImagesList.add(doctorOnLineDetails.getDoctor_clinic_image_first());
                holder.aboutImages_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AndyUtils.showDoctorAboutDialog(position, onLineDetailsList, mContext);
                    }
                });


            }
            if (doctorOnLineDetails.getDoctor_clinic_image_second().equals("")) {
                holder.aboutImages_2.setImageResource(R.drawable.back_ground_image_bg);

            } else {
                Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_second()).into(holder.aboutImages_2);
                dialogImagesList.add(doctorOnLineDetails.getDoctor_clinic_image_second());
                holder.aboutImages_2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AndyUtils.showDoctorAboutDialog(position, onLineDetailsList, mContext);
                    }
                });
            }
            if (doctorOnLineDetails.getDoctor_clinic_image_third().equals("")) {
                holder.aboutImages_3.setImageResource(R.drawable.back_ground_image_bg);

            } else {

                Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_third()).into(holder.aboutImages_3);
                dialogImagesList.add(doctorOnLineDetails.getDoctor_clinic_image_third());
                holder.aboutImages_3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AndyUtils.showDoctorAboutDialog(position, onLineDetailsList, mContext);
                    }
                });
            }

            if (!doctorOnLineDetails.getDoctorRating().equals("")) {
                holder.rating.setVisibility(View.VISIBLE);
                holder.doctorRating.setText(doctorOnLineDetails.getDoctorRating());
            }

            holder.doctorName.setText(doctorOnLineDetails.getDoctorName());
            holder.doctorClinicName.setText(doctorOnLineDetails.getDoctorClinicName());

            if (!doctorOnLineDetails.getDoctorExperience().equals("")) {
                holder.experienceIcon.setVisibility(View.VISIBLE);
                holder.doctorExperience.setText(doctorOnLineDetails.getDoctorExperience() + " " + mContext.getString(R.string.year_experience));
            } else {
                holder.experienceIcon.setVisibility(View.GONE);
                holder.doctorExperience.setVisibility(View.GONE);
            }
            if (!doctorOnLineDetails.getDoctorHelpedPeople().equals("")) {
                holder.consultedIcon.setVisibility(View.VISIBLE);
                holder.doctorHelpedPeople.setText(mContext.getString(R.string.consulted) + " " + doctorOnLineDetails.getDoctorHelpedPeople() + " " + mContext.getString(R.string.people));
            }
            holder.doctorName.setText(doctorOnLineDetails.getDoctorName());

            if (!doctorOnLineDetails.getDoctorClinicName().equals("")) {
                holder.clinicIcon.setVisibility(View.VISIBLE);
                holder.doctorClinicName.setText(doctorOnLineDetails.getDoctorClinicName());
            }

            if (!doctorOnLineDetails.getClinicAddress().equals("")) {
                holder.addressIcon.setVisibility(View.VISIBLE);
                holder.doctorAddress.setText(doctorOnLineDetails.getClinicAddress());
            }


            if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1) {
                holder.chat_icon.setVisibility(View.VISIBLE);
            }
            if (Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1) {
                holder.call_icon.setVisibility(View.VISIBLE);
            }
            if (Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                holder.video_icon.setVisibility(View.VISIBLE);
            }

            if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) && Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                } else if (Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {
                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) + "\n" + mContext.getString(R.string.onwards));
                } else {
                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + "\n" + mContext.getString(R.string.onwards));
                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee())) {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                } else {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorChatConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                } else {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1 && Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                if (Float.parseFloat(doctorOnLineDetails.getDoctorPhoneConsultFee()) < Float.parseFloat(doctorOnLineDetails.getDoctorVideoConsultFee())) {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                } else {

                    holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + "\n" + mContext.getString(R.string.onwards));

                }
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorPhoneConsult()) == 1) {
                holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorPhoneConsultFee())) + "\n" + mContext.getString(R.string.onwards));
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorChatConsult()) == 1) {
                holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorChatConsultFee())) + "\n" + mContext.getString(R.string.onwards));
            } else if (Integer.parseInt(doctorOnLineDetails.getDoctorVideoConsult()) == 1) {
                holder.doctorConsultantFee.setText(currency + " " + form.format(Double.valueOf(doctorOnLineDetails.getDoctorVideoConsultFee())) + "\n" + mContext.getString(R.string.onwards));
            }

            holder.booktext.setVisibility(View.GONE);
            holder.consultText.setText(mContext.getString(R.string.consult));
            holder.consultText.setVisibility(View.VISIBLE);
            holder.connectivityLayout.setVisibility(View.VISIBLE);
            holder.consultText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent consultIntent = new Intent(activity, DoctorConsultConnectivity.class);
                    DoctorOnLineDetails consultDoctorOnLineDetails = onLineDetailsList.get(position);
                    Bundle consultBundle = new Bundle();
                    consultBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, consultDoctorOnLineDetails);
                    consultBundle.putString(Const.DOCTOR_TYPE, type);
                    consultIntent.putExtras(consultBundle);
                    mContext.startActivity(consultIntent);
                }
            });

        }
        holder.doctorsIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoctorOnLineDetails doctorOnLineDetails = onLineDetailsList.get(position);
                AndyUtils.appLog("DoctorOnLineDetails", doctorOnLineDetails.toString());
                Intent profileIntent = new Intent(activity, DoctorProfileActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, doctorOnLineDetails);
                bundle.putString(Const.DOCTOR_TYPE, type);
                profileIntent.putExtras(bundle);
                mContext.startActivity(profileIntent);
            }
        });

    }


    private void addBookMark(int isFavorite, String dId, String type) {
        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
        try {
            String doctorSpecialistsId = (String) PreferenceHelper.getParam(mContext, Const.SPECIALITIES_DOCTOR_ID, "");
            AndyUtils.appLog("DoctorLisAdapter", "DoctorSpecialistId" + doctorSpecialistsId);
            if (!doctorSpecialistsId.equals("")) {
                HashMap<String, String> map = new HashMap<String, String>();
                if (type.equals(Const.ONLINE_CONSULT)) {
                    map.put(Const.Params.URL, Const.ServiceType.GET_DOCTOR_LIST_ONLINE_URL + Const.Params.ID + "="
                            + String.valueOf(PreferenceHelper.getParam(mContext, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                            + PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.SPECIALITY_ID + "="
                            + String.valueOf(doctorSpecialistsId) + "&" + Const.Params.DOCTOR_ID + "="
                            + String.valueOf(dId) + "&" + Const.Params.IS_FAVORITE + "="
                            + String.valueOf(isFavorite));

                    AndyUtils.appLog("DoctorLisAdapter", "DoctorListOnLineMap" + map);
                } else if (type.equals(Const.BOOKING)) {
                    map.put(Const.Params.URL, Const.ServiceType.GET_DOCTOR_LIST_BOOKING_URL + Const.Params.ID + "="
                            + String.valueOf(PreferenceHelper.getParam(mContext, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                            + PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, "") + "&" + Const.Params.SPECIALITY_ID + "="
                            + String.valueOf(doctorSpecialistsId) + "&" + Const.Params.DOCTOR_ID + "="
                            + String.valueOf(dId) + "&" + Const.Params.IS_FAVORITE + "="
                            + String.valueOf(isFavorite));

                    AndyUtils.appLog("DoctorLisAdapter", "DoctorListBookingMap" + map);
                }
                new HttpRequester(mContext, Const.GET, map, Const.ServiceCode.SAVE_FAVORITE_DOCTOR, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return onLineDetailsList.size();
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.ENGLISH);
        onLineDetailsList.clear();
        if (charText.length() == 0) {
            onLineDetailsList.addAll(arraylist);
        }
        else
        {
            for (DoctorOnLineDetails wp : arraylist)
            {
                if (wp.getDoctorName().toLowerCase(Locale.ENGLISH).contains(charText)) {
                    onLineDetailsList.add(wp);
                }
            }
        }
        if (type.equals(Const.ONLINE_CONSULT)) {
            DoctorOnlineConsultFragment.doctorListAdapter.notifyDataSetChanged();
        } else if (type.equals(Const.BOOKING)) {
            DoctorBookingFragment.doctorListAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.SAVE_FAVORITE_DOCTOR:
                AndyUtils.appLog("Ashutosh", "FavoriteDoctorResponse" + response);
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        if (type.equals(Const.BOOKING)) {
                            onLineDetailsList.clear();
                            JSONArray jsonArray = jsonObject.getJSONArray("doctor_list");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject onLineJsonObject = jsonArray.getJSONObject(i);
                                DoctorOnLineDetails doctorOnLineDetails = new DoctorOnLineDetails();
                                doctorOnLineDetails.setDoctorOnLineId(onLineJsonObject.getString("id"));
                                doctorOnLineDetails.setDoctorName(onLineJsonObject.getString("d_name"));
                                doctorOnLineDetails.setDoctorPictureUrl(onLineJsonObject.getString("d_photo"));
                                doctorOnLineDetails.setClinicAddress(onLineJsonObject.optString("c_address"));
                                doctorOnLineDetails.setDoctor_clinic_image_first(onLineJsonObject.getString("c_pic1"));
                                doctorOnLineDetails.setDoctor_clinic_image_second(onLineJsonObject.getString("c_pic2"));
                                doctorOnLineDetails.setDoctor_clinic_image_third(onLineJsonObject.getString("c_pic3"));
                                doctorOnLineDetails.setDoctorChatConsultFee(onLineJsonObject.getString("appointment_fee"));
                                doctorOnLineDetails.setDoctorClinicName(onLineJsonObject.getString("c_name"));
                                doctorOnLineDetails.setDoctorRating(onLineJsonObject.getString("rating"));
                                doctorOnLineDetails.setDoctorHelpedPeople(onLineJsonObject.getString("helped"));
                                doctorOnLineDetails.setDoctorExperience(onLineJsonObject.getString("experience"));
                                doctorOnLineDetails.setdLatitude(onLineJsonObject.optString("d_latitude"));
                                doctorOnLineDetails.setdLongitude(onLineJsonObject.getString("d_longitude"));
                                doctorOnLineDetails.setDoctorNationality(onLineJsonObject.optString("nationality"));
                                doctorOnLineDetails.setBetweenDistance("");
                                doctorOnLineDetails.setDegree1(onLineJsonObject.optString("degree1"));
                                doctorOnLineDetails.setEducation1(onLineJsonObject.optString("univ1"));
                                doctorOnLineDetails.setDegree2(onLineJsonObject.optString("degree2"));
                                doctorOnLineDetails.setEducation2(onLineJsonObject.optString("univ2"));
                                doctorOnLineDetails.setIsFavorite(onLineJsonObject.optString(Const.Params.IS_FAVORITE));
                                onLineDetailsList.add(doctorOnLineDetails);
                            }
                            List<DoctorOnLineDetails> favoriteList = new ArrayList<>();
                            List<DoctorOnLineDetails> nonFavoriteList = new ArrayList<>();
                            for (int i = 0; i < onLineDetailsList.size(); i++) {
                                if (onLineDetailsList.get(i).getIsFavorite().equals("1")) {
                                    favoriteList.add(onLineDetailsList.get(i));
                                } else if (onLineDetailsList.get(i).getIsFavorite().equals("0")) {
                                    nonFavoriteList.add(onLineDetailsList.get(i));
                                }
                            }
                            onLineDetailsList.clear();
                            onLineDetailsList.addAll(favoriteList);
                            onLineDetailsList.addAll(nonFavoriteList);
                            DoctorBookingFragment.doctorListAdapter.notifyDataSetChanged();


                        } else if (type.equals(Const.ONLINE_CONSULT)) {

                            onLineDetailsList.clear();
                            JSONArray jsonArray = jsonObject.getJSONArray("doctor_list");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject onLineJsonObject = jsonArray.getJSONObject(i);
                                DoctorOnLineDetails doctorOnLineDetails = new DoctorOnLineDetails();
                                doctorOnLineDetails.setDoctorOnLineId(onLineJsonObject.getString("id"));
                                doctorOnLineDetails.setDoctorName(onLineJsonObject.getString("d_name"));
                                doctorOnLineDetails.setDoctorPictureUrl(onLineJsonObject.getString("d_photo"));
                                doctorOnLineDetails.setClinicAddress(onLineJsonObject.optString("c_address"));
                                doctorOnLineDetails.setDoctor_clinic_image_first(onLineJsonObject.getString("c_pic1"));
                                doctorOnLineDetails.setDoctor_clinic_image_second(onLineJsonObject.getString("c_pic2"));
                                doctorOnLineDetails.setDoctor_clinic_image_third(onLineJsonObject.getString("c_pic3"));
                                doctorOnLineDetails.setDoctorChatConsult(onLineJsonObject.getString("c_consult"));
                                doctorOnLineDetails.setDoctorVideoConsult(onLineJsonObject.getString("v_consult"));
                                doctorOnLineDetails.setDoctorPhoneConsult(onLineJsonObject.getString("p_consult"));
                                doctorOnLineDetails.setDoctorChatConsultFee(onLineJsonObject.getString("c_fee"));
                                doctorOnLineDetails.setDoctorVideoConsultFee(onLineJsonObject.getString("v_fee"));
                                doctorOnLineDetails.setDoctorPhoneConsultFee(onLineJsonObject.getString("p_fee"));
                                doctorOnLineDetails.setDoctorClinicName(onLineJsonObject.getString("c_name"));
                                doctorOnLineDetails.setDoctorRating(onLineJsonObject.getString("rating"));
                                doctorOnLineDetails.setDoctorHelpedPeople(onLineJsonObject.getString("helped"));
                                doctorOnLineDetails.setDoctorExperience(onLineJsonObject.getString("experience"));
                                doctorOnLineDetails.setLatitude(onLineJsonObject.optString("experience"));
                                doctorOnLineDetails.setdLatitude(onLineJsonObject.optString("d_latitude"));
                                doctorOnLineDetails.setdLongitude(onLineJsonObject.getString("d_longitude"));
                                doctorOnLineDetails.setDoctorNationality(onLineJsonObject.optString("nationality"));
                                doctorOnLineDetails.setBetweenDistance("");
                                doctorOnLineDetails.setDegree1(onLineJsonObject.optString("degree1"));
                                doctorOnLineDetails.setEducation1(onLineJsonObject.optString("univ1"));
                                doctorOnLineDetails.setDegree2(onLineJsonObject.optString("degree2"));
                                doctorOnLineDetails.setEducation2(onLineJsonObject.optString("univ2"));
                                doctorOnLineDetails.setFreeCharge(onLineJsonObject.optString("free_minute"));
                                doctorOnLineDetails.setIsFavorite(onLineJsonObject.optString(Const.Params.IS_FAVORITE));
                                onLineDetailsList.add(doctorOnLineDetails);
                            }

                            List<DoctorOnLineDetails> favoriteList = new ArrayList<>();
                            List<DoctorOnLineDetails> nonFavoriteList = new ArrayList<>();
                            for (int i = 0; i < onLineDetailsList.size(); i++) {
                                if (onLineDetailsList.get(i).getIsFavorite().equals("1")) {
                                    favoriteList.add(onLineDetailsList.get(i));
                                } else if (onLineDetailsList.get(i).getIsFavorite().equals("0")) {
                                    nonFavoriteList.add(onLineDetailsList.get(i));
                                }
                            }
                            onLineDetailsList.clear();
                            onLineDetailsList.addAll(favoriteList);
                            onLineDetailsList.addAll(nonFavoriteList);

                            DoctorOnlineConsultFragment.doctorListAdapter.notifyDataSetChanged();
                        }
                    } else if (jsonObject.optString("success").equals("false")) {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }

    class CustomDoctorListViewHolder extends RecyclerView.ViewHolder {
        private ImageView favoriteIcon, doctorsIcon, clinicIcon, consultedIcon, experienceIcon, addressIcon, rating, aboutImages_1, aboutImages_2, aboutImages_3, call_icon, chat_icon, video_icon;
        private View itemView;
        private TextView consultText, booktext, doctorRating, doctorName, doctorClinicName, doctorAddress, doctorExperience, doctorHelpedPeople, doctorConsultantFee;
        private LinearLayout connectivityLayout;

        public CustomDoctorListViewHolder(View itemView) {
            super(itemView);
            doctorsIcon = (ImageView) itemView.findViewById(R.id.iv_doctor_list_profile_icon);
            favoriteIcon = (ImageView) itemView.findViewById(R.id.iv_doctor_bookmark);
            consultText = (TextView) itemView.findViewById(R.id.tv_doctor_list_consult);
            connectivityLayout = (LinearLayout) itemView.findViewById(R.id.ll_doctor_connectivity);
            clinicIcon = (ImageView) itemView.findViewById(R.id.iv_clinic_name);
            addressIcon = (ImageView) itemView.findViewById(R.id.iv_clinic_address);
            consultedIcon = (ImageView) itemView.findViewById(R.id.iv_consulted);
            experienceIcon = (ImageView) itemView.findViewById(R.id.iv_experience);
            booktext = (TextView) itemView.findViewById(R.id.tv_doctor_list_book);
            aboutImages_1 = (ImageView) itemView.findViewById(R.id.iv_doctor_list_about_1);
            aboutImages_2 = (ImageView) itemView.findViewById(R.id.iv_doctor_list_about_2);
            aboutImages_3 = (ImageView) itemView.findViewById(R.id.iv_doctor_list_about_3);
            call_icon = (ImageView) itemView.findViewById(R.id.iv_doctor_list_call);
            chat_icon = (ImageView) itemView.findViewById(R.id.iv_doctor_list_chat);
            video_icon = (ImageView) itemView.findViewById(R.id.iv_doctor_list_video);
            doctorName = (TextView) itemView.findViewById(R.id.tv_doctorList_doctorName);
            doctorClinicName = (TextView) itemView.findViewById(R.id.tv_doctor_list_doctor_clinic_name);
            doctorAddress = (TextView) itemView.findViewById(R.id.tv_doctor_list_doctor_address);
            doctorExperience = (TextView) itemView.findViewById(R.id.tv_doctorlist_doctorExperience);
            doctorHelpedPeople = (TextView) itemView.findViewById(R.id.tv_doctorlist_doctorhelped);
            doctorConsultantFee = (TextView) itemView.findViewById(R.id.tv_doctor_list_doctorConsultFee);
            doctorRating = (TextView) itemView.findViewById(R.id.tv_doctorList_rating);
            rating = (ImageView) itemView.findViewById(R.id.iv_rating);

        }

    }


}
