package com.patient.ghealth.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;
import com.patient.ghealth.model.ChatObject;
import com.patient.ghealth.utils.AndyUtils;

import java.util.ArrayList;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;


/**
 * Created by Dell on 3/19/2015.
 */
public class ChatAdapter extends ArrayAdapter<ChatObject> {

    List<ChatObject> chat_data;
    Context context;
    int resource;
    ViewHolder holder = null;
    private ImageView[] dots;
    private int dotsCount;


    public ChatAdapter(Context context, int resource, List<ChatObject> chat_data) {
        super(context, resource, chat_data);
        this.chat_data = chat_data;
        this.context = context;
        this.resource = resource;
    }

    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        context = parent.getContext();
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.chat_adapter, null);
            holder = new ViewHolder();
            holder.img_lay = (RelativeLayout) convertView.findViewById(R.id.img_lay);
            holder.text_lay = (RelativeLayout) convertView.findViewById(R.id.text_lay);
            holder.textView_left_chat = (TextView) convertView.findViewById(R.id.msg);
            holder.textView_right_chat = (TextView) convertView.findViewById(R.id.received_msg);
            holder.received_msg_image = (ImageView) convertView.findViewById(R.id.received_msg_image);
            holder.sent_msg_image = (ImageView) convertView.findViewById(R.id.sent_msg_image);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (chat_data.get(position).getData_type().equals("TEXT"))
        {
//        Log.d("pavan","type "+chat_data.get(position).getType());
//        Log.d("pavan","message "+chat_data.get(position).getMessage());
            holder.img_lay.setVisibility(View.GONE);
            holder.text_lay.setVisibility(View.VISIBLE);

            if (chat_data.get(position).getType().equalsIgnoreCase("sent")) {

                holder.textView_left_chat.setText(chat_data.get(position).getMessage());
                holder.textView_right_chat.setVisibility(View.GONE);
                holder.textView_left_chat.setVisibility(View.VISIBLE);
            } else if (chat_data.get(position).getType().equalsIgnoreCase("receive")) {

                holder.textView_right_chat.setText(chat_data.get(position).getMessage());
                holder.textView_left_chat.setVisibility(View.GONE);
                holder.textView_right_chat.setVisibility(View.VISIBLE);
            }
        } else {
            final List<String> imageUrls = new ArrayList<>();
            holder.img_lay.setVisibility(View.VISIBLE);
            holder.text_lay.setVisibility(View.GONE);

            if (chat_data.get(position).getType().equalsIgnoreCase("receive"))
            {
                Glide.with(context).load(chat_data.get(position).getMessage()).override(400, 400).centerCrop().into(holder.sent_msg_image);
                holder.sent_msg_image.setVisibility(View.VISIBLE);
                holder.received_msg_image.setVisibility(View.GONE);
            }
            else if (chat_data.get(position).getType().equalsIgnoreCase("sent"))
            {
                Glide.with(context).load(chat_data.get(position).getMessage()).override(400, 400).centerCrop().into(holder.received_msg_image);
                holder.sent_msg_image.setVisibility(View.GONE);
                holder.received_msg_image.setVisibility(View.VISIBLE);
            }

            holder.sent_msg_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (context != null) {
                        imageUrls.clear();
                        imageUrls.add(chat_data.get(position).getMessage());
                        for (int i = 0; i < chat_data.size(); i++) {
                            if (i != position && chat_data.get(i).getMessage().contains("https:")) {
                                imageUrls.add(chat_data.get(i).getMessage());
                            }
                        }
                        gotoFragment(context, 0, imageUrls);
                    }
                }
            });
            holder.received_msg_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (context != null) {
                        imageUrls.clear();
                        imageUrls.add(chat_data.get(position).getMessage());
                        for (int i = 0; i < chat_data.size(); i++) {
                            if (i != position && chat_data.get(i).getMessage().contains("https:")) {
                                imageUrls.add(chat_data.get(i).getMessage());
                            }
                        }
                        gotoFragment(context, 0, imageUrls);
                    }
                }
            });
        }

        return convertView;
    }

    private void gotoFragment(Context mContext, int pos, List<String> imageList) {
        final Dialog dialog = new Dialog(mContext, R.style.DialogThemeforview);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.fragment_full_screen_image);
        ViewPager product_images = (ViewPager) dialog.findViewById(R.id.product_full_images_viewpager);
        CircleIndicator circleIndicator = (CircleIndicator) dialog.findViewById(R.id.ci_full_screen_images);

        // Set product images
        List<String> clickedImageUrls = new ArrayList<>();
        List<String> temporaryList = imageList;
        clickedImageUrls.add(temporaryList.get(pos));
        temporaryList.remove(pos);
        for (int i = 0; i < temporaryList.size(); i++) {
            clickedImageUrls.add(temporaryList.get(i));
        }

        AndyUtils.appLog("ClickedImageUrlSize", clickedImageUrls.size() + "");
        FullImagesViewPagerAdapter mAdapter = new FullImagesViewPagerAdapter(context, clickedImageUrls);
        product_images.setAdapter(mAdapter);
        circleIndicator.setViewPager(product_images);
        dialog.setCancelable(true);
        dialog.show();
    }

    private class ViewHolder {
        RelativeLayout img_lay, text_lay;
        TextView textView_left_chat;
        TextView textView_right_chat;
        ImageView received_msg_image, sent_msg_image;

    }


}
