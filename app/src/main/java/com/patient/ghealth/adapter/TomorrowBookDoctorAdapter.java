package com.patient.ghealth.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.custom_interface.DoctorBookAvailabilityListener;
import com.patient.ghealth.custom_interface.DoctorBookSlotsListener;
import com.patient.ghealth.model.DoctorBookingScheduleSlots;

import java.util.List;

/**
 * Created by user on 9/17/2016.
 */
public class TomorrowBookDoctorAdapter extends RecyclerView.Adapter<TomorrowBookDoctorAdapter.CustomBookingSlotsViewHolder> {

    private Context mContext;
    public static List<DoctorBookingScheduleSlots> doctorBookingScheduleSlotsList;
    public static TomorrowDoctorBookShiftAdapter shiftSlotsAdapter;



    public TomorrowBookDoctorAdapter(Context context, List<DoctorBookingScheduleSlots> doctorBookingScheduleSlotsList)
    {
        mContext=context;
        this.doctorBookingScheduleSlotsList=doctorBookingScheduleSlotsList;


    }



    @Override
    public CustomBookingSlotsViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view=LayoutInflater.from(mContext).inflate(R.layout.adapter_doctor_slots_item_layout,null);

        CustomBookingSlotsViewHolder holder=new CustomBookingSlotsViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(CustomBookingSlotsViewHolder holder, int position)
    {
        holder.shiftType.setText(doctorBookingScheduleSlotsList.get(position).getType());
//        int count=doctorBookingScheduleSlotsList.get(position).getDoctorBookingSlotsDataList().size();
//        //  double height=(Math.ceil(count/4.0))*80;
//        int height = convertDpToPixels((float) ((Math.ceil(count/4.0))*80),mContext);
//        ViewGroup.LayoutParams layoutParams = holder.doctorBookingSlotsGridView.getLayoutParams();
//        layoutParams.height = height; //this is in pixels
//        holder.doctorBookingSlotsGridView.setLayoutParams(layoutParams);
        shiftSlotsAdapter=new TomorrowDoctorBookShiftAdapter(mContext,doctorBookingScheduleSlotsList.get(position).getDoctorBookingSlotsDataList(),doctorBookingScheduleSlotsList.get(position).getType());
        holder.doctorBookingSlotsGridView.setAdapter(shiftSlotsAdapter);

    }



    public static int convertDpToPixels(float dp, Context context){
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                resources.getDisplayMetrics()
        );
    }

    @Override
    public int getItemCount() {
        return doctorBookingScheduleSlotsList.size();
    }


    public static class CustomBookingSlotsViewHolder extends RecyclerView.ViewHolder{

        TextView shiftType;
        GridView doctorBookingSlotsGridView;

        public CustomBookingSlotsViewHolder(View itemView)
        {
            super(itemView);
            shiftType= (TextView) itemView.findViewById(R.id.tv_adapter_doctor_slots_shift);
            doctorBookingSlotsGridView = (GridView) itemView.findViewById(R.id.gv_adapter_doctor_slots_shift);
        }
    }
}
