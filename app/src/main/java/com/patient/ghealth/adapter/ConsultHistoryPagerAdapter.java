package com.patient.ghealth.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 10/4/2016.
 */
public class ConsultHistoryPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragmentList=new ArrayList<>();
//    private List<String> titleList =new ArrayList<>();

   public ConsultHistoryPagerAdapter(FragmentManager fm)
   {
       super(fm);
   }



    @Override
    public Fragment getItem(int position)
    {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    public void addItem(Fragment fragment)
    {
        fragmentList.add(fragment);
//        titleList.add(title);
    }

//    @Override
//    public CharSequence getPageTitle(int position) {
//        return titleList.get(position);
//    }


}
