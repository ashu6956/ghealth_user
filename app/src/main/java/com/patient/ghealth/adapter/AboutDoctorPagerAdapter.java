package com.patient.ghealth.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.patient.ghealth.R;

import java.util.List;

/**
 * Created by getit on 8/10/2016.
 */
public class AboutDoctorPagerAdapter extends PagerAdapter {

    private Context mContext;
    private List<String> dialogImageList;
    public AboutDoctorPagerAdapter(Context context,List<String> dialogImageList)
    {
        this.dialogImageList=dialogImageList;
        mContext=context;
    }
    @Override
    public int getCount() {
        return dialogImageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.adapter_about_doctor,container,false);
        ImageView aboutDoctor= (ImageView) view.findViewById(R.id.iv_about_doctor);
        Glide.with(mContext).load(dialogImageList.get(position)).into(aboutDoctor);
        container.addView(view,0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
