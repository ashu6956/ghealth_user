package com.patient.ghealth.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.patient.ghealth.R;
import com.patient.ghealth.activity.AddCardActivity;
import com.patient.ghealth.activity.ClinicDoctorDetailsActivity;
import com.patient.ghealth.activity.MainActivity;
import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.fragment.DoctorAvailabilityDialogFragment;
import com.patient.ghealth.model.CardDetails;
import com.patient.ghealth.model.DoctorOnLineDetails;
import com.patient.ghealth.networking.HttpRequester;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;
import com.patient.ghealth.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by user on 10/13/2016.
 */
public class DoctorOnDemandPagerAdapter extends PagerAdapter implements AsyncTaskCompleteListener {
    private Context mContext;
    private List<DoctorOnLineDetails> doctorOnLineDetailsList;
    private ImageView clinicImages;
    private TextView betweenDisatnce, clinicName, clinicAddress, noOfDoctorText;
    private String demandDoctorId;
    private int clickedPosition = 0;
    private Dialog progressDialog, paymentDialog, doctorBookingDialog;
    private String paymentType = "";
    private List<CardDetails> cardDetailsList;
    private MainActivity activity;
    private Location myLocation;
    private String requestId = "";
    private String type = "";

    public DoctorOnDemandPagerAdapter(Context context, List<DoctorOnLineDetails> doctorOnLineDetailsList, Activity activity, Location myLocation, String type) {

        this.activity = (MainActivity) activity;
        mContext = context;
        this.doctorOnLineDetailsList = doctorOnLineDetailsList;
        this.myLocation = myLocation;
        this.type = type;
    }

    @Override
    public int getCount() {
        return doctorOnLineDetailsList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View view = null;
        try {
            view = LayoutInflater.from(mContext).inflate(R.layout.adapter_near_by_item_layout, container, false);
            betweenDisatnce = (TextView) view.findViewById(R.id.tv_clinic_nearby_distance);
            clinicImages = (ImageView) view.findViewById(R.id.iv_near_by_clinic_icon);
            clinicName = (TextView) view.findViewById(R.id.tv_nearby_clinicName);
            clinicAddress = (TextView) view.findViewById(R.id.tv_nearby_clinic_Address);
            noOfDoctorText = (TextView) view.findViewById(R.id.tv_clinic_nearby_number_of_doctors);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent consultIntent = new Intent(activity, ClinicDoctorDetailsActivity.class);
                    DoctorOnLineDetails clinicDoctorDetails = doctorOnLineDetailsList.get(position);
                    AndyUtils.appLog("NearBy ClinicId", clinicDoctorDetails.getClinicId());
                    PreferenceHelper.setParam(mContext, Const.CLINIC_ID, clinicDoctorDetails.getClinicId());
                    Bundle consultBundle = new Bundle();
                    consultBundle.putSerializable(Const.DOCTOR_ONLINE_DETAILS, clinicDoctorDetails);
                    consultBundle.putString(Const.DOCTOR_TYPE, type);
                    consultIntent.putExtras(consultBundle);
                    mContext.startActivity(consultIntent);
//                    activity.finish();

                }
            });

            setValuesOnView(doctorOnLineDetailsList.get(position), position);
            container.addView(view, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }


    @Override
    public float getPageWidth(int position) {
        if (doctorOnLineDetailsList.size() == 1) {
            return 1;

        } else {
            return 0.97f;
        }


    }

    private void setValuesOnView(final DoctorOnLineDetails doctorOnLineDetails, final int scrollPosition) {

        if (!doctorOnLineDetails.getDoctor_clinic_image_first().equals("")) {
            Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_first()).into(clinicImages);
        } else if (!doctorOnLineDetails.getDoctor_clinic_image_second().equals("")) {
            Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_second()).into(clinicImages);
        } else if (!doctorOnLineDetails.getDoctor_clinic_image_third().equals("")) {
            Glide.with(mContext).load(doctorOnLineDetails.getDoctor_clinic_image_third()).into(clinicImages);
        }
        clinicName.setText(doctorOnLineDetails.getDoctorClinicName());
        clinicAddress.setText(doctorOnLineDetails.getClinicAddress());

        try {
            if (!doctorOnLineDetails.getBetweenDistance().equals("")) {
                String distance[] = doctorOnLineDetails.getBetweenDistance().split(" ");
                StringBuilder sb = new StringBuilder();
                if (distance[0] != null) {
                    sb.append(distance[0]);
                }
                if (distance[1] != null) {
                    sb.append(" ").append(distance[1]);
                }
                if (distance[2] != null) {
                    sb.append(" ").append(mContext.getString(R.string.away));
                }
                betweenDisatnce.setText(sb.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (doctorOnLineDetails.getClinicNoOfDoctor().equals("")) {
            noOfDoctorText.setVisibility(View.GONE);
        } else {
            noOfDoctorText.setVisibility(View.VISIBLE);
            noOfDoctorText.setText(doctorOnLineDetails.getClinicNoOfDoctor());
        }

    }


    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    private void showDoctorBookingDialog(final DoctorOnLineDetails details) {
        doctorBookingDialog = new Dialog(mContext);
        doctorBookingDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        doctorBookingDialog.setContentView(R.layout.dialog_doctor_booking_layout);
        ImageView doctorNext = (ImageView) doctorBookingDialog.findViewById(R.id.iv_dialog_on_demand_doctor);
        ImageView bookingNext = (ImageView) doctorBookingDialog.findViewById(R.id.iv_dialog_on_demand_booking);
        TextView needDoctorText = (TextView) doctorBookingDialog.findViewById(R.id.tv_on_demand_need);
        TextView doctorText = (TextView) doctorBookingDialog.findViewById(R.id.tv_doctor_on_demand_doctor);
        TextView bookingText = (TextView) doctorBookingDialog.findViewById(R.id.tv_doctor_on_demand_booking);
        ImageView doctorIcon = (ImageView) doctorBookingDialog.findViewById(R.id.iv_doctor_on_demand_doctor);
        ImageView bookingIcon = (ImageView) doctorBookingDialog.findViewById(R.id.iv_doctor_on_demand_calendar);
        RelativeLayout doctorLayout = (RelativeLayout) doctorBookingDialog.findViewById(R.id.rl_doctor_on_demand_doctor);
        RelativeLayout bookingLayout = (RelativeLayout) doctorBookingDialog.findViewById(R.id.rl_doctor_on_demand_booking);

        doctorIcon.setImageResource(R.drawable.stethoscope);
        doctorText.setText(mContext.getString(R.string.doctor));
        bookingText.setText(mContext.getString(R.string.booking));
        needDoctorText.setText(mContext.getString(R.string.need_doctor));
        doctorNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                getAddedCard();
            }
        });
        bookingNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                demandDoctorId = details.getDoctorOnLineId();
                DoctorAvailabilityDialogFragment availability = new DoctorAvailabilityDialogFragment();
                PreferenceHelper.setParam(mContext, Const.DOCTOR_ID, demandDoctorId);
                AndyUtils.appLog("DoctorOnDemand", "DoctorId" + demandDoctorId);
                availability.show(activity.getSupportFragmentManager(), "DoctorAvailabilityDialogFragment");

            }
        });
        doctorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                getAddedCard();

            }
        });
        bookingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doctorBookingDialog.cancel();
                demandDoctorId = details.getDoctorOnLineId();
                DoctorAvailabilityDialogFragment availability = new DoctorAvailabilityDialogFragment();
                PreferenceHelper.setParam(mContext, Const.DOCTOR_ID, demandDoctorId);
                AndyUtils.appLog("DoctorOnDemand", "DoctorId" + demandDoctorId);
                availability.show(activity.getSupportFragmentManager(), "DoctorAvailabilityDialogFragment");
            }
        });

        doctorBookingDialog.show();
    }

    private void getAddedCard() {
        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
//        AndyUtils.showSimpleProgressDialog(mContext, "Fetching All Cards...", false);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.GET_ADDED_CARDS_URL + Const.Params.ID + "="
                + String.valueOf(PreferenceHelper.getParam(mContext, Const.Params.ID, "")) + "&" + Const.Params.SESSION_TOKEN + "="
                + PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, ""));

        AndyUtils.appLog("Ashutosh", "GetAddedCardMap" + map);

        new HttpRequester(mContext, Const.GET, map, Const.ServiceCode.GET_ADDED_CARDS, this);


    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {

        JSONObject jsonObject;
        switch (serviceCode) {
            case Const.ServiceCode.GET_ADDED_CARDS:
                AndyUtils.appLog("Ashutosh", "GetAddedCardResponse" + response);
                try {

                    cardDetailsList = new ArrayList<>();
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true"))
                    {
                        JSONArray jsonArray = jsonObject.getJSONArray("cards");
                        if (jsonArray != null && jsonArray.length() > 0)
                        {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject cardObject = jsonArray.getJSONObject(i);
                                CardDetails cardDetails = new CardDetails();
                                cardDetails.setCardId(cardObject.optString("id"));
                                cardDetails.setCardNumber(cardObject.optString("last_four"));
                                cardDetails.setIsDefault(cardObject.optString("is_default"));
                                cardDetails.setType(cardObject.optString("card_type"));
                                cardDetailsList.add(cardDetails);
                            }

                        }
                        showPaymentDialog(cardDetailsList);
                    } else {
                        showPaymentDialog(cardDetailsList);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CREATE_REQUEST:
                AndyUtils.appLog("Ashutosh", "CreateRequestResponse" + response);
                try {

                    jsonObject = new JSONObject(response);
                    if (jsonObject.getString("success").equals("true")) {
                        JSONArray jsonArray = jsonObject.optJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            JSONObject requestObject = jsonArray.getJSONObject(0);
                            requestId = requestObject.optString(Const.REQUEST_ID);
                            AndyUtils.appLog("RequestId", requestId);
                        }
                    } else {
//                        AndyUtils.removeProgressDialog();
                        if (progressDialog != null) {
                            progressDialog.cancel();
                        }
                        AndyUtils.showShortToast(jsonObject.getString("error_message"), mContext);
                    }

                } catch (JSONException e) {
                    if (progressDialog != null) {
                        progressDialog.cancel();
                    }
                    e.printStackTrace();
                }
                break;
            case Const.ServiceCode.CREATE_CANCEL_REQUEST:
                try {
                    AndyUtils.appLog("Ashutosh", "CancelRequestResponse" + response);
                    jsonObject = new JSONObject(response);
                    if (jsonObject.optString("success").equals("true")) {
                        AndyUtils.showShortToast(jsonObject.optString("message"), mContext);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;

        }

    }

    private void showPaymentDialog(final List<CardDetails> cardList) {
        paymentType = "";
        DoctorOnLineDetails onLineDetails = doctorOnLineDetailsList.get(clickedPosition);
        paymentDialog = new Dialog(mContext, R.style.DialogDocotrTheme);
        paymentDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        paymentDialog.setContentView(R.layout.dialog_payment_mode_layout);
        final RecyclerView paymentRecyclerView = (RecyclerView) paymentDialog.findViewById(R.id.rv_card_details);
        paymentRecyclerView.setVisibility(View.GONE);
        final LinearLayout paymentLinearLayout = (LinearLayout) paymentDialog.findViewById(R.id.ll_no_card_added);
        paymentLinearLayout.setVisibility(View.GONE);
        final Button addCardButton = (Button) paymentDialog.findViewById(R.id.bn_payment_add_card);
        addCardButton.setVisibility(View.GONE);
        final ImageButton cashButton = (ImageButton) paymentDialog.findViewById(R.id.ib_payment_cash);
        final ImageButton cardButton = (ImageButton) paymentDialog.findViewById(R.id.ib_payment_card);

        TextView appointmentCharge = (TextView) paymentDialog.findViewById(R.id.tv_payment_appointment_charge);
        TextView doctorName = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_name);
        TextView clinicName = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_clinic_name);
        TextView clinicAddress = (TextView) paymentDialog.findViewById(R.id.tv_payment_doctor_clinic_address);
        final Button paymentButton = (Button) paymentDialog.findViewById(R.id.bn_payment_confirm);
        doctorName.setText(onLineDetails.getDoctorName());
        clinicName.setText(onLineDetails.getDoctorClinicName());
        clinicAddress.setText(onLineDetails.getClinicAddress());
        appointmentCharge.setText(mContext.getString(R.string.charge) + " " + onLineDetails.getDoctorChatConsultFee() + " " + mContext.getString(R.string.per_appointment));
        paymentDialog.show();

        paymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (paymentType.equals("")) {
                    AndyUtils.showShortToast(mContext.getString(R.string.please_choose_payment_mode), mContext);
                } else {
                    paymentDialog.cancel();
                    createDoctorRequest();
                }
            }
        });
        cardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                paymentType = Const.CARD;
                cardButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.doctor_booked_bg));
                cashButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.doctor_available_bg));
                LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
                paymentRecyclerView.setLayoutManager(layoutManager);

                if (cardList.size() > 0) {
                    paymentRecyclerView.setVisibility(View.VISIBLE);
                    GetCardsAdapter adapter = new GetCardsAdapter(mContext, cardList);
                    paymentRecyclerView.setAdapter(adapter);
                } else {
                    paymentRecyclerView.setVisibility(View.GONE);
                    paymentLinearLayout.setVisibility(View.VISIBLE);
                    addCardButton.setVisibility(View.VISIBLE);
                    addCardButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            paymentDialog.cancel();
                            Intent intent = new Intent(mContext, AddCardActivity.class);
                            mContext.startActivity(intent);
                        }
                    });
                    paymentButton.setEnabled(false);
                    paymentButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.disable_book_consult_bg));

                }

            }
        });
        cashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentType = Const.CASH;
                paymentRecyclerView.setVisibility(View.GONE);
                cardButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.doctor_available_bg));
                cashButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.doctor_booked_bg));
                paymentLinearLayout.setVisibility(View.GONE);
                addCardButton.setVisibility(View.GONE);
                paymentButton.setEnabled(true);
                paymentButton.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.book_consult_bg));
            }
        });

    }

    private void createDoctorRequest() {
        AndyUtils.appLog("CreateDoctorRequest", clickedPosition + "");
        DoctorOnLineDetails doctorDetails = doctorOnLineDetailsList.get(clickedPosition);
        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
        LatLng latLng = PreferenceHelper.getObject(mContext, Const.CURRENT_LATLNG);
        if (latLng != null) {
            showProgressDialog();
            HashMap<String, String> map = new HashMap<>();
            map.put(Const.Params.URL, Const.ServiceType.CREATE_REQUEST);
            map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(mContext, Const.Params.ID, "")));
            map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, ""));
            map.put(Const.Params.REQUEST_TYPE, String.valueOf(4));
            map.put(Const.Params.DOCTOR_ID, String.valueOf(doctorDetails.getDoctorOnLineId()));
            map.put(Const.Params.S_LATITUDE, String.valueOf(latLng.latitude));
            map.put(Const.Params.S_LONGITUDE, String.valueOf(latLng.longitude));
            map.put(Const.Params.PAYMENT_MODE, paymentType);
            AndyUtils.appLog("Ashutosh", "GetRequestMap" + map);
            new HttpRequester(mContext, Const.POST, map, Const.ServiceCode.CREATE_REQUEST, this);
        }
    }

    private void showProgressDialog() {
        progressDialog = new Dialog(mContext);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_custom_progress_layout);
        final ProgressBar progressBar = (ProgressBar) progressDialog.findViewById(R.id.dialog_progressBar);
        Button cancelButton = (Button) progressDialog.findViewById(R.id.bn_cancel_progress_dialog);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog.cancel();
                progressBar.setVisibility(View.GONE);
                cancelRequest();
            }
        });

        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public void removeProgressDialog() {
        if (progressDialog != null) {
            progressDialog.cancel();
        }
    }

    private void cancelRequest() {
        if (!AndyUtils.isNetworkAvailable(mContext)) {
            AndyUtils.showLongToast(mContext.getString(R.string.no_internet), mContext);
            return;
        }
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.Params.URL, Const.ServiceType.CREATE_CANCEL_REQUEST_URL);
        map.put(Const.Params.ID, String.valueOf(PreferenceHelper.getParam(mContext, Const.Params.ID, "")));
        map.put(Const.Params.SESSION_TOKEN, (String) PreferenceHelper.getParam(mContext, Const.Params.SESSION_TOKEN, ""));
        map.put(Const.Params.REQUEST_ID, String.valueOf(requestId));

        AndyUtils.appLog("Ashutosh", "CancelRequestMap" + map);

        new HttpRequester(mContext, Const.POST, map, Const.ServiceCode.CREATE_CANCEL_REQUEST, this);
    }

}
