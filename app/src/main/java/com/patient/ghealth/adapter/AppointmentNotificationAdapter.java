package com.patient.ghealth.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.patient.ghealth.R;

/**
 * Created by user on 9/2/2016.
 */
public class AppointmentNotificationAdapter extends RecyclerView.Adapter<AppointmentNotificationAdapter.CustomAppointmentViewHolder>{

    private Context mContext;
    public AppointmentNotificationAdapter(Context context)
    {
        mContext=context;
    }

    @Override
    public CustomAppointmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(mContext).inflate(R.layout.adapter_notification_appointment_layout,null);
        CustomAppointmentViewHolder appointmentViewHolder=new CustomAppointmentViewHolder(view);
        return appointmentViewHolder;
    }

    @Override
    public void onBindViewHolder(CustomAppointmentViewHolder holder, int position)
    {
        holder.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return 3;
    }



    public class CustomAppointmentViewHolder extends RecyclerView.ViewHolder
    {
        ImageView nextButton;
        public CustomAppointmentViewHolder(View itemView) {
            super(itemView);
            nextButton= (ImageView) itemView.findViewById(R.id.iv_notification_appointment_next_icon);
        }
    }
}
