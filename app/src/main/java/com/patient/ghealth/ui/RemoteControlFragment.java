package com.patient.ghealth.ui;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.VideoCallActivity;


public class RemoteControlFragment extends Fragment {

    private static final String LOGTAG = VideoCallActivity.class.getName();
    private static final int ANIMATION_DURATION = 7000;

    private VideoCallActivity mActivity;

    private RelativeLayout mContainer;
    private View mRootView;
    private ImageButton mAudioBtn;
    private ImageButton mVideoBtn;

    private RemoteControlCallbacks mControlCallbacks = remoteCallbacks;

    public interface RemoteControlCallbacks {
        public void onDisableRemoteAudio(boolean audio);

        public void onDisableRemoteVideo(boolean video);
    }

    private static RemoteControlCallbacks remoteCallbacks = new RemoteControlCallbacks() {
        @Override
        public void onDisableRemoteAudio(boolean audio) { }

        @Override
        public void onDisableRemoteVideo(boolean video) { }
    };

    private View.OnClickListener mBtnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.remoteAudio:
                    updateRemoteAudio();
                    break;

                case R.id.remoteVideo:
                    updateRemoteVideo();
                    break;
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        Log.i(LOGTAG, "OnAttach RemoteControlFragment");

        super.onAttach(context);

        this.mActivity = (VideoCallActivity) context;
        this.mControlCallbacks = (RemoteControlCallbacks) context;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

            this.mActivity = (VideoCallActivity) activity;
            this.mControlCallbacks = (RemoteControlCallbacks) activity;
        }
    }

    @Override
    public void onDetach() {
        Log.i(LOGTAG, "OnDetach RemoteControlFragment");

        super.onDetach();

        mControlCallbacks = remoteCallbacks;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retain this fragment across configuration changes.
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(LOGTAG, "OnCreate RemoteControlFragment");

        mRootView = inflater.inflate(R.layout.remote_actionbar_fragment, container, false);

        mContainer = (RelativeLayout) this.mActivity.findViewById(R.id.actionbar_remote_fragment_container);
        mAudioBtn = (ImageButton) mRootView.findViewById(R.id.remoteAudio);
        mVideoBtn = (ImageButton) mRootView.findViewById(R.id.remoteVideo);

        mAudioBtn.setOnClickListener(mBtnClickListener);
        mVideoBtn.setOnClickListener(mBtnClickListener);

        mAudioBtn.setImageResource(mActivity.getComm().getRemoteAudio()
                ? R.mipmap.audio
                : R.mipmap.no_audio);

        mVideoBtn.setImageResource(mActivity.getComm().getRemoteVideo()
                ? R.mipmap.video_icon
                : R.mipmap.no_video_icon);

        return mRootView;
    }

    public void updateRemoteAudio(){
        if(!mActivity.getComm().getRemoteAudio()){
            mControlCallbacks.onDisableRemoteAudio(true);
            mAudioBtn.setImageResource(R.mipmap.audio);
        }
        else {
            mControlCallbacks.onDisableRemoteAudio(false);
            mAudioBtn.setImageResource(R.mipmap.no_audio);
        }
    }

    public void updateRemoteVideo(){
        if(!mActivity.getComm().getRemoteVideo()){
            mControlCallbacks.onDisableRemoteVideo(true);
            mVideoBtn.setImageResource(R.mipmap.video_icon);
        }
        else {
            mControlCallbacks.onDisableRemoteVideo(false);
            mVideoBtn.setImageResource(R.mipmap.no_video_icon);
        }
    }

    public void show(){
        mContainer.setVisibility(View.VISIBLE);
        mRootView.setVisibility(View.VISIBLE);

        mContainer.postDelayed(new Runnable() {
            public void run() {
                mContainer.setVisibility(View.INVISIBLE);
            }
        }, ANIMATION_DURATION);
    }

    public void setEnabled(boolean enabled) {
        if (mVideoBtn != null && mAudioBtn != null) {
            if (!enabled) {
                mAudioBtn.setImageResource(R.mipmap.audio);
                mVideoBtn.setImageResource(R.mipmap.video_icon);
            }
        }
    }
    public void restart() {
        setEnabled(false);
        mContainer.setVisibility(View.INVISIBLE);
    }

}
