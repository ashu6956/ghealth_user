package com.patient.ghealth.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.patient.ghealth.R;
import com.patient.ghealth.activity.VideoCallActivity;


public class PreviewControlFragment extends Fragment {

    private static final String LOGTAG = VideoCallActivity.class.getName();
    private static PreviewControlCallbacks previewCallbacks = new PreviewControlCallbacks() {
        @Override
        public void onDisableLocalAudio(boolean audio) {
        }

        @Override
        public void onDisableLocalVideo(boolean video) {
        }

        @Override
        public void onCall() {
        }

        @Override
        public void onTextChat() {

        }

    };
    public TextView countMessages;
    View rootView;
    VectorDrawableCompat drawableStartCall;
    VectorDrawableCompat drawableEndCall;
    VectorDrawableCompat drawableBckBtn;
    private VideoCallActivity mActivity;
    private RelativeLayout mContainer;
    private ImageButton mAudioBtn;
    private ImageButton mVideoBtn;
    private ImageButton mCallBtn;
    private ImageButton mTextChatBtn;
    private PreviewControlCallbacks mControlCallbacks = previewCallbacks;
    private View.OnClickListener mBtnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.localAudio:
                    updateLocalAudio();
                    break;

                case R.id.localVideo:
                    updateLocalVideo();
                    break;

//                case R.id.call:
//                    updateCall();
//                    break;

                case R.id.textChat:
                    updateTextChat();
                    break;
            }
        }
    };

    @Override
    public void onAttach(Context context) {
        Log.i(LOGTAG, "OnAttach PreviewControlFragment");

        super.onAttach(context);

        this.mActivity = (VideoCallActivity) context;
        this.mControlCallbacks = (PreviewControlCallbacks) context;
    }

    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

            this.mActivity = (VideoCallActivity) activity;
            this.mControlCallbacks = (PreviewControlCallbacks) activity;
        }
    }

    @Override
    public void onDetach() {
        Log.i(LOGTAG, "onDetach PreviewControlFragment");

        super.onDetach();

        mControlCallbacks = previewCallbacks;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retain this fragment across configuration changes.
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(LOGTAG, "OnCreate PreviewControlFragment");

        rootView = inflater.inflate(R.layout.preview_actionbar_fragment, container, false);

        mContainer = (RelativeLayout) this.mActivity.findViewById(R.id.actionbar_preview_fragment_container);
        mAudioBtn = (ImageButton) rootView.findViewById(R.id.localAudio);
        mVideoBtn = (ImageButton) rootView.findViewById(R.id.localVideo);
        mCallBtn = (ImageButton) rootView.findViewById(R.id.call);
        mTextChatBtn = (ImageButton) rootView.findViewById(R.id.textChat);
        mTextChatBtn.setVisibility(View.GONE);
        countMessages = (TextView) rootView.findViewById(R.id.tv_count_chat_message);
        countMessages.setVisibility(View.GONE);
        drawableStartCall = VectorDrawableCompat.create(getResources(), R.drawable.initiate_call_button, null);
        drawableEndCall = VectorDrawableCompat.create(getResources(), R.drawable.end_call_button, null);
        drawableBckBtn = VectorDrawableCompat.create(getResources(), R.drawable.bckg_icon, null);

        mTextChatBtn.setBackground(drawableBckBtn);

        mAudioBtn.setImageResource(mActivity.getComm().getLocalAudio()
                ? R.mipmap.mic_icon
                : R.mipmap.muted_mic_icon);
        mAudioBtn.setBackground(drawableBckBtn);

        mVideoBtn.setImageResource(mActivity.getComm().getLocalVideo()
                ? R.mipmap.video_icon
                : R.mipmap.no_video_icon);
        mVideoBtn.setBackground(drawableBckBtn);

        mCallBtn.setImageResource(mActivity.getComm().isStarted()
                ? R.mipmap.hang_up
                : R.mipmap.start_call);

        mCallBtn.setBackground(mActivity.getComm().isStarted()
                ? drawableEndCall
                : drawableStartCall);

        mCallBtn.setOnClickListener(mBtnClickListener);

        setEnabled(mActivity.getComm().isStarted());

        return rootView;
    }

    public void updateLocalAudio() {
        if (!mActivity.getComm().getLocalAudio()) {
            mControlCallbacks.onDisableLocalAudio(true);
            mAudioBtn.setImageResource(R.mipmap.mic_icon);
        } else {
            mControlCallbacks.onDisableLocalAudio(false);
            mAudioBtn.setImageResource(R.mipmap.muted_mic_icon);
        }
    }

    public void updateLocalVideo() {
        if (!mActivity.getComm().getLocalVideo()) {
            mControlCallbacks.onDisableLocalVideo(true);
            mVideoBtn.setImageResource(R.mipmap.video_icon);
        } else {
            mControlCallbacks.onDisableLocalVideo(false);
            mVideoBtn.setImageResource(R.mipmap.no_video_icon);
        }
    }

    public void updateCall() {
        mCallBtn.setImageResource(!mActivity.getComm().isStarted()
                ? R.mipmap.hang_up
                : R.mipmap.start_call);

        mCallBtn.setBackground(!mActivity.getComm().isStarted()
                ? drawableEndCall
                : drawableStartCall);

        if (mControlCallbacks != null)
            mControlCallbacks.onCall();
    }

    public void updateTextChat() {
        mControlCallbacks.onTextChat();
    }

    public void setEnabled(boolean enabled) {
        if (mVideoBtn != null && mAudioBtn != null) {
            if (enabled) {
                mAudioBtn.setOnClickListener(mBtnClickListener);
                mVideoBtn.setOnClickListener(mBtnClickListener);
                mTextChatBtn.setOnClickListener(mBtnClickListener);
            } else {
                mAudioBtn.setOnClickListener(null);
                mVideoBtn.setOnClickListener(null);
                mAudioBtn.setImageResource(R.mipmap.mic_icon);
                mVideoBtn.setImageResource(R.mipmap.video_icon);
                mTextChatBtn.setOnClickListener(null);
            }
        }
    }

    public void restart() {
        setEnabled(false);

        mCallBtn.setBackground(drawableStartCall);
        mCallBtn.setImageResource(R.mipmap.start_call);

    }

    public interface PreviewControlCallbacks {

        public void onDisableLocalAudio(boolean audio);

        public void onDisableLocalVideo(boolean video);

        public void onCall();

        public void onTextChat();

    }
}
