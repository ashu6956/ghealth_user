//package com.patient.ghealth.service;
//
//import android.app.IntentService;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.os.Bundle;
//import android.preference.PreferenceManager;
//
//
//import com.patient.ghealth.activity.MessageActivity;
//import com.patient.ghealth.model.ChatObject;
//import com.patient.ghealth.realmDB.DatabaseHandler;
//import com.patient.ghealth.utils.Const;
//
//import java.io.BufferedInputStream;
//import java.io.File;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.net.URL;
//import java.net.URLConnection;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//
///**
// * Created by user on 8/25/2015.
// */
//
//public class FileDownloadService extends IntentService {
//    public static final int UPDATE_PROGRESS = 8344;
//    private SharedPreferences preferences;
//    private String userId,isGroup,groupId,userName = null;
//
//    public FileDownloadService() {
//        super("FileDownloadService");
//    }
//
//    @Override
//    protected void onHandleIntent(Intent intent) {
//        String urlToDownload = intent.getStringExtra("url");
//        userId = intent.getStringExtra("userId");
//        isGroup = intent.getStringExtra("isGroup");
//
//        if(isGroup!=null && !isGroup.equalsIgnoreCase("null"))
//        {
//            groupId = intent.getStringExtra("groupId");
//            userName = intent.getStringExtra("userName");
//        }
//
//
//        preferences = PreferenceManager.getDefaultSharedPreferences(this);
//        //   ResultReceiver receiver = (ResultReceiver) intent.getParcelableExtra("receiver");
//        try {
//            URL url = new URL(urlToDownload);
//            URLConnection connection = url.openConnection();
//            connection.connect();
//            // this will be useful so that you can show a typical 0-100% progress bar
//            int fileLength = connection.getContentLength();
//
//            Date date = new Date();
//            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
//                    .format(date.getTime());
//
//            // download the file
//            InputStream input = new BufferedInputStream(connection.getInputStream());
//            OutputStream output = null;
//
//            String outputMediaPath = null;
//
//            if (urlToDownload.contains(".jpg") || urlToDownload.contains(".jpeg") || urlToDownload.contains(".png"))
//
//            {
//
//                String filepath = Const.LOCAL_STORAGE_IMAGE_PATH;
//                File file = new File(filepath);
//                if (!file.exists()) {
//                    file.mkdirs();
//                }
//
//
//                outputMediaPath = Const.LOCAL_STORAGE_IMAGE_PATH + File.separator +
//                        "IMG_" + timeStamp + ".jpg";
//            }
//            if (urlToDownload.contains(".mp3"))
//
//            {
//
//                String filepath = Const.LOCAL_STORAGE_AUDIO_PATH;
//                File file = new File(filepath);
//                if (!file.exists()) {
//                    file.mkdirs();
//                }
//
//                outputMediaPath = Const.LOCAL_STORAGE_AUDIO_PATH + File.separator +
//                        "AUD_" + timeStamp + ".mp3";
//            }
//
//            if (urlToDownload.contains(".mp4"))
//
//            {
//
//
//                String filepath = Const.LOCAL_STORAGE_VIDEO_PATH;
//                File file = new File(filepath);
//                if (!file.exists()) {
//                    file.mkdirs();
//                }
//                outputMediaPath = Const.LOCAL_STORAGE_VIDEO_PATH + File.separator +
//                        "VID_" + timeStamp + ".mp4";
//            }
//
//            try {
//                output = new FileOutputStream(outputMediaPath);
//            } catch (Exception e) {
//
//                File file = new File(outputMediaPath);
//                e.printStackTrace();
//            }
//            byte data[] = new byte[1024];
//            long total = 0;
//            int count;
//            while ((count = input.read(data)) != -1) {
//                total += count;
//                // publishing the progress....
//                Bundle resultData = new Bundle();
//                resultData.putInt("progress", (int) (total * 100 / fileLength));
//                //   receiver.send(UPDATE_PROGRESS, resultData);
//                output.write(data, 0, count);
//
//
//            }
//
//
//            DatabaseHandler db = new DatabaseHandler(getApplicationContext());
//
//            ChatObject mediaMessage = null;
//            String str_message;
//            date = new Date();
//            if (outputMediaPath.contains(".jpg") || outputMediaPath.contains(".jpeg") || outputMediaPath.contains(".png")) {
//
//                str_message = "image_" + outputMediaPath;
//
//                //   mediaMessage = new Message(str_message, Message.ATTACHED_IMAGE, date.toString(), "success", "receiver", "9790318161", "receiver", date);
//                if(isGroup!=null && isGroup.equalsIgnoreCase("1"))
//                {
//                    if(MessageActivity.userId!=null)
//                    {
//                        mediaMessage =  new ChatObject(str_message, ChatObject.ATTACHED_IMAGE, date.toString(), "success", "receiver", "phoneNo", "receiver", date, userId, "true", groupId, userName,"read");
//                    }
//                    else
//                    {
//                        mediaMessage =  new ChatObject(str_message, ChatObject.ATTACHED_IMAGE, date.toString(), "success", "receiver", "phoneNo", "receiver", date, userId, "true", groupId, userName,"unread");
//                    }
//
//                }
//                else
//                {
//                    if(MessageActivity.userId!=null)
//                    {
//                        mediaMessage = new ChatObject(str_message, ChatObject.ATTACHED_IMAGE, date.toString(), "success", "receiver", preferences.getString("id", "100"), "receive", date, userId, "false", "", "","read");
//                    }
//                    else
//                    {
//                        mediaMessage = new ChatObject(str_message, ChatObject.ATTACHED_IMAGE, date.toString(), "success", "receiver", preferences.getString("id", "100"), "receive", date, userId, "false", "", "","unread");
//                    }
//
//                }
//
//
//
//
//            } else if (outputMediaPath.contains(".mp3")) {
//                str_message = "audio_" + outputMediaPath;
//
//                //   mediaMessage = new Message(str_message, Message.VOICE_CLIP, date.toString(), "success", "receiver", "9790318161", "receiver", date);
//
//                if(isGroup!=null && isGroup.equalsIgnoreCase("1"))
//                {
//                    if(MessageActivity.userId!=null)
//                    {
//                        mediaMessage =  new ChatObject(str_message, ChatObject.VOICE_CLIP, date.toString(), "success", "receiver", "phoneNo", "receiver", date, userId, "true", groupId, userName,"read");
//                    }
//                    else
//                    {
//                        mediaMessage =  new ChatObject(str_message,ChatObject.VOICE_CLIP, date.toString(), "success", "receiver", "phoneNo", "receiver", date, userId, "true", groupId, userName,"unread");
//                    }
//
//                }
//                else {
//                    if(MessageActivity.userId!=null)
//                    {
//                        mediaMessage = new ChatObject(str_message, ChatObject.VOICE_CLIP, date.toString(), "success", "receiver", preferences.getString("id", "100"), "receiver", date, userId, "false", "", "","read");
//                    }
//                    else
//                    {
//                        mediaMessage = new ChatObject(str_message, ChatObject.VOICE_CLIP, date.toString(), "success", "receiver", preferences.getString("id", "100"), "receiver", date, userId, "false", "", "","unread");
//                    }
//
//
//                }
//
//            } else if (outputMediaPath.contains(".mp4"))
//
//            {
//                str_message = "video_" + outputMediaPath;
//
//                // mediaMessage = new Message(str_message, Message.VIDEO, date.toString(), "success", "receiver", "9790318161", "receiver", date);
//                if(isGroup!=null &&isGroup.equalsIgnoreCase("1"))
//                {
//                    if(MessageActivity.userId!=null)
//                    {
//                        mediaMessage =  new ChatObject(str_message, ChatObject.VIDEO, date.toString(), "success", "receiver", "phoneNo", "receiver", date, userId, "true", groupId, userName,"read");
//                    }
//                    else
//                    {
//                        mediaMessage =  new ChatObject(str_message,ChatObject.VIDEO, date.toString(), "success", "receiver", "phoneNo", "receiver", date, userId, "true", groupId, userName,"unread");
//                    }
//
//
//                }
//                else {
//                    if(MessageActivity.userId!=null)
//                    {
//                        mediaMessage = new ChatObject(str_message, ChatObject.VIDEO, date.toString(), "success", "receiver", preferences.getString("id", "100"), "receiver", date, userId, "false", "", "","read");
//                    }
//                    else
//                    {
//                        mediaMessage = new ChatObject(str_message, ChatObject.VIDEO, date.toString(), "success", "receiver", preferences.getString("id", "100"), "receiver", date, userId, "false", "", "","unread");
//                    }
//
//
//                }
//
//            }
//
//            db.insertMessage(mediaMessage);
//
//            Intent downloadedIntent = new Intent();
//            downloadedIntent.setAction("new_message");
//
//            downloadedIntent.putExtra("Message", "Media_Downloaded");
//            downloadedIntent.putExtra("userId", userId);
//            sendBroadcast(downloadedIntent);
//
//            output.flush();
//            output.close();
//            input.close();
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        Bundle resultData = new Bundle();
//        resultData.putInt("progress", 100);
//        //   receiver.send(UPDATE_PROGRESS, resultData);
//    }
//}
