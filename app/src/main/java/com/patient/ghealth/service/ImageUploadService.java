package com.patient.ghealth.service;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;

import com.patient.ghealth.custom_interface.AsyncTaskCompleteListener;
import com.patient.ghealth.utils.AndroidMultipartEntitity;
import com.patient.ghealth.utils.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;


/**
 * Created by user on 8/19/2015.
 */
public class ImageUploadService extends IntentService implements AsyncTaskCompleteListener {

    Bitmap saveimage;
    byte[] imageBytes;
    /*String api_token = "2d7dad18fb86a213fb1fc3515a383787";
    String push_token = "2256d4641beda5a901ce179d6ef34b7e980786e0ad0e52d52adc74032244995f";*/
    Bitmap profilePic;
    File fileToUpload;


    public ImageUploadService() {
        super("image upload");
    }

    @Override
    protected void onHandleIntent(Intent intent) {


        String image_path = intent.getExtras().getString("image_path");
        String request_id = intent.getExtras().getString("request_id");

        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.Params.URL, Const.ServiceType.UPLOAD_IMAGE_URL);
        map.put("image", image_path);
        map.put(Const.Params.REQUEST_ID, request_id);

        Log.d("mahi", "map send video" + map.toString());
        new AndroidMultipartEntitity(getApplicationContext(),map,
                Const.ServiceCode.UPLOAD_IMAGE, this);

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        switch (serviceCode) {
            case Const.ServiceCode.UPLOAD_IMAGE:
                Log.d("mahi", "upload  img" + response);

                try {
                    JSONObject job1 = new JSONObject(response);
                    if (job1.getString("success").equals("true")) {
                        Intent updateMediaIntent = new Intent("media_upload_receiver");
                        updateMediaIntent.putExtra("response", job1.getString("image"));
                        sendBroadcast(updateMediaIntent);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }


    }
}

