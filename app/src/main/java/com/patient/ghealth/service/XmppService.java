package com.patient.ghealth.service;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;


import com.patient.ghealth.activity.MessageActivity;
import com.patient.ghealth.activity.VideoCallActivity;
import com.patient.ghealth.model.ChatObject;
import com.patient.ghealth.realmDB.DatabaseHandler;
import com.patient.ghealth.utils.AndyUtils;
import com.patient.ghealth.utils.Const;

import org.jivesoftware.smack.AccountManager;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatManagerListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.RosterListener;
import org.jivesoftware.smack.SASLAuthentication;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferNegotiator;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.ping.PingFailedListener;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Random;

public class XmppService extends Service implements Handler.Callback,
        ChatManagerListener, MessageListener,
        ConnectionListener{
    private static XMPPConnection connection;
    private static PingManager mPingManager;
    private static String username;
    private static String password;
    private static String domain;
    private static String ip;
    private String messageType="";
    public static final String DOMAIN = "52.10.103.74";
    private static final String KEY_METHOD = "key_method";
    private static final String KEY_DOMAIN = "key_domain";
    private static final String KEY_IP = "key_ip";
    private static final String KEY_USERNAME = "key_username";
    private static final String KEY_PASSWORD = "key_password";

    private static final String KEY_SEND_TO = "key_send_to";
    private static final String KEY_SEND_TYPE = "key_send_type";
    private static final String KEY_SEND_MESSAGE = "key_send_message";

    private static final String KEY_ROOM_NAME = "key_room_name";
    private static final String KEY_ROOM_NICKNAME = "key_room_nickname";

    private static final int METHOD_SETUP = 1;
    private static final int METHOD_SETUP_CONNECT = 2;
    private static final int METHOD_SEND_CHAT = 3;
    private static final int METHOD_AUTO_START = 100;

    private static final int METHOD_CREATE_GROUP = 4;
    private static final int METHOD_INVITE_GROUP = 5;
    private static final int METHOD_MESSAGE_GROUP = 6;
    private static final int METHOD_JOIN_GROUP = 7;

    private static Handler mHandler;
    private MessageActivity messageActivity;
    private VideoCallActivity videoActivity;
    private final IBinder mBinder = new LocalBinder();

    private static PowerManager.WakeLock mWakeLock;

    private static volatile boolean isConnected = false;
    private static MultiUserChat mMultiUserChat;
    //DBHelper db = new DBHelper(XmppService.this);
    //DatabaseHandler db=new DatabaseHandler(XmppService.this);
    //DatabaseHandler db = new DatabaseHandler(XmppService.this);
    public static String _CHAR = "0123456789";
    SharedPreferences preferences;
    private String userId;

    // LikeMatcheddataForListview likeuser;
    @Override
    public void onCreate() {
        //Thread.setDefaultUncaughtExceptionHandler(new CrashLogger(XmppService.this));
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        //editor = preferences.edit();
        HandlerThread mThread = new HandlerThread("XmppServiceThread",
                android.os.Process.THREAD_PRIORITY_URGENT_AUDIO);
        mThread.start();
        mHandler = new Handler(mThread.getLooper(), this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int method = intent.getIntExtra(KEY_METHOD, -1);
        if (method != -1) {
            mHandler.sendMessage(mHandler.obtainMessage(method, intent));
        }
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        return mBinder;
    }

    // STATIC PUBLIC METHODS

    public static void setup(final Context context, String domain, String ip,
                             String username, String password) {
        Intent xmppServiceIntent = addInfoToSetupIntent(context, METHOD_SETUP,
                domain, ip, username, password);
        context.startService(xmppServiceIntent);
    }

    public static void setupAndConnect(final Context context, String domain,
                                       String ip, String username, String password) {
        Intent xmppServiceIntent = addInfoToSetupIntent(context,
                METHOD_SETUP_CONNECT, domain, ip, username, password);
        context.startService(xmppServiceIntent);

        Log.d("mahi","connecting..");
    }

    public static void joinGroup(final Context context, String groupName, String userId) {
        Intent xmppServiceIntent = addInfoToGroupJoinIntent(context,
                METHOD_JOIN_GROUP, groupName, userId);
        context.startService(xmppServiceIntent);
    }


    public static void disconnect() {
        if (connection != null) {
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        connection.disconnect();
                        connection = null;
                    } catch (SmackException.NotConnectedException e) {
                        e.printStackTrace();
                    }
                }
            };
            new Thread(runnable).start();
        }

        if (mWakeLock != null) {
            if (mWakeLock.isHeld()) {
                mWakeLock.release();
            }
        }

    }

    public static XMPPConnection getConnection() {
        return connection;
    }

    public static void sendMessage(final Context context, String to,
                                   Message.Type type, String message) {

        Log.d("pavan","in xmpp send message ");
        Intent xmppServiceIntent = addInfoToSendIntent(context, to, type,
                message);
        context.startService(xmppServiceIntent);
    }


    // public static void sendImage(final Context context, String to,
    // Message.Type type, String message) {
    //
    // Log.w("TAG", "Come");
    //
    // FileTransferManager manager = new FileTransferManager(connection);
    // org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer transfer =
    // manager
    // .createOutgoingFileTransfer("ajay@" + Constants.DOMAIN + "/Smack");
    // java.io.File file = new java.io.File(
    // Environment.getExternalStorageDirectory() + "/1.jpg");
    // try {
    // transfer.sendFile(file, "test_file");
    // } catch (SmackException e) {
    // e.printStackTrace();
    // }
    // while (!transfer.isDone()) {
    // if (transfer.getStatus().equals(Status.error)) {
    // System.out.println("ERROR!!! " + transfer.getError());
    // } else if (transfer.getStatus().equals(Status.cancelled)
    // || transfer.getStatus().equals(Status.refused)) {
    // System.out.println("Cancelled!!! " + transfer.getError());
    // }
    // try {
    // Thread.sleep(1000L);
    // } catch (InterruptedException e) {
    // e.printStackTrace();
    // }
    // }
    // if (transfer.getStatus().equals(Status.refused)
    // || transfer.getStatus().equals(Status.error)
    // || transfer.getStatus().equals(Status.cancelled)) {
    // System.out
    // .println("refused cancelled error " + transfer.getError());
    // } else {
    // System.out.println("Success");
    // }
    // }

    // PRIVATE METHODS

    private static Intent addInfoToSendIntent(final Context context, String to,
                                              Message.Type type, String message) {
        Intent intent = getXmppServiceIntent(context);
        intent.putExtra(KEY_METHOD, METHOD_SEND_CHAT);
        intent.putExtra(KEY_SEND_TO, to);
        intent.putExtra(KEY_SEND_TYPE, type);
        intent.putExtra(KEY_SEND_MESSAGE, message);
        return intent;
    }

    private static Intent addInfoToSetupIntent(final Context context,
                                               int method, String domain, String ip, String username,
                                               String password) {
        Intent intent = getXmppServiceIntent(context);
        intent.putExtra(KEY_METHOD, method);
        intent.putExtra(KEY_DOMAIN, domain);
        intent.putExtra(KEY_IP, ip);
        intent.putExtra(KEY_USERNAME, username);
        intent.putExtra(KEY_PASSWORD, password);
        return intent;
    }

    private static Intent addInfoToRoomIntent(final Context context,
                                              int method, String groupName, String userId) {
        Intent intent = getXmppServiceIntent(context);
        intent.putExtra(KEY_METHOD, method);
        intent.putExtra("GROUPNAME", groupName);
        intent.putExtra("USERID", userId);
        return intent;
    }

    private static Intent addInfoToInviteGroupIntent(final Context context,
                                                     int method, String groupName, String userId) {
        Intent intent = getXmppServiceIntent(context);
        intent.putExtra(KEY_METHOD, method);
        intent.putExtra("GROUPNAME", groupName);
        intent.putExtra("USERID", userId);
        return intent;
    }

    private static Intent addInfoToGroupMessageIntent(final Context context,
                                                      int method, String roomName, String message) {
        Intent intent = getXmppServiceIntent(context);
        intent.putExtra(KEY_METHOD, method);
        intent.putExtra(KEY_ROOM_NAME, roomName);
        intent.putExtra(KEY_SEND_MESSAGE, message);
        return intent;
    }

    private static Intent addInfoToGroupJoinIntent(final Context context,
                                                   int method, String roomName, String userId) {
        Intent intent = getXmppServiceIntent(context);
        intent.putExtra(KEY_METHOD, method);
        intent.putExtra(KEY_ROOM_NAME, roomName);
        intent.putExtra("USERID", userId);
        return intent;
    }

    public static void createGroup(final Context context, String groupName, String userId) {
        Intent xmppServiceIntent = addInfoToRoomIntent(context,
                METHOD_CREATE_GROUP, groupName, userId);
        context.startService(xmppServiceIntent);
    }

    public static void inviteToGroup(final Context context, String groupName, String user) {
        Intent xmppServiceIntent = addInfoToInviteGroupIntent(context,
                METHOD_INVITE_GROUP, groupName, user);
        context.startService(xmppServiceIntent);
    }

    public static void messageToGroup(final Context context, String groupName, String message) {
        Intent xmppServiceIntent = addInfoToGroupMessageIntent(context,
                METHOD_MESSAGE_GROUP, groupName, message);
        context.startService(xmppServiceIntent);
    }


    private static Intent getXmppServiceIntent(final Context context) {
        return new Intent(context, XmppService.class);
    }

    private void _connect() {
        if (connection != null) {
            try {
                connection.disconnect();
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
            mPingManager = null;
            connection = null;
        }
        if (username != null && password != null && domain != null) {
            try {
                ConnectionConfiguration config = new ConnectionConfiguration(
                        domain, 5222);
                config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
                config.setReconnectionAllowed(false);
                connection = new XMPPTCPConnection(config);
                connection.addConnectionListener(this);
                connection.connect();
                Log.d("mahi","is connected"+connection.isConnected());

                try {
                    AccountManager a = AccountManager.getInstance(connection);
                    a.createAccount(username, "123456");
                } catch (Exception e)
                {
                    e.printStackTrace();
                }

                Log.d("Username, password",""+
                        connection + ", " + username + "," + password);

                connection.login(username, password);
                Presence presence = new Presence(Presence.Type.available);
                presence.setStatus("I’m available");
                connection.sendPacket(presence);

                Roster roster = connection.getRoster();
                roster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);

                preferences = PreferenceManager.getDefaultSharedPreferences(this);
                String userName = preferences.getString("UserName", "user");


                FileTransferManager fileTransferManager = new FileTransferManager(
                        connection);
                FileTransferNegotiator.setServiceEnabled(connection, true);

                mPingManager = PingManager.getInstanceFor(connection);
                mPingManager.setPingInterval(5);
                mPingManager
                        .registerPingFailedListener(new PingFailedListener() {
                            @Override
                            public void pingFailed() {
                                Log.d("Ping failed","");
                            }
                        });

                ChatManager chatManager = ChatManager
                        .getInstanceFor(connection);
                chatManager.addChatListener(XmppService.this);

                PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
                mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                        "XmppService");
                mWakeLock.acquire();

                // throw an event saying that log in is successful
                // EventBus.getDefault().postSticky(new LoginEvent(true));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (SmackException e) {
                e.printStackTrace();
            } catch (XMPPException e) {
                e.printStackTrace();
            }
        }
    }

    private void _sendMessage(String to, Message.Type type, String message) {
        Log.w("Message Detail", to + "-" + type + "-" + message);
        Message msg = new Message(to, type);
        msg.setTo(to);
        msg.setBody(message);
        Log.d("pavan", "in xmpp send message ");

        if (connection != null) {
            // Get the roster
            Roster roster = connection.getRoster();

            try {
                roster.createEntry(to, to, null);
            } catch (SmackException.NotLoggedInException e1) {
                e1.printStackTrace();
            } catch (SmackException.NoResponseException e1) {
                e1.printStackTrace();
            } catch (XMPPException.XMPPErrorException e1) {
                e1.printStackTrace();
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }

            // Sleep for a second
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }

            // Get presence information

            Log.d("to user","" + to);
            Collection<RosterEntry> entries = roster.getEntries();
            for (RosterEntry entry : entries) {
                Log.d("entry","" + entry.getUser());
                if (entry.getUser().equalsIgnoreCase(to)) {
                    Presence presence = roster.getPresence(entry.getUser());
                    Log.d("entry","" + entry);
                    Log.d("presence","" + presence.isAvailable());
                    Log.d("mahi","user available"+presence.isAvailable());
                    Log.d("mahi","user status"+presence.getStatus());
                    Log.d("user ","" + presence.getStatus());
                    Log.d("mode ","" + presence.getMode());
                } else {
                    Log.d("NOT Found","");
                }
            }

            // /
            // if(bol){

            // LogUtils.printLogW("User is online");
            // }else{
            //
            // LogUtils.printLogW("User is Ofline");
            // }

            try {
                connection.sendPacket(msg);
            } catch (Exception e) {
                Log.d("pavan", "in xmpp send message exception "+e.getMessage());
                e.printStackTrace();
            }

            Log.w("Responce", "" + connection.getPacketReplyTimeout());
        }
    }

    @Override
    public boolean handleMessage(android.os.Message msg) {
        int method = msg.what;
        switch (method) {
            case METHOD_SETUP:

            case METHOD_SETUP_CONNECT:
                if (!isConnected) {
                    Intent setupIntent = (Intent) msg.obj;
                    username = setupIntent.getStringExtra(KEY_USERNAME);
                    Log.e("userName", "method_setup" + username);

                    password = setupIntent.getStringExtra(KEY_PASSWORD);
                    domain = setupIntent.getStringExtra(KEY_DOMAIN);
                    ip = setupIntent.getStringExtra(KEY_IP);
                    if (method == METHOD_SETUP_CONNECT) {
                        _connect();
                    }
                }
                break;
            case METHOD_SEND_CHAT:
                Intent sendIntent = (Intent) msg.obj;
                String to = sendIntent.getStringExtra(KEY_SEND_TO);
                Log.d("sendTo", "mehod_send" + to);
                Log.e("key send to", sendIntent.getStringExtra(KEY_SEND_TO));
                Message.Type type = (Message.Type) sendIntent
                        .getSerializableExtra(KEY_SEND_TYPE);
                String message = sendIntent.getStringExtra(KEY_SEND_MESSAGE);
                _sendMessage(to, type, message);
                break;
            case METHOD_AUTO_START:
                // checkAndStart(this);
                break;
            case METHOD_CREATE_GROUP:
                Intent createGroupIntent = (Intent) msg.obj;
                String groupName = createGroupIntent.getStringExtra("GROUPNAME");
                String userID = createGroupIntent.getStringExtra("USERID");
                createNewGrop(groupName, userID);

                break;
            case METHOD_INVITE_GROUP:
                Intent inviteGroupInent = (Intent) msg.obj;
                String strgroupName = inviteGroupInent.getStringExtra("GROUPNAME");
                String userId = inviteGroupInent.getStringExtra("USERID");
               // inviteUser(strgroupName, userId);
                break;
            case METHOD_MESSAGE_GROUP:

                Intent groupMessageIntent = (Intent) msg.obj;
                String groupNameToSendMessage = groupMessageIntent.getStringExtra(KEY_ROOM_NAME);
                String messageToGroup = groupMessageIntent.getStringExtra(KEY_SEND_MESSAGE);
              //  sendMessageToRoomOccupants(groupNameToSendMessage, messageToGroup);
                break;

            case METHOD_JOIN_GROUP:
                Intent groupjoinIntent = (Intent) msg.obj;
                String groupNameToJoin = groupjoinIntent.getStringExtra(KEY_ROOM_NAME);
                String groupuserID = groupjoinIntent.getStringExtra("USERID");
               // joinGroup(groupNameToJoin, groupuserID);
                break;
            default:
                break;
        }
        return false;
    }

    @Override
    public void chatCreated(Chat chat, boolean createdLocally) {
        if (!createdLocally) {
            chat.addMessageListener(XmppService.this);

        }

    }

    public class LocalBinder extends Binder {
        public XmppService getServiceInstance(){
            return XmppService.this;
        }
    }


    @Override
    public void processMessage(Chat chat, Message message) {

        Log.d("Ashutosh","msg recived in xmpp server"+message.getBody()+": "+message.getFrom());

        try {
            JSONObject jobj = new JSONObject(message.getBody());

            Log.d("Ashutosh", "recieved message "+message);

            if (jobj.getString("id").equals("0"))
            {
                String message_ = jobj.getString("message");
                String type = jobj.getString("type");
                String data_type = jobj.getString("type");
                jobj.getString("driver_id");
                jobj.getString("client_id");
                jobj.getString("request_id");
                int num= (int) Math.random();
                AndyUtils.appLog("RandomNumber",num+"");
                ChatObject mesg = new ChatObject(message_,"recieve",data_type, ""
                        + jobj.getString("request_id"),
                        jobj.getString("driver_id"),
                        jobj.getString("client_id"));

                new DatabaseHandler(getApplicationContext()).insertChat(mesg);
                Intent sendIntent = new Intent("message_recieved");
                sendIntent.putExtra("message", message_);
                sendIntent.putExtra("random",num);
                sendIntent.putExtra("data_type", data_type);


//                int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);


                if(messageType.equals(Const.MESSAGE)) {
                    messageActivity.getMessage(message_, data_type);
                }
                else if(messageType.equals(Const.VIDEO_CHAT))
                {
                    videoActivity.mTextChatFragment.getMessage(message_,data_type);
                    LocalBroadcastManager.getInstance(this).sendBroadcast(
                            sendIntent);
                }

            } if (jobj.getString("id").equals("3")) {

                String message_ = jobj.getString("message");
                Intent sendIntent = new Intent("Location_recieved");
                sendIntent.putExtra("driver_lat", jobj.getString("driver_lat"));
                sendIntent.putExtra("driver_long", jobj.getString("driver_long"));



                LocalBroadcastManager.getInstance(this).sendBroadcast(
                        sendIntent);


                Log.d("xmpp", "latlong "+message_);



            }




//			else {
//
//				String message_ = jobj.getString("message");
//
//				generateNotification(getApplicationContext(), message_,
//						jobj.getString("driver_id"),
//						jobj.getString("client_id"),
//						jobj.getString("request_id"));
//				//Log.d("pavan", "got cancel message");
//				Intent cancel_Intent = new Intent("CANCEL_REQUEST");
//			if(new PreferenceHelper(this).getRequestId()!=-1)
//				LocalBroadcastManager.getInstance(this).sendBroadcast(
//						cancel_Intent);
//			}

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // getCurrent Time of message.....

    public void registerMessageClient(Activity activity,String type){
        this.messageActivity = (MessageActivity)activity;
        this.messageType=type;
    }

    public void registerVideoClient(Activity activity,String type){
        this.videoActivity = (VideoCallActivity)activity;
        this.messageType=type;
    }



    private String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        String formattedDate = dateFormat.format(new Date()).toString();
        System.out.println(formattedDate);
        return formattedDate;
    }

    private void checkScreenLock() {
        KeyguardManager myKM = (KeyguardManager) this.getSystemService(Context.KEYGUARD_SERVICE);
        if (myKM.inKeyguardRestrictedInputMode()) {
            //sendNotification();
        }
    }
    //send custom notification......................................


    private int getRandomNumber() {
        int randomInt = 0;
        Random random = new Random();
        randomInt = random.nextInt(_CHAR.length());
        if (randomInt - 1 == -1) {
            return randomInt;
        } else {
            return randomInt - 1;
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        isConnected = false;
        mPingManager = null;
        connection = null;
        scheduleRestart(this);
    }

    // multi user chat stuff

    static {
        try {
            Class.forName(ReconnectionManager.class.getName());
            Class.forName(ServiceDiscoveryManager.class.getName());
        } catch (ClassNotFoundException ex) {
            Log.d("Class not initialized!","");
        }
    }



    @Override
    public void connected(XMPPConnection xmppConnection) {
        connection = xmppConnection;
        isConnected = true;

        Roster roster = connection.getRoster();
        roster.addRosterListener(new RosterListener() {
            // Ignored events public void entriesAdded(Collection<String>
            // addresses) {}

            public void entriesDeleted(Collection<String> addresses) {
            }

            public void entriesUpdated(Collection<String> addresses) {
            }

            public void presenceChanged(Presence presence) {

                Log.d("Roster changed ","" + presence.getFrom() + " "+ presence.getTo() + " "+ presence.getStatus() + " "
                        + presence);

            }

            @Override
            public void entriesAdded(Collection<String> arg0) {
                // TODO Auto-generated method stub

            }
        });

    }

    @Override
    public void authenticated(XMPPConnection xmppConnection) {

    }

    @Override
    public void connectionClosed() {
        isConnected = false;
        try {
            connection.disconnect();
            try {
                connection.connect();
                Log.e("lost conncection", "reconnecting");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (SmackException.NotConnectedException e1) {
            e1.printStackTrace();
        }
        mPingManager = null;
        connection = null;
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        isConnected = false;
        try {
            connection.disconnect();
        } catch (SmackException.NotConnectedException e1) {
            e1.printStackTrace();
        }
        mPingManager = null;
        connection = null;
        if (isOnline(this)) {
            scheduleRestart(this);
        }
    }

    @Override
    public void reconnectingIn(int i) {

    }

    @Override
    public void reconnectionSuccessful() {
        isConnected = true;
    }

    @Override
    public void reconnectionFailed(Exception e) {
        isConnected = false;
        mPingManager = null;
        connection = null;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        // should check null because in air plan mode it will be null
        if (netInfo != null && netInfo.isConnected()) {
            return true;
        }
        return false;
    }


    @SuppressLint("NewApi")
    public static void scheduleRestart(Context context) {
        try {
            AlarmManager alarmManager = (AlarmManager) context
                    .getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(context, XmppService.class); // explicit
            intent.putExtra(KEY_METHOD, METHOD_AUTO_START);
            // intent
            PendingIntent intentExecuted = PendingIntent.getService(context, 0,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.set(AlarmManager.RTC_WAKEUP,
                    System.currentTimeMillis() + 10000, intentExecuted);
        } catch (Exception e) {
        }
    }

    public static Boolean getRoster(String user) {
        boolean avail = false;
        if (connection != null) {
            // Get the roster
            Roster roster = connection.getRoster();

            try {
                roster.createEntry(user + "@" + Const.DOMAIN, user, null);

                Presence presence = new Presence(Presence.Type.subscribe);
                presence.setTo(user + "@" + Const.DOMAIN);
                connection.sendPacket(presence);

            } catch (SmackException.NotLoggedInException e) {
                e.printStackTrace();
            } catch (SmackException.NoResponseException e) {
                e.printStackTrace();
            } catch (XMPPException.XMPPErrorException e) {
                e.printStackTrace();
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }

            roster = connection.getRoster();

            Presence availability = roster.getPresence(user
                    + Const.DOMAIN);
            avail = availability.isAvailable();

            return availability.isAvailable();
        } else {
            Log.e("connction", "connction is null");
        }
        Log.w("mode", "Null");

        return avail;
    }


    private class DownloadReceiver extends ResultReceiver {
        public DownloadReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            if (resultCode == 8344) {
                //  int progress = resultData.getInt("progress");
               /* mProgressDialog.setProgress(progress);
                if (progress == 100) {
                    mProgressDialog.dismiss();
                }*/
            }
        }
    }


    private void createNewGrop(String groupName, String userId) {

        try {
            if (connection == null) {
                ConnectionConfiguration config = new ConnectionConfiguration(
                        DOMAIN, 5222);
                config.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);

                SASLAuthentication.supportSASLMechanism("PLAIN", 0);
                config.setReconnectionAllowed(false);
                connection = new XMPPTCPConnection(config);

                connection.addConnectionListener(this);
                connection.connect();
            }
            ArrayList<String> owners = new ArrayList<>();
            owners.add("whatsappclone_sagar" + DOMAIN);
            //  SmackConfiguration.setDefaultPacketReplyTimeout(10000);
            MultiUserChat chatRoom = new MultiUserChat(connection, groupName + "@conference." + DOMAIN);
            chatRoom.create("NewRoom");
            chatRoom.join(userId);


            Form form = chatRoom.getConfigurationForm();

            Form submitForm = form.createAnswerForm();

            for (Iterator fields = form.getFields().iterator(); fields.hasNext(); ) {
                FormField field = (FormField) fields.next();
                if (!FormField.TYPE_HIDDEN.equals(field.getType()) && field.getVariable() != null) {
                    // Sets the default value as the answer
                    submitForm.setDefaultAnswer(field.getVariable());
                }
            }

            submitForm.setAnswer("muc#roomconfig_persistentroom", true);

            chatRoom.sendConfigurationForm(submitForm);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //callbacks interface for communication with service clients!
    public interface Callbacks{
         void getMessage(String data,String dataType);
    }
}