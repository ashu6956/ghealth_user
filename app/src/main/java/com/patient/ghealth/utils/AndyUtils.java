package com.patient.ghealth.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.patient.ghealth.R;
import com.patient.ghealth.adapter.AboutDoctorPagerAdapter;
import com.patient.ghealth.model.DoctorOnLineDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by getit on 8/5/2016.
 */
public class AndyUtils {
    public static Dialog mDialog;
    public static ProgressDialog mProgressDialog;

    public static void appLog(String msg1, String msg2) {
        Log.d(msg1, msg2);
    }

    public static void showLongToast(String msg, Context context) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void showShortToast(String msg, Context context) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


    public static String message_json(String message_id, String type, String driver_id,
                                      String client_id, String request_id, String message) {

        JSONObject jObject = new JSONObject();
        try {

            jObject.put("id", message_id);
            jObject.put("type", type);
            jObject.put("driver_id", driver_id);
            jObject.put("client_id", client_id);
            jObject.put("request_id", request_id);
            jObject.put("message", message);

            return jObject.toString();

        } catch (JSONException e) {
// TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "";
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean eMailValidation(String emailstring) {
        if (null == emailstring || emailstring.length() == 0) {
            return false;
        }
        Pattern emailPattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher emailMatcher = emailPattern.matcher(emailstring);
        return emailMatcher.matches();
    }

    public static void generateKeyHAsh(Context con) {

        try {
            PackageInfo info = con.getPackageManager().getPackageInfo(
                    con.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {

            e.printStackTrace();
        }
    }

//    public List<String> parseNationality()
//    {
//        Locale[] locale = Locale.getAvailableLocales();
//        ArrayList<String> countries = new ArrayList<String>();
//        String country;
//        countries.add("--Nationality--");
//        for (Locale loc : locale)
//        {
//            country = loc.getDisplayCountry();
//            if (country.length() > 0 && !countries.contains(country)) {
//                countries.add(country);
//            }
//        }
//        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);
////        Spinner citizenship = (Spinner) findViewById(R.id.input_citizenship);
////        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countries);
////        citizenship.setAdapter(adapter);
//
//        return countries;
//    }

    public static void showSimpleProgressDialog(Context context, String msg, boolean isCancelable) {
//        mDialog = new Dialog(context,R.style.DialogImageTheme);
//        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        mDialog.setContentView(R.layout.dialog_progressbar);
//        mProgressDialog = (ProgressBar) mDialog.findViewById(R.id.circular_progressbar);
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

//        TextView please_wait_text= (TextView) mDialog.findViewById(R.id.tv_please_wait);
//        please_wait_text.setText(msg);
//        mDialog.setCancelable(isCancelable);
//        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    public static ProgressDialog getSimpleProgressDialog(Context context, String msg, boolean isCancelable) {
//        mDialog = new Dialog(context,R.style.DialogImageTheme);
//        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        mDialog.setContentView(R.layout.dialog_progressbar);
//        mProgressDialog = (ProgressBar) mDialog.findViewById(R.id.circular_progressbar);
        ProgressDialog mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(msg);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

//        TextView please_wait_text= (TextView) mDialog.findViewById(R.id.tv_please_wait);
//        please_wait_text.setText(msg);
//        mDialog.setCancelable(isCancelable);
//        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        return mProgressDialog;
    }

    public static void removeProgressDialog() {
        mProgressDialog.dismiss();
    }

    public static void hideKeyBoard(Context con) {

        InputMethodManager imm = (InputMethodManager) con.getSystemService(Activity.INPUT_METHOD_SERVICE);
        // if (!imm.isAcceptingText())
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public static void showDoctorAboutDialog(int position, List<DoctorOnLineDetails> onLineDetailsList, Context mContext) {
        AndyUtils.appLog("DialogCLickedPosition", position + "");
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_doctors_details_images_layout);
        final Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DoctorOnLineDetails doctorOnLineDetails = onLineDetailsList.get(position);
        List<String> imageList = new ArrayList<String>();
        if (!doctorOnLineDetails.getDoctor_clinic_image_first().equals("")) {
            imageList.add(doctorOnLineDetails.getDoctor_clinic_image_first());
        }

        if (!doctorOnLineDetails.getDoctor_clinic_image_second().equals("")) {
            imageList.add(doctorOnLineDetails.getDoctor_clinic_image_second());
        }
        if (!doctorOnLineDetails.getDoctor_clinic_image_third().equals("")) {
            imageList.add(doctorOnLineDetails.getDoctor_clinic_image_third());
        }
        ImageView rightNav = (ImageView) dialog.findViewById(R.id.iv_right_arrow);
        ImageView leftNav = (ImageView) dialog.findViewById(R.id.iv_left_arrow);
        ImageView closeDialog = (ImageView) dialog.findViewById(R.id.iv_doctor_close);
        CircleIndicator circleIndicator = (CircleIndicator) dialog.findViewById(R.id.ci_about_doctors);
        final ViewPager viewPager = (ViewPager) dialog.findViewById(R.id.vp_about_doctors);
        AboutDoctorPagerAdapter aboutDoctorPagerAdapter = new AboutDoctorPagerAdapter(mContext, imageList);
        viewPager.setAdapter(aboutDoctorPagerAdapter);
        circleIndicator.setViewPager(viewPager);


// Images left navigation
        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    viewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    viewPager.setCurrentItem(tab);
                }
            }
        });

        // Images right navigatin
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                tab++;
                viewPager.setCurrentItem(tab);
            }
        });

        closeDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    public ArrayList<String> parseNationality(Context context) {
        String response = "";
        ArrayList<String> list = new ArrayList<String>();
        list.add("Malaysia");
        try {
            response = ReadFile.readRawFileAsString(context,
                    R.raw.countrycodes);

            JSONArray array = new JSONArray(response);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                list.add(object.getString("name"));
            }
//            Collections.sort(list);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
