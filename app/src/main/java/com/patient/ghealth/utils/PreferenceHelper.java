package com.patient.ghealth.utils;

/**
 * Created by pavan on 12/2/2015.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.SystemClock;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;


public class PreferenceHelper {

    // private final String WAIT_TIME = "waitTime";
    // For GCM registration
    public static final String PROPERTY_REG_ID = "registration_id";
    public static final String PROPERTY_APP_VERSION = "appVersion";
    private static final String LAT_LONG = "latlong";
    private static final String FILE_NAME = "GHealth_Patient";
    private final String DEVICE_TOKEN = "device_token";
    private final String REQ_TIME = "req_time";
    private final String ID = "id";
    private final String TOKEN = "token";
    private final String REQUEST_ID = "request_id";
    private final String DOCTOR_ID = "doctor_id";
    private final String PHONE_NUMBER = "phone";
    private final String EMAIL = "email";
    private final String PROFILE_IMAGE = "profile_image";
    private final String GENDER = "gender";
    private final String USER_NAME = "user_name";
    private final String MSG_READ = "msg_read";
    private final String LAST_LOGGED_NUMBER = "last_logged_number";
    private SharedPreferences app_prefs;
    private Context context;


    private final String ACCEPT_TIME = "accept_time";
    private final String CURRENT_TIME = "current_time";

    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences(Const.PREF_NAME,
                Context.MODE_PRIVATE);
        this.context = context;
    }

    public static void setParam(Context context, String key, Object object) {

        String type = object.getClass().getSimpleName();
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);
        Editor editor = sp.edit();

        if ("String".equals(type)) {
            editor.putString(key, (String) object);
        } else if ("Integer".equals(type)) {
            editor.putInt(key, (Integer) object);
        } else if ("Boolean".equals(type)) {
            editor.putBoolean(key, (Boolean) object);
        } else if ("Float".equals(type)) {
            editor.putFloat(key, (Float) object);
        } else if ("Long".equals(type)) {
            editor.putLong(key, (Long) object);
        }

        editor.commit();
    }

    /**
     * @param context
     * @param key
     * @param defaultObject
     * @return
     */
    public static Object getParam(Context context, String key,
                                  Object defaultObject) {
        String type = defaultObject.getClass().getSimpleName();
        SharedPreferences sp = context.getSharedPreferences(FILE_NAME,
                Context.MODE_PRIVATE);

        if ("String".equals(type)) {
            return sp.getString(key, (String) defaultObject);
        } else if ("Integer".equals(type)) {
            return sp.getInt(key, (Integer) defaultObject);
        } else if ("Boolean".equals(type)) {
            return sp.getBoolean(key, (Boolean) defaultObject);
        } else if ("Float".equals(type)) {
            return sp.getFloat(key, (Float) defaultObject);
        } else if ("Long".equals(type)) {
            return sp.getLong(key, (Long) defaultObject);
        }
        return null;
    }


    public void putAccept_time(long accept_time) {
        Editor edit = app_prefs.edit();
        edit.putLong(ACCEPT_TIME, accept_time);
        edit.commit();
    }

    public long getAccept_time() {
        return app_prefs.getLong(ACCEPT_TIME, 0L);
    }




    public static LatLng getObject(Context mContext, String key) {
        Gson gson = new Gson();
        SharedPreferences sp = mContext.getSharedPreferences(LAT_LONG, Context.MODE_PRIVATE);
        String json = sp.getString(key, null);
        LatLng obj = gson.fromJson(json, LatLng.class);
        return obj;
    }

    public static void saveObject(LatLng myObject, Context mContext, String key) {
        Gson gson = new Gson();
        String json = gson.toJson(myObject);
        SharedPreferences sp = mContext.getSharedPreferences(LAT_LONG,
                Context.MODE_PRIVATE);
        Editor editor = sp.edit();
        editor.putString(key, json);
        editor.apply();
    }

    public void putRegisterationID(String RegID) {
        Editor edit = app_prefs.edit();
        edit.putString(PROPERTY_REG_ID, RegID);
        edit.apply();
    }

    public void putRegisterationParms(String id, String token, String phone, String email, String picture, String name, String gender) {
        Editor edit = app_prefs.edit();
        edit.putString(ID, id);
        edit.putString(TOKEN, token);
        edit.putString(PHONE_NUMBER, phone);
        edit.putString(EMAIL, email);
        edit.putString(PROFILE_IMAGE, picture);
        edit.putString(USER_NAME, name);
        edit.putString(GENDER, gender);
        edit.apply();
    }

    public void putReq_time(long req_time) {
        Editor edit = app_prefs.edit();
        edit.putLong(REQ_TIME, req_time);
        edit.commit();
    }

    public long getReq_time() {
        return app_prefs.getLong(REQ_TIME, SystemClock.uptimeMillis());

    }

    public void putRequestId(String reqId) {
        Editor edit = app_prefs.edit();
        edit.putString(REQUEST_ID, reqId);
        edit.commit();
    }

    public String getRequestId() {
        return app_prefs.getString(REQUEST_ID, "");
    }

    public String getID() {
        return app_prefs.getString(ID, "");
    }

    public String getToken() {
        return app_prefs.getString(TOKEN, "");
    }

    public String getPhoneNumber() {
        return app_prefs.getString(PHONE_NUMBER, "");
    }

    public String getRegistrationID() {
        return app_prefs.getString(PROPERTY_REG_ID, "");
    }

    public void putAppVersion(int version) {
        Editor edit = app_prefs.edit();
        edit.putInt(PROPERTY_APP_VERSION, version);
        edit.apply();
    }

    public int getAppVersion() {
        return app_prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
    }

    public void putDeviceToken(String deviceToken) {
        Editor edit = app_prefs.edit();
        edit.putString(DEVICE_TOKEN, deviceToken);
        edit.commit();
    }

    public String getDeviceToken() {
        return app_prefs.getString(DEVICE_TOKEN, null);
    }

    public void putMsgRead(String msg_read) {
        Editor edit = app_prefs.edit();
        edit.putString(MSG_READ, msg_read);
        edit.commit();
    }

    public String getMsgRead() {
        return app_prefs.getString(MSG_READ, "");
    }

    public void putLastLoggedNumber(String ph_num) {
        Editor edit = app_prefs.edit();
        edit.putString(LAST_LOGGED_NUMBER, ph_num);
        edit.commit();
    }

    public String getLastLoggedNumber() {

        return app_prefs.getString(LAST_LOGGED_NUMBER, "");
    }

 /*   public void putHeightpx(float height_px) {
        Editor edit = app_prefs.edit();
        edit.putFloat(HEIGHT_PX, height_px);
        edit.commit();
    }

    public float getHeightPx() {

        return app_prefs.getFloat(HEIGHT_PX, 0);
    }


    public void putHeightpoints(float height_points) {
        Editor edit = app_prefs.edit();
        edit.putFloat(HEIGHT_POINTS, height_points);
        edit.commit();
    }

    public float getHeightPoints() {

        return app_prefs.getFloat(HEIGHT_POINTS, 0);
    }


    public void putwidthpoints(float width_points) {
        Editor edit = app_prefs.edit();
        edit.putFloat(WIDTH_POINTS, width_points);
        edit.commit();
    }

    public float getwidthPoints() {

        return app_prefs.getFloat(WIDTH_POINTS, 0);
    }


    public void putwidthpx(float width_px) {
        Editor edit = app_prefs.edit();
        edit.putFloat(WIDTH_PX, width_px);
        edit.commit();
    }

    public float getwidthpx() {

        return app_prefs.getFloat(WIDTH_PX, 0);
    }


    public void putDensity(float density) {
        Editor edit = app_prefs.edit();
        edit.putFloat(SCREEN_DENSITY, density);
        edit.commit();
    }

    public float getDensity() {

        return app_prefs.getFloat(SCREEN_DENSITY, 0);
    }*/


}