package com.patient.ghealth.utils;

public class OpenTokConfig {

    // *** Fill the following variables using your own Project info from the OpenTok dashboard  ***
    // ***                      https://dashboard.tokbox.com/projects                           ***
    // Replace with a generated Session ID
    public static final String SESSION_ID = "1_MX40NTcwNjcwMn5-MTQ3NzEyMTQxOTM2Nn5QaytrWW5jeWFPTGpyY2pxNm9OZVBkZEt-fg";
    // Replace with a generated token (from the dashboard or using an OpenTok server SDK)
    public static final String TOKEN = "T1==cGFydG5lcl9pZD00NTcwNjcwMiZzaWc9MGQ4Mzc3M2JmNzgzN2Q1ZTBjZDBlNTc3Y2Y1ZWM0YWQzMjcxOWJjMzpzZXNzaW9uX2lkPTFfTVg0ME5UY3dOamN3TW41LU1UUTNOekV5TVRReE9UTTJObjVRYXl0cldXNWplV0ZQVEdweVkycHhObTlPWlZCa1pFdC1mZyZjcmVhdGVfdGltZT0xNDc3MTIxNDQ4Jm5vbmNlPTAuNDgxMzA2NjA3ODM0OTk0OCZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNDc5NzEzNDU0";
    // Replace with your OpenTok API key
    public static final String API_KEY = "45797342";

    // Subscribe to a stream published by this client. Set to false to subscribe
    // to other clients' streams only.
    public static final boolean SUBSCRIBE_TO_SELF = false;

    // For internal use only. Please do not modify or remove this code.
    public static final String LOG_CLIENT_VERSION = "android-vsol-1.0.0";
    public static final String LOG_COMPONENTID = "oneToOneTextChatSample";
    public static final String LOG_ACTION_INITIALIZE = "Init";
    public static final String LOG_ACTION_START_COMM = "Start";
    public static final String LOG_ACTION_END_COMM = "End";
    public static final String LOG_ACTION_OPEN_TEXTCHAT = "OpenTextChat";
    public static final String LOG_ACTION_CLOSE_TEXTCHAT = "CloseTextChat";

    public static final String LOG_VARIATION_ATTEMPT = "Attempt";
    public static final String LOG_VARIATION_ERROR = "Failure";
    public static final String LOG_VARIATION_SUCCESS = "Success";


}