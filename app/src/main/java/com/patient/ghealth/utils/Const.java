package com.patient.ghealth.utils;


import android.os.Environment;

import com.paypal.android.sdk.payments.PayPalConfiguration;

/**
 * Created by getit on 8/5/2016.
 */
public class Const {

    //for Paypal

    public static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
    public static final String CONFIG_CLIENT_ID = "ATOjfFibw30b2XbaFw8ADu3pcd3DBRPh4peS8s-3-CoKvecwuG2GdnYxYbxlX83CuppBPQ0j1QomFwZ3";

    public static final String AUTH_TOKEN = "25504013362-63ji2edogema40tvetpemfuqkgc1o0o8.apps.googleusercontent.com";

    public static final String RM = "RM";
    public static final String XMPPUSER = "client_";
    public static final String XMPPDRIVER = "provider_";
    public static final int FILTER_MESSAGE = 1, NO_FILTER_MESSAGE = 2;
    public static final String SERVER = "52.10.103.74";
    public static final String DOMAIN = "52.10.103.74";
    public static final String urlPath = "http://52.10.103.74/";
    public static final String XMPP_PASSWORD = "layosclearlimo"; // common_password
    public static final String XMPP_SECREAT_KEY = "NrGq96FI";
    public static final String XMPP_USER_CREATION = "http://" + SERVER
            + ":9090/plugins/userService/userservice?type=add&secret="
            + XMPP_SECREAT_KEY + "&";
    public static final String SUFFIX_CHAT = "@" + DOMAIN;

    //    public static final String XMPP_PASSWORD = "admin"; // common_password
//    public static final String XMPP_SECREAT_KEY = "znD6LdvB";
    public static final String HOME_FRAGMENT = "home_fragment";
    public static final String DATE_PICKER = "date_picker";
    public static final String ONLINE_CONSULT = "online_consult";
    public static final String BOOKING = "booking";
    public static final String ONDEMAND = "onDemand";
    public static final String DOCTOR_LIST = "doctor_list";
    public static final String DOCTOR_NEAR_BY = "doctor_near_by";
    public static final String DOCTOR_ON_DEMAND = "doctor_on_demand";
    public static final String DOCTOR_FRAGMENT_TYPE = "doctor_fragment_type";
    public static final int TIMEOUT = 35000;
    public static final int MAX_RETRY = 3;
    public static final float DEFAULT_BACKOFF_MULT = 1f;
    public static final String DEVICE_TYPE = "android";
    public static final String MANUAL = "manual";
    public static final String FACEBOOK = "facebook";
    public static final String TWITTER = "twitter";
    public static final String GOOGLE_PLUS = "google";
    public static final String SPECIALITIES_CLICKED_POSITION = "specialitiesClickedPosition";
    public static final String SPECIALITIES_DOCTOR_ID = "specialist_doctorId";
    public static final String SPECIALITIES_DOCTOR_TYPE = "specialist_doctor_type";
    public static final String ANDROID = "android";
    public static final int CHOOSE_PHOTO = 151;
    public static final int TAKE_PHOTO = 152;
    public static final String LOGIN_FIRST_TIME = "login_first_time";
    public static final String NOTIFICATION_FIRST_TIME = "notification_first_time";
    public static final String TWITTER_KEY = "K8mFcFuXveuX9LcnmtKLPtumQ";
    public static final String TWITTER_SECRET = "jpo4Ndm3PoIlsYkjoaajOrU5t4odvA4vfP2Qzs044ozQYwV6MV";
    public static final String DOCTOR_ID = "doctor_id";
    public static final String DOCTOR_ONLINE_DETAILS = "doctorOnLineDetails";
    public static final String DOCTOR_TYPE = "doctorType";
    public static final String CARD = "card";
    public static final String CASH = "cash";
    public static final String DOCTOR_REQUEST_STATUS = "doctorRequestStatus";
    public static final String DOCTOR_INTENT_MESSAGE = "doctorIntentMessage";
    public static final String DOCTOR_NEW_REQUEST_STATUS = "doctorNewRequestStatus";
    public static final String REQUEST_ID = "request_id";
    public static final String DOCTOR_CONSULT_CONNECTIVITY = "doctorConsultConnectivity";
    public static final String CHECK_STATUS_CONNECTIVITY = "checkStatusConnectivity";
    public static final String ARTICLE_CLICKED_POSITION = "articleClickedPosition";
    public static final String CalendarBookDoctorFragment = "CalendarBookDoctor";
    public static final String FollowMoreTopicsFragment = "FollowMoreTopics";
    public static final String HealthFeedsDescriptionFragment = "HealthFeedsDescription";
    public static final String HealthFeedsFragment = "HealthFeeds";
    public static final String HealthPackageFragment = "HealthPackage";
    public static final String HealthPackageAndStoreFragment = "HealthPackageAndStore";
    public static final String MyAllergiesFragment = "MyAllergies";
    public static final String ViewMessageHistoryFragment = "ViewMessageHistory";
    public static final String MyMedicationFragment = "MyMedication";
    public static final String NotificationMessageFragment = "NotificationMessage";
    public static final String NotificationAnswerFragment = "NotificationAnswer";
    public static final String NotificationAppointmentFragment = "NotificationAppointment";
    public static final String UserProfileFragment = "UserProfile";
    public static final String UserSettingsFragment = "UserSettings";
    public static final String DoctorBookingFragment = "DoctorBooking";
    public static final String DoctorNearByBookingFragment = "DoctorNearby";
    public static final String DoctorAvailabilityFragment = "DoctorsAvailability";
    public static final String DoctorOnlineConsultFragment = "DoctorOnlineConsult";
    public static final String DoctorNearByOnlineConsultFragment = "DoctorBearByOnlineConsult";
    public static final String HealthStoreFragment = "HealthStore";
    public static final String MyScheduleFragment = "MySchedule";
    public static final String MyMedicalRecordFragment = "MyMedicalRecord";
    public static final String MyMedicalFragmenmt = "MyMedical";
    public static final String MyFriendlyDoctorFragment = "MyFriendlyDoctor";
    public static final String AskAQuestionFragment = "AskAQuestion";
    public static final String CartDetailsFragment = "CartDetails";
    public static final String AddFamilyFragment = "AddFamily";
    public static final String BookMarkFragment = "BookMark";
    public static final String BookMarkFragmentDescription = "BookMarkDescription";
    public static final String MyPurchaseItemFragment = "MyPurchaseItem";
    public static final String InviteFriendFamilyFragment = "InviteFriendFamily";
    public static final String DoctorOnDemandFragment = "DoctorOnDemand";
    public static final String SearchFragment = "Search";
    public static final String ORDER_DETAILS_FRAGMENT = "ORDER_DETAIL_FRAGMENT";
    public static final String AmbulanceOnDemandFragment = "AmbulanceOnDemand";
    public static final String AmbulanceOnDemandAmbulanceFragment = "AmbulanceOnDemandAmbulance";
    public static final String ReportIssueFragment = "ReportIssue";
    public static final String ChangePassword = "ChangePassword";
    public static final String StoreCategoryFragment = "StoreCategory";
    public static final String ShippingAddressFragment = "ShippingAddress";
    public static final String ProductDetailFragment = "ProductDetail";
    public static final String StoreProductListFragment = "StoreProductList";
    public static final String WebViewFragment = "WebView";
    public static final String AddAddressFragment = "AddAddress";
    public static final String GetAddressFragment = "GetAddress";
    public static final String PaymentMethodFragment = "PaymentMethod";
    public static final String SummaryFragment = "Summary";
    public static final String FullScreenImageFragment = "FullScreenImage";
    public static final String ProductAllReviewsFragment = "ProductAllReviews";
    public static final String VideoConsultHistoryFragment = "VideoConsultHistory";
    public static final String ChatConsultHistoryFragment = "ChatConsultHistory";
    public static final String REQUEST = "request";
    public static final String LAST_REQUEST = "lastRequest";
    public static final String CURRENT_LATLNG = "currentLatLng";
    public static final String DOCTOR_NAME = "doctor_name";
    public static final String PaymentHistoryFragment = "paymentHistory";
    public static final String ConsultHistoryFragment = "consultHistory";
    public static final String AddCardFragment = "addCard";
    public static final String SpecialitiesName = "specialitiesName";
    public static final String CATEGORY = "Category";
    public static final String TOPICS_ARTICLE_ID = "topicsArticleId";
    public static final String SOURCE_ADDRESS = "source_address";
    public static final String STATUS = "status";
    public static final String SUCCESS = "success";
    public static final String QUANTITY = "quantity";
    public static final String RESPONSE = "response";
    /*to send image, .mp3,mp4 set up url*/
    public static String mediaURL = "http://52.10.103.74/uploads/media/";
    public static String upload_media = urlPath + "api/upload";
    public static String LOCAL_STORAGE_PATH = Environment.getExternalStorageDirectory().toString() + "/WhatsAppClone";
    public static String LOCAL_STORAGE_MEDIA_PATH = LOCAL_STORAGE_PATH + "/WhatsAppCloneMedia";
    public static String LOCAL_STORAGE_IMAGE_PATH = LOCAL_STORAGE_MEDIA_PATH + "/WhatsAppCloneImages";
    public static String LOCAL_STORAGE_AUDIO_PATH = LOCAL_STORAGE_MEDIA_PATH + "/WhatsAppCloneAudio";
    public static String LOCAL_STORAGE_VIDEO_PATH = LOCAL_STORAGE_MEDIA_PATH + "/WhatsAppCloneVideo";
    public static String PREF_NAME = "GHEALTH_PATIENT_PREFERENCE";
    public static int GET = 0;
    public static int POST = 1;
    public static int CHAT = 1;
    public static int VOICE = 2;
    public static int VIDEO = 3;
    public static int DELETE = 3;
    public static int PUT = 2;
    public static String PICTURE = "picture";
    public static String INVALID_TOKEN = "Invalid token.";
    public static String NEW_USER = "Not a valid social registration User";
    public static String NOTIFICATION = "notification";
    public static String NOTIFICATIONCOUNT = "notificationCount";
    public static String ANSWERCOUNT = "answerCount";
    public static String CLINIC_ID = "clinicId";
    public static String PRODUCT_CATEGORY = "productCategory";
    public static String INCOMING_BUNDLE = "incomingBundle";
    public static String WEB_VIEW_INTENT_KEY = "web_view_intent_key";
    public static String CHECKOUT_METHOD_KEY = "cart_content";
    public static String SAMSUNG = "samsung";
    public static String MESSAGE = "message";
    public static String VIDEO_CHAT = "video_chat";

    public class Params {
        public static final String ID = "id";
        public static final String URL = "url";
        public static final int GOOGLE_SIGN_IN = 251;
        public static final String GUEST_TOKEN = "guest_token";
        public static final String CART_ID = "cart_id";

        public static final String STATUS_CODE = "statusCode";
        public static final String STATUS_MESSAGE = "statusMessage";
        public static final String SESSION_TOKEN = "session_token";
        public static final String _ID = "_id";
        public static final String NAME = "name";
        public static final String IMAGE = "IMAGE";
        public static final String TEXT = "TEXT";

        public static final String CATEGORY = "category";
        public static final String DEVICE_TYPE = "device_type";
        public static final String DEVICE_TOKEN = "device_token";
        public static final String PV_COD = "pv_code";
        public static final String LOGIN_BY = "login_by";
        public static final String REQUEST_ID = "request_id";
        public static final String PROVIDER_ID = "provider_id";
        public static final String ACCEPTED = "accepted";
        public static final String SPECIALITY_ID = "speciality_id";
        public static final String LATITUDE = "latitude";
        public static final String LONGITUDE = "longitude";
        public static final String S_LATITUDE = "s_latitude";
        public static final String S_LONGITUDE = "s_longitude";
        public static final String DOCTOR_ID = "doctor_id";
        public static final String SUBJECT = "subject";
        public static final String AUTHORIZATION = "Authorization";

        public static final String FIRST_NAME = "first_name";
        public static final String LAST_NAME = "last_name";
        public static final String EMAIL = "email";
        public static final String PASSWORD = "password";
        public static final String NEW_PASSWORD = "new_password";
        public static final String CONFIRM_PASSWORD = "confirm_password";
        public static final String OLD_PASSWORD = "old_password";
        public static final String ADDRESS = "address";
        public static final String ADDRESS_ID = "address_id";
        public static final String PICTURE = "picture";
        public static final String CONTENT = "content";
        public static final String PHONE = "phone";
        public static final String SOCIAL_UNIQUE_ID = "social_unique_id";
        public static final String BILLING_STREET = "b_street";
        public static final String BILLING_ADDRESS = "billing_address";
        public static final String SHIPPING_ADDRESS = "shipping_address";
        public static final String SHIPPING_LATITUDE = "shipping_latitude";
        public static final String SHIPPING_LONGITUDE = "shipping_longitude";
        public static final String BILLING_LANDMARK = "blandmark";
        public static final String BILLING_BLOCK_NO = "bblock_no";
        public static final String SHIPPING_LANDMARK = "slandmark";
        public static final String SHIPPING_BLOCK_NO = "sblock_no";
        public static final String DOB = "dob";
        public static final String GENDER = "gender";
        public static final String GENDER_TITLE = "title";
        public static final String PHONE_COUNTRY_CODE = "country_code";
        public static final String SAME = "same";
        public static final String REQUEST_TYPE = "request_type";
        public static final String PAYMENT_MODE = "payment_mode";
        public static final String RATING = "rating";
        public static final String COMMENTS = "comments";

        public static final String ORIGINS = "origins";
        public static final String DESTINATION = "destinations";
        public static final String MODE = "mode";
        public static final String SENSOR = "sensor";

        public static final String ARTICLE_ID = "article_id";
        public static final String NATIONALITY = "nationality";
        public static final String DATE = "date";
        public static final String AVAILABLE_SLOTS_IDS = "available_slot_ids";
        public static final String PAYMENT_METHOD_NONCE = "payment_method_nonce";
        public static final String LAST_FOUR = "last_four";
        public static final String ARTICLES_CATEGORY_ID = "article_category_id";
        public static final String IS_BOOKMARKED = "is_bookmarked";
        public static final String IS_DEFAULT = "is_default";
        public static final String IS_FAVORITE = "is_favorite";
        public static final String OTP = "otp";
        public static final String SSN = "ssn";
        public static final String CLINIC_ID = "clinic_id";
        public static final String CATEGORY_ID = "category_id";
        public static final String PRODUCT_ID = "product_id";
        public static final String REQ_QUANTITY = "req_quantity";
        public static final String ECOM_TOKEN = "ecom_token";
        public static final String MESSAGE_ID = "message_id";
        public static final String REPLY = "reply";
        public static final String START = "&start";
        public static final String LANGUAGE = "language";
        public static final String LANGUAGE_POSITION = "language_position";
        public static final String CURRENCY = "currency";
        public static final String TIMEZONE = "timezone";
        public static final String EMAIL_VERIFICATION = "email_verification";
        public static final String SKIP = "skip";
        public static final String TAKE = "take";
        public static final String ACCESS_TOKEN = "access_token";
        public static final String CODE = "code";


    }

    public class ServiceType {
        //        public static final String HOST_URL = "http://ghealth-1861890249.ap-southeast-1.elb.amazonaws.com/"; // live server
        public static final String HOST_URL = "https://ghealth.net/"; // live server
//        public static final String HOST_URL = "https://ghealthweb.azurewebsites.net/"; // azure secure server

        //        public static final String HOST_URL = "http://52.220.36.122/"; // our purpose server (Staging)
//        public static final String HOST_URL = "http://52.77.67.33/"; // our purpose server (temporary)
        //        public static final String HOST_URL = "http://139.59.46.81/"; // for pHealth  server (temporary)
        public static final String ECOM_HOST_URL = "http://52.220.193.142:3000/";
        public static final String ECOM_BASE_URL = ECOM_HOST_URL + "api/v1/users/";

        //        public static final String SOCKET_URL = "http://52.77.67.33:3000/";
        public static final String SOCKET_URL = "https://ghealth.net:3000";

        public static final String BASE_URL = HOST_URL + "userapi/";
        public static final String REGISTER_URL = BASE_URL + "register";
        public static final String LOGIN_URL = BASE_URL + "login";

        public static final String POST_FORGOT_PASSWORD_URL = HOST_URL + "forgetpassword";
        public static final String GET_SPECIALITIES_URL = BASE_URL + "getspeciality?";
        public static final String UPDATE_PROFILE_URL = BASE_URL + "updateprofile";
        public static final String GET_DOCTOR_LIST_ONLINE_URL = BASE_URL + "getdoctorlistonline?";
        public static final String GET_DOCTOR_LIST_BOOKING_URL = BASE_URL + "getdoctorlistbooking?";
        public static final String GET_DOCTOR_NEARBY_ONLINE_URL = BASE_URL + "getnearbyonline?";
        public static final String GET_DOCTOR_NEARBY_BOOKING_URL = BASE_URL + "getnearbybooking?";
        public static final String GET_DOCTOR_BOOKING_SLOTS_AVAILABILITY_URL = BASE_URL + "getavailableslots?";
        public static final String GET_DOCTOR_ON_DEMAND_URL = BASE_URL + "getdoctorondemand?";
        public static final String CREATE_REQUEST = BASE_URL + "createrequest";
        public static final String GOOGLE_MATRIX_URL = "http://maps.googleapis.com/maps/api/distancematrix/json?";
        public static final String DOCTOR_RATE_URL = BASE_URL + "rateDoctor";
        public static final String CHECK_STATUS_REQUEST_URL = BASE_URL + "requestStatusCheck";
        public static final String CHECK_STATUS_REQUEST_ID_URL = BASE_URL + "checkrequest";
        public static final String GET_ARTICLES_URL = BASE_URL + "getarticles?";
        public static final String GET_ARTICLE_DESCRIPTION_URL = BASE_URL + "getondearticle?";
        public static final String CREATE_DOCTOR_SLOTS_BOOKING_REQUEST_URL = BASE_URL + "bookslots";
        public static final String GET_MY_DATE_SCHEDULE_URL = BASE_URL + "getscheduledates?";
        public static final String GET_MY_SCHEDULE_URL = BASE_URL + "myschedule?";
        public static final String GET_PAYPAL_ACCESS_URL = BASE_URL + "getpaypalaccess";
        public static final String CREATE_ADD_CARD_URL = BASE_URL + "addcard";
        public static final String GET_ADDED_CARDS_URL = BASE_URL + "getcards?";
        public static final String CREATE_SELECT_CARD_URL = BASE_URL + "selectcard";
        public static final String CREATE_REPORT_ISSUE_URl = BASE_URL + "reportissue";
        public static final String CREATE_CHAT_HISTORY_URl = BASE_URL + "consulthistorychat?";
        public static final String CREATE_PHONE_HISTORY_URl = BASE_URL + "consulthistoryphone?";
        public static final String CREATE_VIDEO_HISTORY_URl = BASE_URL + "consulthistoryvideo?";
        public static final String GET_ARTICLES_CATEGORY_URL = BASE_URL + "getarticlecategory?";
        public static final String CREATE_ADD_BOOKMARK_URL = BASE_URL + "addbookmark";
        public static final String GET_BOOK_MARK_URL = BASE_URL + "viewbookmark?";
        public static final String GENERATE_OTP_URL = BASE_URL + "generateotp";
        public static final String CREATE_CANCEL_REQUEST_URL = BASE_URL + "cancelrequest";
        public static final String GET_DOCTOR_SPECIALITIES_ID_URL = BASE_URL + "getdoctorbyspeciality?";
        public static final String REQUEST_ASK_QUESTION_URL = BASE_URL + "askquestion";
        public static final String REQUEST_VIEW_ANSWER_URL = BASE_URL + "viewanswers?";
        public static final String GET_OPENTOK = BASE_URL + "generateopentok?";
        public static final String REQUEST_CHECK_TOKEN_URL = BASE_URL + "checktoken";
        public static final String CHANGE_PASSWORD_URL = BASE_URL + "changepassword";
        public static final String GET_CLINIC_DOCTOR_ONLINE_DETAILS_URL = BASE_URL + "getdoctoronlineclinic?";
        public static final String GET_CLINIC_DOCTOR_BOOKING_DETAILS_URL = BASE_URL + "getdoctorbookingclinic?";
        public static final String GET_CLINIC_DOCTOR_ONDEMAND_DETAILS_URL = BASE_URL + "getdoctorondemandclinic?";
        public static final String GET_ADDRESS_URL = BASE_URL + "getaddress?";
        public static final String ADD_ADDRESS_URL = BASE_URL + "addaddress";
        public static final String GET_QUESTION_URL = BASE_URL + "getquestion?";
        public static final String SELECT_ADDRESS_URL = BASE_URL + "selectaddress";
        public static final String UPLOAD_IMAGE_URL = HOST_URL + "uploadattachment";
        public static final String GET_CHAT_HISTORY_MESSAGE_URL = HOST_URL + "getchatmessage?";
        public static final String CLEAR_DEBT_AMOUNT_URL = BASE_URL + "cleardebt";
        public static final String GET_DOCTOR_MESSAGE_lIST_URL = BASE_URL + "getdoctormessages?";
        public static final String CREATE_REPLY_MESSAGE_URL = BASE_URL + "replydoctormessage";
        public static final String POST_DELETE_CARD_URL = BASE_URL + "deletecard";
        public static final String POST_SAVE_VAULT_ID_URL = BASE_URL + "savevaultid";
        public static final String POST_LOGOUT_URL = BASE_URL + "logout";

        public static final String XMPPUSER = "client_";
        public static final String XMPPDRIVER = "provider_";


        //Ecommerce Api

        public static final String GET_CATEGORY_URL = ECOM_BASE_URL + "category/get-categories";
        public static final String GET_STORE_PRODUCT_URL = ECOM_BASE_URL + "product/get-products?";
        public static final String GET_SINGLE_PRODUCT_URL = ECOM_BASE_URL + "product/get-single-product/";
        public static final String ADD_CART_PRODUCT_URL = ECOM_BASE_URL + "cart/add-product";
        public static final String GET_CART_COUNT_URL = ECOM_BASE_URL + "cart/count";
        public static final String GET_CART_DETAIL_URL = ECOM_BASE_URL + "cart/get-product";
        public static final String DELETE_REMOVE_CART_URL = ECOM_BASE_URL + "cart/remove-product?";
        public static final String UPDATE_CART_URL = ECOM_BASE_URL + "cart/update-product";
        public static final String GET_USER_ADDRESS_URL = ECOM_BASE_URL + "account/get-address";
        public static final String ADD_USER_ADDRESS_URL = ECOM_BASE_URL + "account/add-address";
        public static final String REMOVE_ADDRESS_URL = ECOM_BASE_URL + "account/remove-address/";
        public static final String UPDATE_USER_ADDRESS_URL = ECOM_BASE_URL + "account/update-address/";
        public static final String BUY_NOW_DETAIL_URL = ECOM_BASE_URL + "cart/type/buy";
        public static final String CART_NOW_DETAIL_URL = ECOM_BASE_URL + "cart/type/cart";
        public static final String COUPON_VALID_URL = ECOM_HOST_URL + "api/v1/coupons/is-valid/";
        public static final String BUY_NOW_COMPLETED_URL = ECOM_BASE_URL + "checkout/buy-now-completed";
        public static final String CART_NOW_COMPLETED_URL = ECOM_BASE_URL + "checkout/buy-cart-completed";
        public static final String GET_ORDER_HISTORY_URL = ECOM_BASE_URL + "order/get-order";
        public static final String POST_GOOGLE_ACCESS_TOKEN_URL = ECOM_BASE_URL + "get_google_access";
        public static final String GIVE_PRODUCT_REVIEW_URL = ECOM_BASE_URL + "product/save-review/";
        public static final String GET_SINGLE_PRODUCT_REVIEW_URL = ECOM_BASE_URL + "product/get-review/";

    }


    public class ServiceCode {
        public static final int REGISTER = 1;
        public static final int LOGIN = 2;
        public static final int GET_SPECIALITIES = 3;
        public static final int UPDATE_PROFILE = 4;
        public static final int GET_DOCTOR_LIST_ONLINE = 5;
        public static final int GET_DOCTOR_LIST_BOOKING = 6;
        public static final int GET_DOCTOR_NEARBY_ONLINE = 7;
        public static final int GET_DOCTOR_NEARBY_BOOKING = 8;
        public static final int GET_DOCTOR_BOOKING_SLOTS_AVAILABILITY = 9;
        public static final int GET_DOCTOR_ON_DEMAND = 10;
        public static final int CREATE_REQUEST = 11;
        public static final int GOOGLE_MATRIX = 12;
        public static final int DOCTOR_RATE = 13;
        public static final int CHECK_STATUS_REQUEST = 14;
        public static final int CHECK_STATUS_REQUEST_ID = 15;
        public static final int GET_ARTICLES = 16;
        public static final int CHAT_REQUEST = 17;
        public static final int VOICE_REQUEST = 18;
        public static final int VIDEO_REQUEST = 19;
        public static final int GET_ARTICLE_DESCRIPTION = 20;
        public static final int CREATE_DOCTOR_SLOTS_BOOKING_REQUEST = 21;
        public static final int GET_MY_DATE_SCHEDULE = 22;
        public static final int GET_MY_SCHEDULE = 23;
        public static final int GET_PAYPAL_ACCESS = 24;
        public static final int CREATE_ADD_CARD = 25;
        public static final int GET_ADDED_CARDS = 26;
        public static final int CREATE_SELECT_CARD = 27;
        public static final int CREATE_REPORT_ISSUE = 28;
        public static final int CREATE_CHAT_HISTORY = 29;
        public static final int CREATE_PHONE_HISTORY = 30;
        public static final int CREATE_VIDEO_HISTORY = 31;
        public static final int GET_ARTICLES_CATEGORY = 32;
        public static final int CREATE_ADD_BOOKMARK = 33;
        public static final int GET_BOOK_MARK = 33;
        public static final int GENERATE_OTP = 34;
        public static final int CREATE_CANCEL_REQUEST = 35;
        public static final int GET_DOCTOR_SPECIALITIES_ID = 36;
        public static final int REQUEST_ASK_QUESTION = 37;
        public static final int REQUEST_VIEW_ANSWER = 38;
        public static final int GET_OPENTOK = 39;
        public static final int REQUEST_CHECK_TOKEN = 40;
        public static final int CHANGE_PASSWORD = 41;
        public static final int GET_CLINIC_DOCTOR_ONLINE = 42;
        public static final int GET_CLINIC_DOCTOR_BOOKING = 43;
        public static final int GET_CLINIC_DOCTOR_ONDEMAND = 44;
        public static final int GET_PRODUCT_CATEGORIES = 45;
        public static final int GET_PRODUCT = 46;
        public static final int GET_SINGLE_PRODUCT_DETAILS = 47;
        public static final int GET_ADD_CART = 48;
        public static final int GET_CART = 49;
        public static final int CREATE_ADD_WISHLIST = 50;
        public static final int GET_WISHLIST = 50;
        public static final int REMOVE_WISHLIST = 51;
        public static final int REMOVE_CART = 52;
        public static final int GET_ADDRESS = 53;
        public static final int ADD_ADDRESS = 54;
        public static final int SELECT_ADDRESS = 55;
        public static final int GET_QUESTION = 56;
        public static final int GET_CATEGORY = 57;
        public static final int GET_STORE_PRODUCT = 58;
        public static final int REGISTER_ON_XMPP = 58;
        public static final int GET_SINGLE_PRODUCT = 59;
        public static final int UPLOAD_IMAGE = 60;
        public static final int ADD_CART_PRODUCT = 61;
        public static final int GET_CART_COUNT = 62;
        public static final int GET_CART_DETAIL = 63;
        public static final int DELETE_REMOVE_CART = 64;
        public static final int UPDATE_CART = 65;
        public static final int GET_USER_ADDRESS = 66;
        public static final int ADD_USER_ADDRESS = 67;
        public static final int UPDATE_USER_ADDRESS = 68;
        public static final int BUY_NOW_DETAIL = 69;
        public static final int CART_NOW_DETAIL = 70;
        public static final int COUPON_VALID = 71;
        public static final int BUY_NOW_COMPLETED = 72;
        public static final int CART_NOW_COMPLETED = 73;
        public static final int GET_ORDER_HISTORY = 74;
        public static final int GIVE_PRODUCT_REVIEW = 75;
        public static final int GET_SINGLE_PRODUCT_REVIEW = 76;
        public static final int GET_CHAT_HISTORY_MESSAGE = 77;
        public static final int CLEAR_DEBT_AMOUNT = 78;
        public static final int SAVE_FAVORITE_DOCTOR = 78;
        public static final int GET_DOCTOR_MESSAGE_lIST_URL = 79;
        public static final int CREATE_REPLY_MESSAGE = 80;
        public static final int REMOVE_ADDRESS = 81;
        public static final int POST_GOOGLE_ACCESS_TOKEN = 82;
        public static final int POST_FORGOT_PASSWORD = 83;
        public static final int POST_DELETE_CARD = 84;
        public static final int POST_SAVE_VAULT_ID = 85;
        public static final int POST_LOGOUT = 86;
    }

}
