package com.patient.ghealth.realmDB;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by user on 8/30/2016.
 */
public class RealmDBController
{
    private static RealmDBController instance;
    private final Realm realm;

    public RealmDBController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmDBController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmDBController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmDBController with(Activity activity) {

        if (instance == null) {
            instance = new RealmDBController(activity.getApplication());
        }
        return instance;
    }

    public static RealmDBController with(Application application) {

        if (instance == null) {
            instance = new RealmDBController(application);
        }
        return instance;
    }

    public static RealmDBController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    //Refresh the realm istance
    public void refresh() {

        realm.refresh();
    }

//    clear all objects from PatientProfile.class
    public void clearAll() {

        realm.beginTransaction();
        realm.clear(PatientProfile.class);
        realm.commitTransaction();
    }

    //find all objects in the PatientProfile.class
    public RealmResults<PatientProfile> getPatientDetails() {

        return realm.where(PatientProfile.class).findAll();
    }

    //query a single item with the given id
    public PatientProfile getBook(String id) {

        return realm.where(PatientProfile.class).equalTo("id", id).findFirst();
    }

    //check if Book.class is empty
    public boolean hasBooks() {

        return !realm.allObjects(PatientProfile.class).isEmpty();
    }

    //query example
    public RealmResults<PatientProfile> queryedBooks() {

        return realm.where(PatientProfile.class)
                .contains("author", "Author 0")
                .or()
                .contains("title", "Realm")
                .findAll();

    }
}
