package com.patient.ghealth.realmDB;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by user on 8/30/2016.
 */
public class PatientProfile extends RealmObject {

    @PrimaryKey
    private int patientId;
    private String pictureUrl;
    private String genderTitle;
    private String gender;
    private String firstName;
    private String lastName;
    private String dob;
    private String emailId;
    private String countryCode;
    private String mobileNumber;

    private String shippingAddress;
    private String billingAddress;

    private String nationality;
    private boolean isMale;
    private boolean isFemale;
    private String billingLandMark;
    private String shippingLandMark;
    private String billingBlockNo;
    private String shippingBlockNo;
    private String currency;
    private String language;
    private String timeZone;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getBillingLandMark() {
        return billingLandMark;
    }

    public void setBillingLandMark(String billingLandMark) {
        this.billingLandMark = billingLandMark;
    }

    public String getShippingLandMark() {
        return shippingLandMark;
    }

    public void setShippingLandMark(String shippingLandMark) {
        this.shippingLandMark = shippingLandMark;
    }

    public String getBillingBlockNo() {
        return billingBlockNo;
    }

    public void setBillingBlockNo(String billingBlockNo) {
        this.billingBlockNo = billingBlockNo;
    }

    public String getShippingBlockNo() {
        return shippingBlockNo;
    }

    public void setShippingBlockNo(String shippingBlockNo) {
        this.shippingBlockNo = shippingBlockNo;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    public boolean isFemale() {
        return isFemale;
    }

    public void setFemale(boolean female) {
        isFemale = female;
    }

    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getGenderTitle() {
        return genderTitle;
    }

    public void setGenderTitle(String genderTitle) {
        this.genderTitle = genderTitle;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(String billingAddress) {
        this.billingAddress = billingAddress;
    }
}
